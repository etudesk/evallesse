<?php

Route::post('/login', 'Auth\LoginController@login');

Route::get('/classroom-list', 'ManagerController@classroomList');
Route::post('/register', 'Auth\RegisterController@register');
Route::post('/actived-account', 'Auth\RegisterController@activedAccount');
Route::post('/send-activated-account-sms', 'Auth\RegisterController@sendActivatedAccountSms');
Route::post('/send-activated-account-sms-by-tel', 'Auth\RegisterController@sendActivatedAccountSmsTel');

Route::post('/password/reset', 'Auth\LoginController@sendResetPasswordCode');
// Route::post('/send-reset-password-code-sms', 'Auth\LoginController@sendResetPasswordCodeSms');
Route::post('/verify-reset-password-code', 'Auth\RegisterController@verifyResetPasswordCode');

Route::middleware('auth:api')->group(function () {
  Route::post('/logout', 'Auth\LoginController@logout');
  Route::post('/password/reset-action', 'Auth\RegisterController@resetPassword');

  Route::get('/preferences/subjects-list', 'Auth\PreferenceController@subjectsList');
  Route::post('/preferences/add', 'Auth\PreferenceController@add');
});

Route::middleware(['auth:api', 'verifyPreferencesMobile'])->group(function () {
  Route::get('/preferences/user-subjects', 'ManagerController@userPreferSubjects');
  Route::get('/intermediate/content-list/{type}', 'ManagerController@preferSubjectContentListByType');

  Route::get('/progressions/subject_{subject_id}', 'ManagerController@userProgressbySubject')->name('api.user-progress.by-subject');
  Route::get('/profile', 'ManagerController@profile');

  Route::get('/lesson-list/subject_{subject_id}', 'LessonController@list')->name('api.lesson.list');
  Route::get('/lesson/lesson_{lesson_id}/c_{c_id}', 'LessonController@index')->name('api.specific-lesson');
  Route::post('/lesson/compute-progress/lesson_{lesson_id}/c_{c_id}', 'LessonController@computeProgress')->name('api.compute-lesson-progress');

  Route::get('/quiz-list/subject_{subject_id}', 'QuizController@list')->name('api.quiz.list');
  Route::get('/quiz/quiz_{quiz_id}/questions', 'QuizController@getQuestions')->name('api.quiz.questions');
  Route::post('/quiz/quiz_{quiz_id}/question_{question_id}/validate-response', 'QuizController@addUserQuizResponse')->name('api.quiz.validate-response');

  Route::get('/examen-list/subject_{subject_id}', 'ExamenController@list')->name('api.examen.list');
  Route::get('/examen/examen_{examen_id}/questions', 'ExamenController@getQuestions')->name('api.examen.questions');
  Route::post('/examen/examen_{examen_id}/add-responses', 'ExamenController@addResponses')->name('api.examen.add-responses');
  Route::get('/examen/examen_{examen_id}/result', 'ExamenController@result')->name('api.examen.result');
  Route::get('/examen/examen_{examen_id}/correction', 'ExamenController@correction')->name('api.examen.correction');

  Route::get('/files-list/subject_{subject_id}', 'FileController@list')->name('api.file.list');;
  Route::get('/download-file/file_{file_id}', 'FileController@download')->name('api.file.download');
});
