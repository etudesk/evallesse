<?php

Route::group(
    ['middleware' => 'guest'],
    function () {
        Route::get('/', 'ManagerController@index')->name('index');
        Route::get('/about', 'ManagerController@about')->name('about');

        Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', 'Auth\LoginController@login');
        Route::get('sign-up', 'Auth\RegisterController@showRegistrationForm')->name('sign-up');
        Route::post('/sign-up/save', 'Auth\RegisterController@register')->name('sign-up.save');
    }
);

Route::get('/activity-book', 'ManagerController@activityBook')->name('activity-book');
Route::post('/booking', 'ManagerController@activityBooking')->name('activity-booking');

Route::group(
    ['middleware' => 'auth'],
    function () {
        Route::get('/sign-up/preferences', 'ManagerController@showSignupPreferences')->name('sign-up.preferences');
        Route::post('/sign-up/preferences/save', 'ManagerController@signupPreferences')->name('sign-up.preferences.save');

        Route::get('/home/profile/preference', 'UserController@preference')->name('home.profile.preference');
        Route::post('/home/profile/modify-preference', 'UserController@modifyPreference')->name('home.profile.modify-preference');
    }
);

Route::group(
    ['middleware' => ['auth', 'verifyPreferences']],
    function () {
        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        Route::get('/home', 'UserController@home')->name('home');
        Route::get('/home/courses', 'UserController@courses')->name('home.courses');
        Route::get('/home/courses/{slug}/{lesson_id}/l/{l_id}', 'CourseController@show')->name('home.courses.show');
        Route::post('/home/courses/compute-progress', 'CourseController@computeProgress')->name('home.courses.computeProgress');

        Route::get('/home/quizs', 'UserController@quizs')->name('home.quizs');
        Route::get('/home/quizs/{slug}/{quiz_id}', 'QuizController@show')->name('home.quizs.show');

        Route::get('/home/examens', 'UserController@examens')->name('home.examens');
        Route::get('/home/examens/{slug}/{examen_id}', 'ExamenController@show')->name('home.examens.show');
        Route::get('/home/examens/{slug}/{examen_id}/result', 'ExamenController@result')->name('home.examens.result');
        Route::get('/home/examens/{slug}/{examen_id}/correction', 'ExamenController@correction')->name('home.examens.correction');

        Route::get('/home/files', 'UserController@files')->name('home.files');
        Route::get('/home/files/download/{es_id}', 'UserController@filesDownload')->name('home.files.download');


        Route::get('/home/profile', 'UserController@profile')->name('home.profile');
        Route::post('/home/profile/make', 'UserController@profileMake')->name('home.profile.make');

        Route::get('/home/profile/password', 'UserController@password')->name('home.profile.password');
        Route::post('/home/profile/modify-password', 'UserController@modifyPassword')->name('home.profile.modify-password');
    }
);