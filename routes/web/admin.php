<?php

//Login page
// Route::get('/', 'LoginController@index')->name('login');

// Auth
Route::group(
  ['middleware' => 'guest:admin'],
  function () {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    // Route::post('/login', 'Auth\LoginController@makeLogin')->name('login.make');
  }
);

//Reset password page

Route::middleware('auth.admin')->group(function () {

  // Route for single
  // --------------------------------------------------
  Route::post('logout', 'Auth\LoginController@logout')->name('logout');

  // Dashboard page
  // --------------------------------------------------
  Route::get('/', 'DashboardController@index')->name('dashboard');

  Route::get('/password_forgotten', 'ResetPasswordController@index')->name('password_forgotten');

  // Book page
  // --------------------------------------------------
  Route::get('/book', 'BookController@index')->name('book');
  Route::get('/book/add', 'BookController@add')->name('book.add');
  Route::get('/book/{book_id}/edit', 'BookController@edit')->name('book.edit');

  // Classroom page
  // --------------------------------------------------
  Route::get('/classroom', 'ClassroomController@index')->name('classroom');

  // Setting page
  // --------------------------------------------------
  Route::get('/setting', 'SettingController@index')->name('setting');

  // Lesson page
  // --------------------------------------------------
  Route::get('/lesson', 'LessonController@index')->name('lesson');

  // Quiz page
  // --------------------------------------------------
  Route::get('/quiz', 'QuizController@index')->name('quiz');
  Route::get('/quiz/test/{id}', 'QuizController@test')->name('quiz.test');


  // Exam page
  // --------------------------------------------------
  Route::get('/examen', 'ExamController@index')->name('examen');
  Route::get('/correction', 'CorrectedExamFileController@index')->name('correction');
  Route::livewire('/examen/{id}', 'examen-show')->name('examen.show');

  Route::livewire('/quiz/{id}/show', 'quiz-show')->name('quiz.show');
  Route::livewire('/lesson/{id}/show', 'lesson-show')->name('lesson.show');

  Route::get('/home', 'HomeController@index')->name('home');


  // Subject page
  // --------------------------------------------------
  Route::get('/subject/{classe_id}/add', 'SubjectController@index')->name('subject.add');
  Route::livewire('/subject/{id}', 'subject-show')->name('subject.show');

  // Student page
  // --------------------------------------------------
  Route::get('/student/{classe_id}/add', 'StudentController@index')->name('student.add');
  Route::livewire('/student/{id}/show', 'student-show')->name('student.show');
});

//admin a ajouter sur toutes les routes
// Route::resource('user', 'UserController');
// Route::resource('admin', 'AdminController');
// Route::resource('subject', 'SubjectController');
//Route::resource('lesson', 'LessonController');
// Route::resource('chapter', 'ChapterController');
//Route::resource('quiz', 'QuizController');
// Route::resource('quizquestion', 'QuizQuestionController');
// Route::resource('quizanswer', 'QuizAnswerController');
//Route::resource('exam', 'ExamController');
// Route::resource('examquestion', 'ExamQuestionController');
// Route::resource('examanswer', 'ExamAnswerController');
// Route::resource('notification', 'NotificationController');
// Route::resource('book', 'BookController');
// Route::resource('bookcategory', 'BookCategoryController');
// Route::resource('bookorder', 'BookOrderController');
// Route::resource('correctedexamfile', 'CorrectedExamFileController');
// Route::resource('media', 'MediaController');
// Route::resource('userquizresponse', 'UserQuizResponseController');
// Route::resource('progression', 'ProgressionController');
// Route::resource('userexamresponse', 'UserExamResponseController');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
