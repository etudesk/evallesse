<?php

list($front, $frontNew, $dashboard) = get_domain();

// e-Vallesse Back-office Routes
// --------------------------------------------------
Route::group(
    ['domain' => $dashboard, 'namespace' => 'Admin', 'as' => 'admin.'],
    function () {
        include __DIR__ . '/web/admin.php';
    }
);


// e-Vallesse Front-office Routes
// --------------------------------------------------
Route::group(
    ['domain' =>  $front, 'namespace' => 'FrontNew', 'as' => 'front-new.'],
    function () {
        include __DIR__ . '/web/new.php';
    }
);
