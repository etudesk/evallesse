<center>PLEASE CHECK IT NOW!</center>
<hr>
CLIENT REFERER: {{ \Request::header('referer') ? \Request::header('referer') : '-'  }}<br>
CLIENT BROWER INFO: {{ \Request::userAgent() }}<br>
CLIENT GEO: [{{ $ip }}]<br>
@if (\Auth::check())
<hr>
USERNAME: {{ \Auth::user()->name }}<br>
EMAIL: {{ \Auth::user()->email }}<br>
COUNTRY: {{ \Auth::user()->country }}<br>
ACCOUNT CREATED AT: {{ \Auth::user()->created_at->format('d/m/Y H:i:s') }}<br>
@endif
<hr>
REQUEST DATA<br />
<p>
  {{ json_encode(\Request::all()) }}
</p>
<hr>
LOCATION: {{ $route }}<br>
ERROR MESSAGE: {{ $error_message }}<br>
ERROR FILENAME: {{ $file }}<br>
ERROR LINE: {{ $line }}<br>
DATE: {{ date('d/m/Y H:i:s') }}<br>
<hr>
DETAIL<br>
@foreach(preg_split('/#[0-9]+/', $trace) as $t)
{{ $t }}<br>
@endforeach