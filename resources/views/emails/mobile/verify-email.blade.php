@component('mail::message')
## Bonjour !

Veuillez entrer le code suivant sur notre application mobile afin d'activer votre compte: <b>{{ $code }}</b>.

<p>
  Si vous n'avez pas créé de compte, aucune autre action n'est requise.
</p>

Cordialement,<br>
Evallesse
@endcomponent