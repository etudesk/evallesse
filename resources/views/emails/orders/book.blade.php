@component('mail::message')
# Nouvelle Commande

<b>Nom du client:</b> {{ $data['name'] }} <br>
<b>Email du client:</b> {{ $data['email'] }} <br>
<b>Téléphone du client:</b> {{ $data['phone'] }} <br>
<b>Article:</b> {{ $data['book']->title }} <br>
<b>Prix:</b> {{ $data['book']->price }} FCFA<br>
<b>Quantité:</b> {{ $data['quantity'] }}<br>

<b>Observation :</b> <br>
{!! $data['observation'] ?? "Aucune observation" !!}

Cordialement,<br>
{{ config('app.name') }}
@endcomponent