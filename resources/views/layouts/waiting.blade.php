<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>@yield('title') - e-Vallesse</title>
  {{-- Fonts --}}
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;400;500;600;700&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="{{ mix('css/app.css') }}">
  <link rel="icon" type="image/png" href="{{ asset('images/logo-e-vallesse.png') }}" />


  @yield('css')
</head>

<body>
  @yield('content')

  <script src="{{ mix('js/app.js') }}"></script>
  @yield('js')
</body>

</html>
