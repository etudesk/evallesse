<!DOCTYPE html>
<html lang="fr" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> eVallesse - @yield('title') </title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href="{{asset('administrator/vendor/perfect-scrollbar.css')}}" rel="stylesheet">

    <!-- App CSS -->

    @livewireStyles
    <link type="text/css" href="{{asset('administrator/css/app.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/app.rtl.css')}}" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="{{asset('administrator/css/vendor-material-icons.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-material-icons.rtl.css')}}" rel="stylesheet">
    <link rel="icon" type="image/png" href="{{ asset('images/logo-e-vallesse.png') }}" />

    <!-- Font Awesome FREE Icons -->
    <link type="text/css" href="{{asset('administrator/css/vendor-fontawesome-free.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-fontawesome-free.rtl.css')}}" rel="stylesheet">

    <!-- ion Range Slider -->
    <link type="text/css" href="{{asset('administrator/css/vendor-ion-rangeslider.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-ion-rangeslider.rtl.css')}}" rel="stylesheet">


    <!-- Flatpickr -->
    <link type="text/css" href="{{asset('administrator/css/vendor-flatpickr.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-flatpickr.rtl.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-flatpickr-airbnb.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-flatpickr-airbnb.rtl.css')}}" rel="stylesheet">

    <!-- Quill Theme -->
    <link type="text/css" href="{{asset('administrator/css/vendor-quill.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-quill.rtl.css')}}" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;400;500;600;700&display=swap"
        rel="stylesheet">

    <style>
        body {
            font-family: "Poppins" !important;
            font-size: 15px;
            font-weight: 400;
            color: 333;
        }

        [x-cloak] {
            display: none !important;
        }

        [dir=ltr] .bg-primary {
            background-color: #f9f9f9 !important;
        }

        [dir=ltr] .breadcrumb-item {
            display: inline-block !important;
        }

        [dir=ltr] .breadcrumb {
            display: inline-block !important;
            background-color: inherit !important;
        }

        [dir=ltr] .sidebar-light .sidebar-menu-button {
            color: white !important;
        }

        [data-toggle="collapse"] .fa-plus-moins:before {
            content: "\f068";
            float: right;
            padding: 5px;
            border-radius: 50%;
            color: #666666;
            border: 3px solid #666666;

        }

        [data-toggle="collapse"].collapsed .fa-plus-moins:before {
            /* symbol for "collapsed" panels */
            content: "\f067";
        }

        .upload-btn-wrapper {
            position: relative;
            overflow: hidden;
            display: inline-block;
            cursor: pointer;

        }

        /*RICH TEXT*/
        [dir=ltr] .ql-toolbar.ql-snow+.ql-container.ql-snow {
            height: 200px !important;
        }

        .upload-btn-wrapper input[type=file] {
            font-size: 100px;
            position: absolute;
            left: 0;
            top: 0;
            opacity: 0;
        }

        .grey {
            color: #666666;
        }

        [dir=ltr] .card-perso:not(.clear-shadow) {
            box-shadow: 0 10px 25px 0 rgba(50, 50, 93, 0.07), 0 5px 4px 0 rgba(0, 0, 0, 0.07);
        }

        .card-raduis-perso {
            border-radius: 10px !important;
        }

        .collapse-header-bg {
            background-color: #f6f6f6 !important;
        }

        .active {
            background: linear-gradient(90deg, rgba(0, 170, 245, 1) 0%, rgba(162, 233, 237, 1) 100%) !important;
        }

        [dir=ltr] .sidebar-light .sidebar-menu-icon {
            color: white !important;
        }

        [dir=ltr] .sidebar-light .sidebar-account a {
            color: black !important;

        }

        [dir=ltr] .bg-blue {
            background-color: #1d3893 !important;
        }

        .space-info-admin {
            height: 275px !important;
        }

        .h-400 {
            height: 400px !important;
        }

        [dir=ltr] .sidebar-light .sidebar-account {
            background: white !important;
        }

        [dir=ltr] .sidebar-light {
            background: #1d3893 !important;
        }

        .round_bg,
        .btn-primary {
            background-color: #2946aa !important;
        }

        .icon_color {
            color: #fff !important;
        }

        .dropdown-menu.dropdown-menu-right.show::before,
        [dir=ltr] .dropdown-menu::before,
        [dir=ltr] .dropdown-menu::after {
            content: "";
            border: 0px solid transparent !important;
            opacity: 0;
        }

        [dir=ltr] .sidebar-light .sidebar-account a {
            color: #fff !important;
        }

        .mt-60 {
            margin-top: 60px;
        }

        .t-0 {
            top: 0px !important;
        }

        .pt-35-percent {
            padding-top: 35% !important;
        }

        .color_date_range {
            color: rgb(220, 220, 220) !important;
        }

        .datetimeflat {
            border: 2px solid whitesmoke;
            border-radius: 20px;
            padding: 12px 10px;
            width: 250px;
        }

        [dir=ltr] .flatpickr-months .flatpickr-month {
            height: 50px !important;
        }

        .badge-success-light {
            background-color: rgba(23, 177, 92, 0.3) !important;
        }

        .badge-primary-light {
            background-color: rgba(19, 119, 201, 0.3);
        }

        .badge-warning-light {
            background-color: rgba(240, 202, 77, 0.3);
        }

        .badge-danger-light {
            background-color: rgba(187, 52, 52, 0.3);
        }
    </style>
    @yield('style-perso')

    @bukStyles(true)
</head>

<body class="layout-default">
    <!-- Header Layout -->
    <div class="mdk-header-layout js-mdk-header-layout">
        <!-- Header Layout Content -->
        <div class="mdk-header-layout__content">

            <div class="mdk-drawer-layout js-mdk-drawer-layout">
                <div class="mdk-drawer-layout__content"
                    style="width: calc(100% - 250px);float: right;position: relative;padding: 0 10px;">

                    {{--menu aside mobile version--}}
                    <button class="navbar-toggler navbar-toggler-custom d-lg-none d-flex mr-navbar" type="button"
                        data-toggle="sidebar">
                        <span class="material-icons">short_text</span>
                    </button>


                    @yield('content')


                </div>
                {{--include aside--}}
                @include('layouts.partials.aside')
            </div>
        </div>
    </div>
    <div id="app-settings"></div>
    <!-- jQuery -->
    @livewireScripts
    <script src="{{asset('administrator/vendor/jquery.min.js')}}"></script>
    <!-- Bootstrap -->
    <script src="{{asset('administrator/vendor/popper.min.js')}}"></script>
    <script src="{{asset('administrator/vendor/bootstrap.min.js')}}"></script>
    <!-- Perfect Scrollbar -->
    <script src="{{asset('administrator/vendor/perfect-scrollbar.min.js')}}"></script>
    <!-- DOM Factory -->
    <script src="{{asset('administrator/vendor/dom-factory.js')}}"></script>
    <!-- MDK -->
    <script src="{{asset('administrator/vendor/material-design-kit.js')}}"></script>


    <!-- Range Slider -->
    <script src="{{asset('administrator/vendor/ion.rangeSlider.min.js')}}"></script>
    <script src="{{asset('administrator/js/ion-rangeslider.js')}}"></script>

    <!-- App -->
    <script src="{{asset('administrator/js/toggle-check-all.js')}}"></script>
    <script src="{{asset('administrator/js/check-selected-row.js')}}"></script>
    <script src="{{asset('administrator/js/dropdown.js')}}"></script>
    <script src="{{asset('administrator/js/sidebar-mini.js')}}"></script>
    <script src="{{asset('administrator/js/app.js')}}"></script>

    <!-- App Settings (safe to remove) -->


    @livewire('admin.modal-quiz')

    @livewire('modal-classe')
    @livewire('add-question-modal')

    <script src="{{asset('administrator/js/app-settings.js')}}"></script>


    <!-- Flatpickr -->
    <script src="{{asset('administrator/vendor/flatpickr/flatpickr.min.js')}}"></script>
    <script src="{{asset('administrator/js/flatpickr.js')}}"></script>

    <script src="{{asset('administrator/js/settings.js')}}"></script>
    <script src="{{asset('administrator/js/fr.js')}}"></script>
    <script src="{{asset('administrator/vendor/Chart.min.js')}}"></script>

    <!-- App Charts JS -->
    <script src="{{asset('administrator/js/charts.js')}}"></script>

    <!-- Chart Samples -->
    <script src="{{asset('administrator/js/page.admin-dashboard.js')}}"></script>

    <!-- Vector Maps -->
    <script src="{{asset('administrator/vendor/jqvmap/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('administrator/vendor/jqvmap/maps/jquery.vmap.world.js')}}"></script>
    <script src="{{asset('administrator/js/vector-maps.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.min.js"></script>

    <script>
        $(document).ready(function () {

            if (("#labelOp").length > 0) {
                var html = "";
                document.addEventListener("trix-change", function (event) {
                    var element = event.target;
                    html = element.value;
                });

                $(document).on('focusout', '.trix-content', function (e) {
                    // console.log(html);
                    window.livewire.emit('updateLabel', html);
                });
            }



            /*  $('.modal').appendTo('body'); */


            /* $("#update-modal-classe").css("z-index", "100000"); */

            window.livewire.on('quizStore', () => {

                $('.modal').modal('hide');

                $('#modal-delete').modal('hide')

                location.reload();
            });

            window.livewire.on('refresheditquiz', () => {




                window.livewire.emit('refresheditquizpublic');
            });






            window.livewire.on('fileChoosen', () => {

                let inputField = document.getElementById('image')
                let file = inputField.files[0]
                let reader = new FileReader();


                reader.onloadend = () => {
                    window.livewire.emit('fileUpload', reader.result)
                }
                reader.readAsDataURL(file);

            });

            window.livewire.on('fileChoosen2', (fileInput) => {
                let file = fileInput.files[0]
                let reader = new FileReader();


                reader.onloadend = () => {
                    window.livewire.emit('fileUpload', reader.result)
                }
                reader.readAsDataURL(file);
            });



            window.livewire.on('examFileChoosen', () => {

                let inputField = document.getElementById('examen_image')
                let file = inputField.files[0]
                let reader = new FileReader();


                reader.onloadend = () => {
                    window.livewire.emit('examFileUpload', reader.result)
                }
                reader.readAsDataURL(file);

            });

            window.livewire.on('examFileChoosen2', (fileInput) => {

                let file = fileInput.files[0]
                let reader = new FileReader();


                reader.onloadend = () => {
                    window.livewire.emit('examFileUpload', reader.result)
                }
                reader.readAsDataURL(file);

            });



            window.livewire.on('fileChoosenEditQuestion', () => {

                let inputField = document.getElementById('image_edit_question')
                let file = inputField.files[0]
                let reader = new FileReader();


                reader.onloadend = () => {
                    window.livewire.emit('fileUploadEditQuestion', reader.result)
                }
                reader.readAsDataURL(file);

            });


            window.livewire.on('fileChoosenEditExamQuestion', () => {

                let inputField = document.getElementById('image_edit_exam_question')
                let file = inputField.files[0]
                let reader = new FileReader();


                reader.onloadend = () => {
                    window.livewire.emit('fileUploadEditExamQuestion', reader.result)
                }
                reader.readAsDataURL(file);

            });




        });

    </script>





    @notifyJs
    @bukScripts(true)
    @yield('script-perso')
</body>

</html>