<footer class="py-4 border-top" style="border-color: #e0e0e0 !important">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <p class="text-center mb-0" style="font-size: 12px;">
          <span class="text-info">&copy; {{ date('Y') }} {{ config('app.name') }}. Tous droits reservés.</span>
        </p>
      </div>
    </div>
  </div>
</footer>