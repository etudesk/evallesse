<div class="topnavbar-primary">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light px-0" style="z-index: 20;">
      <a class="navbar-brand" href="{{ route('front-new.index') }}"><img src="{{ asset('images/logo-e-vallesse.png') }}"
          height="60" alt="Logo e-Vallesse"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
        aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-lg-2 mt-2 mt-lg-0 ml-auto">
          @if (Auth::guest())
          <li class="nav-item {{ is_active('front-new.login') }}">
            <a class="nav-link"
              href="{{ Auth::guest()?route('front-new.sign-up'):route('front-new.sign-up.preferences') }}">S'incrire</a>
          </li>
          @endif
          <li class="nav-item {{ is_active('front-new.activity-book') }}">
            <a class="nav-link" href="{{ route('front-new.activity-book') }}">Cahiers d'activités</a>
          </li>

          @if(!Auth::guest())
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle d-inline-flex align-items-center" href="#" id="navbarDropdown"
              role="button" data-toggle="dropdown" style="font-size: 14px;" aria-haspopup="true"
              aria-expanded="false"><span class="mr-1">{{ Auth::user()->name }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right py-0" aria-labelledby="navbarDropdown"
              style="font-size: 14px;">
              <a class="dropdown-item py-2" href="{{ route('front-new.home.profile') }}">Mon Profile</a>
              <div class="dropdown-divider my-0"></div>
              <a class="dropdown-item py-2" href="{{ route('front-new.logout') }}"
                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">Déconnexion</a>
              <form id="logout-form" action="{{ route('front-new.logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </div>
          </li>
          @endif
          {{-- <li class="nav-item {{ is_active('front-new.about') }}">
          <a class="nav-link" href="{{ route('front-new.about') }}">A propos</a>
          </li> --}}
        </ul>
        @if (Auth::guest())
        <form class="form-inline my-2 my-lg-0">
          <a href="{{ route('front-new.login') }}" class="btn btn-primary btn-sm btn-cta my-2 my-sm-0">Se connecter</a>
        </form>
        @endif
      </div>
    </nav>
  </div>
</div>