<div class="topnavbar-primary">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light px-0" style="z-index: 20;">
      <a class="navbar-brand" href="{{ route('front-new.index') }}"><img src="{{ asset('images/logo-e-vallesse.png') }}"
          height="60" alt="Logo e-Vallesse"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
        aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0 ml-lg-4">
          @if (Auth::guest())
          <li class="nav-item {{ is_active('front-new.login') }}">
            <a class="nav-link" href="{{ route('front-new.sign-up') }}">S'inscrire</a>
          </li>
          @endif
          <li class="nav-item {{ is_active('front-new.activity-book') }}">
            <a class="nav-link" href="{{ route('front-new.activity-book') }}">Cahiers d'activités</a>
          </li>
          {{-- <li class="nav-item {{ is_active('front-new.about') }}">
          <a class="nav-link" href="{{ route('front-new.about') }}">A propos</a>
          </li> --}}
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <a href="{{ route('front-new.login') }}" class="btn btn-light btn-cta my-2 my-sm-0">Se connecter</a>
        </form>
      </div>
    </nav>
  </div>
</div>