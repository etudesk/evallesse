<div class="topnavbar-primary">
  <div class="container">
    <nav class="navbar navbar-expand-lg navbar-light px-0" style="z-index: 20;">
      <a class="navbar-brand" href="{{ route('front-new.index') }}"><img src="{{ asset('images/logo-e-vallesse.png') }}"
          height="60" alt="Logo e-Vallesse"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
        aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mt-2 ml-auto mt-lg-0">
          <li class="nav-item {{ is_active('front-new.activity-book') }}">
            <a class="nav-link" href="{{ route('front-new.activity-book') }}">Cahiers d'activités</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle d-inline-flex align-items-center" href="#" id="navbarDropdown"
              role="button" data-toggle="dropdown" style="font-size: 14px;" aria-haspopup="true"
              aria-expanded="false"><span class="mr-1">{{ Auth::user()->name }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right py-0" aria-labelledby="navbarDropdown"
              style="font-size: 14px;">
              <a class="dropdown-item py-2" href="{{ route('front-new.home.profile') }}">Mon Profile</a>
              <div class="dropdown-divider my-0"></div>
              <a class="dropdown-item py-2" href="{{ route('front-new.logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Déconnexion</a>
              <form id="logout-form" action="{{ route('front-new.logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
              </form>
            </div>
          </li>
        </ul>
      </div>
    </nav>
  </div>
</div>
<div class="home-secondary-menu" style="border-top: 1px solid #e0e0e0; border-bottom: 1px solid #e0e0e0;">
  <div class="container">
    <nav class="nav d-none d-sm-flex">
      <a class="nav-link py-3 {{ is_active('front-new.home') }}" href="{{ route('front-new.home') }}">Accueil</a>
      <a class="nav-link py-3 {{ is_active('front-new.home.courses') }} {{ is_active('front-new.home.courses.show') }}"
        href="{{ route('front-new.home.courses') }}">Cours</a>
      <a class="nav-link py-3 {{ is_active('front-new.home.quizs') }} {{ is_active('front-new.home.quizs.show') }}"
        href="{{ route('front-new.home.quizs') }}">Quiz</a>
      <a class="nav-link py-3 {{ is_active('front-new.home.examens') }} {{ is_active('front-new.home.examens.show') }} {{ is_active('front-new.home.examens.result') }}"
        href="{{ route('front-new.home.examens') }}">Examens</a>
      <a class="nav-link py-3 ml-auto {{ is_active('front-new.home.files') }}"
        href="{{ route('front-new.home.files') }}">Sujets et corrigés</a>
    </nav>

    <nav class="nav d-flex d-sm-none">
      <a class="nav-link py-3 d-flex align-items-center {{ is_active('front-new.home') }}"
        href="{{ route('front-new.home') }}">
        <x-heroicon-o-home height="25" /></a>
      <a class="nav-link py-3 d-flex align-items-center {{ is_active('front-new.home.courses') }} {{ is_active('front-new.home.courses.show') }}"
        href="{{ route('front-new.home.courses') }}"><svg height="25" viewBox="0 0 16 16" class="bi bi-book-half"
          fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd"
            d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
        </svg></a>
      <a class="nav-link py-3 d-flex align-items-center {{ is_active('front-new.home.quizs') }} {{ is_active('front-new.home.quizs.show') }}"
        href="{{ route('front-new.home.quizs') }}">
        <x-heroicon-s-question-mark-circle height="25" /></a>
      <a class="nav-link py-3 d-flex align-items-center {{ is_active('front-new.home.examens') }} {{ is_active('front-new.home.examens.show') }} {{ is_active('front-new.home.examens.result') }}"
        href="{{ route('front-new.home.examens') }}">
        <x-fas-graduation-cap height="25" /></a>
      <a class="nav-link py-3 d-flex align-items-center ml-auto {{ is_active('front-new.home.files') }}"
        href="{{ route('front-new.home.files') }}">
        <x-fas-file-pdf height="25" /></a>
    </nav>
  </div>
</div>