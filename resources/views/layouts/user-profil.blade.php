<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title') - e-Vallesse</title>
    {{-- Fonts --}}
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;400;500;600;700&display=swap"
          rel="stylesheet">

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="icon" type="image/png" href="{{ asset('images/logo-e-vallesse.png') }}"/>
    <link rel="stylesheet" href="{{mix('css/materialdesignicons.css')}}">
    @yield('css')
</head>

<body>

<nav class="navbar m-0 navbar-light d-flex justify-content-between row w-100">
    <div class="navbar-brand m-0">
        <a class="" href="{{ route('front.waiting-page') }}">
            <img src="{{ asset('images/logo-e-vallesse.png') }}" height="60" alt="Logo e-Vallesse">
        </a>
    </div>
    <div class="d-flex flex-1 justify-content-end profil">
        <span class="mdi mdi-account-circle"></span>
        <span class="ml-1">Mohamed Diomandé</span>
    </div>
</nav>
<!---->
<div class="navbar navbar-profil navbar-light d-flex justify-content-between row w-100">
    <ul class="list-unstyled">
        <li class="nav-item"><a autofocus href="{{ url('home') }}">Accueil</a></li>
        <li class="nav-item"><a class="" href="{{url("cours")}}">Cours</a></li>
        <li class="nav-item"><a href="{{url("quiz")}}">Quiz</a></li>
        <li class="nav-item"><a href="{{url("examens")}}">Examens</a></li>
    </ul>
    <ul class="h-100">
        <li class="nav-item">
            <a href="{{url("sujet-pdf")}}" class="bold text-gray-900">Sujet PDF</a>
        </li>
    </ul>
</div>

@yield('content')

<script src="{{ mix('js/app.js') }}"></script>
@yield('js')
</body>

</html>
