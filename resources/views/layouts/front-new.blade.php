<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>@yield('title') - eVallesse</title>
  <link rel="stylesheet" href="{{ mix('css/new.css') }}">
  <link rel="icon" type="image/png" href="{{ asset('images/logo-e-vallesse.png') }}" />
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;400;500;600;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">

  @livewireStyles
  @yield('css')
</head>

<body>
  @yield('content')

  <script src="{{ mix('js/app.js') }}"></script>
  @livewireScripts
  @include('flashy::message')

  @yield('js')

  @if (App::environment() == 'production')
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-WPHXJFJJBW"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-WPHXJFJJBW');
  </script>
  @endif
</body>

</html>