@extends('layouts.front-new')

@section('title', 'Connexion')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 col-lg-7 login-cover d-none d-lg-block">
      </div>
      <div class="col-12 col-lg-5">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="login-right d-flex align-items-center justify-content-center">
                <div class="login-right-content text-center w-100 px-lg-5">

                @if(Session::has('error'))
                    <p class="alert alert-danger">{{ Session::get('error') }}</p>
                @endif

                  <a href="{{ route('front-new.index') }}"><img class="mb-4"
                      src="{{ asset('images/logo-e-vallesse.png') }}" height="70" alt="Logo evallesse"></a>
                  <h1 class="auth-text-header mb-4">Connecte toi !</h1>
                  <form  method="POST" action="{{ route('front-new.login') }}" class="text-left">
                    <div class="form-group">
                      <label for="email">Email ou numéro de téléphone</label>
                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus aria-describedby="emailHelp">
                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                      <label for="password">Mot de passe</label>
                      <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                       @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} id="customControlAutosizing">
                        <label class="custom-control-label" for="customControlAutosizing">Se souvenir</label>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-block btn-primary btn-cta mb-2">Se connecter</button>
                    <div class="text-center">
                      <a class="text-info btn-link" href="{{ route('front-new.sign-up') }}" style="font-weight: 400;">Je
                        m'inscris maintenant</a>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@endsection