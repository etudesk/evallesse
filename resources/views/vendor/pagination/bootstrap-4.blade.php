@if ($paginator->hasPages())
    <nav class="d-none d-lg-block">
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="page-link" aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li class="page-item">
                    @if(is_null(Request::get('search')) && is_null(Request::get('subject')))
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                    @else
                    <a class="page-link" href="{{ $paginator->previousPageUrl()."&search=".Request::get('search')."&subject=".Request::get('subject') }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                    @endif
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page"><span class="page-link">{{ $page }}</span></li>
                        @else
                            @if(is_null(Request::get('search')) && is_null(Request::get('subject')))
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                            @else
                            <li class="page-item"><a class="page-link" href="{{ $url."&search=".Request::get('search')."&subject=".Request::get('subject') }}">{{ $page }}</a></li>
                            @endif
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    @if(is_null(Request::get('search')) && is_null(Request::get('subject')))
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                    @else
                    <a class="page-link" href="{{ $paginator->nextPageUrl()."&search=".Request::get('search')."&subject=".Request::get('subject') }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                    @endif
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="page-link" aria-hidden="true">&rsaquo;</span>
                </li>
            @endif
        </ul>
    </nav>
@endif

@if ($paginator->hasPages())
    <nav class="d-block d-lg-none">
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">@lang('pagination.previous')</span>
                </li>
            @else
                <li class="page-item">
                    @if(is_null(Request::get('search')) && is_null(Request::get('subject')))
                    <a class="page-link" href="{{ $paginator->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a>
                    @else
                    <a class="page-link" href="{{ $paginator->previousPageUrl()."&search=".Request::get('search')."&subject=".Request::get('subject') }}" rel="prev">@lang('pagination.previous')</a>
                    @endif
                </li>
            @endif

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item">
                    @if(is_null(Request::get('search')) && is_null(Request::get('subject')))
                    <a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">@lang('pagination.next')</a>
                    @else
                    <a class="page-link" href="{{ $paginator->nextPageUrl()."&search=".Request::get('search')."&subject=".Request::get('subject') }}" rel="next">@lang('pagination.next')</a>
                    @endif
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">@lang('pagination.next')</span>
                </li>
            @endif
        </ul>
    </nav>
@endif
