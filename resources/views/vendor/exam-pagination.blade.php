@if ($paginator->hasPages())
{{-- Previous Page Link --}}
@if ($paginator->onFirstPage())
<button class="btn btn-sm btn-block btn-outline-info disabled" aria-disabled="true" disabled>
  Précédant</button>
@else
<button wire:click="previousPage" class="btn btn-sm btn-block btn-outline-info">
  Précédant</button>
@endif


{{-- Next Page Link --}}
@if ($paginator->hasMorePages())
<button wire:click="nextPage" class="btn btn-sm btn-block btn-info">Suivant</button>
@else
<button wire:click="nextPage" class="btn btn-sm btn-block btn-info disabled" aria-disabled="true"
  disabled>Suivant</button>
@endif
@endif