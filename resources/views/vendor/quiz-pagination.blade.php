@if ($paginator->hasPages())
{{-- Previous Page Link --}}
@if ($paginator->onFirstPage())
<button class="btn btn-sm btn-secondary mr-2 d-inline-flex align-items-center disabled" aria-disabled="true" disabled>
  <x-heroicon-o-arrow-circle-left height="20" /></button>
@else
<button wire:click="previousPage" class="btn btn-sm btn-secondary mr-2 d-inline-flex align-items-center">
  <x-heroicon-o-arrow-circle-left height="20" /></button>
@endif


{{-- Next Page Link --}}
@if ($paginator->hasMorePages())
<button wire:click="nextPage" class="btn btn-sm btn-secondary mr-2 d-inline-flex align-items-center">
  <x-heroicon-o-arrow-circle-right height="20" /></button>
@else
<button class="btn btn-sm btn-secondary mr-2 d-inline-flex align-items-center disabled" aria-disabled="true" disabled>
  <x-heroicon-o-arrow-circle-right height="20" /></button>
@endif
@endif