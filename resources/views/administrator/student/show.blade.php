@extends('layouts.app')
@section('style-perso')
   
@endsection

@section('title')
    Classe
@endsection

@section('content')
@livewire('student-show')
@endsection

@section('script-perso')
    <!-- List.js -->
    <script src="{{asset('administrator/vendor/list.min.js')}}"></script>
    <script src="{{asset('administrator/js/list.js')}}"></script>
@endsection