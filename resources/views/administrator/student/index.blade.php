@extends('layouts.app')
@section('title')
    Elève
@endsection

@section('content')
@livewire('student')
@endsection
@section('script-perso')
    <script type="text/javascript" src="{{asset('administrator/js/add_remove.js')}}"></script>
@endsection