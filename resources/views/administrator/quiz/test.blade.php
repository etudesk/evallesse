@extends('layouts.app')
@section('title')
Tableau de bord
@endsection



@section('header_text_and_add_button')
<div class="container-fluid page__heading-container">
    <div
        class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
        <h1 class="m-lg-0">Ajouter Question</h1>
    </div>
</div>
@endsection

@section('content')
<div class="card card-form">
    <div class="row no-gutters">
        <div class="col-lg-4 card-body">
            <p><strong class="headings-color">Ajouter question</strong></p>
        </div>
        <div class="col-lg-8 card-form__body card-body">
            <form action="{{ route('admin.quizquestion.store') }}" method="post">
                @csrf

                <div class="form-group">
                    <label for="matiere">Matieres :</label>
                    <select name="matiere" id="matiere" class="form-control">
                        <option value="">Math</option>
                        <option value="">Physique</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="label">Label :</label>
                    <input type="text" name="label" class="form-control" id="label">
                </div>

                <div class="form-group">
                    <label for="label">Type :</label>
                    <select class="form-control" name="type" id="">
                        <option value="unique">unique</option>
                        <option value="multiple">multiple</option>
                    </select>
                </div>


                <div class="form-group">
                    <label for="label">Quiz :</label>

                    <select class="form-control" name="quiz_id">
                        @foreach($quiz as $qui)
                        <option value='{{$qui->id}}' @if($qui->id == $id) selected @endif >{{$qui->label}}</option>
                        @endforeach
                    </select>

                </div>

                <div>
                    <div class="input-group mb-1">
                        <strong>Reponse question (cliquez sur + pour ajouter et - pour retirer)</strong>
                    </div>
                    <input class="form-control" type="hidden" id="nombre_de_champs" value="0" name="nombre_de_champs">
                    <div id="champs"></div>

                    <div class="input-group-append mb-3 right">
                        <button class="btn btn-inverse" type="button" onclick="multiple_fields();"><i
                                class="fa fa-plus"></i></button>
                        <button class="btn btn-inverse" type="button" onclick="remove_mulptiple_fields();"><i
                                class="fa fa-minus"></i></button>
                    </div>
                </div>
        </div>
    </div>

</div>
<div class="text-right mb-5">

    <input type="submit" class="btn btn-primary" value="Sauvegarder">
</div>
</form>
@endsection
@section('script-perso')
<script type="text/javascript" src="{{asset('administrator/js/add_remove.js')}}"></script>
@endsection