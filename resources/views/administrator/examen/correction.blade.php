@extends('layouts.app')
@section('title')
    Sujet examen corrigé
@endsection

@section('content')
@livewire('correction')
@endsection

@section('script-perso')   
<!-- WHWYH -->


<script type="text/javascript" src="{{asset('administrator/vendor/quill.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('administrator/js/quill.js')}}"></script>


<!--END WHWYH -->
@endsection