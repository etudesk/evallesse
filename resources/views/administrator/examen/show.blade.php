@extends('layouts.app')
@section('title')
    Examen
@endsection

@section('content')
@livewire('examen-show')
@endsection

@section('script-perso')
    <script type="text/javascript" src="{{asset('administrator/js/add_remove.js')}}"></script>
@endsection