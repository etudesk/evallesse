@extends('layouts.app')
@section('title')
    Tableau de bord
@endsection
@section('header_text_and_add_button')
    <div class="container-fluid page__heading-container">
        <div class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
            <h1 class="m-lg-0">Paramètre</h1>
        </div>
    </div>
@endsection

@section('content')
    <div class="card card-form">
        <div class="row no-gutters">
            <div class="col-lg-4 card-body">
                <p><strong class="headings-color">Information du compte</strong></p>
                <p class="text-muted mb-0">Changer les informations du compte</p>
            </div>
            <div class="col-lg-8 card-form__body card-body">
                <div class="form-group">
                    <label for="nom">Nom</label>
                    <input id="nom" type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label for="prenom">Prénom</label>
                    <input id="prenom" type="text" class="form-control">
                </div>
                <div>
                    <div class="input-group mb-1">
                        <strong>Reponse question (cliquez sur + pour ajouter et  - pour retirer)</strong>
                    </div>
                    <input class="form-control" type="hidden" id="nombre_de_champs" value="0" name="nombre_de_champs">
                    <div id="champs"></div>

                    <div class="input-group-append mb-3 right">
                        <button class="btn btn-inverse" type="button" onclick="multiple_fields();"><i class="fa fa-plus"></i></button>
                        <button class="btn btn-inverse" type="button" onclick="remove_mulptiple_fields();"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-form">
        <div class="row no-gutters">
            <div class="col-lg-4 card-body">
                <p><strong class="headings-color">Mise à jour du mot de passe</strong></p>
                <p class="text-muted mb-0">Changer de mot de passe.</p>
            </div>
            <div class="col-lg-8 card-form__body card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="old_pass">Ancien mot de passe</label>
                            <input id="old_pass" type="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="new_password">Nouveau mot de passe</label>
                            <input id="new_password" type="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Confirmer mot de passe</label>
                            <input id="confirm_password" type="password" class="form-control">
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <div class="text-right mb-5">
        <a href="" class="btn btn-primary">Sauvegarder</a>
    </div>
@endsection