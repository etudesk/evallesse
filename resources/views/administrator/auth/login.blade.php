<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href="{{asset('administrator/vendor/perfect-scrollbar.css')}}" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{asset('administrator/css/app.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/app.rtl.css')}}" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="{{asset('administrator/css/vendor-material-icons.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-material-icons.rtl.css')}}" rel="stylesheet">

    <!-- Font Awesome FREE Icons -->
    <link type="text/css" href="{{asset('administrator/css/vendor-fontawesome-free.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-fontawesome-free.rtl.css')}}" rel="stylesheet">

    <!-- ion Range Slider -->
    <link type="text/css" href="{{asset('administrator/css/vendor-ion-rangeslider.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-ion-rangeslider.rtl.css')}}" rel="stylesheet">

    <link rel="icon" type="image/png" href="{{asset('administrator/images/logos/logo.png')}}" />

    @livewireStyles
    @notifyCss

    @yield('css')
</head>

<body>
    @livewire('login')

    <div id="app-settings"></div>

    <!-- jQuery -->
    <script src="{{asset('administrator/vendor/jquery.min.js')}}"></script>

    <!-- Bootstrap -->
    <script src="{{asset('administrator/vendor/popper.min.js')}}"></script>
    <script src="{{asset('administrator/vendor/bootstrap.min.js')}}"></script>

    <!-- Perfect Scrollbar -->
    <script src="{{asset('administrator/vendor/perfect-scrollbar.min.js')}}"></script>

    <!-- DOM Factory -->
    <script src="{{asset('administrator/vendor/dom-factory.js')}}"></script>

    <!-- MDK -->
    <script src="{{asset('administrator/vendor/material-design-kit.js')}}"></script>

    <!-- Range Slider -->
    <script src="{{asset('administrator/vendor/ion.rangeSlider.min.js')}}"></script>
    <script src="{{asset('administrator/js/ion-rangeslider.js')}}"></script>

    <!-- App -->
    <script src="{{asset('administrator/js/toggle-check-all.js')}}"></script>
    <script src="{{asset('administrator/js/check-selected-row.js')}}"></script>
    <script src="{{asset('administrator/js/dropdown.js')}}"></script>
    <script src="{{asset('administrator/js/sidebar-mini.js')}}"></script>
    <script src="{{asset('administrator/js/app.js')}}"></script>

    <!-- App Settings (safe to remove) -->
    <script src="{{asset('administrator/js/app-settings.js')}}"></script>

    @livewireScripts
    @include('notify::messages')
    @notifyJs

    @yield('js')

</body>

</html>