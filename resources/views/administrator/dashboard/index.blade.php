@extends('layouts.app')
@section('title')
Tableau de bord
@endsection
@section('header_text_and_add_button')

@endsection

@section('content')
<div class="container page__heading-container">
    <div
        class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
        <h4 class="m-lg-0">Tableau de bord</h4>
    </div>
</div>

<div class="container page__container">

    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header card-header-large bg-light d-flex align-items-center">
                    <div class="flex">
                        <h4 class="card-header__title " style="font-family: Poppins;">Nombre d'elèves
                            {{-- <span class="ml-3">
                                <svg xmlns="http://www.w3.org/2000/svg" version="1.1"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 40 40" width="22"
                                    height="22">
                                    <g transform="matrix(1.6666666666666667,0,0,1.6666666666666667,0,0)">
                                        <path
                                            d="M0.5,13.985h3c0.276,0,0.5-0.224,0.5-0.5C3.95,11.87,3.532,10.289,2.778,8.86C2.76,8.825,2.75,8.786,2.75,8.746V5.205 c0-0.414-0.336-0.75-0.75-0.75s-0.75,0.336-0.75,0.75v3.541c0,0.04-0.009,0.079-0.027,0.114C0.468,10.288,0.05,11.87,0,13.485 C0,13.761,0.224,13.985,0.5,13.985z M21.187,20.271l-0.017-0.006l-4.97-1.647c-0.175-0.057-0.367-0.013-0.5,0.114l-3.357,3.231 c-0.193,0.185-0.498,0.185-0.691,0l-3.4-3.218c-0.133-0.126-0.325-0.169-0.5-0.112l-4.938,1.638 c-1.381,0.511-2.426,1.663-2.8,3.087c-0.07,0.267,0.09,0.54,0.357,0.611c0.041,0.011,0.084,0.016,0.126,0.016h23 c0.276,0,0.5-0.225,0.499-0.501c0-0.042-0.005-0.084-0.016-0.125C23.608,21.936,22.566,20.783,21.187,20.271z M23.586,1.993 L12.429,0.052c-0.284-0.049-0.574-0.049-0.858,0L0.414,1.993C0.142,2.041-0.04,2.3,0.008,2.572c0.036,0.207,0.199,0.37,0.406,0.406 l11.157,1.94c0.284,0.051,0.574,0.051,0.858,0l11.157-1.94c0.272-0.048,0.454-0.307,0.406-0.579 C23.956,2.192,23.793,2.029,23.586,1.993z M18.458,5.393l-6.115,1.063c-0.227,0.039-0.458,0.039-0.685,0L5.543,5.393 C5.407,5.369,5.277,5.46,5.254,5.596C5.251,5.61,5.25,5.625,5.25,5.639V10c0,0.061-0.022,0.12-0.063,0.166 c-0.336,0.408-0.506,0.927-0.476,1.455C4.7,12.634,5.252,13.57,6.144,14.05l0.151,0.192c0.51,3.047,4.02,4.99,5.686,4.99 s5.175-1.943,5.685-4.989l0.151-0.192c0.891-0.482,1.443-1.417,1.433-2.43c0.026-0.507-0.131-1.006-0.442-1.407 c-0.037-0.045-0.058-0.101-0.058-0.159V5.639c0-0.138-0.111-0.249-0.248-0.25C18.487,5.389,18.472,5.39,18.458,5.393z M17.192,12.693c-0.53,0.242-0.904,0.732-1,1.306c-0.377,2.249-3.174,3.739-4.207,3.739S8.15,16.246,7.773,14 c-0.096-0.574-0.471-1.063-1-1.306C6.41,12.45,6.198,12.037,6.21,11.6c-0.02-0.152,0.016-0.305,0.1-0.433 c0.266-0.121,0.437-0.386,0.438-0.678V8.548c0-0.079,0.037-0.153,0.1-0.2c0.061-0.047,0.141-0.063,0.216-0.044 C8.675,8.73,10.334,8.948,12,8.952c1.667-0.003,3.326-0.22,4.937-0.646c0.134-0.035,0.27,0.045,0.305,0.179 c0.005,0.021,0.008,0.042,0.008,0.063v1.821c-0.055,0.321,0.105,0.639,0.395,0.787c0.089,0.128,0.126,0.285,0.105,0.44 c0.014,0.436-0.197,0.85-0.558,1.095V12.693z M12.939,14.306c-0.076,0.041-0.155,0.074-0.237,0.1 c-0.187,0.055-0.38,0.082-0.575,0.081l0,0c-0.196,0.001-0.392-0.026-0.58-0.082c-0.082-0.026-0.162-0.059-0.238-0.1 c-0.365-0.196-0.82-0.06-1.016,0.305c-0.196,0.365-0.06,0.82,0.305,1.016l0,0c0.162,0.087,0.333,0.157,0.51,0.21 c0.33,0.099,0.673,0.15,1.017,0.149h0.007c0.343,0.002,0.685-0.048,1.014-0.147c0.177-0.054,0.348-0.125,0.511-0.212 c0.367-0.192,0.509-0.645,0.317-1.012s-0.645-0.509-1.012-0.317c-0.006,0.003-0.012,0.007-0.019,0.01L12.939,14.306z M11.189,12 c0.277-0.308,0.253-0.782-0.055-1.059c-0.769-0.62-1.867-0.62-2.636,0c-0.293,0.293-0.292,0.768,0.001,1.061 c0.271,0.27,0.701,0.294,0.999,0.054c0.195-0.111,0.433-0.111,0.628,0c0.307,0.278,0.781,0.255,1.059-0.052 C11.187,12.003,11.188,12.001,11.189,12z M13.064,10.945c-0.293,0.293-0.292,0.768,0.001,1.061c0.271,0.27,0.701,0.294,0.999,0.054 c0.195-0.111,0.433-0.111,0.628,0c0.293,0.293,0.768,0.293,1.061,0.001c0.293-0.293,0.293-0.768,0.001-1.061 c-0.019-0.019-0.04-0.038-0.061-0.055C14.924,10.329,13.832,10.329,13.064,10.945z"
                                            stroke="none" fill="currentColor" stroke-width="0" stroke-linecap="round"
                                            stroke-linejoin="round"></path>
                                    </g>
                                </svg>
                            </span> --}}
                        </h4>

                    </div>
                </div>
                <ul class="list-group list-group-flush mb-0" style="z-index: initial;">
                    <li class="list-group-item" style="z-index: initial;">
                        <div class="d-flex align-items-center">
                            <div class="flex">
                                <a href="javascript:void(0)" class="text-body">
                                    <h3 class="text-success">{{count($eleves)}}</h3>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header card-header-large bg-light d-flex align-items-center">
                    <div class="flex">
                        <h4 class="card-header__title" style="font-family: Poppins;">Nombre de Quiz</h4>
                    </div>
                </div>
                <ul class="list-group list-group-flush mb-0" style="z-index: initial;">
                    <li class="list-group-item" style="z-index: initial;">
                        <div class="d-flex align-items-center">
                            <div class="flex">
                                <a href="javascript:void(0)" class="text-body">
                                    <h3 class="text-success">{{ $quizNb }}</h3>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card">
                <div class="card-header card-header-large bg-light d-flex align-items-center">
                    <div class="flex">
                        <h4 class="card-header__title" style="font-family: Poppins;">Nombre d'examens</h4>
                    </div>
                </div>
                <ul class="list-group list-group-flush mb-0" style="z-index: initial;">
                    <li class="list-group-item" style="z-index: initial;">
                        <div class="d-flex align-items-center">
                            <div class="flex">
                                <a href="javascript:void(0)" class="text-body">
                                    <h3 class="text-success">{{ $examNb }}</h3>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg">
            <div class="row card-group-row">
                <div class="col-lg-12 card-group-row__col h-400">
                    <div class="card card-group-row__card card-body card-body-x-lg"
                        style="position: relative; padding-bottom: calc(80px - 1.25rem); overflow: hidden;">
                        <div class="card-header__title text-muted mb-2" style="font-family: Poppins;">Nombre d'élèves
                            inscrits par mois</div>
                        <div class="chart px-3 pb-3"
                            style="position: absolute; height: 320px; left: 0; right: 0; bottom: 0;">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand">
                                    <div class=""></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink">
                                    <div class=""></div>
                                </div>
                            </div>
                            <canvas id="quizChart" class="chartjs-render-monitor" width="400" height="400"></canvas>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-lg-6 card-group-row__col h-400">
                    <div class="card card-group-row__card card-body card-body-x-lg"
                        style="position: relative; padding-bottom: calc(80px - 1.25rem); overflow: hidden; z-index: 0;">
                        <div class="card-header__title text-muted mb-2">Graphe Examens</div>
                        <div class="chart" style="height: 300px; position: absolute; left: 0; right: 0; bottom: 0;">
                            <div class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand">
                                    <div class=""></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink">
                                    <div class=""></div>
                                </div>
                            </div>
                            <canvas id="examenhChart" width="286" height="80" class="chartjs-render-monitor"
                                style="display: block; width: 300px; height: 80px;"></canvas>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('script-perso')
<script>
    var GrapheQuiz = function GrapheQuiz(id) {
        var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'line';
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        var data = arguments.length > 3 ? arguments[3] : undefined;
        var datas = [<?php echo '"'.implode('","', $data).'"' ?>];



        options = Chart.helpers.merge({
            elements: {
                line: {
                    fill: 'start',
                    backgroundColor: settings.charts.colors.area,
                    tension: 0,
                    borderWidth: 1
                },
                point: {
                    pointStyle: 'circle',
                    radius: 5,
                    hoverRadius: 5,
                    backgroundColor: settings.colors.white,
                    borderColor: settings.colors.primary[700],
                    borderWidth: 2
                }
            },
            scales: {
                yAxes: [{
                    display: true
                }],
                xAxes: [{
                    display: true
                }]
            }
        }, options);
        data = data || {
            labels: ["Jan", "Fev", "Mars", "Avr", "Mai", "Juin", "Juil", "Aout", "Sep", "Oct", "Nov", "Dec"],
            datasets: [{
                label: "Earnings",
                data:   datas
            }]
        };
        Charts.create(id, type, options, data);
    };

    // var GrapheExamen = function GrapheExamen(id) {
    //     var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'line';
    //     var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    //     var data = arguments.length > 3 ? arguments[3] : undefined;
    //     options = Chart.helpers.merge({
    //         elements: {
    //             line: {
    //                 fill: 'start',
    //                 backgroundColor: settings.charts.colors.area,
    //                 tension: 0,
    //                 borderWidth: 1
    //             },
    //             point: {
    //                 pointStyle: 'circle',
    //                 radius: 5,
    //                 hoverRadius: 5,
    //                 backgroundColor: settings.colors.white,
    //                 borderColor: settings.colors.primary[700],
    //                 borderWidth: 2
    //             }
    //         },
    //         scales: {
    //             yAxes: [{
    //                 display: true
    //             }],
    //             xAxes: [{
    //                 display: true
    //             }]
    //         }
    //     }, options);
    //     data = data || {
    //         labels: ["Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
    //         datasets: [{
    //             label: "Earnings",
    //             data: [400, 200, 450, 460, 220, 380, 800]
    //         }]
    //     };
    //     Charts.create(id, type, options, data);
    // };
    // GrapheExamen('#examenhChart')
    GrapheQuiz('#quizChart');

</script>
@endsection