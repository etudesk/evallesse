<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Login</title>

    <!-- Prevent the demo from appearing in search engines -->
    <meta name="robots" content="noindex">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href="{{asset('administrator/vendor/perfect-scrollbar.css')}}" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{asset('administrator/css/app.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/app.rtl.css')}}" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="{{asset('administrator/css/vendor-material-icons.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-material-icons.rtl.css')}}" rel="stylesheet">

    <!-- Font Awesome FREE Icons -->
    <link type="text/css" href="{{asset('administrator/css/vendor-fontawesome-free.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-fontawesome-free.rtl.css')}}" rel="stylesheet">

    <!-- ion Range Slider -->
    <link type="text/css" href="{{asset('administrator/css/vendor-ion-rangeslider.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('administrator/css/vendor-ion-rangeslider.rtl.css')}}" rel="stylesheet">

    <link rel="icon" type="image/png" href="{{asset('administrator/images/logos/logo.png')}}"/>


</head>

<body class="layout-login-centered-boxed">
<div class="layout-login-centered-boxed__form">
    <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-4 navbar-light">
        <a href="index.html" class="text-center text-light-gray mb-4">
            <!-- LOGO -->
            <img src="{{asset('administrator/images/logos/logo.png')}}" width="200"/>
        </a>
    </div>

    <div class="card card-body">

        {{---Afficher les messages d'erreurs debut--}}
        {{--Exemple mis ci-dessous--}}
        <div class="alert alert-soft-info d-flex" role="info">
            <i class="material-icons mr-3">info</i>
            <div class="text-body">Entrez votre email et par la suite un mail de confirmation vous serez envoyé</div>
        </div>
        {{---Afficher les messages d'erreurs fin--}}


        <form action="index.html" novalidate>
            <div class="form-group">
                <label class="text-label" for="email_2">Email:</label>
                <div class="input-group input-group-merge">
                    <input id="email_2" type="email" required="" class="form-control form-control-prepended" placeholder="Entrez votre email">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <span class="far fa-envelope"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group mb-1">
                <button class="btn btn-block btn-primary" type="submit" style="background: #026ab3; border-color: #fff !important;">Valider</button>
            </div>
            <div class="form-group text-center mb-0">
                <a href="{{route('login')}}">Retournez à la page de connexion</a>
            </div>
        </form>
    </div>
</div>


<!-- jQuery -->
<script src="{{asset('administrator/vendor/jquery.min.js')}}"></script>

<!-- Bootstrap -->
<script src="{{asset('administrator/vendor/popper.min.js')}}"></script>
<script src="{{asset('administrator/vendor/bootstrap.min.js')}}"></script>

<!-- Perfect Scrollbar -->
<script src="{{asset('administrator/vendor/perfect-scrollbar.min.js')}}"></script>

<!-- DOM Factory -->
<script src="{{asset('administrator/vendor/dom-factory.js')}}"></script>

<!-- MDK -->
<script src="{{asset('administrator/vendor/material-design-kit.js')}}"></script>

<!-- Range Slider -->
<script src="{{asset('administrator/vendor/ion.rangeSlider.min.js')}}"></script>
<script src="{{asset('administrator/js/ion-rangeslider.js')}}"></script>

<!-- App -->
<script src="{{asset('administrator/js/toggle-check-all.js')}}"></script>
<script src="{{asset('administrator/js/check-selected-row.js')}}"></script>
<script src="{{asset('administrator/js/dropdown.js')}}"></script>
<script src="{{asset('administrator/js/sidebar-mini.js')}}"></script>
<script src="{{asset('administrator/js/app.js')}}"></script>

<!-- App Settings (safe to remove) -->
<script src="{{asset('administrator/js/app-settings.js')}}"></script>

</body>

</html>