@extends('layouts.app')
@section('title')
    Matière
@endsection

@section('content')
@livewire('subject-show')
@endsection
@section('script-perso')
    <script type="text/javascript" src="{{asset('administrator/js/add_remove.js')}}"></script>
@endsection