@extends('layouts.app')
@section('title')
    Livre
@endsection
@section('content')
   @livewire("book")
@endsection
@section('script-perso')
    <script type="text/javascript" src="{{asset('administrator/js/add_remove.js')}}"></script>
@endsection


