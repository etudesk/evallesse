@extends('layouts.app')
@section('title')
Leçon
@endsection
@section('header_text_and_add_button')
<div class="container-fluid page__heading-container">
    <div
        class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
        <h1 class="m-lg-0">Leçon <span class="badge badge-success">4</span></h1>
        <a href="" class="btn btn-success ml-lg-3" data-toggle="modal" data-target="#modal-standard">Ajouter <i
                class="material-icons">add</i></a>
    </div>
</div>
@endsection
@section('content')
@livewire('lessons')
@endsection