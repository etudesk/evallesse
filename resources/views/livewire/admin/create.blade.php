<div wire:ignore.self id="modal-add-quiz" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Ajout</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Label :</label>
                    <input type="text" wire:model.debounce.500ms='label' class="form-control">
                    @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description :</label>
                    <textarea rows="5" name="description" wire:model.debounce.500ms='description' class="form-control">

                        </textarea>
                    @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="level">Level :</label>
                    <select class="form-control" wire:model.lazy='level'>
                        <option value="easy">Facile</option>
                        <option value="medium">Moyen</option>
                        <option value="hard">Difficile</option>
                    </select>
                    @error('level') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="label">Sujet :</label>
                    {{--foreach for display subject--}}
                    <select class="form-control" wire:model.lazy='subject_id'>
                        <option value="1" selected>Sujet 1</option>
                        <option value="2">Sujet 2</option>
                    </select>
                    @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary close-modal"
                    wire:click.prevent="store()">Sauvegarder</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-add-question" data-backdrop="false" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Ajout question</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="matiere">Matieres :</label>
                    <select name="" id="matiere" class="form-control">
                        <option value="">Math</option>
                        <option value="">Physique</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="label">Label :</label>
                    <input type="text" class="form-control" id="label">
                </div>

                <div class="form-group">
                    <label for="type">Type :</label>
                    <input type="text" class="form-control" id="type">
                </div>


                <div class="form-group">
                    <label for="label">Quiz :</label>
                    {{--foreach for display subject--}}
                    <select class="form-control">
                        <option>Quiz 1</option>
                        <option>Quiz 2</option>
                    </select>
                </div>
                <div>
                    <div class="input-group mb-1">
                        <strong>Reponse question (cliquez sur + pour ajouter et - pour retirer)</strong>
                    </div>
                    <input class="form-control" type="hidden" id="nombre_de_champs" value="0" name="nombre_de_champs">
                    <div id="champs"></div>

                    <div class="input-group-append mb-3 right">
                        <button class="btn btn-inverse" type="button" onclick="multiple_fields();"><i
                                class="fa fa-plus"></i></button>
                        <button class="btn btn-inverse" type="button" onclick="remove_mulptiple_fields();"><i
                                class="fa fa-minus"></i></button>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary" wire:click.prevent="store()">Sauvegarder</button>
            </div>
        </div>
    </div>
</div>