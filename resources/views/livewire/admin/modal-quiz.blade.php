<div>

    <div wire:ignore.self id="modal-add-quiz" class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="modal-standard-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-standard-title">Ajout</h5>
                    <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="label">Label :</label>
                        <input type="text" wire:model.debounce.500ms='label' class="form-control">
                        @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description :</label>
                        <textarea rows="5" name="description" wire:model.debounce.500ms='description'
                            class="form-control">

                        </textarea>
                        @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="level">Level :</label>
                        <select class="form-control" wire:model.lazy='level'>
                            <option value="easy" selected>Facile</option>
                            <option value="medium">Moyen</option>
                            <option value="hard">Difficile</option>
                        </select>
                        @error('level') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="label">Sujet :</label>
                        {{--foreach for display subject--}}
                        <select class="form-control" wire:model.lazy='subject_id'>
                            <option value="">Selectionner la matière</option>
                            @foreach($subjects as $subject)
                            <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}
                            </option>

                            @endforeach
                        </select>
                        @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal"
                        wire:click="cancel()">Annuler</button>
                    <button type="button" class="btn btn-primary close-modal"
                        wire:click.prevent="store">Sauvegarder</button>
                </div>
            </div>
        </div>
    </div>



    <div wire:ignore.self id="modal-add-exam" class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="modal-standard-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-standard-title">Ajout examen</h5>
                    <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="label">Label :</label>
                        <input type="text" wire:model.debounce.500ms='label' class="form-control">
                        @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Description :</label>
                        <textarea rows="5" name="description" wire:model.debounce.500ms='description'
                            class="form-control">

                        </textarea>
                        @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="level">Durée Examen :</label>
                        <input type="text" wire:model.debounce.500ms='estimate_duration' class="form-control">
                        @error('estimate_time') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                    <div class="form-group">
                        <label for="label">Sujet :</label>
                        {{--foreach for display subject--}}
                        <select class="form-control" wire:model.lazy='subject_id'>
                            <option value="">Selectionner la matière</option>
                            @foreach($subjects as $subject)
                            <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}
                            </option>

                            @endforeach
                        </select>
                        @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal"
                        wire:click="cancel()">Annuler</button>
                    <button type="button" class="btn btn-primary close-modal"
                        wire:click.prevent="storeExam()">Sauvegarder <div wire:loading wire:target="storeExam">
                            <div class="spinner-border spinner-border-sm m-1" role="status">
                                <span class="sr-only"> Loading...</span>
                            </div>
                        </div></button>
                </div>
            </div>
        </div>
    </div>



</div>