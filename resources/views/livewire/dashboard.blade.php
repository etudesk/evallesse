<div>
<div class="container-fluid page__heading-container">
        <div class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
            <h4 class="m-lg-0">Tableau de bord</h4>
        </div>
    </div>

    <div class="container-fluid page__container">

    <div class="row">
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header card-header-large bg-light d-flex align-items-center">
                    <div class="flex">
                        <h4 class="card-header__title">Quiz en progession</h4>
                    </div>
                </div>
                <ul class="list-group list-group-flush mb-0" style="z-index: initial;">
                    <li class="list-group-item" style="z-index: initial;">
                        <div class="d-flex align-items-center">
                            <div class="flex">
                                <a href="#" class="text-body"><h3 class="text-warning">150</h3></a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header card-header-large bg-light d-flex align-items-center">
                    <div class="flex">
                        <h4 class="card-header__title">Quiz terminé</h4>
                    </div>
                </div>
                <ul class="list-group list-group-flush mb-0" style="z-index: initial;">
                    <li class="list-group-item" style="z-index: initial;">
                        <div class="d-flex align-items-center">
                            <div class="flex">
                                <a href="#" class="text-body"><h3 class="text-success">200</h3></a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
      
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header card-header-large bg-light d-flex align-items-center">
                    <div class="flex">
                        <h4 class="card-header__title">Examens terminés</h4>
                    </div>
                </div>
                <ul class="list-group list-group-flush mb-0" style="z-index: initial;">
                    <li class="list-group-item" style="z-index: initial;">
                        <div class="d-flex align-items-center">
                            <div class="flex">
                                <a href="#" class="text-body"><h3 class="text-success">50</h3></a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

        <div class="row">
            <div class="col-lg">
                <div class="row card-group-row">
                    <div class="col-lg-6 card-group-row__col h-400">
                        <div class="card card-group-row__card card-body card-body-x-lg" style="position: relative; padding-bottom: calc(80px - 1.25rem); overflow: hidden; z-index: 0;">
                            <div class="card-header__title text-muted mb-2">Graphe Quiz</div>
                            <div class="chart" style="height: 300px; position: absolute; left: 0; right: 0; bottom: 0;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                <canvas id="quizChart" width="286" height="80" class="chartjs-render-monitor" style="display: block; width: 300px; height: 300px;"></canvas>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 card-group-row__col">
                        <div class="card card-group-row__card card-body card-body-x-lg" style="position: relative; padding-bottom: calc(80px - 1.25rem); overflow: hidden; z-index: 0;">
                            <div class="card-header__title text-muted mb-2">Graphe Examens</div>
                            <div class="chart" style="height: 300px; position: absolute; left: 0; right: 0; bottom: 0;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                <canvas id="examenhChart" width="286" height="80" class="chartjs-render-monitor" style="display: block; width: 300px; height: 80px;"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @section('script-perso')
<script>
    var GrapheQuiz = function GrapheQuiz(id) {
        var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'line';
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        var data = arguments.length > 3 ? arguments[3] : undefined;
        options = Chart.helpers.merge({
            elements: {
                line: {
                    fill: 'start',
                    backgroundColor: settings.charts.colors.area,
                    tension: 0,
                    borderWidth: 1
                },
                point: {
                    pointStyle: 'circle',
                    radius: 5,
                    hoverRadius: 5,
                    backgroundColor: settings.colors.white,
                    borderColor: settings.colors.primary[700],
                    borderWidth: 2
                }
            },
            scales: {
                yAxes: [{
                    display: true
                }],
                xAxes: [{
                    display: true
                }]
            }
        }, options);
        data = data || {
            labels: ["Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
            datasets: [{
                label: "Earnings",
                data: [400, 200, 450, 460, 220, 380, 800]
            }]
        };
        Charts.create(id, type, options, data);
    };

    var GrapheExamen = function GrapheExamen(id) {
        var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'line';
        var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
        var data = arguments.length > 3 ? arguments[3] : undefined;
        options = Chart.helpers.merge({
            elements: {
                line: {
                    fill: 'start',
                    backgroundColor: settings.charts.colors.area,
                    tension: 0,
                    borderWidth: 1
                },
                point: {
                    pointStyle: 'circle',
                    radius: 5,
                    hoverRadius: 5,
                    backgroundColor: settings.colors.white,
                    borderColor: settings.colors.primary[700],
                    borderWidth: 2
                }
            },
            scales: {
                yAxes: [{
                    display: true
                }],
                xAxes: [{
                    display: true
                }]
            }
        }, options);
        data = data || {
            labels: ["Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"],
            datasets: [{
                label: "Earnings",
                data: [400, 200, 450, 460, 220, 380, 800]
            }]
        };
        Charts.create(id, type, options, data);
    };
    GrapheExamen('#examenhChart')
    GrapheQuiz('#quizChart');

</script>
@endsection
</div>
