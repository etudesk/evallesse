<div>
    <div wire:ignore.self id="update-modal-classe" class="modal fade" tabindex="1000" role="dialog"
        aria-labelledby="modal-standard-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-standard-title">Edition</h5>
                    <button type="button" class="close" data-dismiss="modal" wire:click="cancel()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="">
                    <div class="modal-body">

                        <input type="hidden" wire:model.debounce.500ms="classe_id" class="form-control">
                        <div class="form-group">
                            <label for="label">Nom :</label>
                            <input type="text" wire:model.debounce.500ms="label" class="form-control">
                            @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Description :</label>
                            <textarea rows="5" class="form-control" wire:model.debounce.500ms="description">

                        </textarea>
                            @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal"
                            wire:click.prevent="cancel()">Annuler</button>
                        <button type="button" class="btn btn-warning" wire:click.prevent="update()">Sauvegarder <div
                                wire:loading wire:target="update">
                                <div class="spinner-border spinner-border-sm m-1" role="status">
                                    <span class="sr-only"> Loading...</span>
                                </div>
                            </div></button>
                    </div>
                    <form action="">
            </div>
        </div>
    </div>

    <div wire:ignore.self id="add-modal-classe" data-backdrop="false" style="background-color:rgba(0,0,0, 0.5)"
        class="modal fade" tabindex="1000" role="dialog" aria-labelledby="modal-standard-title" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-standard-title">Ajout</h5>
                    <button type="button" class="close" data-dismiss="modal" wire:click="cancel()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="">
                    <div class="modal-body">

                        <input type="hidden" wire:model.debounce.500ms="classe_id" class="form-control">
                        <div class="form-group">
                            <label for="label">Nom :</label>
                            <input type="text" wire:model.debounce.500ms="label" class="form-control">
                            @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Description :</label>
                            <textarea rows="5" class="form-control" wire:model.debounce.500ms="description">

                        </textarea>
                            @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal"
                            wire:click="cancel()">Annuler</button>
                        <button type="button" class="btn btn-primary" wire:click.prevent="store()">Sauvegarder <div
                                wire:loading wire:target="store">
                                <div class="spinner-border spinner-border-sm m-1" role="status">
                                    <span class="sr-only"> Loading...</span>
                                </div>
                            </div></button>
                    </div>
                    <form action="">
            </div>
        </div>
    </div>







    <div wire:ignore.self id="modal-delete-classe" class="modal fade" data-backdrop="false"
        style="background-color:rgba(0,0,0, 0.5)" tabindex="-1" role="dialog" aria-labelledby="modal-standard-title"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression {{$label}}</h5>
                    <button type="button" class="close" data-dismiss="modal" wire:click="cancel()" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Attention la suppression de la classe engendrera la supression des quiz, examens, leçons,
                        matières, en relation avec cette classe !! </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                    <button type="button" class="btn btn-danger"
                        wire:click.prevent="deleteClasse({{ $classeId }}) ">Valider<div wire:loading
                            wire:target="deleteClasse">
                            <div class="spinner-border spinner-border-sm m-1" role="status">
                                <span class="sr-only"> Loading...</span>
                            </div>
                        </div></button>
                </div>
            </div>
        </div>
    </div>





</div>