<div>
    <div wire:ignore.self id="modal-add-question" class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="modal-standard-title" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-standard-title">Ajout questions</h5>
                    <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" wire:ignore.self>


                    <div class="form-group row">

                        <div class="col-md-8">
                            <label for="type">Label :</label>
                            <div wire:ignore>
                                <input type="hidden">
                                <trix-editor class="trix-content" wire:model.debounce.999999ms="label"
                                    style="height:100px; overflow-y:scroll;">
                                </trix-editor>
                            </div>
                            {{-- <input type="text" class="form-control" wire:model.debounce.500ms='label'> --}}
                            <input type="hidden" class="form-control" wire:model.debounce.999999ms='quizId'>
                            <input type="hidden" class="form-control" wire:model.debounce.999999ms='refreshInput'>
                            @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>








                        <div class="col-md-4">

                            @if($questionImage)
                            <img src="{{$questionImage}}" class="img-fluid" style="max-width: 200px;" />
                            @endif
                            <div class="input-group upload-btn-wrapper mt-4 w-100">
                                <button class="btn btn-primary btn-block">Illustration</button>
                                <input type="file" id="image" wire:change="$emit('fileChoosen')"
                                    wire:model.debounce.500ms="quizQuestionImage">
                            </div>
                        </div>
                    </div>





                    <div class="form-group">
                        <label for="label">Type :</label>
                        <select class="form-control" name="type" wire:model.debounce.999999ms='type'>
                            <option value="">Choisir le type</option>
                            <option value="unique">unique</option>
                            <option value="multiple">multiple</option>
                        </select>
                        @error('type') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>

                    {{-- <input class="form-control" type="hidden" id="nombre_de_champs" value="0" name="nombre_de_champs"> --}}
                    <!-- <div class="form-group">
                        <label for="label">Quiz :</label>
                        {{--foreach for display subject--}}
                        <select class="form-control">
                            <option>Quiz 1</option>
                            <option>Quiz 2</option>
                        </select>
                    </div> -->
                    <div>
                        <div class="input-group mb-1">
                            {{-- <strong>Reponse question (cliquez sur + pour ajouter et - pour retirer)</strong> --}}
                            <span class="text-danger error">{{ $erreur }}</span>
                        </div>
                        <input class="form-control" type="hidden" id="nombre_de_champs" value="0"
                            name="nombre_de_champs">
                        @foreach($inputs as $key => $value)
                        <div id="champs">

                            <div class="row">
                                <div class="col-md-12 mt-2">
                                    <label class="text-dark">Option de réponse :</label>
                                </div>
                            </div>
                            <div class=" form-group row">
                                <div class="col-md-7">
                                    <div class="input-group">

                                        <input type="text" wire:model.debounce.500ms="reponse.{{ $value }}"
                                            id="option_reponse_id.{{$value}}" class="form-control">
                                        @error('reponse.'.$value) <span
                                            class="text-danger error">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                                <div class="col-md-1">

                                    <div class="custom-control custom-checkbox-toggle custom-control-inline ">
                                        <input type="checkbox" wire:model.debounce.500ms="correct.{{ $value }}"
                                            name='values[]' id="{{$value}}" autocomplete="off"
                                            value="correct_id.{{ $value }}" class="custom-control-input">
                                        <label class="custom-control-label" for="{{$value}}"></label>

                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <div class="input-group upload-btn-wrapper mb-3 w-100">
                                        <button class="btn btn-primary btn-block">Illustration</button>
                                        <input type="file" wire:model.debounce.500ms="image_option.{{ $value }}" />
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <label> Explication :</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group ">
                                        <input type="text" wire:model.debounce.999999ms="explication.{{ $value }}"
                                            id="explication_id.{{$value}}" class="form-control" required>
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                            </div>

                            <hr>

                        </div>
                        <div>

                            <button class="btn btn-inverse" type="button" wire:click.prevent="remove({{$key}})"><i
                                    class="fa fa-minus"></i></button>

                        </div>
                        @endforeach

                        {{-- <div class="input-group-append mb-3 right">
                            <button class="btn btn-inverse" type="button" wire:click.prevent="add({{$i}})"><i
                            class="fa fa-plus"></i></button>

                    </div> --}}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary" wire:click.prevent="store()">Sauvegarder <div wire:loading
                        wire:target="store">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>






<div wire:ignore.self id="modal-add-question-exam" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Ajout questions</h5>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" wire:ignore.self>


                <div class="form-group row">

                    <div class="col-md-6">
                        <label for="type">Label :</label>
                        <div wire:ignore>
                            {{-- <input type="hidden">
                            <trix-editor class="trix-content" wire:model="label"
                                style="height:100px; overflow-y:scroll;">
                            </trix-editor> --}}
                            <input name="label" id="labelExamQ" value="" type="hidden">
                            <trix-editor class="trix-content" wire:model.lazy="label" input="labelExamQ">
                            </trix-editor>
                        </div>
                        {{-- <input type="text" class="form-control" wire:model.debounce.500ms='label'> --}}
                        <input type="hidden" class="form-control" wire:model='examId'>
                        <input type="hidden" class="form-control" wire:model='refreshInput'>
                        @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>





                    <div class="col-md-2">
                        <label for="type">Coefficient :</label>
                        <input type="numeric" class="form-control" wire:model='scale'>

                        @error('scale') <span class="text-danger error">{{ $message }}</span>@enderror
                    </div>



                    <div class="col-md-4">

                        @if($examQuestionImage)
                        <img src="{{$examQuestionImage}}" class="img-fluid" style="max-width: 200px;" />
                        @endif
                        <div class="input-group upload-btn-wrapper mt-4 w-100">
                            <button class="btn btn-primary btn-block">Illustration</button>
                            <input type="file" id="examen_image" wire:model="quizExamQuestionImage"
                                wire:change="$emit('examFileChoosen')">
                        </div>
                    </div>
                </div>





                <div class="form-group">
                    <label for="label">Type :</label>
                    <select class="form-control" name="type" wire:model='type'>
                        <option value="">Choisir le type</option>
                        <option value="unique">Unique</option>
                        <option value="multiple">Multiple</option>
                    </select>
                    @error('type') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                {{-- <input class="form-control" type="hidden" id="nombre_de_champs" value="0" name="nombre_de_champs"> --}}
                <!-- <div class="form-group">
                        <label for="label">Quiz :</label>
                        {{--foreach for display subject--}}
                        <select class="form-control">
                            <option>Quiz 1</option>
                            <option>Quiz 2</option>
                        </select>
                    </div> -->
                <div>
                    <div class="input-group mb-1">
                        {{--  <strong>Reponse question (cliquez sur + pour ajouter et - pour retirer)</strong> --}}
                        <span class="text-danger error">{{ $erreur }}</span>
                    </div>
                    <input class="form-control" type="hidden" id="nombre_de_champs" value="0" name="nombre_de_champs">
                    @foreach($inputs as $key => $value)
                    <div id="champs">

                        <div class="row">
                            <div class="col-md-12 mt-2">
                                <label class="text-dark">Option de réponse :</label>
                            </div>
                        </div>
                        <div class=" form-group row">
                            <div class="col-md-7">
                                <div class="input-group">

                                    <input type="text" wire:model.debounce.500ms="reponse.{{ $value }}"
                                        id="option_reponse_id.{{$value}}" class="form-control">
                                    @error('reponse.'.$value) <span
                                        class="text-danger error">{{ $message }}</span>@enderror
                                </div>
                            </div>
                            <div class="col-md-1">

                                <div class="custom-control custom-checkbox-toggle custom-control-inline ">
                                    <input type="checkbox" wire:model.debounce.500ms="correct.{{ $value }}"
                                        name='values[]' id="{{$value}}" autocomplete="off"
                                        value="correct_id.{{ $value }}" class="custom-control-input">
                                    <label class="custom-control-label" for="{{$value}}"></label>

                                </div>

                            </div>
                            <div class="col-md-4">
                                <div class="input-group upload-btn-wrapper mb-3 w-100">
                                    <button class="btn btn-primary btn-block">Illustration</button>
                                    <input type="file" wire:model.debounce.500ms="image_option.{{ $value }}" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <label> Explication :</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="input-group ">
                                    <input type="text" wire:model.debounce.500ms="explication.{{ $value }}"
                                        id="explication_id.{{$value}}" class="form-control" required>
                                </div>
                            </div>

                        </div>

                        <div class="row">

                        </div>

                        <hr>

                    </div>
                    <div>

                        <button class="btn btn-inverse" type="button" wire:click.prevent="remove({{$key}})"><i
                                class="fa fa-minus"></i></button>

                    </div>
                    @endforeach

                    {{-- <div class="input-group-append mb-3 right">
                        <button class="btn btn-inverse" type="button" wire:click.prevent="add({{$i}})"><i
                        class="fa fa-plus"></i></button>

                </div> --}}
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
            <button type="button" class="btn btn-primary" wire:click.prevent="storeExamQuestion()">Sauvegarder
                <div wire:loading wire:target="storeExamQuestion">
                    <div class="spinner-border spinner-border-sm m-1" role="status">
                        <span class="sr-only"> Loading...</span>
                    </div>
                </div>
            </button>
        </div>
    </div>
</div>
</div>









<div wire:ignore.self id="modal-edit-option-reponse" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Edition</h6>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label class="text-dark">Option de réponse :</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="mb-3">
                            {{-- <input type="text" wire:model.debounce.500ms="label" class="form-control"
                                id="option_reponse"> --}}
                            <div wire:ignore>
                                <input value="{{ $this->label }}" type="hidden">
                                <trix-editor wire:model.debounce.999999ms="label" class="trix-content"
                                    style="height:100px; overflow-y:scroll;">
                                </trix-editor>
                            </div>
                            @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                            <input checked="" type="checkbox" id="correct" wire:model="correct"
                                class="custom-control-input">
                            <label class="custom-control-label" for="correct"></label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group upload-btn-wrapper mb-3 w-100">
                            <button class="btn btn-primary btn-block" type="button">Illustration</button>
                            <input type="file" id="image_edit_answer" wire:model="image_option" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label> Explication :</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group mb-3">
                            <input type="text" name="explication" wire:model.debounce.500ms="explication"
                                class="form-control">
                            @error('explication') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" class="btn btn-warning"
                    wire:click.prevent="updateOptionReponse({{$answer_id}})">Modifier <div wire:loading
                        wire:target="updateOptionReponse">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>


<div wire:ignore.self id="modal-exam-edit-option-reponse" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Edition</h6>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label class="text-dark">Option de réponse :</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <div wire:ignore>
                                <input value="{{ $this->label }}" type="hidden">
                                <trix-editor wire:model.debounce.999999ms="label" class="trix-content"
                                    style="height:100px; overflow-y:scroll;">
                                </trix-editor>
                            </div>
                            {{-- <input type="text" wire:model.debounce.500ms="label" class="form-control"> --}}
                            @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                            <input checked="" type="checkbox" id="correct" wire:model="correct"
                                class="custom-control-input">
                            <label class="custom-control-label" for="correct"></label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group upload-btn-wrapper mb-3 w-100">
                            <button class="btn btn-primary btn-block" type="button">Illustration</button>
                            <input type="file" id="image_edit_answer" wire:model="image_option"
                                wire:change="$emit('fileChoosenAnswer')" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label> Explication :</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group mb-3">
                            <input type="text" name="explication" wire:model.debounce.500ms="explication"
                                class="form-control">
                            @error('explication') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" class="btn btn-warning"
                    wire:click.prevent="updateExamOptionReponse({{$answer_id}})">Modifier <div wire:loading
                        wire:target="updateExamOptionReponse">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>


<div wire:ignore.self id="modal-add-option-reponse" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Ajouter Option de reponse</h6>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label class="text-dark">Option de réponse :</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-7">
                        <div class="mb-3">
                            <div wire:ignore>
                                <input type="hidden">
                                {{-- <trix-editor class="trix-content" wire:model="label"
                                    style="height:100px; width: 100%; overflow-y: scroll;">
                                </trix-editor> --}}
                                <input name="label" id="labelOp" value="" type="hidden">
                                <trix-editor class="trix-content" wire:model.lazy="label" input="labelOp">
                                </trix-editor>
                                {{-- <trix-editor wire:keydown="$set('label', $event.target.value)" class="trix-content">
                                </trix-editor> --}}
                            </div>
                            {{-- <input type="text" wire:model.debounce.500ms="label" class="form-control"> --}}
                            <input type="hidden" wire:model="quizQuestionId" class="form-control">
                            @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                            <input checked="" type="checkbox" id="correct" wire:model="correct"
                                class="custom-control-input">
                            <label class="custom-control-label" for="correct"></label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        @if($questionImage)
                        <img src="{{$questionImage}}" class="img-fluid" style="max-width: 200px;" />
                        @endif
                        <div class="input-group upload-btn-wrapper mt-3 mb-3 w-100">
                            <button class="btn btn-primary btn-block" type="button">Illustration</button>
                            <input type="file" wire:model="optionQuizFile"
                                wire:change="$emit('fileChoosen2', $event.target)" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label> Explication :</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group mb-3">
                            <input type="text" name="explication" wire:model="explication" class="form-control"
                                required>
                            @error('explication') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary" wire:click.prevent="addOptionReponse()">Ajouter <div
                        wire:loading wire:target="addOptionReponse">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>


<div wire:ignore.self id="modal-add-exam-option-reponse" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Ajout</h6>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label class="text-dark">Option de réponse :</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <div wire:ignore>
                                {{-- <input type="hidden">
                                <trix-editor class="trix-content" wire:model.debounce.999999ms="label"
                                    style="height:100px; width: 100%; overflow-y: scroll;">
                                </trix-editor> --}}
                                <input name="label" id="labelExamOp" value="" type="hidden">
                                <trix-editor class="trix-content" wire:model.lazy="label" input="labelExamOp">
                                </trix-editor>
                            </div>
                            {{-- <input type="text" wire:model.debounce.500ms="label" class="form-control"> --}}
                            <input type="hidden" wire:model="examQuestionId" class="form-control" value="tes">
                            @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="custom-control custom-checkbox-toggle custom-control-inline mr-1">
                            <input checked="" type="checkbox" wire:model="correct" id="correct"
                                class="custom-control-input">
                            <label class="custom-control-label" for="correct"></label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        {{-- <div class="input-group upload-btn-wrapper mb-3 w-100">
                            <button class="btn btn-primary btn-block" type="button">Illustration</button>
                            <input type="file" name="myfile" wire:model="examOptionImage" />
                        </div> --}}
                        @if($examQuestionImage)
                        <img src="{{$examQuestionImage}}" class="img-fluid" style="max-width: 200px;" />
                        @endif
                        <div class="input-group upload-btn-wrapper mt-4 w-100">
                            <button class="btn btn-primary btn-block">Illustration</button>
                            <input type="file" wire:model="examOptionImage"
                                wire:change="$emit('examFileChoosen2', $event.target)">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <label> Explication :</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group mb-3">
                            <input type="text" name="explication" wire:model="explication" class="form-control"
                                required>
                            @error('explication') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary" wire:click.prevent="addExamOptionReponse()">Ajouter
                    <div wire:loading wire:target="addExamOptionReponse">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-edit-question" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Edition</h6>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <label for="label">Label :</label>
                            <div wire:ignore>
                                <input value="{{ $this->label }}" type="hidden">
                                <trix-editor wire:model.debounce.999999ms="label" class="trix-content"
                                    style="height:200px; overflow-y:scroll;">
                                </trix-editor>
                            </div>
                            {{-- <input type="text" class="form-control" wire:model.debounce.500ms="label"> --}}
                            <input type="hidden" wire:model.debounce.999999ms="question_id" class="form-control">
                            @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                        <div class="col-md-4">




                            {{-- @if($image==1)
                                <img src="{{url('storage/'.$file)}}" width="100" />
                            @endif --}}

                            @if($image==2)
                            <img src="{{$file}}" class="img-fluid" style="max-width: 100px;" />
                            @endif
                            <div class="input-group upload-btn-wrapper mt-4 mb-3 w-100">
                                <button class="btn btn-primary btn-block">Illustration</button>
                                <input type="file" id="image_edit_question"
                                    wire:change="$emit('fileChoosenEditQuestion')"
                                    wire:model.debounce.500ms="quizQuestionImage">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="type">Type :</label>
                    <select class="form-control" wire:model.debounce.500ms="type">
                        <option value=""> Selectionner Type</option>
                        <option value="unique">Unique</option>
                        <option value="multiple">Multiple</option>
                    </select>
                    @error('type') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" class="btn btn-warning" wire:click.prevent="update()">Modifier <div wire:loading
                        wire:target="update">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-edit-quiz" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Edition</h6>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Label :</label>
                    {{-- <div wire:ignore>
                        <input value="{{ $this->label }}" type="hidden">
                    <trix-editor wire:model.debounce.999999ms="label" class="trix-content"
                        style="height:200px; overflow:hidden;">
                    </trix-editor>
                </div> --}}
                <input type="text" class="form-control" wire:model.debounce.500ms="label">
                @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Description :</label>
                <textarea rows="5" class="form-control" wire:model.debounce.500ms="description">

                        </textarea>
                @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
            </div>

            <div class="form-group">
                <label for="label">Level :</label>
                {{--foreach for display subject--}}
                <select class="form-control" wire:model.debounce.500ms="level">
                    <option value="">Choisir le niveau</option>
                    <option value="easy">Facile</option>
                    <option value="medium">Moyen</option>
                    <option value="hard">Difficile</option>
                </select>
                @error('level') <span class="text-danger error">{{ $message }}</span>@enderror
            </div>
            <div class="form-group">
                <label for="label">Sujet :</label>
                {{--foreach for display subject--}}
                <select class="form-control" wire:model.debounce.500ms="subject_id">
                    <option value="">Choisir la matiere</option>
                    @foreach($subjects as $subject)
                    <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}
                    </option>

                    @endforeach
                </select>
                @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
            <button type="button" class="btn btn-warning" wire:click.prevent="updateQuiz({{$quizId}})">Modifier
                <div wire:loading wire:target="updateQuiz">
                    <div class="spinner-border spinner-border-sm m-1" role="status">
                        <span class="sr-only"> Loading...</span>
                    </div>
                </div>
            </button>
        </div>
    </div>
</div>
</div>
<div wire:ignore.self id="modal-delete-question" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression</h5>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" wire:click.prevent="delete({{ $question_id }}) " class="btn btn-danger">Valider
                    <div wire:loading wire:target="delete">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-delete-reponse" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression</h5>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" wire:click.prevent="deleteOptionReponse({{ $answer_id }})"
                    class="btn btn-danger">Valider <div wire:loading wire:target="deleteOptionReponse">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-delete-exam-reponse" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression</h5>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" wire:click.prevent="deleteExamOptionReponse({{ $examAnswerId }})"
                    class="btn btn-danger">Valider <div wire:loading wire:target="deleteExamOptionReponse">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>


<div wire:ignore.self id="modal-delete-quiz" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression {{ $label }}</h5>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" wire:click.prevent="deletequiz({{ $quizId }})" class="btn btn-danger">Valider
                    <div wire:loading wire:target="deletequiz">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>



<div wire:ignore.self id="modal-delete-lesson" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression {{ $label }}</h5>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" wire:click.prevent="deletelesson({{ $lessonId }})" class="btn btn-danger">Valider
                    <div wire:loading wire:target="deletelesson">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div>
                </button>

            </div>

        </div>

    </div>
</div>


<div wire:ignore.self id="modal-add-book" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Ajout</h5>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Titre :</label>
                    <input type="text" wire:model.debounce.500ms='title' class="form-control">
                    @error('title') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description :</label>
                    <textarea rows="5" name="description" wire:model.debounce.500ms='description' class="form-control">

                        </textarea>
                    @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="input-group upload-btn-wrapper mb-3 w-100">

                    <button type="button" class="btn btn-primary btn-block">Charger fichier</button>
                    <input type="file" wire:model.debounce.500ms="bookFile" />
                    @error('bookFile') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="label">Prix :</label>
                    <input type="text" wire:model.debounce.500ms='price' class="form-control">
                    @error('price') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="label">Pages :</label>
                    <input type="number" wire:model.debounce.500ms='page_number' class="form-control">
                    @error('page_number') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="label">Sujet :</label>
                    {{--foreach for display subject--}}
                    <select class="form-control" wire:model.debounce.500ms="subject_id">
                        <option value="">Choisir la matiere</option>
                        @foreach($subjects as $subject)
                        <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}
                        </option>

                        @endforeach
                    </select>
                    @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary close-modal" wire:click.prevent="storeBook">Sauvegarder
                    <div wire:loading wire:target="storeBook">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>












<div wire:ignore.self id="modal-add-lesson" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Ajouter leçon</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Nom :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="label">
                    @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description :</label>
                    <textarea rows="5" class="form-control" wire:model.debounce.500ms="description">

                        </textarea>
                    @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="label">Sujet :</label>
                    {{--foreach for display subject--}}
                    <select class="form-control" wire:model.debounce.500ms="subject_id">
                        <option value="">Choisir la matiere</option>
                        @foreach($subjects as $subject)
                        <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}
                        </option>

                        @endforeach
                    </select>
                    @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Temps estimé du cours :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="estimate_time">
                    @error('estimate_time') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary close-modal" wire:click.prevent="storelesson()">Sauvegarder
                    <div wire:loading wire:target="storelesson">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>






<div wire:ignore.self id="modal-edit-lesson" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Editer leçon</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Nom :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="label">
                    @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description :</label>
                    <textarea rows="5" class="form-control" wire:model.debounce.500ms="description">

                        </textarea>
                    @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="label">Sujet :</label>
                    {{--foreach for display subject--}}
                    <select class="form-control" wire:model.debounce.500ms="subject_id">
                        <option value="">Choisir la matiere</option>
                        @foreach($subjects as $subject)
                        <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}
                        </option>

                        @endforeach
                    </select>
                    @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Temps estimé du cours :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="estimate_time">
                    @error('estimate_time') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary close-modal"
                    wire:click.prevent="updatelesson()">Sauvegarder <div wire:loading wire:target="updatelesson">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>




<div wire:ignore.self id="modal-edit-chapter" class="modal  fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Edition</h6>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="label">Titre :</label>
                            <input type="text" wire:model.debounce.500ms="title" class="form-control">
                            <input type="hidden" wire:model.debounce.500ms="lessonId" class="form-control">
                            @error('title') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Contenu :</label>
                    <div>
                        <div wire:ignore>
                            <input value="{{ $this->content }}" type="hidden">
                            <trix-editor wire:model.debounce.999999ms="content" class="trix-content"
                                style="height:200px; overflow-y: scroll;">
                            </trix-editor>
                        </div>
                    </div>
                </div>


                <div class="input-group upload-btn-wrapper mb-3 w-100">

                    <button class="btn btn-primary btn-block">Charger fichier</button>
                    <input type="file" wire:model="filePdf" accept='application/pdf' />
                    @error('filePdf') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-warning" wire:click.prevent="updateChapter()">Modifier <div
                        wire:loading wire:target="updateChapter">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div></button>
            </div>
        </div>
    </div>
</div>


<div wire:ignore.self id="modal-delete-chapter" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression {{$title}}</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-danger" wire:click.prevent="deleteChapter({{$chapterId}})">Valider
                    <div wire:loading wire:target="deleteChapter">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                </button>
            </div>
        </div>
    </div>
</div>
<div wire:ignore.self id="modal-add-chapter" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Ajout</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <label for="label">Titre :</label>
                            <input type="text" wire:model.debounce.500ms="title" class="form-control">
                            <input type="hidden" wire:model.debounce.500ms="lessonId" class="form-control">
                            @error('title') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>Contenu :</label>
                    <div class="h-200">
                        <div wire:ignore wire:model.debounce.999999ms="content">
                            <input type="hidden">
                            <trix-editor class="trix-content" style="height:200px; overflow-y:scroll;">
                            </trix-editor>
                        </div>
                    </div>
                </div>
                <div class="input-group upload-btn-wrapper position-relative mb-3 w-100">
                    <button class="btn btn-primary btn-block" type="button">Charger fichier</button>
                    <input role="button" type="file" wire:model="filePdf" accept='application/pdf'
                        style="cursor: pointer; bottom: 0; right: 0;" />
                    @error('filePdf') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary" wire:click.prevent="addChapter()">Sauvegarder <div
                        wire:loading wire:target="addChapter">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div></button>
            </div>
        </div>
    </div>
</div>




<div wire:ignore.self id="modal-delete-student" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression {{$studentName}}</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:clieck.prevent="cancel()">
                    Annuler</button>
                <button type="button" class="btn btn-danger" wire:click.prevent="deleteUser({{$studentId}})">Valider
                    <div wire:loading wire:target="deleteUser">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                </button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-add-subject" class="modal fade" role="dialog" aria-labelledby="modal-standard-title"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Ajout matière</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Label :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="label">
                    <input type="hidden" class="form-control" wire:model.debounce.500ms="classeId">
                    @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description :</label>
                    <textarea rows="5" class="form-control" wire:model.debounce.500ms="description"></textarea>
                    @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary" wire:click.prevent="addSubject()">Sauvegarder <div
                        wire:loading wire:target="addSubject">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div></button>
            </div>
        </div>
    </div>
</div>
<div wire:ignore.self id="modal-edit-subject" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Editer Matière</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Nom :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="label">
                    @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description :</label>
                    <textarea rows="5" class="form-control" wire:model.debounce.500ms="description"></textarea>
                    @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" wire:click.prevent="cancel()"
                    data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-warning"
                    wire:click.prevent="updateSubject({{$subjectId}})">Modifier <div wire:loading
                        wire:target="updateSubject">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div></button>
            </div>
        </div>
    </div>
</div>


<div wire:ignore.self id="modal-add-student" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Ajout</h6>
                <button type="button" class="close" data-dismiss="modal" wire:clieck.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">Nom :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="nom">
                    <input type="hidden" class="form-control" wire:model.debounce.500ms="classeId">
                    @error('nom') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="surname">Prénom :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="prenom">
                    @error('prenom') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="email">Email :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="email">
                    @error('email') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="password">Mot de passe :</label>
                    <input type="password" class="form-control" wire:model.debounce.500ms="password">
                    @error('password') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="adresse">Adresse :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="adresse">
                    @error('adresse') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="tel">Country :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="country">
                    @error('country') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary" wire:click.prevent="addUser()">Ajouter<div wire:loading
                        wire:target="addUser">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div></button>
            </div>
        </div>
    </div>
</div>
<div wire:ignore.self id="modal-edit-student" class="modal  fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Edition</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="name">Nom :</label>
                    <input type="text" class="form-control" id="name">
                </div>

                <div class="form-group">
                    <label for="surname">Prénom :</label>
                    <input type="text" class="form-control" id="surname">
                </div>

                <div class="form-group">
                    <label for="email">Email :</label>
                    <input type="text" class="form-control" id="email">
                </div>

                <div class="form-group">
                    <label for="password">Mot de passe :</label>
                    <input type="text" class="form-control" id="password">
                </div>

                <div class="form-group">
                    <label for="adresse">Adresse :</label>
                    <input type="text" class="form-control">
                </div>

                <div class="form-group">
                    <label for="tel">Country :</label>
                    <input type="text" class="form-control" id="tel">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-warning">Modifier</button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-delete-subject" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression </h5>
                <button type="button" class="close" data-dismiss="modal" wire:click="cancel()" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Attention la suppression de la matière engendrera la supression des quiz, examens, leçons en
                    relation avec cette matière !! </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" class="btn btn-danger" wire:click.prevent="deleteSubject({{$subjectId}})">Valider
                    <div wire:loading wire:target="deleteSubject">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                </button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-edit-exam" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Edition</h6>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Label :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="label">
                    @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description :</label>
                    <textarea rows="5" class="form-control" wire:model.debounce.500ms="description">

                        </textarea>
                    @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="label">Durée Examen :</label>
                    {{--foreach for display subject--}}
                    <input type="text" class="form-control" wire:model.debounce.500ms="estimate_duration">
                    @error('estimate_duration') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="label">Sujet :</label>
                    {{--foreach for display subject--}}
                    <select class="form-control" wire:model.debounce.500ms="subject_id">
                        <option value="">Choisir la matiere</option>
                        @foreach($subjects as $subject)
                        <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}
                        </option>

                        @endforeach
                    </select>
                    @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" class="btn btn-warning" wire:click.prevent="updateExam({{$examId}})">Modifier
                    <div wire:loading wire:target="updateExam">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>


<div wire:ignore.self id="modal-delete-exam" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression {{ $label }}</h5>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" wire:click.prevent="deleteExam({{ $examId }})" class="btn btn-danger">Valider
                    <div wire:loading wire:target="deleteExam">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-delete-exam-question" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression {{ $examQuestionId }}
                </h5>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" wire:click.prevent="deleteExamQuestion({{ $examQuestionId }}) "
                    class="btn btn-danger">Valider <div wire:loading wire:target="deleteExamQuestion">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>


<div wire:ignore.self id="modal-edit-exam-question" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="modal-standard-title">Edition Question Examen</h6>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <label for="label">Label :</label>
                            <div wire:ignore>
                                <input value="{{ $this->label }}" type="hidden">
                                <trix-editor wire:model.debounce.999999ms="label" class="trix-content"
                                    style="height:200px; overflow-y:scroll;">
                                </trix-editor>
                            </div>
                            {{-- <input type="text" class="form-control" wire:model.debounce.500ms="label"> --}}
                            <input type="hidden" wire:model.debounce.500ms="examQuestionId" class="form-control">
                            @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                        </div>

                        <div class="col-md-4">

                            {{-- @if($imageExam==1)
                                <img src="{{url('storage/'.$file)}}" width="100" />
                            @endif --}}

                            @if($imageExam==2)
                            <img src="{{$file}}" class="img-fluid" style="max-width: 100px;" />
                            @endif
                            <div class="input-group upload-btn-wrapper mt-4 mb-3 w-100">
                                <button class="btn btn-primary btn-block">Illustration</button>
                                <input type="file" id="image_edit_exam_question"
                                    wire:change="$emit('fileChoosenEditExamQuestion')"
                                    wire:model.debounce.500ms="examQuestionImage">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">

                    <label for="label">Coefficient :</label>
                    <input type="numeric" class="form-control" wire:model.debounce.500ms="scale">

                    @error('scale') <span class="text-danger error">{{ $message }}</span>@enderror

                </div>


                <div class="form-group">
                    <label for="type">Type :</label>
                    <select class="form-control" wire:model.debounce.500ms="type">
                        <option value=""> Selectionner Type</option>
                        <option value="unique">Unique</option>
                        <option value="multiple">Multiple</option>
                    </select>
                    @error('type') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal" wire:click="cancel()">Annuler</button>
                <button type="button" class="btn btn-warning" wire:click.prevent="updateExamQuestion()">Modifier
                    <div wire:loading wire:target="update">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>


<div wire:ignore.self id="modal-add-correction" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Ajouter Sujet corrigé</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Label :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="label">
                    @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description :</label>
                    <textarea rows="5" class="form-control" wire:model.debounce.500ms="description">

                        </textarea>
                    @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="input-group upload-btn-wrapper mb-3 w-100">

                    <button class="btn btn-primary btn-block">Charger fichier</button>
                    <input type="file" wire:model="filePdf" accept='application/pdf' />
                    @error('filePdf') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="label">Matière :</label>
                    {{--foreach for display subject--}}
                    <select class="form-control" wire:model.debounce.500ms="subject_id">
                        <option value="">Choisir la matiere</option>
                        @foreach($subjects as $subject)
                        <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}
                        </option>

                        @endforeach
                    </select>
                    @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary close-modal"
                    wire:click.prevent="storeCorrection()">Sauvegarder <div wire:loading wire:target="storeCorrection">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-delete-correction" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression {{$label}}</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-danger"
                    wire:click.prevent="deleteCorrection({{$correctionId}})">Valider <div wire:loading
                        wire:target="deleteCorrection">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div></button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-edit-correction" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Ajouter Sujet corrigé</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Label :</label>
                    <input type="text" class="form-control" wire:model.debounce.500ms="label">
                    @error('label') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description :</label>
                    <textarea rows="5" class="form-control" wire:model.debounce.500ms="description">

                        </textarea>
                    @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="input-group upload-btn-wrapper mb-3 w-100">

                    <button class="btn btn-primary btn-block">Charger fichier</button>
                    <input type="file" wire:model="filePdf" accept='application/pdf' />
                    @error('filePdf') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="form-group">
                    <label for="label">Matière :</label>
                    {{--foreach for display subject--}}
                    <select class="form-control" wire:model.debounce.500ms="subject_id">
                        <option value="">Choisir la matiere</option>
                        @foreach($subjects as $subject)
                        <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}
                        </option>

                        @endforeach
                    </select>
                    @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary close-modal"
                    wire:click.prevent="updateCorrection()">Sauvegarder <div wire:loading
                        wire:target="updateCorrection">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div></button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-delete-book" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Confirmer la suppression {{$title}}</h5>
                <button type="button" class="close" data-dismiss="modal" wire:click.prevent="cancel()"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Etes vous sur de vouloir supprimer cet enregistrement ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-danger" wire:click.prevent="deleteBook({{$bookId}})">Valider
                    <div wire:loading wire:target="deleteChapter">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                </button>
            </div>
        </div>
    </div>
</div>

<div wire:ignore.self id="modal-edit-book" class="modal fade" tabindex="-1" role="dialog"
    aria-labelledby="modal-standard-title" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-standard-title">Ajout</h5>
                <button type="button" class="close" wire:click="cancel()" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="label">Titre :</label>
                    <input type="text" wire:model.debounce.500ms='title' class="form-control">
                    <input type="hidden" wire:model.debounce.500ms='bookId' class="form-control">
                    @error('title') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Description :</label>
                    <textarea rows="5" name="description" wire:model.debounce.500ms='description' class="form-control">

                        </textarea>
                    @error('description') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>

                <div class="input-group upload-btn-wrapper mb-3 w-100">

                    <button class="btn btn-primary btn-block">Charger fichier</button>
                    <input type="file" wire:model.debounce.500ms="bookFile" />
                    @error('bookFile') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="label">Prix :</label>
                    <input type="text" wire:model.debounce.500ms='price' class="form-control">
                    @error('price') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="label">Pages :</label>
                    <input type="text" wire:model.debounce.500ms='page_number' class="form-control">
                    @error('page_number') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
                <div class="form-group">
                    <label for="label">Sujet :</label>
                    {{--foreach for display subject--}}
                    <select class="form-control" wire:model.debounce.500ms="subject_id">
                        <option value="">Choisir la matiere</option>
                        @foreach($subjects as $subject)
                        <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}
                        </option>

                        @endforeach
                    </select>
                    @error('subject_id') <span class="text-danger error">{{ $message }}</span>@enderror
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal"
                    wire:click.prevent="cancel()">Annuler</button>
                <button type="button" class="btn btn-primary close-modal" wire:click.prevent="updateBook">Sauvegarder
                    <div wire:loading wire:target="updateBook">
                        <div class="spinner-border spinner-border-sm m-1" role="status">
                            <span class="sr-only"> Loading...</span>
                        </div>
                    </div>
                </button>
            </div>
        </div>
    </div>
</div>


<!-- WHWYH -->


<script type="text/javascript" src="{{asset('administrator/vendor/quill.min.js')}}"></script>
<script type="text/javascript" src="{{asset('administrator/js/quill.js')}}"></script>


<!--END WHWYH -->






</div>