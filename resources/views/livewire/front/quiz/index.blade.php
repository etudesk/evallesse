<div wire:poll.keep-alive>
    <div class="home-content" style="margin-top: 1rem; margin-bottom: 2rem;">
        <div class="container">
            <div class="row">
                @foreach ($questions as $question)
                <div class="col-12 col-md-4 mb-3">
                    <div class="el-card py-3 px-3">
                        <span class="d-inline-flex align-items-center bg-info text-white rounded-circle py-2 px-2">
                            <x-heroicon-s-question-mark-circle height="20" />
                        </span> <br>
                        <a href="javascript:void(0)"
                            class="d-inline-block el-card-title mt-3 text-info">{{ $quiz->label }}</a>
                        <br>
                        <div class="d-inline-flex align-items-center mt-2 mb-3 flex-wrap marker">
                            {{-- <x-heroicon-o-bookmark class="mr-2" height="20" /> --}}
                            <svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" fill="currentColor"
                                stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                class="feather feather-bar-chart-2 mr-2">
                                <line x1="18" y1="20" x2="18" y2="10" />
                                <line x1="12" y1="20" x2="12" y2="4" />
                                <line x1="6" y1="20" x2="6" y2="14" /></svg>
                            <span
                                class="mt-1">{{ $quiz->level == "easy" ? "Facile" : ($quiz->level == "medium" ? "Moyen" : "Difficile") }}</span>
                            <span class="separator mx-3"></span>
                            <svg height="20" viewBox="0 0 16 16" class="bi bi-book-half" fill="currentColor"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                            </svg>
                            <span class="ml-2 mt-1">{{ $quiz->subject->label }}</span>
                        </div>

                        <div class="d-flex align-items-center flex-column justify-content-center my-2">
                            <span
                                style="font-size: 30px; font-weight: 600;">{{ $userCorrectAnswerNumber }}/{{ $quiz->questions()->count() }}</span>
                            <span class="text-success" style="font-size: 12px;">Bonne réponse{{ $userCorrectAnswerNumber > 1 ? "s" : "" }}</span>
                        </div>

                        <div class="progress mt-4" style="height: 3px;">
                            @php $percent = ceil($userCorrectAnswerNumber/$quiz->questions()->count() * 100); @endphp
                            <div class="progress-bar bg-info" role="progressbar" style="width: {{ $percent }}%;"
                                aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                </div>
                <div class=" col-12 col-md-8">
                    <div class="el-card py-3 px-3 question-item mb-4">
                        <div class="d-flex flex-column justify-content-center">
                            @if(!is_null($question->getFirstMedia()))
                            <div class="mb-3 text-center">
                                <img class="rounded img-fluid" src="{{ $question->getFirstMedia()->getUrl() }}"
                                    alt="illustration" style="max-height: 150px; height: auto;">
                            </div>
                            @endif
                            <div class="mb-0 formatQLabel">{!! $question->label !!}</div>
                        </div>
                    </div>

                    @if($question->type == "unique")
                    <div class="quiz-instruction mb-2">Options de réponses. Attention <b>une seule réponse</b> est
                        correct.
                    </div>
                    @else
                    <div class="quiz-instruction mb-2">Options de réponses. Attention <b>plusieurs réponses</b> peuvent
                        être correct.
                    </div>
                    @endif

                    @php $uGoodResp = $question->getUserResponses($user)->pluck('quiz_answer_id')->toArray(); @endphp

                    @if(!empty($uGoodResp))
                    @foreach ($question->answers as $a)
                    <div
                        class="el-card py-2 px-3 {{ in_array($a->id, $uGoodResp) ? 'question-item-correct' : 'question-item' }} mb-2">
                        <div class="d-flex flex-column justify-content-center" style="font-size: 14px;">
                            @if(!is_null($a->getFirstMedia()))
                            <div class="mb-3 text-center">
                                <img class="rounded img-fluid" src="{{ $a->getFirstMedia()->getUrl() }}"
                                    alt="illustration" style="max-height: 150px; height: auto;">
                            </div>
                            @endif
                            <div
                                class="custom-control {{ $question->type == 'unique' ? 'custom-radio' : 'custom-checkbox' }}">
                                <input value="{{ $a->id }}"
                                    {{-- wire:model="{{ $question->type == 'unique' ? 'choice' : 'options' }}" --}}
                                    name="{{ $question->type == 'unique' ? 'choice['.$a->id.']' : 'options['.$a->id.']' }}"
                                    type="{{ $question->type == 'unique' ? 'radio' : 'checkbox' }}"
                                    class="custom-control-input" id="customCheck{{ $a->id }}"
                                    {{ in_array($a->id, $uGoodResp) ? 'checked' : 'disabled' }}
                                    {{ $question->type == 'multiple' ? 'disabled' : '' }}>
                                <label class="custom-control-label text-break formatQLabel" for="customCheck{{ $a->id }}"
                                    style="margin-top: 2px;">{!! $a->label !!}</label>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    @foreach ($question->answers as $a)
                    <div class="el-card py-2 px-3 question-item mb-2">
                        <div class="d-flex flex-column justify-content-center" style="font-size: 14px;">
                            @if(!is_null($a->getFirstMedia()))
                            <div class="mb-3 text-center">
                                <img class="rounded img-fluid" src="{{ $a->getFirstMedia()->getUrl() }}"
                                    alt="illustration" style="max-height: 150px; height: auto;">
                            </div>
                            @endif
                            <div
                                class="custom-control {{ $question->type == 'unique' ? 'custom-radio' : 'custom-checkbox' }}">
                                <input value="{{ $a->id }}"
                                    wire:model="{{ $question->type == 'unique' ? 'choice' : 'options' }}"
                                    name="{{ $question->type == 'unique' ? 'choice['.$a->id.']' : 'options['.$a->id.']' }}"
                                    type="{{ $question->type == 'unique' ? 'radio' : 'checkbox' }}"
                                    class="custom-control-input" id="customCheck{{ $a->id }}">
                                <label class="custom-control-label formatQLabel text-break" for="customCheck{{ $a->id }}"
                                    style="margin-top: 2px;">{!! $a->label !!}</label>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif

                    {{-- Navigation  --}}
                    <div class="flex mt-3">
                        <div wire:target="addUserQuizResponse" wire:loading class="loader"></div>
                        <div wire:target="nextPage" wire:loading class="loader"></div>
                        <div wire:target="previousPage" wire:loading class="loader"></div>
                    </div>
                    <div class="d-flex justify-content-between justify-content-center mt-1">
                        @if(empty($uGoodResp))
                        <button class="btn btn-sm btn-info"
                            wire:click="addUserQuizResponse(`{{ $questions->first()->id }}`, `{{ $questions->hasMorePages() }}`)">Valider</button>
                        @else
                        <div></div>
                        @endif
                        {{-- <button class="btn btn-sm btn-info" data-toggle="modal"
                            data-target="#modalQuizError">Valider</button> --}}

                        <div class="flex">
                            {{ $questions->links('vendor.quiz-pagination') }}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>