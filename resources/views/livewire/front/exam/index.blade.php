<div wire:poll.keep-alive>
    <div class="home-content" style="margin-top: 1rem; margin-bottom: 2rem;">
        <div class="container">
            @foreach ($questions as $question)
            <div class="row">
                <div class="col-12 col-md-4 mb-3">
                    <div class="el-card py-3 px-3">
                        <span class="d-inline-flex align-items-center bg-info text-white rounded-circle py-2 px-2">
                            <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 20 20">
                                <path
                                    d="M3.302 12.238c.464 1.879 1.054 2.701 3.022 3.562 1.969.86 2.904 1.8 3.676 1.8.771 0 1.648-.822 3.616-1.684 1.969-.861 1.443-1.123 1.907-3.002L10 15.6l-6.698-3.362zm16.209-4.902l-8.325-4.662c-.652-.365-1.72-.365-2.372 0L.488 7.336c-.652.365-.652.963 0 1.328l8.325 4.662c.652.365 1.72.365 2.372 0l5.382-3.014-5.836-1.367a3.09 3.09 0 0 1-.731.086c-1.052 0-1.904-.506-1.904-1.131 0-.627.853-1.133 1.904-1.133.816 0 1.51.307 1.78.734l6.182 2.029 1.549-.867c.651-.364.651-.962 0-1.327zm-2.544 8.834c-.065.385 1.283 1.018 1.411-.107.579-5.072-.416-6.531-.416-6.531l-1.395.781c0-.001 1.183 1.125.4 5.857z" />
                            </svg>
                        </span>
                        <br />
                        <a href="#" class="d-inline-block el-card-title mt-3 text-info">{{ $exam->label }}</a>
                        <br />
                        <div class="d-inline-flex align-items-center mt-2 mb-3 flex-wrap marker">
                            <svg height="20" viewBox="0 0 16 16" class="bi bi-book-half" fill="currentColor"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                            </svg>
                            <span class="ml-2 mt-1">{{ $exam->subject->label }}</span>
                        </div>

                        <div class="d-flex align-items-center flex-column justify-content-center my-2">
                            <span style="font-size: 30px; font-weight: 600;">{{ $questions->firstItem() }} -
                                {{ $questions->total() }}</span>
                            <span class="text-primary" style="font-size: 12px; font-weight: 400;">Questions</span>
                        </div>

                        {{-- <div class="mt-3 d-flex align-items-center justify-content-center">
                            {{ $questions->links() }}
                    </div> --}}
                </div>
            </div>

            <div class=" col-12 col-md-8">
                <div class="el-card py-3 px-3 question-item mb-4">
                    <div class="d-flex flex-column justify-content-center">
                        @if(!is_null($question->getFirstMedia()))
                        <div class="mb-3 text-center">
                            <img class="rounded img-fluid" src="{{ $question->getFirstMedia()->getUrl() }}"
                                alt="illustration" style="max-height: 150px; height: auto;">
                        </div>
                        @endif
                        <div class="mb-0 formatQLabel">
                            {!! $question->label !!}
                        </div>
                    </div>
                </div>

                <div class="quiz-instruction mb-2">
                    @if($question->type == "unique")
                    <div class="quiz-instruction mb-2">Options de réponses. Attention <b>une seule réponse</b> est
                        correcte.
                    </div>
                    @else
                    <div class="quiz-instruction mb-2">Options de réponses. Attention <b>plusieurs réponses</b>
                        peuvent
                        être correctes.
                    </div>
                    @endif
                </div>

                @php $uResp = $question->getUserResponses($user)->pluck('exam_answer_id')->toArray(); @endphp

                @if(!empty($uResp))
                @foreach ($question->answers as $a)
                <div class="el-card py-2 px-3 question-item mb-2">
                    <div class="d-flex flex-column justify-content-center" style="font-size: 14px;">
                        @if(!is_null($a->getFirstMedia()))
                        <div class="mb-3 text-center">
                            <img class="rounded img-fluid" src="{{ $a->getFirstMedia()->getUrl() }}" alt="illustration"
                            style="max-height: 150px; height: auto;">
                        </div>
                        @endif
                        <div
                            class="custom-control {{ $question->type == 'unique' ? 'custom-radio' : 'custom-checkbox' }}">
                            @if($question->type == "unique")
                            <input value="{{ $a->id }}"
                                wire:click="changeInputState(`{{ $question->type }}`, `{{ $a->id }}`)"
                                name="{{ $question->type == 'unique' ? 'choice' : 'options[]' }}"
                                type="{{ $question->type == 'unique' ? 'radio' : 'checkbox' }}"
                                class="custom-control-input" id="customCheck{{ $a->id }}"
                                {{ in_array($a->id, $uResp) ? 'checked' : '' }}>
                            @else
                            <input value="{{ $a->id }}"
                                wire:click="changeInputState(`{{ $question->type }}`, `{{ $a->id }}`)"
                                name="{{ $question->type == 'unique' ? 'choice' : 'options[]' }}"
                                type="{{ $question->type == 'unique' ? 'radio' : 'checkbox' }}"
                                class="custom-control-input" id="customCheck{{ $a->id }}"
                                {{ in_array($a->id, $uResp) ? 'checked' : '' }}>
                            @endif
                            <label class="custom-control-label formatQLabel text-break" for="customCheck{{ $a->id }}"
                                style="margin-top: 2px;">{!! $a->label !!}</label>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                @foreach ($question->answers as $a)
                <div class="el-card py-2 px-3 question-item mb-2">
                    <div class="d-flex flex-column justify-content-center" style="font-size: 14px;">
                        @if(!is_null($a->getFirstMedia()))
                        <div class="mb-3 text-center">
                            <img class="rounded img-fluid" src="{{ $a->getFirstMedia()->getUrl() }}" alt="illustration"
                            style="max-height: 150px; height: auto;">
                        </div>
                        @endif
                        <div
                            class="custom-control {{ $question->type == 'unique' ? 'custom-radio' : 'custom-checkbox' }}">
                            <input value="{{ $a->id }}"
                                wire:model="{{ $question->type == 'unique' ? 'choice' : 'options' }}"
                                name="{{ $question->type == 'unique' ? 'choice['.$a->id.']' : 'options['.$a->id.']' }}"
                                type="{{ $question->type == 'unique' ? 'radio' : 'checkbox' }}"
                                class="custom-control-input" id="customCheck{{ $a->id }}">
                            <label class="custom-control-label formatQLabel text-break" for="customCheck{{ $a->id }}"
                                style="margin-top: 2px;">{!! $a->label !!}</label>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif


                {{-- Validation  --}}
                <div class="mt-4"></div>
                <div class="flex my-1">
                    <div wire:target="addExamResponse" wire:loading class="loader"></div>
                    <div wire:target="nextPage" wire:loading class="loader"></div>
                    <div wire:target="gotoPage" wire:loading class="loader"></div>
                    <div wire:target="previousPage" wire:loading class="loader"></div>
                </div>
                <div class="d-flex justify-content-center justify-content-center">
                    {{ $questions->links() }}
                </div>
                <div class="d-flex justify-content-center justify-content-center">

                    @if($questions->hasMorePages())
                    <a wire:click.prevent="addExamResponse(`{{ $questions->first()->id }}`, `{{ $questions->first()->type }}`, `{{ $questions->hasMorePages() }}`)"
                        href="#" class="btn btn-info btn-block -mt-14">Valider</a>
                    @else
                    <a href="#"
                        wire:click.prevent="addExamResponse(`{{ $questions->first()->id }}`, `{{ $questions->first()->type }}`, `{{ $questions->hasMorePages() }}`)"
                        class="btn btn-primary btn-block -mt-14">J'ai
                        fini</a>
                    @endif
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
</div>