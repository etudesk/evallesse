<div>
    <div class="layout-login-centered-boxed">
        <div class="layout-login-centered-boxed__form">
            <div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-4 navbar-light">
                <a href="/" class="text-center text-light-gray mb-4">
                    <!-- LOGO -->
                    <img src="{{asset('administrator/images/logos/logo.png')}}" width="100" />
                </a>
            </div>

            <div class="card card-body">

                <form wire:submit.prevent="submit">
                    <div class="form-group">
                        <label class="text-label" for="email_2">Email: </label>
                        <div class="input-group input-group-merge">
                            <input id="email_2" wire:model.debounce.500ms="form.email" type="email"
                                class="form-control form-control-prepended" placeholder="Entrez votre email">

                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="far fa-envelope"></span>
                                </div>
                            </div>

                        </div>

                        @error('form.email') <span class="text-danger d-inline-block error mt-1">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="text-label" for="password_2">Mot de passe:</label>
                        <div class="input-group input-group-merge">
                            <input id="password_2" wire:model.debounce.500ms="form.password" type="password"
                                class="form-control form-control-prepended" placeholder="Entrez votre mot de passe">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <span class="fa fa-key"></span>
                                </div>
                            </div>
                        </div>
                        @error('form.password') <span
                            class="text-danger d-inline-block mt-1 error">{{ $message }}</span> @enderror
                    </div>
                    <div class="form-group mb-1">
                        <button class="btn btn-block btn-primary" type="submit"
                            style="background: #026ab3; border-color: #fff !important;">Connexion</button>
                    </div>

                    {{-- <span>Email: admin@gmail.com</span><br>
                    <span>Password: password</span><br><br> --}}
                    {{-- <div class="text-center">
                        <a href="{{route('password_forgotten')}}">Mot de passe oublié?</a>
            </div> --}}

        </div>
        </form>
    </div>
</div>
</div>

<div>




</div>
</div>