<div>
<div class="container-fluid page__heading-container mt-4">
        <div class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
            <h4 class="m-lg-0">Ajouter Elève</h4>
        </div>
        <nav aria-label="breadcrumb ">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item" aria-current="page">Classe</li>
                <li class="breadcrumb-item" aria-current="page">Classe 1</li>
                <li class="breadcrumb-item" aria-current="page">Ajouter Elève</li>
            </ol>
        </nav>
    </div>
    <div class="container-fluid page__container">

        <div class="card card-form">
            <div class="row no-gutters">
                <div class="col-lg-4 card-body">
                    <p><strong class="headings-color">Ajouter Elève</strong></p>
                    <span>les champs marqués (<span class="text-danger">*</span>) sont obligatoires </span>
                </div>
                <div class="col-lg-8 card-form__body card-body">

                    <div class="form-group">
                        <label for="name">Nom :</label>
                        <input type="text" class="form-control" id="name">
                    </div>

                    <div class="form-group">
                        <label for="surname">Prénom :</label>
                        <input type="text" class="form-control" id="surname">
                    </div>

                    <div class="form-group">
                        <label for="email">Email :</label>
                        <input type="text" class="form-control" id="email">
                    </div>

                    <div class="form-group">
                        <label for="password">Mot de passe :</label>
                        <input type="text" class="form-control" id="password">
                    </div>

                    <div class="form-group">
                        <label for="adresse">Adresse :</label>
                        <input type="text" class="form-control" id="adresse">
                    </div>

                    <div class="form-group">
                        <label for="tel">Téléphone :</label>
                        <input type="text" class="form-control" id="tel">
                    </div>
                </div>
            </div>

        </div>

        <div class="text-right mb-5">
            <a href="" class="btn btn-primary">Ajouter</a>
        </div>
    </div>
</div>
