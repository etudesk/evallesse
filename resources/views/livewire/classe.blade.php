<div>

    <div class="container-fluid page__heading-container mt-60">
        <form class="search-form search-form--light d-none d-sm-flex flex page__container" action="">
            <input type="text" wire:model.debounce.500ms="query" class="form-control" placeholder="Rechercher">
            <button class="btn" type="submit"><i class="material-icons">search</i></button>
        </form>
    </div>

    @include('flashy::message')
    <div class="container-fluid page__heading-container">
        <div
            class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
            <h4 class="m-lg-0">Classe <span class="badge badge-success">{{count($classe)}}</span></h4>
            <a href="" data-toggle="modal" data-target="#add-modal-classe" class="btn btn-success ml-lg-3">Ajouter <i
                    class="material-icons">add</i></a>

        </div>
    </div>
    <div class="container-fluid page__container">
        @if(count($classe)==0)
        <div class="d-block row text-center ">
            <div class=" mb-2 d-inline-block">
                <span class="color_date_range ">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"
                        viewBox="0 0 40 40" width="100" height="100">
                        <g transform="matrix(1.6666666666666667,0,0,1.6666666666666667,0,0)">
                            <path
                                d="M2.5,16C2.224,16,2,15.776,2,15.5v-11C2,4.224,2.224,4,2.5,4h14.625c0.276,0,0.5,0.224,0.5,0.5V8c0,0.552,0.448,1,1,1 s1-0.448,1-1V4c0-1.105-0.895-2-2-2H2C0.895,2,0,2.895,0,4v12c0,1.105,0.895,2,2,2h5.375c0.138,0,0.25,0.112,0.25,0.25v1.5 c0,0.138-0.112,0.25-0.25,0.25H5c-0.552,0-1,0.448-1,1s0.448,1,1,1h7.625c0.552,0,1-0.448,1-1s-0.448-1-1-1h-2.75 c-0.138,0-0.25-0.112-0.25-0.25v-1.524c0-0.119,0.084-0.221,0.2-0.245c0.541-0.11,0.891-0.638,0.781-1.179 c-0.095-0.466-0.505-0.801-0.981-0.801L2.5,16z M3.47,9.971c-0.303,0.282-0.32,0.757-0.037,1.06c0.282,0.303,0.757,0.32,1.06,0.037 c0.013-0.012,0.025-0.025,0.037-0.037l2-2c0.293-0.292,0.293-0.767,0.001-1.059c0,0-0.001-0.001-0.001-0.001l-2-2 c-0.282-0.303-0.757-0.32-1.06-0.037s-0.32,0.757-0.037,1.06C3.445,7.006,3.457,7.019,3.47,7.031l1.293,1.293 c0.097,0.098,0.097,0.256,0,0.354L3.47,9.971z M7,11.751h2.125c0.414,0,0.75-0.336,0.75-0.75s-0.336-0.75-0.75-0.75H7 c-0.414,0-0.75,0.336-0.75,0.75S6.586,11.751,7,11.751z M18.25,16.5c0,0.276-0.224,0.5-0.5,0.5s-0.5-0.224-0.5-0.5v-5.226 c0-0.174-0.091-0.335-0.239-0.426c-1.282-0.702-2.716-1.08-4.177-1.1c-0.662-0.029-1.223,0.484-1.252,1.146 c-0.001,0.018-0.001,0.036-0.001,0.054v7.279c0,0.646,0.511,1.176,1.156,1.2c1.647-0.011,3.246,0.552,4.523,1.593 c0.14,0.14,0.33,0.219,0.528,0.218c0.198,0.001,0.388-0.076,0.529-0.215c1.277-1.044,2.878-1.61,4.527-1.6 c0.641-0.023,1.15-0.547,1.156-1.188v-7.279c-0.001-0.327-0.134-0.64-0.369-0.867c-0.236-0.231-0.557-0.353-0.886-0.337 c-1.496,0.016-2.963,0.411-4.265,1.148c-0.143,0.092-0.23,0.251-0.23,0.421V16.5z"
                                stroke="none" fill="currentColor" stroke-width="0" stroke-linecap="round"
                                stroke-linejoin="round"></path>
                        </g>
                    </svg>
                    <h3 class="">Aucun enregistrement</h3>

                </span>
            </div>
        </div>
        @else
        <div class="row card-group-row">
            @foreach($classes as $classe)
            <div class="col-lg-4 col-md-4 card-group-row__col">
                <div class="card card-group-row__card ">
                    <div class="card-header">

                        <x-dropdown class="float-right ml-auto position-relative">
                            <x-slot name="trigger">
                                <a href="javascript:void(0)" class="text-muted"><i
                                        class="material-icons">more_horiz</i></a>
                            </x-slot>

                            <div style="display: block; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-136px, 22px, 0px); transition: opacity 0.3s ease, margin-top 0.3s ease, visibility 0.3s ease; margin-top: 7px !important; background-clip: initial; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.12); z-index: 1000; float: left; min-width: 10rem; padding: 0.625rem 0; margin: 0.125rem 0 0; font-size: 1rem; color: #353535; text-align: left;  background-color: #fff; border: 1px solid #efefef; border-radius: 5px;"
                                x-cloak>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#update-modal-classe"
                                    wire:click="edit({{ $classe->id }})">Modifier</a>
                                <a data-toggle="modal" data-target="#modal-delete-classe" class="dropdown-item"
                                    wire:click.prevent="deleting({{ $classe->id }})" href="#">Supprimer</a>
                            </div>
                        </x-dropdown>

                        {{-- <div class="dropdown ml-auto  float-right">
                        <a href="#" data-toggle="dropdown" data-caret="false" class="text-muted"
                            aria-expanded="false"><i class="material-icons">more_horiz</i></a>
                        <div class="dropdown-menu dropdown-menu-right">

                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#update-modal-classe"
                                wire:click="edit({{ $classe->id }})">Modifier</a>
                        <a data-toggle="modal" data-target="#modal-delete-classe" class="dropdown-item"
                            wire:click.prevent="deleting({{ $classe->id }})" href="#">Supprimer</a>

                    </div>
                </div> --}}
            </div>
            <div class="card-body d-flex flex-column text-center">
                <div class="d-block">
                    <div class="avatar mb-2 text-center ">
                        <span class="bg-blue avatar-title rounded-circle">
                            <svg xmlns="http://www.w3.org/2000/svg" version="1.1"
                                xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 40 40" width="22" height="22">
                                <g transform="matrix(1.6666666666666667,0,0,1.6666666666666667,0,0)">
                                    <path
                                        d="M2.5,16C2.224,16,2,15.776,2,15.5v-11C2,4.224,2.224,4,2.5,4h14.625c0.276,0,0.5,0.224,0.5,0.5V8c0,0.552,0.448,1,1,1 s1-0.448,1-1V4c0-1.105-0.895-2-2-2H2C0.895,2,0,2.895,0,4v12c0,1.105,0.895,2,2,2h5.375c0.138,0,0.25,0.112,0.25,0.25v1.5 c0,0.138-0.112,0.25-0.25,0.25H5c-0.552,0-1,0.448-1,1s0.448,1,1,1h7.625c0.552,0,1-0.448,1-1s-0.448-1-1-1h-2.75 c-0.138,0-0.25-0.112-0.25-0.25v-1.524c0-0.119,0.084-0.221,0.2-0.245c0.541-0.11,0.891-0.638,0.781-1.179 c-0.095-0.466-0.505-0.801-0.981-0.801L2.5,16z M3.47,9.971c-0.303,0.282-0.32,0.757-0.037,1.06c0.282,0.303,0.757,0.32,1.06,0.037 c0.013-0.012,0.025-0.025,0.037-0.037l2-2c0.293-0.292,0.293-0.767,0.001-1.059c0,0-0.001-0.001-0.001-0.001l-2-2 c-0.282-0.303-0.757-0.32-1.06-0.037s-0.32,0.757-0.037,1.06C3.445,7.006,3.457,7.019,3.47,7.031l1.293,1.293 c0.097,0.098,0.097,0.256,0,0.354L3.47,9.971z M7,11.751h2.125c0.414,0,0.75-0.336,0.75-0.75s-0.336-0.75-0.75-0.75H7 c-0.414,0-0.75,0.336-0.75,0.75S6.586,11.751,7,11.751z M18.25,16.5c0,0.276-0.224,0.5-0.5,0.5s-0.5-0.224-0.5-0.5v-5.226 c0-0.174-0.091-0.335-0.239-0.426c-1.282-0.702-2.716-1.08-4.177-1.1c-0.662-0.029-1.223,0.484-1.252,1.146 c-0.001,0.018-0.001,0.036-0.001,0.054v7.279c0,0.646,0.511,1.176,1.156,1.2c1.647-0.011,3.246,0.552,4.523,1.593 c0.14,0.14,0.33,0.219,0.528,0.218c0.198,0.001,0.388-0.076,0.529-0.215c1.277-1.044,2.878-1.61,4.527-1.6 c0.641-0.023,1.15-0.547,1.156-1.188v-7.279c-0.001-0.327-0.134-0.64-0.369-0.867c-0.236-0.231-0.557-0.353-0.886-0.337 c-1.496,0.016-2.963,0.411-4.265,1.148c-0.143,0.092-0.23,0.251-0.23,0.421V16.5z"
                                        stroke="none" fill="currentColor" stroke-width="0" stroke-linecap="round"
                                        stroke-linejoin="round"></path>
                                </g>
                            </svg>
                        </span>
                    </div>
                </div>


                <div class="d-block">
                    <p class="text-dark mb-2">
                        <strong>{{$classe->label}}</strong>
                    </p>

                    <div class="p-1">
                        <small class=" text-muted">
                            Nombre d'eleves
                        </small>
                        <p class="h6">
                            {{count($classe->users()->get())}}
                        </p>
                        <small class=" text-muted">
                            Nombre de matière
                        </small>
                        <p class="h6">
                            {{count($classe->subjects()->get())}}
                        </p>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6 text-center ">
                        <div class="avatar mb-2">
                            <span class="bg-blue float-rigth avatar-title rounded-circle">
                                <a href="{{route('admin.student.show',$classe->id)}}" data-toggle="tooltip"
                                    data-placement="top" title="Afficher les élèves de la classe"> <i
                                        class="fa fa-users text-white"></i></a>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6 text-center">
                        <div class="avatar mb-2">
                            <span class="bg-success float-rigth avatar-title rounded-circle">
                                <a href="{{route('admin.subject.show',$classe->id)}}" data-toggle="tooltip"
                                    data-placement="top" title="Afficher les matières de la classe"> <i
                                        class="fa fa-book text-white"></i></a>
                            </span>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    @endforeach

</div>
@endif
@if (session()->has('message'))


<div class="alert alert-success alert-dismissible fade show" role="alert"
    style="margin-top:30px; top:20px ; right:20px; left:0px ;width:50% ; position:absolute">
    <strong> {{ session('message') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="mt-3 col-md-6 mx-auto">
    {{$classes->links()}}
</div>
</div>

</div>