<div>
    <div class="container-fluid page__heading-container mt-4">
        <form class="search-form search-form--light d-none d-sm-flex flex page__container" action="">
            <input type="text" wire:model.debounce.500ms="query" class="form-control" placeholder="Rechercher">
            <button class="btn" type="submit"><i class="material-icons">search</i></button>
        </form>
    </div>
    <div class="container-fluid page__heading-container">
        <div class="page__heading row text-lg-left">
            {{--<div class="page__heading d-flex flex-column flex-md-row  text-center text-lg-left">--}}
            <div class="col-md-10">
                <h4 class="m-lg-0">Livre <span class="badge badge-success">{{count($books)}}</span></h4>
            </div>
            {{--<div class="col-md-2 col-md-2 col-sm-2 col-sm-12 float-right">--}}
            {{--<div class="form-group">--}}
            {{--<select id="category" class="bg-danger text-white custom-select ">--}}
            {{--<option value="usa">Filtrer par classe</option>--}}
            {{--<option value="usa">Classe 1</option>--}}
            {{--<option value="usa">Classe 2</option>--}}
            {{--</select>--}}
            {{--</div>--}}
            {{--</div>--}}

            {{--<div class="col-md-2 col-md-2 col-sm-2 float-right">--}}
            {{--<div class="form-group">--}}
            {{--<select id="category " class="bg-danger text-white custom-select ">--}}
            {{--<option value="usa">Filtrer par leçon</option>--}}
            {{--<option value="usa">Leçon 1</option>--}}
            {{--<option value="usa">Leçon 2</option>--}}
            {{--</select>--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="col-md-2 col-md-2 col-sm-2 col-12 float-right">
                <a href="" href="" data-toggle="modal" data-target="#modal-add-book"
                    class="btn btn-success btn-block ml-lg-3 float-right">Ajouter <i class="material-icons">add</i></a>
            </div>
        </div>
    </div>
    <div class="container-fluid page__container">
        @if(count($books)==0)
        <div class="d-block row text-center ">
            <div class=" mb-2 d-inline-block">
                <span class="color_date_range ">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"
                        viewBox="0 0 40 40" width="100" height="100">
                        <g transform="matrix(1.6666666666666667,0,0,1.6666666666666667,0,0)">
                            <path
                                d="M24,1.5C24,0.672,23.328,0,22.5,0h-21C0.672,0,0,0.672,0,1.5v21C0,23.328,0.672,24,1.5,24h21c0.828,0,1.5-0.672,1.5-1.5 V1.5z M10,21.5c0,0.276-0.224,0.5-0.5,0.5h-3C6.224,22,6,21.776,6,21.5v-6C6,15.224,6.224,15,6.5,15h3c0.276,0,0.5,0.224,0.5,0.5 V21.5z M15.5,21.5c0,0.276-0.224,0.5-0.5,0.5h-2c-0.276,0-0.5-0.224-0.5-0.5v-5c0-0.276,0.224-0.5,0.5-0.5h2 c0.276,0,0.5,0.224,0.5,0.5V21.5z M20.5,21.5c0,0.276-0.224,0.5-0.5,0.5h-2c-0.276,0-0.5-0.224-0.5-0.5v-6 c0-0.276,0.224-0.5,0.5-0.5h2c0.276,0,0.5,0.224,0.5,0.5V21.5z M23,11.75c0,0.414-0.336,0.75-0.75,0.75H1.75 C1.336,12.5,1,12.164,1,11.75S1.336,11,1.75,11H3c0.276,0,0.5-0.224,0.5-0.5V3.487C3.487,3.232,3.683,3.014,3.938,3h2.624 C6.817,3.014,7.013,3.232,7,3.487V10.5C7,10.776,7.224,11,7.5,11h1C8.776,11,9,10.776,9,10.5V6c0.012-0.288,0.254-0.511,0.542-0.5 h2.166c0.288-0.011,0.53,0.212,0.542,0.5v4.5c0,0.276,0.224,0.5,0.5,0.5h3.106c0.138,0,0.25-0.112,0.25-0.25 c0-0.029-0.005-0.059-0.015-0.086l-2.565-7c-0.079-0.229,0.043-0.479,0.272-0.558c0.007-0.003,0.015-0.005,0.023-0.007l1.8-0.577 c0.242-0.082,0.505,0.039,0.6,0.276l2.886,7.871c0.072,0.197,0.259,0.328,0.469,0.328h2.674c0.414,0,0.75,0.336,0.75,0.75 c0,0.001,0,0.003,0,0.004V11.75z"
                                stroke="none" fill="currentColor" stroke-width="0" stroke-linecap="round"
                                stroke-linejoin="round"></path>
                        </g>
                    </svg>
                    <h3 class="">Aucun enregistrement</h3>

                </span>
            </div>
        </div>
        @else
        <div class="row card-group-row">
            @foreach($books as $book)
            <div class="col-lg-4 col-md-4 card-group-row__col">
                <div class="card card-group-row__card ">
                    <div class="card-header">
                        <x-dropdown class="float-right ml-auto position-relative">
                            <x-slot name="trigger">
                                <a href="javascript:void(0)" class="text-muted"><i
                                        class="material-icons">more_horiz</i></a>
                            </x-slot>

                            <div style="display: block; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-136px, 22px, 0px); transition: opacity 0.3s ease, margin-top 0.3s ease, visibility 0.3s ease; margin-top: 7px !important; background-clip: initial; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.12); z-index: 1000; float: left; min-width: 10rem; padding: 0.625rem 0; margin: 0.125rem 0 0; font-size: 1rem; color: #353535; text-align: left;  background-color: #fff; border: 1px solid #efefef; border-radius: 5px;"
                                x-cloak>
                                <a class="dropdown-item" data-toggle="modal"
                                    wire:click.prevent="editingBook({{$book->id}})" data-target="#modal-edit-book"
                                    href="#">Modifier</a>
                                <a class="dropdown-item" data-toggle="modal"
                                    wire:click.prevent="deletingBook({{$book->id}})" data-target="#modal-delete-book"
                                    href="#">Supprimer</a>
                            </div>
                        </x-dropdown>
                        {{-- <div class="dropdown ml-auto  float-right">
                            <a href="#" data-toggle="dropdown" data-caret="false" class="text-muted"
                                aria-expanded="false"><i class="material-icons">more_horiz</i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" data-toggle="modal"
                                    wire:click.prevent="editingBook({{$book->id}})" data-target="#modal-edit-book"
                        href="#">Modifier</a>
                        <a class="dropdown-item" data-toggle="modal" wire:click.prevent="deletingBook({{$book->id}})"
                            data-target="#modal-delete-book" href="#">Supprimer</a>
                    </div>
                </div> --}}
            </div>
            <div class="card-body d-flex flex-column">
                @php $file = $book->getMedia();



                @endphp
                @foreach($file as $fi)
                <img src="{{url($fi->getUrl() )}}" class="img-fluid" style="max-height: 300px; width: auto;" />
                @endforeach
                <a href="#" class="p-1 m-1 text-dark text-center">
                    <strong class="">{{$book->title}}</strong>
                </a>
                <p class=" p-1 m-1 text-center">{{$book->description}}</p>
                <span href="#" class="p-1 m-1 text-dark text-center">

                    Prix : <strong class="text-danger">{{$book->price}}FCFA</strong>
                </span>
            </div>
        </div>

    </div>

    @endforeach

</div>
@endif
</div>



@if (session()->has('message'))


<div class="alert alert-success alert-dismissible fade show" role="alert"
    style="margin-top:30px; top:20px ; right:20px; left:0px ;width:50% ; position:absolute">
    <strong> {{ session('message') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="mt-3 ">
    {{$books->links()}}
</div>

</div>