<div>




    <div class="container-fluid page__heading-container ">
        <div
            class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
            <h4 class="m-lg-0">Sujets examens corrigés</h4>
            <nav aria-label=" ">
                <ol class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="{{ route('admin.correction') }}">Sujets corrigés</a></li>
                    <li class="breadcrumb-item">Correction</li>
                    <li class="breadcrumb-item" aria-current="page">Examens</li>
                    <li class="breadcrumb-item" aria-current="page">Sujets</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid page__heading-container mb-4">
        <form class="search-form search-form--light d-none d-sm-flex flex page__container" action="">
            <input type="text" wire:model.debounce.500ms="query" class="form-control" placeholder="Rechercher">
            <button class="btn" type="submit"><i class="material-icons">search</i></button>
        </form>
    </div>
    <div class="container-fluid page__container">

        <div class=" card-form">
            <div class="row no-gutters">

                <div class="col-lg-12 card-form__body ">

                    @foreach($sujets as $corrige)
                    <div id="accordion{{$corrige->id}}">
                        <div class="card card-perso card-raduis-perso">


                            <div class="card-header collapse-header-bg">
                                <h5 class="mb-0">
                                    <button class="btn btn-link d-block w-100">

                                        <span class="float-left grey font-weight-bold" data-toggle="collapse"
                                            data-target="#collapseOne{{$corrige->id}}" aria-controls=""><i
                                                class="fa fa-th grey border-solid-3 p-2"
                                                aria-hidden="true"></i>{{$corrige->label}}</span>
                                        <a data-toggle="modal" data-target="#modal-delete-correction"
                                            wire:click.prevent="deleting({{$corrige->id}})" href=""
                                            class="float-right text-danger"><span><i class="fa fa-trash"></i></span></a>
                                        <a href="" class="float-right mr-4" data-toggle="modal"
                                            wire:click.prevent="editing({{$corrige->id}})"
                                            data-target="#modal-edit-correction"> <span><i
                                                    class="fa fa-pen"></i></span></a>
                                    </button>

                                </h5>
                            </div>

                            <div id="collapseOne{{$corrige->id}}" class="collapse " aria-labelledby="headingOne"
                                data-parent="#accordion{{$corrige->id}}">
                                <div class="row">
                                    <div class="col-md-12 p-lg-5">
                                        <p>{!! $corrige->description !!}.</p>
                                        @php
                                        $file = $corrige->getMedia();
                                        @endphp


                                        @foreach($file as $fi)
                                        <a href="{{url($fi->getUrl() )}}" target='_blank'><span><i
                                                    class="fa fa-file-pdf"></i></span> {{$corrige->label}}</a>
                                        @endforeach

                                        <!-- <a href="{{url('storage/'.$corrige->file)}}" target='_blank'><span><i class="fa fa-file-pdf"></i></span> {{$corrige->label}}</a> -->
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    @endforeach
                </div>

            </div>

        </div>

        <div class="text-right mb-5 d-block text-center">
            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#modal-add-correction">Ajouter un
                chapitre <span><i class="fa fa-plus"></i></span></a>
        </div>
        @if(session()->has('message'))


        <div class="alert alert-success alert-dismissible fade show" role="alert"
            style="margin-top:30px; top:20px ; right:20px; left:0px; width:50% ; position:absolute">
            <strong> {{ session('message') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <div class="mt-3 col-md-6 mx-auto">
            {{$sujets->links()}}
        </div>


    </div>



</div>