<div>
    <style>
        .active {
            background: inherit !important;
        }
    </style>
    <div class="container-fluid page__heading-container ">
        <div
            class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left mb-">
            <h4 class="m-lg-0"> <a href="{{route('admin.subject.show',$classe->id)}}" class="text-dark">
                    {{$classe->label}} </a></h4>
            <nav aria-label=" ">
                <ol class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="{{route('admin.classroom')}}">Classes</a></li>
                    <li class="breadcrumb-item"><a href="{{route('admin.subject.show',$classe->id)}}">
                            {{$classe->label}} </a>
                    </li>
                    <li class="breadcrumb-item">Elèves</li>
                </ol>
            </nav>
        </div>

    </div>
    <div class="container-fluid page__container">



        <div class=" card-form">
            <div class="row no-gutters">
                {{--<div class="col-lg-4 card-body">--}}
                {{--<p><strong class="headings-color">Ajouter des chapters au quiz</strong></p>--}}
                {{--<span>les champs marqués (<span class="text-danger">*</span>) sont obligatoires </span>--}}
                {{--</div>--}}
                <div class="col-lg-12 card-form__body ">
                    <div class="container-fluid page__container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row ">
                                    <div class="col-md-12 d-block">
                                        <!-- <a href="" data-toggle="modal" data-target="#modal-add-student" wire:click.prevent="addingUser({{$classe->id}})" class="btn btn-success ml-lg-3 float-right">Ajouter <i class="material-icons">add</i></a> -->
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="col-md-2 d-inline-block search-form search-form--light m-3">
                                            <select class="form-control" wire:model.lazy="perPage">
                                                <option value="10">Show</option>
                                                <option value="10">10</option>
                                                <option value="15">15</option>
                                                <option value="30">30</option>
                                            </select>
                                        </div>
                                        <div class="col-md-4  d-inline-block float-right search-form--light m-3">
                                            <input type="text" class="form-control float-right  search"
                                                wire:model.debounce.500ms="query" placeholder="Search">
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    {{--<div class="card-header card-header-large bg-white d-flex align-items-center">--}}
                                    {{--<h4 class="card-header__title flex m-0">Liste</h4>--}}
                                    {{--</div>--}}

                                    <div class="card-header card-header-tabs-basic nav" role="tablist">
                                        <a href="#activity_all" class="active" data-toggle="tab" role="tab"
                                            aria-controls="activity_all" aria-selected="true">Elève</a>
                                    </div>
                                    <div class="card-body tab-content">
                                        <div class="tab-pane active show fade" id="activity_all">
                                            <div class="table-responsive border-bottom" data-toggle="lists"
                                                data-lists-values='["js-lists-values-employee-name"]'>


                                                <table class="table mb-0 thead-border-top-0">
                                                    <thead>
                                                        <tr>


                                                            <th>#</th>
                                                            <th>Nom</th>
                                                            <th>Numéro Tel.</th>
                                                            <th>Email</th>
                                                            <th>Modules suivis</th>
                                                            <th>Classe</th>
                                                            {{-- <th>Statut</th> --}}
                                                            <th>Inscription</th>
                                                            <!--   <th>Dernière connexion</th> -->
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="list" id="staff">
                                                        @foreach($students as $student)
                                                        <tr>
                                                            <td>
                                                                <div class="media align-items-center">
                                                                    <div class="avatar avatar-xs mr-2">
                                                                        <img src="{{asset('administrator/images/avatar/avatar-default-icon.png')}}"
                                                                            alt="Avatar"
                                                                            class="avatar-img rounded-circle">
                                                                    </div>
                                                                </div>

                                                            </td>
                                                            <td>
                                                                <div class="media-body">
                                                                    <span
                                                                        class="js-lists-values-employee-name">{{{$student->name}}}</span>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                {{ $student->tel }}
                                                            </td>
                                                            <td>
                                                                <small>{{ $student->email }}</small>
                                                            </td>
                                                            <td>{{count($student->progress()->get()->groupBy('lesson_id'))}}
                                                            </td>

                                                            <td class="text-success">{{$student->classroom->label}}</td>
                                                            {{-- <td><span
                                                                    class="badge badge-pill badge-success-light">{{$student->role}}</span>
                                                            </td> --}}
                                                            <td><small
                                                                    class="text-success">{{$student->created_at->diffForHumans()}}</small>
                                                            </td>
                                                            <!--
                                                        <td><small class="text-muted">Il y a 1 mois</small></td> -->

                                                            <td>

                                                                <a data-toggle="modal"
                                                                    data-target="#modal-delete-student" href=""
                                                                    wire:click.prevent="deletingUser({{$student->id}})"
                                                                    class="text-danger"><span><i
                                                                            class="fa fa-trash"></i></span></a>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="activity_purchases">
                                            Quam ducimus culpa eveniet adipisci officiis ab, quas sint aliquid eius
                                            tempore natus magnam similique placeat, perferendis explicabo eum qui quod
                                            facilis quae enim harum. Nihil dolores enim, dicta modi expedita architecto
                                            distinctio!
                                        </div>
                                        <div class="tab-pane fade" id="activity_emails">
                                            Ducimus aperiam aut corporis, facere nobis id quos dignissimos, ut corrupti
                                            asperiores reprehenderit culpa praesentium exercitationem, officia commodi.
                                        </div>
                                        <div class="tab-pane fade" id="activity_quotes">
                                            Odit consectetur dolore maxime similique qui officia deserunt fugiat quo
                                            tempore consequuntur dicta ratione sint commodi eum eligendi, magnam aliquid
                                            expedita quas accusantium, sed nobis tenetur illum mollitia. Quis ipsum
                                            tenetur distinctio tempore vitae atque quam.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




            </div>

        </div>



    </div>





    @if (session()->has('message'))


    <div class="alert alert-success alert-dismissible fade show" role="alert"
        style="margin-top:30px; top:20px ; right:20px; left:0px width:50% ; position:absolute">
        <strong> {{ session('message') }}</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif


    <div class="mt-3 col-md-6 mx-auto">
        {{$students->links()}}
    </div>

</div>