<div>
    <div class="container-fluid page__heading-container mt-4">
        <form class="search-form search-form--light d-none d-sm-flex flex page__container" action="">
            <input type="text" class="form-control" wire:model.debounce.500ms="query" placeholder="Rechercher">
            <button class="btn" type="submit"><i class="material-icons">search</i></button>
        </form>
    </div>
    <div class="container-fluid page__heading-container">
        <div
            class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
            <!-- <h4 class="m-lg-0"> {{$classe->label}} / Matière <span class="badge badge-success">{{count($subjects)}}</span></h4> -->
            <ol class="breadcrumb mt-2">

                <li class="breadcrumb-item"><a href="{{ route('admin.classroom') }}">Classe</a></li>
                <li class="breadcrumb-item">{{$classe->label}}</li>
                <li class="breadcrumb-item">Matières <span class="badge badge-success">{{count($subjects)}}</span></li>
            </ol>
            <a href="" data-toggle="modal" data-target="#modal-add-subject"
                wire:click.prevent="addingSubject({{$classe->id}})" class="btn btn-success ml-lg-3">Ajouter <i
                    class="material-icons">add</i></a>
        </div>
    </div>
    <div class="container-fluid page__container">
        <div class="row card-group-row">
            @forelse($subjects as $subject)
            <div class="col-lg-4 col-md-4 card-group-row__col">
                <div class="card card-group-row__card ">
                    <div class="card-header">
                        <x-dropdown class="float-right ml-auto position-relative">
                            <x-slot name="trigger">
                                <a href="javascript:void(0)" class="text-muted"><i
                                        class="material-icons">more_horiz</i></a>
                            </x-slot>

                            <div style="display: block; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-136px, 22px, 0px); transition: opacity 0.3s ease, margin-top 0.3s ease, visibility 0.3s ease; margin-top: 7px !important; background-clip: initial; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.12); z-index: 1000; float: left; min-width: 10rem; padding: 0.625rem 0; margin: 0.125rem 0 0; font-size: 1rem; color: #353535; text-align: left;  background-color: #fff; border: 1px solid #efefef; border-radius: 5px;"
                                x-cloak>
                                <a data-toggle="modal" data-target="#modal-edit-subject"
                                    wire:click.prevent="editing({{$subject->id}})" class="dropdown-item"
                                    href="#">Modifier</a>
                                <a data-toggle="modal" data-target="#modal-delete-subject"
                                    wire:click.prevent="deletingSubject({{$subject->id}})" class="dropdown-item"
                                    href="#">Supprimer</a>
                            </div>
                        </x-dropdown>
                        {{-- <div class="dropdown ml-auto  float-right">
                            <a href="#" data-toggle="dropdown" data-caret="false" class="text-muted"
                                aria-expanded="false"><i class="material-icons">more_horiz</i></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a data-toggle="modal" data-target="#modal-edit-subject"
                                    wire:click.prevent="editing({{$subject->id}})" class="dropdown-item"
                        href="#">Modifier</a>
                        <a data-toggle="modal" data-target="#modal-delete-subject"
                            wire:click.prevent="deletingSubject({{$subject->id}})" class="dropdown-item"
                            href="#">Supprimer</a>
                    </div>
                </div> --}}
            </div>
            <div class="card-body d-flex flex-column">
                <div class="avatar mb-2">
                    <span class="bg-soft-primary avatar-title rounded-circle text-center active">
                        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"
                            viewBox="0 0 40 40" width="22" height="22">
                            <g transform="matrix(1.6666666666666667,0,0,1.6666666666666667,0,0)">
                                <path
                                    d="M12.75,5h-6C6.336,5,6,5.336,6,5.75S6.336,6.5,6.75,6.5h6c0.414,0,0.75-0.336,0.75-0.75S13.164,5,12.75,5z M13.5,9.25 c0-0.414-0.336-0.75-0.75-0.75h-8C4.336,8.5,4,8.836,4,9.25S4.336,10,4.75,10h8C13.164,10,13.5,9.664,13.5,9.25z M4.75,12 C4.336,12,4,12.336,4,12.75s0.336,0.75,0.75,0.75h5.5c0.414,0,0.75-0.336,0.75-0.75S10.664,12,10.25,12H4.75z M11.3,17.655 c-0.039-0.093-0.13-0.154-0.231-0.155H2.5C2.224,17.5,2,17.276,2,17V2.5C2,2.224,2.224,2,2.5,2h13C15.776,2,16,2.224,16,2.5v10.07 c0,0.138,0.112,0.25,0.251,0.249c0.066,0,0.13-0.027,0.176-0.073l1.28-1.28C17.895,11.279,18,11.024,18,10.759V2 c0-1.105-0.895-2-2-2H2C0.895,0,0,0.895,0,2v15.5c0,1.105,0.895,2,2,2h7.868c0.22,0,0.413-0.143,0.478-0.353 c0.095-0.311,0.267-0.592,0.5-0.819l0.4-0.4C11.318,17.856,11.339,17.749,11.3,17.655z M12.062,20.131 c-0.099-0.097-0.258-0.096-0.355,0.003c-0.034,0.034-0.057,0.078-0.067,0.125L11.012,23.4c-0.055,0.271,0.119,0.535,0.39,0.59 c0.033,0.007,0.066,0.01,0.1,0.01c0.034,0,0.067-0.003,0.1-0.01l3.143-0.629c0.135-0.027,0.223-0.159,0.195-0.295 c-0.01-0.048-0.034-0.093-0.068-0.127L12.062,20.131z M23.228,11.765c-1.023-1.02-2.677-1.02-3.7,0l-6.5,6.5 c-0.195,0.195-0.195,0.512,0,0.707l3,3c0.195,0.195,0.512,0.195,0.707,0l6.5-6.5C24.257,14.447,24.254,12.787,23.228,11.765 C23.228,11.765,23.228,11.765,23.228,11.765z"
                                    stroke="none" fill="currentColor" stroke-width="0" stroke-linecap="round"
                                    stroke-linejoin="round"></path>
                            </g>
                        </svg>
                    </span>
                </div>
                <a href="#" class="p-1 m-1 text-dark">
                    <strong>{{$subject->label}}</strong>
                </a>
                <p class=" p-1 m-1">{{$subject->description}}</p>


            </div>
        </div>
    </div>
    @empty
    <div class="col-lg-12">
        <div class="py-3 px-4 text-gray text-center mt-5">
            <i style="font-weight: 700; font-size: 18px;" class="fa fa-book"></i> <br>
            <p class="text-uppercase mt-2" style="font-weight: 600;">Aucune matière de disponible pour le moment</p>
        </div>
    </div>
    @endforelse
</div>
</div>

@if (session()->has('message'))


<div class="alert alert-success alert-dismissible fade show" role="alert"
    style="margin-top:30px; top:20px ; right:20px; left:0px width:50% ; position:absolute">
    <strong> {{ session('message') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>

@endif
</div>