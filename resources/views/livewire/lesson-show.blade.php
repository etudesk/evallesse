<div>

    <div class="container-fluid page__heading-container ">
        <div
            class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
            <h4 class="m-lg-0">{{$lesson->label}} <a href="" data-toggle="modal" data-target="#modal-edit-lesson"
                    wire:click.prevent="editinglesson({{$lesson->id}})" class="h6"><span><i
                            class="fa fa-pen"></i></span></a></h4>
            <nav aria-label=" ">
                <ol class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="{{route('admin.lesson')}}">Leçons</a></li>
                    <!-- <li class="breadcrumb-item">{{$lesson->label}}</li> -->
                    <li class="breadcrumb-item" aria-current="page">{{$classe->label}}</li>
                    <li class="breadcrumb-item" aria-current="page">{{$lesson->subject->label}}</li>
                </ol>
            </nav>
        </div>

    </div>
    <div class="container-fluid page__container">

        <div class=" card-form">
            <div class="row no-gutters">

                <div class="col-lg-12 card-form__body ">
                    @php

                    $lesson_chapters = $lesson->chapters()->latest()->paginate(5);

                    @endphp
                    @foreach($lesson_chapters as $chapter)
                    <div id="accordion{{$chapter->id}}">
                        <div class="card card-perso card-raduis-perso">


                            <div class="card-header collapse-header-bg py-1 px-2">
                                <h5 class="mb-0">
                                    <button class="btn btn-link d-block w-100 py-0">

                                        <span class="float-left grey font-weight-bold " data-toggle="collapse"
                                            data-target="#collapseOne{{$chapter->id}}" aria-controls=""><i
                                                class="fa fa-th grey border-solid-3 p-2"
                                                aria-hidden="true"></i>{{$chapter->title}}</span>
                                        <a data-toggle="modal" data-target="#modal-delete-chapter"
                                            wire:click.prevent="deleting({{$chapter->id}})" href=""
                                            class="float-right text-danger"><span><i class="fa fa-trash"></i></span></a>
                                        <a href="" class="float-right mr-4  " data-toggle="modal"
                                            wire:click.prevent="editing({{$chapter->id}})"
                                            data-target="#modal-edit-chapter"> <span><i
                                                    class="fa fa-pen"></i></span></a>
                                    </button>

                                </h5>
                            </div>

                            <div id="collapseOne{{$chapter->id}}" class="collapse " aria-labelledby="headingOne"
                                data-parent="#accordion{{$chapter->id}}">
                                <div class="row">
                                    <div class="col-md-12 px-lg-5 pb-3">
                                        <p>{!! $chapter->content !!}</p>
                                        @php
                                        $file = $chapter->getMedia();
                                        @endphp


                                        @foreach($file as $fi)
                                        <a href="{{$fi->getFullUrl()}}" target='_blank'><span><i
                                                    class="fa fa-file-pdf"></i></span> {{$chapter->title}}</a>
                                        @endforeach


                                        <!-- <a href="{{url('storage/'.$chapter->pdf)}}" target='_blank'><span><i class="fa fa-file-pdf"></i></span> {{$chapter->title}}</a> -->
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                    @endforeach
                </div>

            </div>

        </div>

        <div class="text-right mb-5 d-block text-center">
            <a href="" class="btn btn-primary" data-toggle="modal" wire:click="addingChapter({{ $lesson->id }})"
                data-target="#modal-add-chapter">Ajouter du contenu <span><i class="fa fa-plus"></i></span></a>
        </div>
        @if (session()->has('message'))


        <div class="alert alert-success alert-dismissible fade show" role="alert"
            style="margin-top:30px; top:20px ; right:20px; left:0px width:50% ; position:absolute">
            <strong> {{ session('message') }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <div class="mt-3 col-md-6 mx-auto">
            {{$lesson_chapters->links()}}
        </div>


    </div>



</div>