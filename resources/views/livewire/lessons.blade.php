<div>
    <div class="container-fluid page__heading-container mt-4">
        <form class="search-form search-form--light d-none d-sm-flex flex page__container" action="">
            <input type="text" wire:model.debounce.500ms="query" class="form-control" placeholder="Rechercher">
            <button class="btn" type="submit"><i class="material-icons">search</i></button>
        </form>
    </div>
    <div class="container-fluid page__heading-container">
        <div class="page__heading row text-lg-left">
            {{--<div class="page__heading d-flex flex-column flex-md-row  text-center text-lg-left">--}}
            <div class="col-md-6">
                <h4 class="m-lg-0">Leçons <span class="badge badge-success">{{count($lesson)}}</span></h4>
            </div>
            <div class="col-md-4 col-md-4 col-sm-4 col-sm-12 float-right">
                <div class="form-group">
                    <select id="category" wire:model.lazy="subject" class="bg-danger text-white custom-select ">
                        <option value="">Filtrer par matiere</option>
                        @foreach($subjects as $subject)
                        <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}</option>

                        @endforeach
                    </select>
                </div>
            </div>


            <div class="col-md-2 col-md-2 col-sm-2 col-12 float-right">
                <a href="" class="btn btn-success btn-block ml-lg-3 float-right" data-toggle="modal"
                    data-target="#modal-add-lesson">Ajouter <i class="material-icons">add</i></a>
            </div>
        </div>
    </div>


    {{--

        <div class="container-fluid page__heading-container mt-4">
        <form class="search-form search-form--light d-none d-sm-flex flex page__container" action="">
            <input type="text" class="form-control"  wire:model.debounce.500ms="query" placeholder="Rechercher">
            <button class="btn" type="submit"><i class="material-icons">search</i></button>
        </form>
    </div>
    @include('flashy::message')
    <div class="container-fluid page__heading-container">
        <div class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
            <h4 class="m-lg-0">Leçon <span class="badge badge-success">{{count($lesson)}}</span></h4>
    <a href="" data-toggle="modal" data-target="#modal-add-lesson" class="btn btn-success ml-lg-3">Ajouter <i
            class="material-icons">add</i></a>

    <div class="col-md-4 col-md-3 col-sm-2 col-sm-12 float-right">
        <div class="form-group">
            <select id="category" wire:model.lazy="subject" class="bg-danger text-white custom-select ">
                <option value="">Filtrer par matiere</option>
                @foreach($subjects as $subject)
                <option value="{{$subject->id}}">{{$subject->label}} - {{$subject->classroom->label}}</option>

                @endforeach


            </select>
        </div>
    </div>
</div>


</div> --}}
<div class="container-fluid page__container">
    @if(count($lessons)==0)
    <div class="d-block row text-center ">
        <div class=" mb-2 d-inline-block">
            <span class="color_date_range ">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"
                    viewBox="0 0 40 40" width="100" height="100">
                    <g transform="matrix(1.6666666666666667,0,0,1.6666666666666667,0,0)">
                        <path
                            d="M12.619,8.412c-0.001-0.41-0.333-0.742-0.743-0.742H5.938c-0.41,0.015-0.73,0.36-0.715,0.77 c0.014,0.389,0.326,0.701,0.715,0.715h5.938C12.286,9.155,12.619,8.822,12.619,8.412L12.619,8.412z M9.586,19 c-0.02-0.115-0.119-0.199-0.236-0.2H3.464c-0.276,0-0.5-0.224-0.5-0.5V5.443c0.003-0.274,0.226-0.495,0.5-0.495h10.887 c0.272,0.003,0.491,0.223,0.494,0.495v4.039c-0.002,0.135,0.106,0.245,0.241,0.247c0.018,0,0.037-0.002,0.054-0.005 c0.807-0.152,1.623-0.249,2.443-0.29c0.131-0.007,0.232-0.116,0.231-0.247V3.464c0.001-0.82-0.663-1.484-1.483-1.485 c0,0-0.001,0-0.001,0h-3.957c-0.085,0-0.163-0.046-0.205-0.119C11.103,0.059,8.78-0.537,6.979,0.528 C6.43,0.853,5.972,1.311,5.647,1.86c-0.042,0.073-0.12,0.118-0.205,0.119H1.485C0.665,1.979,0,2.644,0,3.464c0,0,0,0,0,0v16.825 c0.001,0.82,0.665,1.484,1.485,1.484h8.847c0.135,0,0.244-0.109,0.244-0.244c0-0.046-0.013-0.092-0.038-0.131 C10.091,20.657,9.769,19.846,9.586,19z M11.035,12.523c0.286-0.376,0.604-0.726,0.951-1.046c0.085-0.079,0.028-0.343-0.11-0.343 H5.938c-0.41,0.015-0.73,0.36-0.715,0.77c0.014,0.389,0.326,0.701,0.715,0.715h4.907C10.92,12.619,10.99,12.583,11.035,12.523z M5.938,14.6c-0.41,0-0.742,0.331-0.743,0.741c0,0.41,0.331,0.742,0.741,0.743c0,0,0.001,0,0.001,0h3.37 c0.117,0,0.216-0.085,0.235-0.2c0.061-0.337,0.145-0.669,0.251-0.995c0.032-0.1,0.055-0.29-0.129-0.29L5.938,14.6z M17.32,10.639 c-3.69-0.001-6.681,2.99-6.682,6.68s2.99,6.681,6.68,6.682c3.69,0.001,6.681-2.99,6.682-6.68c0,0,0-0.001,0-0.001 C23.996,13.632,21.008,10.643,17.32,10.639z M17.32,22.021c-2.596,0-4.7-2.104-4.7-4.7s2.104-4.7,4.7-4.7s4.7,2.104,4.7,4.7 C22.017,19.915,19.914,22.018,17.32,22.021z M19.3,16.33h-0.742c-0.137,0-0.248-0.111-0.248-0.248v-1.237 c-0.017-0.546-0.474-0.975-1.021-0.958c-0.522,0.017-0.941,0.436-0.958,0.958v2.475c0,0.546,0.443,0.989,0.989,0.989 c0,0,0.001,0,0.001,0H19.3c0.546,0.017,1.004-0.412,1.021-0.958s-0.412-1.004-0.958-1.021C19.342,16.329,19.321,16.329,19.3,16.33z"
                            stroke="none" fill="currentColor" stroke-width="0" stroke-linecap="round"
                            stroke-linejoin="round"></path>
                    </g>
                </svg>
                <h3 class="">Aucun enregistrement</h3>

            </span>
        </div>
    </div>
    @else
    <div class="row card-group-row">
        @foreach($lessons as $lesson)
        <div class="col-lg-4 col-md-4 card-group-row__col">
            <div class="card card-group-row__card ">
                <div class="card-header">
                    <x-dropdown class="float-right ml-auto position-relative">
                        <x-slot name="trigger">
                            <a href="javascript:void(0)" class="text-muted"><i class="material-icons">more_horiz</i></a>
                        </x-slot>

                        <div style="display: block; position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-136px, 22px, 0px); transition: opacity 0.3s ease, margin-top 0.3s ease, visibility 0.3s ease; margin-top: 7px !important; background-clip: initial; box-shadow: 0 2px 4px rgba(0, 0, 0, 0.12); z-index: 1000; float: left; min-width: 10rem; padding: 0.625rem 0; margin: 0.125rem 0 0; font-size: 1rem; color: #353535; text-align: left;  background-color: #fff; border: 1px solid #efefef; border-radius: 5px;"
                            x-cloak>
                            <a class="dropdown-item"
                                href="{{route('admin.lesson.show',['id'=>$lesson->id])}}">Afficher</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#modal-edit-lesson"
                                wire:click.prevent="editinglesson({{$lesson->id}})" href="#">Modifier</a>
                            <a class="dropdown-item" data-toggle="modal" data-target="#modal-delete-lesson"
                                wire:click.prevent="deletinglesson({{ $lesson->id }})" href="#">Supprimer</a>
                        </div>
                    </x-dropdown>
                    {{-- <div class="dropdown ml-auto  float-right">
                            <a href="#" data-toggle="dropdown" data-caret="false" class="text-muted"
                                aria-expanded="false"><i class="material-icons">more_horiz</i></a>
                            <div class="dropdown-menu dropdown-menu-right" style="display: none;">
                                <a class="dropdown-item"
                                    href="{{route('admin.lesson.show',['id'=>$lesson->id])}}">Afficher</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-edit-lesson"
                        wire:click.prevent="editinglesson({{$lesson->id}})" href="#">Modifier</a>
                    <a class="dropdown-item" data-toggle="modal" data-target="#modal-delete-lesson"
                        wire:click.prevent="deletinglesson({{ $lesson->id }})" href="#">Supprimer</a>
                </div>
            </div> --}}
        </div>
        <div class="card-body d-flex flex-column">
            <div class="avatar mb-2">
                <span class="bg-soft-primary avatar-title rounded-circle text-center active">
                    <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink"
                        viewBox="0 0 40 40" width="22" height="22">
                        <g transform="matrix(1.6666666666666667,0,0,1.6666666666666667,0,0)">
                            <path
                                d="M12.619,8.412c-0.001-0.41-0.333-0.742-0.743-0.742H5.938c-0.41,0.015-0.73,0.36-0.715,0.77 c0.014,0.389,0.326,0.701,0.715,0.715h5.938C12.286,9.155,12.619,8.822,12.619,8.412L12.619,8.412z M9.586,19 c-0.02-0.115-0.119-0.199-0.236-0.2H3.464c-0.276,0-0.5-0.224-0.5-0.5V5.443c0.003-0.274,0.226-0.495,0.5-0.495h10.887 c0.272,0.003,0.491,0.223,0.494,0.495v4.039c-0.002,0.135,0.106,0.245,0.241,0.247c0.018,0,0.037-0.002,0.054-0.005 c0.807-0.152,1.623-0.249,2.443-0.29c0.131-0.007,0.232-0.116,0.231-0.247V3.464c0.001-0.82-0.663-1.484-1.483-1.485 c0,0-0.001,0-0.001,0h-3.957c-0.085,0-0.163-0.046-0.205-0.119C11.103,0.059,8.78-0.537,6.979,0.528 C6.43,0.853,5.972,1.311,5.647,1.86c-0.042,0.073-0.12,0.118-0.205,0.119H1.485C0.665,1.979,0,2.644,0,3.464c0,0,0,0,0,0v16.825 c0.001,0.82,0.665,1.484,1.485,1.484h8.847c0.135,0,0.244-0.109,0.244-0.244c0-0.046-0.013-0.092-0.038-0.131 C10.091,20.657,9.769,19.846,9.586,19z M11.035,12.523c0.286-0.376,0.604-0.726,0.951-1.046c0.085-0.079,0.028-0.343-0.11-0.343 H5.938c-0.41,0.015-0.73,0.36-0.715,0.77c0.014,0.389,0.326,0.701,0.715,0.715h4.907C10.92,12.619,10.99,12.583,11.035,12.523z M5.938,14.6c-0.41,0-0.742,0.331-0.743,0.741c0,0.41,0.331,0.742,0.741,0.743c0,0,0.001,0,0.001,0h3.37 c0.117,0,0.216-0.085,0.235-0.2c0.061-0.337,0.145-0.669,0.251-0.995c0.032-0.1,0.055-0.29-0.129-0.29L5.938,14.6z M17.32,10.639 c-3.69-0.001-6.681,2.99-6.682,6.68s2.99,6.681,6.68,6.682c3.69,0.001,6.681-2.99,6.682-6.68c0,0,0-0.001,0-0.001 C23.996,13.632,21.008,10.643,17.32,10.639z M17.32,22.021c-2.596,0-4.7-2.104-4.7-4.7s2.104-4.7,4.7-4.7s4.7,2.104,4.7,4.7 C22.017,19.915,19.914,22.018,17.32,22.021z M19.3,16.33h-0.742c-0.137,0-0.248-0.111-0.248-0.248v-1.237 c-0.017-0.546-0.474-0.975-1.021-0.958c-0.522,0.017-0.941,0.436-0.958,0.958v2.475c0,0.546,0.443,0.989,0.989,0.989 c0,0,0.001,0,0.001,0H19.3c0.546,0.017,1.004-0.412,1.021-0.958s-0.412-1.004-0.958-1.021C19.342,16.329,19.321,16.329,19.3,16.33z"
                                stroke="none" fill="currentColor" stroke-width="0" stroke-linecap="round"
                                stroke-linejoin="round"></path>
                        </g>
                    </svg>
                </span>
            </div>
            <a href="{{route('admin.lesson.show',['id'=>$lesson->id])}}" class="p-1 m-1 text-dark">
                <strong>{{$lesson->label}}</strong>
            </a>
            <p class=" p-1 m-1">{{$lesson->description}}</p>

            <div class=" p-1 m-1 d-flex justify-content-between align-items-center">
                <div>
                    <span>
                        <i class="material-icons color_date_range">date_range</i>
                    </span>
                    <small class=" text-muted">
                        {{$lesson->created_at->diffForHumans()}}
                    </small>
                </div>
            </div>
            <div class="border-top border-bottom p-1 m-1 d-flex justify-content-between align-items-center">
                <div class="border-right p-1">
                    <small class=" text-muted">
                        Contenu
                    </small>
                    <p class="h6">
                        {{count($lesson->chapters()->get())}}
                    </p>
                </div>
                <div class="p-1">
                    <small class=" text-muted">
                        Temps estimé du cours
                    </small>
                    <p class="h6">
                        {{$lesson->estimate_time}}
                    </p>
                </div>
            </div>
            <div class="col-md-12">
                {{-- @php
                $progress = 0;
                $users = \App\User::all();
                $nbr_chap_total = count($lesson->chapters()->get());
                $percent_array=[];
                foreach ($users as $user){
                $nbr_chap_user = count(\App\Progression::where('user_id', $user->id)->where('lesson_id',
                $lesson->id)->get());
                if($nbr_chap_total!=0){
                $user_percent = ($nbr_chap_user*100)/$nbr_chap_total;
                array_push($percent_array, $user_percent);
                $progress = 1;
                }else{
                $progress = 0;
                }


                }
                if($progress!=0){
                $sum = 0;
                $coeff = 0;
                foreach($percent_array as $percent){
                $sum +=$percent;
                $coeff++;
                }

                $progress = $sum/$coeff;
                $progress = ceil($progress);

                }




                @endphp --}}
                <h6 class="text-muted">
                    Progression {{$lesson->getGlobalProgress()}} %
                </h6>
                <div class="progress" style="height: 4px;">
                    <div class="progress-bar active" role="progressbar"
                        style="width: {{$lesson->getGlobalProgress()}}%;"
                        aria-valuenow="{{$lesson->getGlobalProgress()}}" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
        <!--  <div class="card-footer">
                        <div class="col-md-12">
                            <a href="" class="btn btn-success btn-block" data-toggle="modal" data-target="#modal-add-chapter">Ajouter chapitre <i class="material-icons">add</i></a>
                        </div>
                    </div> -->
    </div>
</div>
@endforeach
</div>
@endif
@if (session()->has('message'))


<div class="alert alert-success alert-dismissible fade show" role="alert"
    style="margin-top:30px; top:20px ; right:20px; left:0px width:50% ; position:absolute">
    <strong> {{ session('message') }}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif
<div class="mt-3 col-md-6 mx-auto">
    {{$lessons->links()}}
</div>

</div>








</div>