<div>
    <div class="container-fluid page__heading-container ">

        <div
            class="page__heading d-flex flex-column flex-md-row align-items-center justify-content-center justify-content-lg-between text-center text-lg-left">
            <h4 class="m-lg-0">{{$examens->label}}<a href="" data-toggle="modal" data-target="#modal-edit-exam"
                    wire:click.prevent="updatingExam({{$examens->id}})" class="h6"><span class="p-2 m-2"><i
                            class="fa fa-pen"></i></span></a></h4>
            <nav aria-label=" ">
                <ol class="breadcrumb mt-2">
                    <li class="breadcrumb-item"><a href="{{route('admin.examen')}}">Examens</a></li>
                    <li class="breadcrumb-item" aria-current="page">{{$classe->label}}</li>
                    <li class="breadcrumb-item" aria-current="page">{{$examens->subject->label}}</li>
                </ol>
            </nav>
        </div>

    </div>
    <div class="container-fluid page__container">

        <div class=" card-form">
            <div class="row no-gutters">
                {{--<div class="col-lg-4 card-body">--}}
                {{--<p><strong class="headings-color">Ajouter des questions au quiz</strong></p>--}}
                {{--<span>les champs marqués (<span class="text-danger">*</span>) sont obligatoires </span>--}}
                {{--</div>--}}
                <div class="col-lg-12 card-form__body ">

                    @php

                    $examen_questions = $examens->questions()->latest()->paginate(5);

                    @endphp
                    @foreach($examen_questions as $questions)
                    <div id="accordion{{$questions->id}}">
                        <div class="card card-perso card-raduis-perso">
                            <div class="row m-3">
                                <div class="col-md-12">
                                    <a href="" class="btn btn-sm text-white btn-warning mr-2 " data-toggle="modal"
                                        wire:click.prevent="editingExamQuestion({{$questions->id}})"
                                        data-target="#modal-edit-exam-question"> <span><i
                                                class="fa fa-pen"></i></span></a> <a data-toggle="modal"
                                        data-target="#modal-delete-exam-question"
                                        wire:click.prevent="deleting({{$questions->id}})" href=""
                                        class="btn btn-sm btn-danger"><span><i class="fa fa-trash"></i></span></a>
                                </div>
                            </div>
                            <div class="card-header collapse-header-bg py-0" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link text-dark d-block w-100" data-toggle="collapse"
                                        data-target="#collapseOne{{$questions->id}}" aria-expanded="true"
                                        aria-controls="collapseOne">
                                        <span class="float-left font-weight-bold">{!! $questions->label !!} </span> @php
                                        $file = $questions->getMedia();
                                        @endphp
                                        @if($questions->image !=0)

                                        @foreach($file as $fi)
                                        <img class="img-fluid rounded" style="max-height: 100px;"
                                            src="{{url($fi->getUrl() )}}" />
                                        @endforeach
                                        @endif

                                        <i style="font-size: 12px !important;"
                                            class="fa fa-plus-moins grey border-solid-3 p-2 rounded-circle float-right"
                                            aria-hidden="true"></i>
                                        {{-- <i class="float-right grey fa fa-image "></i> --}}
                                    </button>

                                </h5>
                            </div>

                            <div id="collapseOne{{$questions->id}}" class="collapse" aria-labelledby="headingOne"
                                data-parent="#accordion{{$questions->id}}">
                                <div class="card-body">
                                    @foreach($questions->answers as $answer)
                                    <div class="card">
                                        <div class="card-body pt-1 pb-3">
                                            <p> {!! $answer->label !!} @php

                                                $file = $answer->getMedia();
                                                @endphp
                                                @if($answer->image !=0)

                                                @foreach($file as $fi)
                                                <img src="{{url($fi->getUrl() )}}" class="img-fluid"
                                                    style="max-height: 100px;" />
                                                @endforeach
                                                @endif


                                                @if($answer->correct_answer == 1)

                                                <span class="float-right mr-1">

                                                    <i class="fas fa-check  text-success"></i>
                                                </span>
                                                @else
                                                <span class="float-right mr-1">

                                                    <i class="fas fa-times  text-danger"></i>
                                                </span>

                                                @endif
                                            </p>
                                            <a href="" class="btn btn-sm text-white btn-warning mr-2 "
                                                data-toggle="modal"
                                                wire:click.prevent="updatingOptionReponse({{$answer->id}})"
                                                data-target="#modal-exam-edit-option-reponse"> <span><i
                                                        class="fa fa-pen"></i></span></a> <a data-toggle="modal"
                                                data-target="#modal-delete-exam-reponse"
                                                wire:click.prevent="deletingOptionReponse({{$answer->id}})" href=""
                                                class="btn btn-sm btn-danger"><span><i
                                                        class="fa fa-trash"></i></span></a>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="text-right mb-3 ">
                                        <a href="" class="btn btn-sm btn-primary" data-toggle="modal"
                                            wire:click="addingOptionReponse({{$questions->id}})"
                                            data-target="#modal-add-exam-option-reponse">Ajouter Une option de réponse
                                            <span><i class="fa fa-plus"></i></span></a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                    @endforeach
                    <div class="mt-3 col-md-6 mx-auto">
                        {{$examen_questions->links()}}
                    </div>
                </div>

                @if(session()->has('message'))


                <div class="alert alert-success alert-dismissible fade show" role="alert"
                    style="margin-top:30px; top:20px ; right:20px; left:0px ;width:50% ; position:absolute">
                    <strong> {{ session('message') }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
            </div>

        </div>

        <div class="text-right mb-5 d-block text-center">
            <a href="" class="btn btn-primary" data-toggle="modal" wire:click="editExam({{ $examens->id }})"
                data-target="#modal-add-question-exam">Ajouter Question <span><i class="fa fa-plus"></i></span></a>
        </div>
    </div>
</div>