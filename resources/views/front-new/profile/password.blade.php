@extends('layouts.front-new')

@section('title', 'Course')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')

    <div class="home-content" style="margin-top: 1rem; margin-bottom: 2rem;">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-8">
            <h3 class="mb-0" style="font-size: 18px; color: #333;">Mot de passe</h3>
            <hr class="my-3">
            <form action="{{ route('front-new.home.profile.modify-password') }}" method="post" class="text-left">
              @csrf
              <div class="form-group">
                <input type="password" name="old_password" class="form-control" placeholder="Ancien mot de passe">
              </div>
              <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Nouveau mot de passe">
              </div>
              <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control"
                  placeholder="Confirmation nouveau mot de passe">
              </div>
              <button type="submit" class="btn btn-primary">Modifier</button>
            </form>
          </div>
          <div class=" col-12 col-md-4">
            @include('front-new.profile.partials.sidenavbar', ['active' => 'password'])
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.partials.footer')
</div>
@endsection

@section('js')

@endsection