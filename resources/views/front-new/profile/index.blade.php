@extends('layouts.front-new')

@section('title', 'Course')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')

    <div class="home-content" style="margin-top: 1rem; margin-bottom: 2rem;">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-8">
            <h3 class="mb-0" style="font-size: 18px; color: #333;">Informations générales</h3>
            <hr class="my-3">
            <form action="{{ route('front-new.home.profile.make') }}" method="post" class="text-left">
              @csrf
              <div class="form-group">
                <input type="text" name="name" value="{{ auth()->user()->name }}" class="form-control"
                  placeholder="Nom & prénoms*">
              </div>
              <div class="form-group">
                <input type="email" name="email" value="{{ auth()->user()->email }}" class="form-control"
                  placeholder="Email">
              </div>
              <div class="form-group">
                <input type="text" name="tel" class="form-control" value="{{ auth()->user()->tel }}"
                  placeholder="Téléphone*" disabled>
              </div>
              <div class="form-group">
                <select name="country" class="custom-select">
                  @foreach (get_country_list() as $country)
                  <option value="{{ $country }}" {{ $country == auth()->user()->country  ? "selected" : "" }}>
                    {{ $country }}</option>
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" name="city" value="{{ auth()->user()->city }}"
                  placeholder="Ville*">
              </div>
              <div class="form-group">
                <input type="text" name="classrom_id" class="form-control"
                  value="{{ auth()->user()->classroom->label }}" placeholder="Téléphone*" disabled>
              </div>
              <div class="form-group d-flex align-items-center">
                <div class="custom-control custom-radio mr-3">
                  <input type="radio" id="customRadio1" value="male" name="gender" class="custom-control-input"
                    {{ auth()->user()->gender  == "male" ? "checked" : "" }}>
                  <label class="custom-control-label" for="customRadio1">Homme
                  </label>
                </div>
                <div class="custom-control custom-radio">
                  <input value="female" type="radio" id="customRadio2" name="gender" class="custom-control-input"
                    {{ auth()->user()->gender == "female" ? "checked" : "" }}>
                  <label class="custom-control-label" for="customRadio2">Femme</label>
                </div>
              </div>
              <button type="submit" class="btn btn-primary">Modifier</button>
            </form>
          </div>
          <div class=" col-12 col-md-4">
            @include('front-new.profile.partials.sidenavbar', ['active' => 'infos'])
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.partials.footer')
</div>
@endsection

@section('js')

@endsection