<div class="list-group" style="font-size: 14px;">
  <a href="{{ route('front-new.home.profile') }}"
    class="list-group-item list-group-item-action {{ $active == "infos" ? "active" : "" }}">
    Informations générales
  </a>
  <a href="{{ route('front-new.home.profile.preference') }}"
    class="list-group-item list-group-item-action {{ $active == "pref" ? "active" : "" }}">Préférences</a>
  <a href="{{ route('front-new.home.profile.password') }}"
    class="list-group-item list-group-item-action {{ $active == "password" ? "active" : "" }}">Mot de
    passe</a>
</div>