@extends('layouts.front-new')

@section('title', 'Préférences')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')

    <div class="home-content" style="margin-top: 1rem; margin-bottom: 2rem;">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-8">
            <h3 class="mb-0" style="font-size: 18px; color: #333;">Préférences de matières</h3>
            <hr class="my-3">
            <form action="{{ route('front-new.home.profile.modify-preference') }}" method="post" class="text-left">
              @csrf
              @forelse ($subjects as $subject)
              <div class="form-group">
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <div class="custom-control custom-checkbox" style="padding-left: 17px;">
                        <input class="custom-control-input" id="customCheckbox{{ $subject->id }}" type="checkbox"
                          value="{{ $subject->id }}" name="subject_id[]"
                          {{ in_array($subject->id, $userPreferences) ? "checked" : "" }}>
                        <label class="custom-control-label" for="customCheckbox{{ $subject->id }}"
                          for="customCheckbox{{ $subject->id }}"></label>
                      </div>
                    </div>
                  </div>
                  <input type="text" class="form-control" value="{{ $subject->label }}" readonly>
                </div>
              </div>
              @empty
              <div class="col-lg-12">
                <div class="py-3 px-4 text-gray text-center mt-5">
                  <i style="font-weight: 700; font-size: 18px;" class="fa fa-book"></i> <br>
                  <p class="text-uppercase mt-2" style="font-weight: 600;">Aucune matière pour le moment</p>
                </div>
              </div>
              @endforelse
              <button type="submit" class="btn btn-primary">Modifier</button>
            </form>
          </div>
          <div class=" col-12 col-md-4">
            @include('front-new.profile.partials.sidenavbar', ['active' => 'pref'])
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.partials.footer')
</div>
@endsection

@section('js')

@endsection