@if($active == "course-page")
<div class="container">
  <div class="row">
    <div class="col-12">
      <nav class="nav breadcrumb-top mt-3">
        <a class="nav-link pr-2" href="{{ route('front-new.home') }}">Accueil</a>
        <span class="nav-link px-0">></span>
        <a class="nav-link px-2" href="{{ route('front-new.home.courses') }}">Cours</a>
        <span class="nav-link px-0">></span>
        <span class="nav-link text-primary font-weight-bold pl-2">{{ $chapter->label }}</span>
      </nav>
    </div>
  </div>
</div>
@endif

@if($active == "quiz-page")
<div style="border-top: 1px solid #e0e0e0;">
  <div class="container px-0">
    <div class="row">
      <div class="col-12">
        <nav class="nav breadcrumb-top mt-3">
          <a class="nav-link pr-2" href="{{ route('front-new.index') }}">Accueil</a>
          <span class="nav-link px-0">></span>
          <a class="nav-link px-2" href="{{ route('front-new.home.quizs') }}">Quiz</a>
          <span class="nav-link px-0">></span>
          <span class="nav-link text-primary pl-2 font-weight-bold">{{ $quiz->label }}</span>
        </nav>
      </div>
    </div>
  </div>
</div>
@endif


@if($active == "examen-page")
<div style="border-top: 1px solid #e0e0e0;">
  <div class="container px-0">
    <div class="row">
      <div class="col-12">
        <nav class="nav breadcrumb-top mt-3">
          <a class="nav-link pr-2" href="{{ route('front-new.index') }}">Accueil</a>
          <span class="nav-link px-0">></span>
          <a class="nav-link px-2" href="{{ route('front-new.home.examens') }}">Examens</a>
          <span class="nav-link px-0">></span>
          <span class="nav-link text-primary pl-2 font-weight-bold">{{ $exam->label }}</span>
        </nav>
      </div>
    </div>
  </div>
</div>
@endif

@if($active == "books-list")
<div style="border-top: 1px solid #e0e0e0;">
  <div class="container px-0">
    <div class="row">
      <div class="col-12">
        <nav class="nav breadcrumb-top mt-3">
          <a class="nav-link pr-2" href="{{ route('front-new.index') }}">Accueil</a>
          <span class="nav-link px-0">></span>
          <span class="nav-link text-primary pl-2 font-weight-bold">Cahiers d'activités</span>
        </nav>
      </div>
    </div>
  </div>
</div>
@endif