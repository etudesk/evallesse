<!-- Modal -->
<div class="modal fade" id="modalFormBooking" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header d-flex justify-content-start align-items-center flex-row">
        <div class="d-inline-flex align-items-center bg-primary text-white rounded-circle py-2 px-2 mr-2">
          <svg height="21" viewBox="0 0 16 16" class="bi bi-book-half" fill="currentColor"
            xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd"
              d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
          </svg>
        </div>
        <h5 class="modal-title" id="exampleModalLongTitle">Commander un cahier d'activité</h5>
      </div>
      <div class="modal-body">
        <form id="bookingForm" method="POST" action="{{ route('front-new.activity-booking') }}" class="text-left">
          @csrf

          <input id="fieldIdBook" type="hidden" name="idBook" />
          <div class="form-group">
            <input value="{{Auth::guest() ? '' : Auth::user()->name}}" name="name" type="text" class="form-control"
              placeholder="Nom complet" id="exampleInputName" aria-describedby="nameHelp" />
          </div>
          <div class="form-group">
            <input value="{{Auth::guest() ? '' : Auth::user()->email}}" name="email" type="email" class="form-control"
              placeholder="E-mail" id="exampleInputEmail1" aria-describedby="emailHelp" />
          </div>
          <div class="form-group">
            <input value="{{Auth::guest() ? '' : Auth::user()->tel}}" name="phone" type="text"
              placeholder="Numéro de téléphone" class="form-control" id="exampleInputPassword1">
          </div>
          <div class="form-group">
            <input name="quantity" value="1" type="number" placeholder="Nombre d'exemplaire" class="form-control"
              id="exampleInputPassword1">
          </div>
          <div class="form-group mb-0">
            <textarea name="observation" class="form-control" placeholder="Observation" id="exampleFormControlTextarea1"
              rows="3"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer py-3">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annuler</button>
        <button type="submit" form="bookingForm" class="btn btn-primary">Commander</button>
      </div>
    </div>
  </div>
</div>
{{-- <div class="modal" id="modalFormBooking" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div> --}}