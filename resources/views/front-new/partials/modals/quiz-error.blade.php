<!-- Modal -->
<div class="modal fade" id="modalQuizError" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header d-flex justify-content-center align-items-center flex-column border-bottom-0">
        <div class="d-inline-flex align-items-center bg-primary text-white rounded-circle py-3 px-3 mb-3">
          <x-heroicon-o-x height="40" />
          {{-- <x-zondicon-close height="40" /> --}}
        </div>
        <h5 class="modal-title" id="exampleModalLongTitle">Mauvaise réponse</h5>
      </div>
      <div class="modal-body pt-0 pb-4 text-center" id="exp">

      </div>
      <div class="modal-footer justify-content-center py-3">
        <button type="button" class="btn btn-info mr-0" data-dismiss="modal">Reprendre</button>
        <div class="nextPageAction">
        </div>
      </div>
    </div>
  </div>
</div>