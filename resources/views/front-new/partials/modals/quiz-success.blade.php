<!-- Modal -->
<div class="modal fade" id="modalQuizSuccess" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header d-flex justify-content-center align-items-center flex-column border-bottom-0">
        <div class="d-inline-flex align-items-center bg-success text-white rounded-circle py-3 px-3 mb-3">
          <x-heroicon-o-check height="40" />
        </div>
        <h5 class="modal-title" id="exampleModalLongTitle">Félicitations</h5>
      </div>
      <div class="modal-body pt-0 pb-4 text-center">
        <p class="mb-0">Bravo, votre réponse est correcte.</p>
      </div>
      <div class="modal-footer justify-content-center py-3 nextPageAction2">
        {{-- <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annuler</button> --}}
        {{-- <a href="{{ route('front-new.home.quizs.show', ['slug' => 'limite-continuite-courbe', 'quiz_id' => 2]) }}"
        class="btn btn-block btn-success mx-5">Question suivante</a> --}}
      </div>
    </div>
  </div>
</div>