<!-- Modal -->
<div class="modal fade" id="modalRetakeExam" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header d-flex justify-content-start align-items-center flex-row">
        <div class="d-inline-flex align-items-center bg-primary text-white rounded-circle py-2 px-2 mr-2">
          <svg width="1.0625em" height="1em" viewBox="0 0 17 16" class="bi bi-exclamation-triangle-fill"
            fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd"
              d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 5zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
          </svg>
        </div>
        <h5 class="modal-title" id="exampleModalLongTitle">Reprendre l'examen</h5>
      </div>
      <div class="modal-body">
        <p class="mb-0">Vous vous apprêtez à reprendre l'exemple <strong>
            Limite & continuité - Courbe</strong>. En êtes-vous sûr ?</p>
      </div>
      <div class="modal-footer py-3">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annuler</button>
        <button type="button" class="btn btn-primary">Reprendre</button>
      </div>
    </div>
  </div>
</div>