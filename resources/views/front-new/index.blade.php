@extends('layouts.front-new')

@section('title', 'Accueil')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.primary-navbar')

    <div class="home-block">
      <div class="container">
        <div class="row">
          <div class="col-12 col-lg-6">
            <h1 class="home-text-header">Je prépare mes examens avec des contenus de qualités</h1>
            <p class="mt-3">Trouve des résumés de cours simplifiés et détaillés, suivi de quiz et d'examens conçus par
              des professeurs certifiés pour réussir vos examens de fins de cycles. Quelques matières :</p>
            <div class="home-tag mb-3">
              <button type="button" class="btn btn-cta btn-outline-info mr-2 mb-2">Mathématiques</button>
              <button type="button" class="btn btn-cta btn-outline-info mr-2 mb-2">Physique - Chimie</button>
              {{-- <button type="button" class="btn btn-cta btn-outline-info mr-2 mb-2">Anglais</button>
              <button type="button" class="btn btn-cta btn-outline-info mr-2 mb-2">Français</button> --}}
              <button type="button" class="btn btn-cta btn-outline-info mr-2 mb-2">SVT</button>
            </div>
            <a class="btn-link" href="{{ route('front-new.sign-up') }}" style="font-weight: 400;">Je m'inscris
              maintenant</a>
            <div class="mt-3">
              {{-- <a href="{{ asset('evallesse.apk') }}"><img
                src="{{ asset('images/disponible-sur-Google-play.png') }}" height="40" alt="google play"></a> --}}
              <a class="btn btn-info" href="{{ asset('evallesse.apk') }}"><i
                  class="bi bi-download mr-2" style="font-size: 16px;"></i><span>Télécharger l'application mobile</span></a>
            </div>
          </div>

          <div class="col-12 col-lg-6 main-cover d-none d-lg-block"></div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection