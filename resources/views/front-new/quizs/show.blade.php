@extends('layouts.front-new')

@section('title', 'Quiz')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')

    @include('front-new.partials.breadcrumb', ['active' => 'quiz-page'])

    <livewire:front.quiz.index :quiz="$quiz" :user="$user" />
  </div>

  @include('layouts.partials.footer')
  @include('front-new.partials.modals.quiz-success')
  @include('front-new.partials.modals.quiz-error')
</div>
@endsection

@section('js')
<script>
  window.livewire.on('bad_answers', (explanations, hasMorePages) => {

    $("#exp").html(" ");

    $.each(explanations, function( index, value ) {
      $("#exp").append('<p class="mb-0">'+ value + '</p>');
    });

    if (hasMorePages == true) {
      $(".nextPageAction").html(" ");
      $(".nextPageAction").append('<a href="javascript:void(0)" class="btn btn-outline-secondary ml-3 next-question">Question suivante</a>');
    } else {
      $(".nextPageAction").html(" ");
    }

    $('#modalQuizError').modal('show');

    if (hasMorePages == true) {
      $(".next-question").click(function(e) {
        window.livewire.emit('nextPage');
        $('#modalQuizError').modal('hide');
      });
    }
  });

  window.livewire.on('good_answers', hasMorePages => {
    if (hasMorePages == true) {
      $(".nextPageAction2").html(" ");
      $(".nextPageAction2").append('<a href="javascript:void(0)" class="btn btn-outline-secondary next-question">Question suivante</a>');
    } else {
      $(".nextPageAction2").html(" ");
      $(".nextPageAction2").append(`<a href="{{ route('front-new.home.quizs') }}" class="btn btn-primary next-question">Voir tous les quiz</a>`);
    }

    $('#modalQuizSuccess').modal('show');

    if (hasMorePages == true) {
      $(".next-question").click(function(e) {
        window.livewire.emit('nextPage');
        $('#modalQuizSuccess').modal('hide');
      });
    }
  });
</script>
@endsection