@extends('layouts.front-new')

@section('title', 'Quiz')

@section('css')

@endsection

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')

    {{-- @include('front-new.partials.breadcrumb', ['active' => 'courses-list']) --}}

    <div class="home-content" style="margin-top: 2rem; margin-bottom: 2rem;">
      <div class="container">
        <div class="row">
          {{-- <div class="col-12 col-lg-2">
            <h2 class="listing-page-title mb-3">Filtrer par</h2>
            <div class="subject-filter-bloc">
              <h3 class="home-el-title" style="margin-bottom: 8px;">Matières</h3>
              <div class="custom-control custom-checkbox mb-1">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label text-break" for="customCheck1"
                  style="margin-top: 2px;">Mathématique</label>
              </div>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customCheck2">
                <label class="custom-control-label text-break" for="customCheck2"
                  style="margin-top: 2px;">Physique-Chimie</label>
              </div>
            </div>
          </div> --}}

          <div class="col-12 col-lg-12">
            <h2 class="listing-page-title mb-3 mt-4 mt-lg-0">Quiz</h2>

            <div class="row mb-4">
              <div class="col-12 col-lg-9">
                <form action="" class="filter">
                  <div class="input-group mb-3">
                    <input type="text" name="search" class="form-control" placeholder="Rechercher ..." aria-label="Rechercher ..." value="{{ $search }}" aria-describedby="button-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="submit" id="button-addon2"><i class="bi bi-search"></i></button>
                    </div>
                  </div>
                  <input type="hidden" name="subject" value="{{ $subject }}">
                </form>
              </div>
              <div class="col-12 col-lg-3">
                <select name="subjects" class="custom-select">
                  @forelse ($subjects as $s)
                  <option value="{{ $s->id }}" {{ $s->id == $subject ? "selected" : "" }}>{{ $s->label }}</option>
                  @empty
                  <option>Aucune matière disponible</option>
                  @endforelse
                </select>
              </div>
            </div>

            <div class="row">
              @forelse ($quizzes as $quiz)
              <div class="col-12 col-md-6 col-lg-4 mb-3">
                <div class="el-card py-3 px-3">
                  <span class="d-inline-flex align-items-center bg-info text-white rounded-circle py-2 px-2">
                    <x-heroicon-s-question-mark-circle height="20" />
                  </span> <br>
                  <a data-label="{{ $quiz->label }}"
                    data-url="{{ route('front-new.home.quizs.show', ['slug' => \Str::slug($quiz->label), 'quiz_id' => $quiz->id]) }}"
                    data-toggle="modal" data-target="#modalTakeQuiz" href="javascript:void(0)"
                    class="d-inline-block el-card-title mt-3 text-info begin-quiz">{{ $quiz->label }}</a>
                  <br>
                  <div class="d-inline-flex align-items-center mt-2 mb-3 marker">
                    {{-- <x-heroicon-o-bookmark class="mr-2" height="20" /> --}}
                    <svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" fill="currentColor"
                      stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                      class="feather feather-bar-chart-2 mr-2">
                      <line x1="18" y1="20" x2="18" y2="10" />
                      <line x1="12" y1="20" x2="12" y2="4" />
                      <line x1="6" y1="20" x2="6" y2="14" /></svg>
                    <span
                      class="mt-1">{{ $quiz->level == "easy" ? "Facile" : ($quiz->level == "medium" ? "Moyen" : "Difficile") }}</span>
                  </div>
                  <div class="descritpion" style="font-size: 14px;">
                    <p class="mb-3">{{ \Str::limit($quiz->description, 50) }}</p>
                  </div>

                  <div style="line-height: 0.8; margin-bottom: 1rem;">
                    <label style="font-size: 14px; font-weight: 500;">Nombre de question</label><br>
                    <span style="color: #6c757d; font-size: 15px;">{{ $quiz->questions()->count() }}</span>
                  </div>

                  @if(auth()->user()->computeQuizGoodResponse($quiz->id) == 0)
                  <a class="btn btn-sm d-block btn-info begin-quiz" data-label="{{ $quiz->label }}"
                    data-url="{{ route('front-new.home.quizs.show', ['slug' => \Str::slug($quiz->label), 'quiz_id' => $quiz->id]) }}"
                    data-toggle="modal" data-target="#modalTakeQuiz" href="javascript:void(0)">Participer</a>
                  @else
                  <a class="btn btn-sm d-block btn-success begin-quiz" data-label="{{ $quiz->label }}"
                    data-url="{{ route('front-new.home.quizs.show', ['slug' => \Str::slug($quiz->label), 'quiz_id' => $quiz->id]) }}"
                    data-toggle="modal" data-target="#modalTakeQuiz" href="javascript:void(0)">Continuer</a>
                  @endif
                  <div class="d-flex align-items-end justify-content-between">
                    @if(auth()->user()->computeQuizGoodResponse($quiz->id) == 0)
                    <div class="invisible d-inline-flex">
                      <x-heroicon-s-check-circle class="text-success" height="23" />
                    </div>
                    @else
                    <div class="visible d-inline-flex">
                      <span class="text-success"
                        style="font-size: 12px;">{{ auth()->user()->computeQuizGoodResponse($quiz->id) }}/{{ $quiz->questions()->count() }}</span>
                    </div>
                    @endif
                    <div class="d-inline-flex align-items-center mt-3 marker" style="font-size: 12px;">
                      <svg height="20" viewBox="0 0 16 16" class="bi bi-book-half" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                          d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                      </svg>
                      <span class="ml-2">{{ $quiz->subject->label }}</span>
                    </div>
                  </div>
                </div>
              </div>
              @empty
              <div class="col-12">
                <div class="no-data d-flex align-items-center justify-content-center p-5">
                  <p class="text-secondary"><i>Aucun quiz de disponible pour le moment</i></p>
                </div>
              </div>
              @endforelse

              {{-- <div class="col-12 col-md-6 col-lg-4 mb-3">
                <div class="el-card py-3 px-3">
                  <span class="d-inline-flex align-items-center bg-info text-white rounded-circle py-2 px-2">
                    <x-heroicon-s-question-mark-circle height="20" />
                  </span> <br>
                  <a href="#" class="d-inline-block el-card-title mt-3 text-info">Fonction</a>
                  <br>
                  <div class="d-inline-flex align-items-center mt-2 mb-3 marker">
                    <svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" fill="currentColor"
                      stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                      class="feather feather-bar-chart-2 mr-2">
                      <line x1="18" y1="20" x2="18" y2="10" />
                      <line x1="12" y1="20" x2="12" y2="4" />
                      <line x1="6" y1="20" x2="6" y2="14" /></svg>
                    <span class="mt-1">Difficile</span>
                  </div>
                  <div class="descritpion" style="font-size: 14px;">
                    <p class="mb-3">Lorem ipsum dolor sit amet, ipsum. Lorem dolor sit amet...</p>
                  </div>

                  <div style="line-height: 0.8; margin-bottom: 1rem;">
                    <label style="font-size: 14px; font-weight: 500;">Nombre de question</label><br>
                    <span style="color: #6c757d; font-size: 15px;">22</span>
                  </div>

                  <a class="btn btn-sm d-block btn-success" data-toggle="modal" data-target="#modalTakeQuiz"
                    href="javascript:void(0)">Continuer</a>

                  <div class="d-flex align-items-end justify-content-between">
                    <div class="visible d-inline-flex">
                      <span class="text-success" style="font-size: 12px;">10 Répondus</span>
                    </div>
                    <div class="d-inline-flex align-items-center mt-3 marker" style="font-size: 12px;">
                      <svg height="20" viewBox="0 0 16 16" class="bi bi-book-half" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                          d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                      </svg>
                      <span class="ml-2">Mathématique</span>
                    </div>
                  </div>
                </div>
              </div> --}}
            </div>
            <div class="row">
              <div class="col-12">
                {{ $quizzes->links() }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.partials.footer')
  @include('front-new.partials.modals.take-quiz')
</div>
@endsection

@section('js')
<script>
  $('.begin-quiz').click(function() {
    let $this = $(this);
    $("#modalTakeQuiz #q-label").html($this.data('label'));
    $("#modalTakeQuiz .bQAction").attr('href', $this.data('url'));
  });

  $('[name="subjects"]').on('change', function(e) {
    $('[name="subject"]').val($(this).val());
    $('.filter').submit();
  });
</script>
@endsection