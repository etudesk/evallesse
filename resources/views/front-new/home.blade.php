@extends('layouts.front-new')

@section('title', 'Accueil')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')

    <div class="home-content" style="margin-top: 2rem; margin-bottom: 2rem;">
      <div class="courses mb-3">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <h2 class="home-el-title mb-4">Statistiques</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-12 mb-3">
              <div class="d-flex align-items-center mb-1" style="font-weight: 500;">
                <a href="{{ route('front-new.home.courses') }}" class="d-flex justify-content-start mr-4 text-info" style="line-height: 1;"><span class="mr-2">Suivre des leçons</span><i class="bi bi-arrow-right-circle" style="font-size: 15px;"></i></a>
                <a href="{{ route('front-new.home.quizs') }}" class="d-flex justify-content-start mr-4 text-info" style="line-height: 1;"><span class="mr-2">Passer des quizs</span><i class="bi bi-arrow-right-circle" style="font-size: 15px;"></i></a>
                <a href="{{ route('front-new.home.examens') }}" class="d-flex justify-content-start mr-4 text-info" style="line-height: 1;"><span class="mr-2">Passer des examens</span><i class="bi bi-arrow-right-circle" style="font-size: 15px;"></i></a>
              </div>
            </div>
            @forelse ($subjects as $subject)
            <div class="col-12 col-md-6 mb-3">
              <div class="el-card py-2 px-3">
                <h3 class="stat-subject-title text-primary mb-3 mt-1">{{ $subject->label }}</h3>
                <div class="d-flex align-items-center mb-4">
                  <div class="d-inline-flex align-items-center mr-3">
                    <span class="d-inline-flex align-items-center bg-primary text-white rounded-circle py-2 px-2 mr-2">
                      <svg height="16" viewBox="0 0 16 16" class="bi bi-book-half" fill="currentColor"
                        xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd"
                          d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                      </svg>
                    </span>
                    <span class="" style="line-height: 1;">{{ $subject->lessons()->count() }} Leçon{{$subject->lessons()->count() > 1 ? "s" : ""}}</span>
                  </div>
                  <div class="d-inline-flex align-items-center mr-3">
                    <span class="d-inline-flex align-items-center bg-primary text-white rounded-circle py-2 px-2 mr-2">
                      <x-heroicon-s-question-mark-circle height="17" />
                    </span>
                    <span class="" style="line-height: 1;">{{ $subject->quizs()->count() }} Quiz{{$subject->quizs()->count() > 1 ? "s" : ""}}</span>
                  </div>
                  <div class="d-inline-flex align-items-center mr-3">
                    <span class="d-inline-flex align-items-center bg-primary text-white rounded-circle py-2 px-2 mr-2">
                      <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" height="17" viewBox="0 0 20 20">
                        <path
                          d="M3.302 12.238c.464 1.879 1.054 2.701 3.022 3.562 1.969.86 2.904 1.8 3.676 1.8.771 0 1.648-.822 3.616-1.684 1.969-.861 1.443-1.123 1.907-3.002L10 15.6l-6.698-3.362zm16.209-4.902l-8.325-4.662c-.652-.365-1.72-.365-2.372 0L.488 7.336c-.652.365-.652.963 0 1.328l8.325 4.662c.652.365 1.72.365 2.372 0l5.382-3.014-5.836-1.367a3.09 3.09 0 0 1-.731.086c-1.052 0-1.904-.506-1.904-1.131 0-.627.853-1.133 1.904-1.133.816 0 1.51.307 1.78.734l6.182 2.029 1.549-.867c.651-.364.651-.962 0-1.327zm-2.544 8.834c-.065.385 1.283 1.018 1.411-.107.579-5.072-.416-6.531-.416-6.531l-1.395.781c0-.001 1.183 1.125.4 5.857z" />
                      </svg>
                    </span>
                    <span class="" style="line-height: 1;">{{ $subject->exams()->count() }} Exam{{$subject->exams()->count() > 1 ? "s" : ""}}</span>
                  </div>
                </div>
                <div class="mb-3">
                  <div class="mb-2">
                    <label style="font-size: 14px; font-weight: 500;">Progression des leçons <span class="font-weight-bold">{{ $nbFLesson = $user->getFollowLessonNbBy($subject->lessons()->pluck('id')->toArray()) }}/{{ $nbLesson = $subject->lessons()->count() }}</span></label>
                    <div class="progress mb-1" style="height: 3px;">
                      <div class="progress-bar bg-info" role="progressbar"
                        style="width: {{ $lessonPercent = ceil($nbFLesson / ($nbLesson == 0 ? 1 : $nbLesson) * 100) }}%;"
                        aria-valuenow="{{ $lessonPercent }}" aria-valuemin="0"
                        aria-valuemax="100"></div>
                    </div>
                  </div>
                  <div class="mb-2">
                    <label style="font-size: 14px; font-weight: 500;">Progression des quizs <span class="font-weight-bold">{{ $nbFQuiz = $user->getFollowQuizNbBy($subject->quizs()->pluck('id')->toArray()) }}/{{ $nbQuiz = $subject->quizs()->count() }}</span></label>
                    <div class="progress mb-1" style="height: 3px;">
                      <div class="progress-bar bg-info" role="progressbar"
                        style="width: {{ $quizPercent = ceil($nbFQuiz / ($nbQuiz == 0 ? 1 : $nbQuiz) * 100) }}%;"
                        aria-valuenow="{{ $quizPercent }}" aria-valuemin="0"
                        aria-valuemax="100"></div>
                    </div>
                  </div>
                  <div>
                    <label style="font-size: 14px; font-weight: 500;">Progression des examens <span class="font-weight-bold">{{ $nbFExam = $user->getFollowExamNbBy($subject->exams()->pluck('id')->toArray()) }}/{{ $nbExam = $subject->exams()->count() }}</span></label>
                    <div class="progress mb-1" style="height: 3px;">
                      <div class="progress-bar bg-info" role="progressbar"
                        style="width: {{ $examPercent = ceil($nbFExam / ($nbExam == 0 ? 1 : $nbExam) * 100) }}%;"
                        aria-valuenow="{{ $examPercent }}" aria-valuemin="0"
                        aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
                {{-- <div class="d-flex align-items-center mb-1">
                  <a href="{{ route('front-new.home.courses') }}" class="d-flex justify-content-start mr-3 text-info" style="line-height: 1;"><span class="mr-2">Suivre des leçons</span><i class="bi bi-arrow-right-circle" style="font-size: 15px;"></i></a>
                  <a href="{{ route('front-new.home.quizs') }}" class="d-flex justify-content-start mr-3 text-info" style="line-height: 1;"><span class="mr-2">Passer des quizs</span><i class="bi bi-arrow-right-circle" style="font-size: 15px;"></i></a>
                  <a href="{{ route('front-new.home.examens') }}" class="d-flex justify-content-start mr-3 text-info" style="line-height: 1;"><span class="mr-2">Passer des examens</span><i class="bi bi-arrow-right-circle" style="font-size: 15px;"></i></a>
                </div> --}}
              </div>
            </div>
            @empty
            <div class="col-12">
              <div class="no-data d-flex align-items-center justify-content-center p-5">
                <p class="text-secondary"><i>Vous ne suivez aucun cours pour le moment.</i></p>
              </div>
            </div>
            @endforelse
          </div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.partials.footer')
  @include('front-new.partials.modals.retake-an-exam')
  @include('front-new.partials.modals.take-quiz')
</div>
@endsection

@section('js')
<script>
  $('.begin-quiz').click(function() {
    let $this = $(this);
    $("#modalTakeQuiz #q-label").html($this.data('label'));
    $("#modalTakeQuiz .bQAction").attr('href', $this.data('url'));
  });
</script>
@endsection