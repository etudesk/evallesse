@extends('layouts.front-new')

@section('title', 'Examen')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')
    @include('front-new.partials.breadcrumb', ['active' => 'examen-page'])

    <livewire:front.exam.index :exam="$exam" :user="$user" />

  </div>

  @include('layouts.partials.footer')
</div>
@endsection

@section('js')
@endsection