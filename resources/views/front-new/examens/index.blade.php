@extends('layouts.front-new')

@section('title', 'Examen')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')

    {{-- @include('front-new.partials.breadcrumb', ['active' => 'courses-list']) --}}

    <div class="home-content" style="margin-top: 2rem; margin-bottom: 2rem;">
      <div class="container">
        <div class="row">
          {{-- <div class="col-12 col-lg-2">
            <h2 class="listing-page-title mb-3">Filtrer par</h2>
            <div class="subject-filter-bloc">
              <h3 class="home-el-title" style="margin-bottom: 8px;">Matières</h3>
              <div class="custom-control custom-checkbox mb-1">
                <input type="checkbox" class="custom-control-input" id="customCheck1">
                <label class="custom-control-label text-break" for="customCheck1"
                  style="margin-top: 2px;">Mathématique</label>
              </div>
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="customCheck2">
                <label class="custom-control-label text-break" for="customCheck2"
                  style="margin-top: 2px;">Physique-Chimie</label>
              </div>
            </div>
          </div> --}}

          <div class="col-12 col-lg-12">
            <h2 class="listing-page-title mb-3 mt-4 mt-lg-0">Examens</h2>

            <div class="row mb-4">
              <div class="col-12 col-lg-9">
                <form action="" class="filter">
                  <div class="input-group mb-3">
                    <input type="text" name="search" class="form-control" placeholder="Rechercher ..." aria-label="Rechercher ..." value="{{ $search }}" aria-describedby="button-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="submit" id="button-addon2"><i class="bi bi-search"></i></button>
                    </div>
                  </div>
                  <input type="hidden" name="subject" value="{{ $subject }}">
                </form>
              </div>
              <div class="col-12 col-lg-3">
                <select name="subjects" class="custom-select">
                  @forelse ($subjects as $s)
                  <option value="{{ $s->id }}" {{ $s->id == $subject ? "selected" : "" }}>{{ $s->label }}</option>
                  @empty
                  <option>Aucune matière disponible</option>
                  @endforelse
                </select>
              </div>
            </div>

            <div class="row">
              @forelse ($exams as $exam)
              <div class="col-12 col-md-6 col-lg-4 mb-3">
                <div class="el-card py-3 px-3">
                  <span class="d-inline-flex align-items-center bg-info text-white rounded-circle py-2 px-2">
                    <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 20 20">
                      <path
                        d="M3.302 12.238c.464 1.879 1.054 2.701 3.022 3.562 1.969.86 2.904 1.8 3.676 1.8.771 0 1.648-.822 3.616-1.684 1.969-.861 1.443-1.123 1.907-3.002L10 15.6l-6.698-3.362zm16.209-4.902l-8.325-4.662c-.652-.365-1.72-.365-2.372 0L.488 7.336c-.652.365-.652.963 0 1.328l8.325 4.662c.652.365 1.72.365 2.372 0l5.382-3.014-5.836-1.367a3.09 3.09 0 0 1-.731.086c-1.052 0-1.904-.506-1.904-1.131 0-.627.853-1.133 1.904-1.133.816 0 1.51.307 1.78.734l6.182 2.029 1.549-.867c.651-.364.651-.962 0-1.327zm-2.544 8.834c-.065.385 1.283 1.018 1.411-.107.579-5.072-.416-6.531-.416-6.531l-1.395.781c0-.001 1.183 1.125.4 5.857z" />
                    </svg>
                  </span> <br>
                  <a href="javascript:void(0)" data-label="{{ $exam->label }}"
                    data-url="{{ route('front-new.home.examens.show', ['slug' => \Str::slug($exam->label), 'examen_id' => $exam->id]) }}"
                    class="d-inline-block el-card-title mt-3 text-info begin-exam" data-toggle="modal"
                    data-target="#modalTakeExam">{{ $exam->label }}</a>
                  <br>
                  <div class="d-inline-flex align-items-center mt-1 mb-3 marker">
                    <svg height="20" viewBox="0 0 16 16" class="bi bi-book-half mr-2" fill="currentColor"
                      xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd"
                        d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                    </svg>
                    <span class="mt-1">{{ $exam->subject->label }}</span>
                  </div>
                  <div class="descritpion" style="font-size: 14px;">
                    <p class="mb-3">{{ \Str::limit($exam->description, 50) }}</p>
                  </div>

                  {{-- <div style="line-height: 0.8; margin-bottom: 1rem;">
                    <label style="font-size: 14px; font-weight: 500;">Nombre de question</label><br>
                    <span style="color: #6c757d; font-size: 15px;">14</span>
                  </div> --}}

                  <a class="btn btn-sm d-block btn-info begin-exam" data-label="{{ $exam->label }}"
                    data-url="{{ route('front-new.home.examens.show', ['slug' => \Str::slug($exam->label), 'examen_id' => $exam->id]) }}"
                    data-toggle="modal" data-target="#modalTakeExam" href="javascript:void(0)"
                    style="margin-top: 42px;">Passer</a>
                </div>
              </div>
              @empty
              <div class="col-12">
                <div class="no-data d-flex align-items-center justify-content-center p-5">
                  <p class="text-secondary"><i>Aucun examen de disponible pour le moment</i></p>
                </div>
              </div>
              @endforelse


              {{-- <div class="col-12 col-md-6 col-lg-4 mb-3">
                <div class="el-card py-3 px-3">
                  <span class="d-inline-flex align-items-center bg-info text-white rounded-circle py-2 px-2">
                    <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 20 20">
                      <path
                        d="M3.302 12.238c.464 1.879 1.054 2.701 3.022 3.562 1.969.86 2.904 1.8 3.676 1.8.771 0 1.648-.822 3.616-1.684 1.969-.861 1.443-1.123 1.907-3.002L10 15.6l-6.698-3.362zm16.209-4.902l-8.325-4.662c-.652-.365-1.72-.365-2.372 0L.488 7.336c-.652.365-.652.963 0 1.328l8.325 4.662c.652.365 1.72.365 2.372 0l5.382-3.014-5.836-1.367a3.09 3.09 0 0 1-.731.086c-1.052 0-1.904-.506-1.904-1.131 0-.627.853-1.133 1.904-1.133.816 0 1.51.307 1.78.734l6.182 2.029 1.549-.867c.651-.364.651-.962 0-1.327zm-2.544 8.834c-.065.385 1.283 1.018 1.411-.107.579-5.072-.416-6.531-.416-6.531l-1.395.781c0-.001 1.183 1.125.4 5.857z" />
                    </svg>
                  </span> <br>
                  <a href="#" class="d-inline-block el-card-title mt-3 text-info">BAC Blanc 2000</a>
                  <br>
                  <div class="d-inline-flex align-items-center mt-1 mb-3 marker">
                    <svg height="20" viewBox="0 0 16 16" class="bi bi-book-half mr-2" fill="currentColor"
                      xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd"
                        d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                    </svg>
                    <span class="mt-1">Mathématique</span>
                  </div>
                  <div class="descritpion" style="font-size: 14px;">
                    <p class="mb-1">Lorem ipsum dolor sit amet, consectetur. Lorem dolor sit amet...</p>
                  </div>

                  <div class="d-flex align-items-center justify-content-between mb-2">
                    <div class="d-flex text-success">
                      <label class="mb-0" style="font-size: 12px; font-weight: 500;">Note:</label>
                      <span class="ml-1 text-success" style="font-size: 12px; font-weight: 500;">15/20</span>
                    </div>

                    <a class="btn-link btn-sm text-info" href="#">Reprendre</a>
                  </div>

                  <a class="btn btn-sm d-block btn-success" href="#">Voir</a>
                </div>
              </div> --}}

            </div>
            <div class="row">
              <div class="col-12">
                {{ $exams->links() }}
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>

  @include('layouts.partials.footer')
  @include('front-new.partials.modals.take-exam')
</div>
@endsection

@section('js')
<script>
  $('.begin-exam').click(function() {
    let $this = $(this);
    $("#modalTakeExam #e-label").html($this.data('label'));
    $("#modalTakeExam .bEAction").attr('href', $this.data('url'));
  });

  $('[name="subjects"]').on('change', function(e) {
    $('[name="subject"]').val($(this).val());
    $('.filter').submit();
  });
</script>
@endsection