@extends('layouts.front-new')

@section('title', 'Examen Correction')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')
    @include('front-new.partials.breadcrumb', ['active' => 'examen-page'])

    <livewire:front.exam.correction :exam="$exam" :user="$user" />

  </div>

  @include('layouts.partials.footer')
</div>
@endsection

@section('js')
@endsection