@extends('layouts.front-new')

@section('title', 'Cahiers d\'activités')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.secondary-navbar')

    @include('front-new.partials.breadcrumb', ['active' => 'books-list'])



    <div class="home-content" style="margin-top: 2rem; margin-bottom: 2rem;">
      <div class="container">

        <div class="row">
          {{-- <div class="col-12 col-lg-2">
            <h2 class="listing-page-title mb-3">Filtrer par</h2>
            <div class="subject-filter-bloc">
              <h3 class="home-el-title" style="margin-bottom: 8px;">Matières</h3>

              <form id="filterForm" method="GET">
                @foreach ($subjects as $subject)
                <div class="custom-control custom-checkbox mb-1">
                  <input name="m" type="checkbox" class="custom-control-input" id="customCheck{{$subject->id}}">
          <label class="custom-control-label text-break" for="customCheck{{$subject->id}}"
            style="margin-top: 2px;">{{$subject->label}}</label>
        </div>
        @endforeach
        </form>
        <button form="filterForm" class="btn btn-sm btn-info mt-3 d-block" type="submit">Appliquer</button>
      </div>
    </div> --}}
    <div class="col-12 col-lg-12">

      @if (\Session::has('success'))
      <div class="alert alert-success alert-dismissible fade show" role="alert">
        {!! \Session::get('success') !!}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif


      @if (\Session::has('error'))
      <div class="alert alert-danger alert-dismissible fade show" role="alert">

        {!! \Session::get('error') !!}

        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @endif
      <h2 class="listing-page-title mb-3 mt-4 mt-lg-0">Cahiers d'activités</h2>

      <div class="row">

        @forelse ($books as $book)

        <div class="col-12 col-md-6 col-lg-3 mb-3">
          <div class="el-card pb-3" style="overflow: hidden;">
            <div class="cover-image text-center">
              <img class="img-fluid" src="{{ $book->getFirstMedia()->getUrl() }}" alt="cover" style="height: 330px;">
            </div>
            <div class="book-desc px-3 pt-3">
              <h2 class="price text-primary mb-1">{{$book->price}} FCFA</h2>
              <h1 class="title mb-2">{{$book->title}}</h1>
              <p class="desc mb-1"
                style=" overflow: hidden; text-overflow: ellipsis; display: -webkit-box;  -webkit-line-clamp: 1;  -webkit-box-orient: vertical;">
                <i>{{$book->description}}</i></p>
              <span>{{$book->page_number}} page{{$book->page_number>1?"s":""}}</span> <br>
              <a class="btn btn-sm btn-info mt-3 d-block open-AddBookDialog" data-price="{{$book->price}}"
                data-id="{{$book->id}}" data-title="{{$book->title}}" data-toggle="modal"
                data-target="#modalFormBooking" href="#modalFormBooking">Commander</a>
            </div>
          </div>
        </div>
        @empty
        <div class="col-12">
          <div class="no-data d-flex align-items-center justify-content-center p-5">
            <p class="text-secondary"><i>Aucun cahier d'activité de disponible pour le moment</i></p>
          </div>
        </div>
        @endforelse
      </div>
      <div class="row">
        <div class="col-12">
          {{ $books->links() }}
        </div>
      </div>
    </div>

  </div>
</div>
</div>
</div>
</div>

@include('layouts.partials.footer')
@include('front-new.partials.modals.booking')
</div>
@endsection