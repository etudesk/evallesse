@extends('layouts.front-new')

@section('title', 'Course')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')

    @include('front-new.partials.breadcrumb', ['active' => 'course-page', 'chapter' => $chapter])

    <div class="home-content" style="margin-top: 1rem; margin-bottom: 2rem;">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div style="height: 40rem;">
              <div id="pdf" class="mx-auto h-100"></div>
            </div>
            <div class="mt-2">
              {!! $lesson->content !!}
            </div>
            <form action="{{ route('front-new.home.courses.computeProgress') }}" method="post">
              @csrf
              <div class="d-flex flex-row align-items-center justify-content-between mt-3">
                <div class="custom-control custom-checkbox mb-1">
                  <input type="hidden" name="c_id" value="{{ $lesson->id }}">
                  <input type="hidden" name="l_id" value="{{ $chapter->id }}">
                  <input name="valid" value="1" type="checkbox" class="custom-control-input" id="customCheck1"
                    {{ $lesson->isValidateL($user, $chapter->id) ? 'checked' : ''}}>
                  <label class="custom-control-label text-break" for="customCheck1"
                    style="margin-top: 2px; font-weight: 500;">Marquer comme
                    terminer</label>
                </div>
                <div class="d-flex mb-1">
                  <a href="{{ route('front-new.home.quizs', ['search' => $chapter->label]) }}" class="btn btn-secondary mr-3">Je m'exerce !</a>
                  <button type="submit" class="btn btn-primary">Valider</button>
                </div>
              </div>
            </form>
          </div>
          {{-- <div class=" col-12 col-md-4">
            <div class="card">
              <div class="card-header">
                <b>Leçons</b>
              </div>
              <ul class="list-group list-group-flush">
                @forelse ($chapter->chapters()->get()->sortBy('created_at') as $l)
                @if($l->isValidateL($user, $chapter->id))
                <li class="list-group-item"><a
                    href="{{ route('front-new.home.courses.show', ['slug' => \Str::slug($chapter->label), 'lesson_id' => $chapter->id, 'l_id' => $l->id]) }}"
                    class="text-success">{{ $l->title }}</a></li>
                @elseif($l->id == $lesson->id)
                <li class="list-group-item"><a
                    href="{{ route('front-new.home.courses.show', ['slug' => \Str::slug($chapter->label), 'lesson_id' => $chapter->id, 'l_id' => $l->id]) }}"
                    class="text-info"><b>{{ $l->title }}</b></a></li>
                @else
                <li class="list-group-item"><a
                    href="{{ route('front-new.home.courses.show', ['slug' => \Str::slug($chapter->label), 'lesson_id' => $chapter->id, 'l_id' => $l->id]) }}"
                    class="text-info">{{ $l->title }}</a></li>
                @endif

                @empty
                <li class="list-group-item"><a href="#" class="text-danger">Aucune Leçon disponible</a></li>
                @endforelse
              </ul>
            </div>
          </div> --}}
        </div>
      </div>
    </div>
  </div>

  @include('layouts.partials.footer')
</div>
@endsection

@section('js')
<script src="https://unpkg.com/pdfobject@2.2.4/pdfobject.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.6.347/pdf.min.js"
  integrity="sha512-Z8CqofpIcnJN80feS2uccz+pXWgZzeKxDsDNMD/dJ6997/LSRY+W4NmEt9acwR+Gt9OHN0kkI1CTianCwoqcjQ=="
  crossorigin="anonymous"></script>
<script>
  var options = {
    pdfOpenParams: {
      navpanes: 0,
      statusbar: 0,
      view: "FitV",
    },
    forcePDFJS: true,
    PDFJS_URL: "{{ asset('pdfjs/web/viewer.html') }}"
    // PDFJS_URL: "/pdfjs/web/viewer.html"
  };

  PDFObject.embed("{{ $lesson->getFirstMedia()->getUrl() }}", "#pdf", options);
  // PDFObject.embed("https://etd-lms.s3.eu-west-3.amazonaws.com/1006/6-Quels-sont-les-capitaux-garantis.pdf", "#pdf", options);
</script>
@endsection