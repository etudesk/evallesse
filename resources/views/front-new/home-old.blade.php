@extends('layouts.front-new')

@section('title', 'Accueil')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="page-header">
    @include('layouts.partials.home-navbar')

    <div class="home-content" style="margin-top: 2rem; margin-bottom: 2rem;">
      <div class="courses mb-3">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <h2 class="home-el-title mb-3">Cours suivis</h2>
            </div>
          </div>
          <div class="row">
            @forelse ($courses as $course)
            <div class="col-12 col-md-6 col-lg-4 mb-3">
              <div class="el-card py-3 px-3">
                <span class="d-inline-flex align-items-center bg-info text-white rounded-circle py-2 px-2">
                  <svg height="20" viewBox="0 0 16 16" class="bi bi-book-half" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                      d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                  </svg>
                </span> <br>
                <a href="{{ $course->lesson->getProgressLink($user) }}"
                  class="d-inline-block el-card-title mt-3 text-info">{{ $course->lesson->label }}</a>
                <br>
                <div class="d-inline-flex align-items-center mt-2 mb-3 marker">
                  <x-heroicon-o-bookmark class="mr-2" height="20" />
                  <span>{{ $course->lesson->chapters()->count() }} Leçons</span>
                </div>
                <div class="descritpion" style="margin-bottom: 1.6rem; font-size: 14px;">
                  <p>{{ \Illuminate\Support\Str::limit($course->lesson->description, 60) }}</p>
                </div>
                <label style="font-size: 13px; font-weight: 500;">Progression</label>
                <div class="progress mb-1" style="height: 3px;">
                  <div class="progress-bar bg-info" role="progressbar"
                    style="width: {{ $course->lesson->getProgressPercent($user) }}%;"
                    aria-valuenow="{{ $course->lesson->getProgressPercent($user) }}" aria-valuemin="0"
                    aria-valuemax="100"></div>
                </div>
              </div>
            </div>
            @empty
            <div class="col-12">
              <div class="no-data d-flex align-items-center justify-content-center p-5">
                <p class="text-secondary"><i>Vous ne suivez aucun cours pour le moment.</i></p>
              </div>
            </div>
            @endforelse

          </div>
        </div>
      </div>

      <div class="quizs mb-3">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <h2 class="home-el-title mb-3">Quiz passés</h2>
            </div>
          </div>
          <div class="row">
            @forelse ($quizzes as $quiz)
            <div class="col-12 col-md-6 col-lg-4 mb-3">
              <div class="el-card py-3 px-3">
                <span class="d-inline-flex align-items-center bg-info text-white rounded-circle py-2 px-2">
                  <x-heroicon-s-question-mark-circle height="20" />
                  {{-- <svg height="20" viewBox="0 0 16 16" class="bi bi-book-half" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                      d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                  </svg> --}}
                </span> <br>
                <a data-label="{{ $quiz->quiz->label }}"
                  data-url="{{ route('front-new.home.quizs.show', ['slug' => \Str::slug($quiz->quiz->label), 'quiz_id' => $quiz->quiz->id]) }}"
                  data-toggle="modal" data-target="#modalTakeQuiz" href="javascript:void(0)"
                  class="d-inline-block el-card-title mt-3 text-info begin-quiz">{{ $quiz->quiz->label }}</a>
                <br>
                <div class="d-inline-flex align-items-center mt-2 mb-3 marker">
                  {{-- <x-heroicon-o-bookmark class="mr-2" height="20" /> --}}
                  <svg xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 24 24" fill="currentColor"
                    stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                    class="feather feather-bar-chart-2 mr-2">
                    <line x1="18" y1="20" x2="18" y2="10" />
                    <line x1="12" y1="20" x2="12" y2="4" />
                    <line x1="6" y1="20" x2="6" y2="14" /></svg>
                  <span
                    class="mt-1">{{ $quiz->quiz->level == "easy" ? "Facile" : ($quiz->quiz->level == "medium" ? "Moyen" : "Difficile") }}</span>
                </div>
                <div class="descritpion" style="margin-bottom: 1.6rem; font-size: 14px;">
                  <p>{{ \Str::limit($quiz->quiz->description, 50) }}</p>
                </div>
                <label style="font-size: 13px; font-weight: 500;">Note</label>
                <div class="progress mb-1" style="height: 3px;">
                  <div class="progress-bar bg-info" role="progressbar"
                    style="width: {{ $quiz->quiz->getProgressPercent($user) }}%;"
                    aria-valuenow="{{ $quiz->quiz->getProgressPercent($user) }}" aria-valuemin="0" aria-valuemax="100">
                  </div>
                </div>
                <div class="d-flex align-items-end justify-content-between">
                  <div
                    class="{{ $quiz->quiz->getProgressPercent($user) == 100 ? "visible" : "invisible" }} d-inline-flex">
                    <x-heroicon-s-check-circle class="text-success" height="23" />
                  </div>
                  <div class="d-inline-flex align-items-center mt-4 marker">
                    <svg height="20" viewBox="0 0 16 16" class="bi bi-book-half" fill="currentColor"
                      xmlns="http://www.w3.org/2000/svg">
                      <path fill-rule="evenodd"
                        d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                    </svg>
                    <span class="ml-2">{{ $quiz->quiz->subject->label }}</span>
                  </div>
                </div>
              </div>
            </div>
            @empty
            <div class="col-12">
              <div class="no-data d-flex align-items-center justify-content-center p-5">
                <p class="text-secondary"><i>Vous n'avez pas encore passé de quiz.</i></p>
              </div>
            </div>
            @endforelse


          </div>
        </div>
      </div>

      <div class="examens">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <h2 class="home-el-title mb-3">Examens passés</h2>
            </div>
          </div>
          <div class="row">
            {{-- <div class="col-12 col-md-6 col-lg-4 mb-3">
              <div class="el-card py-3 px-3">
                <span class="d-inline-flex align-items-center bg-info text-white rounded-circle py-2 px-2">
                  <svg fill="currentColor" xmlns="http://www.w3.org/2000/svg" height="20" viewBox="0 0 20 20">
                    <path
                      d="M3.302 12.238c.464 1.879 1.054 2.701 3.022 3.562 1.969.86 2.904 1.8 3.676 1.8.771 0 1.648-.822 3.616-1.684 1.969-.861 1.443-1.123 1.907-3.002L10 15.6l-6.698-3.362zm16.209-4.902l-8.325-4.662c-.652-.365-1.72-.365-2.372 0L.488 7.336c-.652.365-.652.963 0 1.328l8.325 4.662c.652.365 1.72.365 2.372 0l5.382-3.014-5.836-1.367a3.09 3.09 0 0 1-.731.086c-1.052 0-1.904-.506-1.904-1.131 0-.627.853-1.133 1.904-1.133.816 0 1.51.307 1.78.734l6.182 2.029 1.549-.867c.651-.364.651-.962 0-1.327zm-2.544 8.834c-.065.385 1.283 1.018 1.411-.107.579-5.072-.416-6.531-.416-6.531l-1.395.781c0-.001 1.183 1.125.4 5.857z" />
                  </svg>
                </span> <br>
                <a href="#" class="d-inline-block el-card-title mt-3 text-info">Limite & continuité - Courbe</a>
                <br>
                <div class="d-inline-flex align-items-center mt-2 mb-3 marker">
                  <svg height="20" viewBox="0 0 16 16" class="bi bi-book-half" fill="currentColor"
                    xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd"
                      d="M8.5 2.687v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z" />
                  </svg>
                  <span class="ml-2">Mathématique</span>
                </div>
                <div class="descritpion" style="margin-bottom: 1.6rem; font-size: 14px;">
                  <p>Lorem ipsum dolor sit amet, consectetur. Lorem dolor sit amet...</p>
                </div>
                <div class="d-inline-flex align-items-center">
                  <label class="mb-0" style="font-size: 13px; font-weight: 500;">Note:</label>
                  <span class="ml-2 text-success" style="font-size: 22px; font-weight: 500;">15/20</span>
                </div>
                <div class="d-flex align-items-center justify-content-end mt-2">
                  <a class="btn btn-sm btn-info mr-2" href="#">Correction</a>
                  <a class="btn btn-sm btn-outline-info" data-toggle="modal" data-target="#modalRetakeExam"
                    href="javascript:void(0)">Reprendre</a>
                </div>
              </div>
            </div> --}}
            <div class="col-12">
              <div class="no-data d-flex align-items-center justify-content-center p-5">
                <p class="text-secondary"><i>Vous n'avez pas encore passé un examen.</i></p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

  @include('layouts.partials.footer')
  @include('front-new.partials.modals.retake-an-exam')
  @include('front-new.partials.modals.take-quiz')
</div>
@endsection

@section('js')
<script>
  $('.begin-quiz').click(function() {
    let $this = $(this);
    $("#modalTakeQuiz #q-label").html($this.data('label'));
    $("#modalTakeQuiz .bQAction").attr('href', $this.data('url'));
  });
</script>
@endsection