@extends('layouts.front-new')

@section('title', 'Inscription')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 col-lg-7 login-cover d-none d-lg-block">
      </div>
      <div class="col-12 col-lg-5">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="login-right d-flex align-items-center justify-content-center">
                <div class="login-right-content text-center w-100 px-lg-4">
                  <a href="{{ route('front-new.index') }}"><img class="mb-4"
                      src="{{ asset('images/logo-e-vallesse.png') }}" height="70" alt="Logo evallesse"></a>
                  <h1 class="auth-text-header mb-4">Inscris toi !</h1>
                  <form class="text-left" action="{{ route('front-new.sign-up.save') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <input name="name" type="text" class="form-control @error('name') is-invalid @enderror"
                        placeholder="Nom & prénoms*" value="{{ old('name') }}" aria-describedby="nameHelp">
                      @error('name')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                        placeholder="Email" value="{{ old('email') }}" aria-describedby="emailHelp">
                      @error('email')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <input type="text" name="tel" class="form-control @error('tel') is-invalid @enderror"
                        placeholder="Téléphone*" value="{{ old('tel') }}" aria-describedby="telHelp">
                      @error('tel')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <input type="password" name="password"
                        class="form-control @error('password') is-invalid @enderror" placeholder="Mot de passe"
                        value="{{ old('password') }}" aria-describedby="passwordHelp">
                      @error('password')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <select class="custom-select @error('classroom_id') is-invalid @enderror" name="classroom_id">
                        @foreach (get_classroom_list() as $classroom)
                        <option value="{{ $classroom->id }}">
                          {{ $classroom->label }}</option>
                        @endforeach
                      </select>
                      @error('classroom_id')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                    <div class="form-group d-none">
                      <select class="custom-select @error('country') is-invalid @enderror" name="country">
                        @foreach (get_country_list() as $country)
                        <option value="{{ $country }}" {{ $country == "Côte D'ivoire" ? "selected" : "" }}>
                          {{ $country }}</option>
                        @endforeach
                      </select>
                      @error('email')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                      @enderror
                    </div>
                    {{-- <div class="form-group">
                      <input type="text" class="form-control @error('city') is-invalid @enderror" placeholder="Ville*"
                        name="city" value="{{ old('city') }}" aria-describedby="cityHelp">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div> --}}
                <div class="form-group d-flex align-items-center">
                  <div class="custom-control d-flex align-items-center custom-radio mr-3">
                    <input type="radio" id="customRadio1" name="gender" value="male"
                      class="custom-control-input @error('gender') is-invalid @enderror">
                    <label class="custom-control-label" for="customRadio1">Homme</label>
                  </div>
                  <div class="custom-control d-flex align-items-center custom-radio">
                    <input type="radio" id="customRadio2" name="gender" value="female"
                      class="custom-control-input @error('gender') is-invalid @enderror">
                    <label class="custom-control-label" for="customRadio2">Femme</label>
                  </div>
                  @error('gender')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                {{-- <button type="submit" class="btn btn-block btn-primary btn-cta mb-2">Continuer</button> --}}
                <button type="submit" class="btn btn-block btn-primary btn-cta mb-2"
                  style="font-weight: 400;">Continuer</button>
                <div class="text-center">
                  <a class="text-info btn-link" href="{{ route('front-new.login') }}" style="font-weight: 400;">Je
                    me connecte</a>
                </div>
                </form>
                {{-- <div class="text-right mt-3">
                    <button type="button" class="btn btn-sm btn-info mr-2">Etape 1</button>
                    <button type="button" class="btn btn-sm btn-outline-info">Etape 2</button>
                  </div> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
@endsection