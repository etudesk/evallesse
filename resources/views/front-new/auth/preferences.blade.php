@extends('layouts.front-new')

@section('title', 'Préférences')

@section('content')
<div class="waiting-main-page d-flex justify-content-between flex-column position-relative">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-lg-7 login-cover d-none d-lg-block">
            </div>
            <div class="col-12 col-lg-5">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="login-right d-flex align-items-center justify-content-center">

                                <div class="login-right-content text-center w-100 px-lg-5">
                                    <a href="{{ route('front-new.index') }}"><img class="mb-4"
                                            src="{{ asset('images/logo-e-vallesse.png') }}" height="70"
                                            alt="Logo evallesse"></a>
                                    <h1 class="auth-text-header mb-4">Choisi tes matières</h1>
                                    <form class="text-left" action="{{ route('front-new.sign-up.preferences.save') }}"
                                        method="POST">

                                        {{ csrf_field() }}

                                        {{-- <input type="hidden" name="register_data" value="{{\GuzzleHttp\json_encode($data)}}">
                                        --}}
                                        @foreach (get_subject_list(auth()->user()->classroom_id) as $subject)
                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <div class="input-group-text">
                                                        <div class="custom-control custom-checkbox"
                                                            style="padding-left: 17px;">
                                                            <input class="custom-control-input" value="{{$subject->id}}"
                                                                id="customCheckbox{{$subject->id}}" type="checkbox"
                                                                name="subject_id[]">
                                                            <label class="custom-control-label"
                                                                for="customCheckbox{{$subject->id}}"></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="text" class="form-control" value="{{$subject->label}}"
                                                    readonly>
                                            </div>
                                        </div>
                                        @endforeach
                                        <button type="submit" class="btn btn-block btn-primary btn-cta mb-2">
                                            S'inscrire
                                        </button>
                                        {{-- <div class="text-center">
                                            <a class="text-info btn-link" href="{{ route('front-new.login') }}"
                                        style="font-weight: 400;">Je
                                        me connecte</a>
                                </div> --}}
                                </form>
                                {{-- <div class="text-right mt-3">
                                          <button type="button" class="btn btn-sm btn-info mr-2">Etape 1</button>
                                          <button type="button" class="btn btn-sm btn-info">Etape 2</button>
                                        </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection