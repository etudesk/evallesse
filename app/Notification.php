<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model 
{

    protected $table = 'notifications';
    public $timestamps = true;
    protected $fillable = array('recipient_type', 'data', 'type', 'reference_id', 'reference_type', 'sender_id', 'sender_type', 'readed');

}