<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\Exception\SuspiciousOperationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
        SuspiciousOperationException::class,
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        $this->registerErrorViewPaths();

         /**
         * When an exception is throw by the system
         * System send an email to sys admin email
         */
        if (
            !$this->isHttpException($exception)
            && !($exception instanceof ModelNotFoundException || $exception instanceof SuspiciousOperationException
                || $exception instanceof ValidationException || $exception instanceof AuthenticationException
                || $exception instanceof TokenMismatchException)
        ) {
            if (!\Session::has('reporting.' . get_class($exception))) {
                session(['reporting.' . get_class($exception) => true]);
                \Mail::send(
                    new \App\Mail\Reporting\SystemError(
                        [
                            'route' => $request->fullUrl(),
                            'error_message' => $exception->getMessage(),
                            'file' => $exception->getFile(),
                            'trace' => $exception->getTraceAsString(),
                            'line' => $exception->getLine(),
                            'ip' => $request->ip()
                        ]
                    )
                );
            }
        }

        if (in_array(\App::environment(), ['production', 'local'])) {
            if ($exception instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
                return response()->view('errors.404', ['exception' => $exception], 404, $exception->getHeaders());
            }

            if ($exception instanceof TokenMismatchException) {
                flashy()->info(__('controller.page-expire-msg'));
                return redirect()->back();
            }

            if ($this->isHttpException($exception) && $exception->getStatusCode() === 500 && env('APP_DEBUG') === true) {
                return $this->convertExceptionToResponse($exception);
            }

            if ($exception instanceof ModelNotFoundException || $exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException || $exception instanceof SuspiciousOperationException) {
                return response()->view('errors.404', ['exception' => $exception], 404);
            }

            if (\App::environment() === 'production' && !($exception instanceof ValidationException || $exception instanceof AuthenticationException)) {
                return response()->view('errors.500', ['exception' => $exception], 500);
            }

            return parent::render($request, $exception);
        }

        return parent::render($request, $exception);
    }
}
