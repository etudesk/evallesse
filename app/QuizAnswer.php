<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class QuizAnswer extends Model implements HasMedia
{

    use HasMediaTrait;
    protected $table = 'quiz_answers';
    public $timestamps = true;
    protected $fillable = array('label','file', 'explanation', 'explanation_type', 'correct_answer', 'image', 'quiz_question_id');

    public function question()
    {
        return $this->belongsTo('QuizQuestion', 'quiz_question_id');
    }

    public function userResponseCollections()
    {
        return $this->hasMany('UserQuizResponse', 'quiz_answer_id');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('default')
            ->singleFile();
    }

}
