<?php

namespace App\Mail\Reporting;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SystemError extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * @var array
   */
  private $data;

  /**
   * Create a new message instance.
   *
   * @param array $data
   */
  public function __construct($data)
  {
    $this->data = $data;

    $this->onQueue('contact');
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->view('emails.reporting.errors', $this->data)
      ->to('ismael.diomande@etudesk.org')
      ->subject('Evallesse - error report at ' . date('d/m/Y H:i:s'));
  }
}
