<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class Book extends Model implements HasMedia
{

    use HasMediaTrait;
    protected $table = 'books';
    public $timestamps = true;
    protected $fillable = array('title', 'price','page_number', 'description', 'book_categoriy_id', 'subject_id','file');

    public function category()
    {
        return $this->belongsTo('BookCategory', 'book_category_id');
    }

    public function subject()
    {
        return $this->belongsTo('Subject', 'subject_id');
    }

    public function orders()
    {
        return $this->hasMany('BookOrder', 'book_id');
    }

}