<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class Chapter extends Model implements HasMedia
{

    use HasMediaTrait;
    protected $table = 'chapters';
    public $timestamps = true;
    protected $fillable = array('title', 'content', 'lesson_id', 'pdf');

    public function lesson()
    {
        return $this->belongsTo('Lesson', 'lesson_id');
    }

    public function progress()
    {
        return $this->hasMany(Progression::class, 'chapter_id');
    }

    public function isValidateL($user, $lesson_id)
    {
        return $this->progress()->where('user_id', $user->id)
            ->where('lesson_id', $lesson_id)->exists();
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('default')
            ->singleFile();
    }
}
