<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail, HasMedia
{
    use Notifiable;
    use HasMediaTrait;
    use HasApiTokens;

    protected $table = 'users';
    public $timestamps = true;
    protected $fillable = array('name', 'role', 'email', 'password', 'country', 'adresse', 'classroom_id', 'tel', 'created_at', 'gender', 'account_state', 'validation_code');
    protected $hidden = array('password', 'remember_token');

    public function classroom()
    {
        return $this->belongsTo('App\Classroom');
    }

    public function preferences()
    {
        return $this->hasMany(UserSubject::class, 'user_id');
    }

    public function hasPreferences()
    {
        return $this->preferences()->get()->isNotEmpty();
    }

    public function userResponseCollections()
    {
        return $this->hasMany('App\UserQuizResponse', 'user_id');
    }

    public function progress()
    {
        return $this->hasMany('App\Progression', 'user_id');
    }

    public function getFollowLessonNbBy($lessonIdsArray)
    {
        return $this->progress()->whereIn('lesson_id', $lessonIdsArray)
            ->get()->unique('lesson_id')->count();
    }

    public function getFollowQuizNbBy($quizIdsArray)
    {
        return UserQuizResponse::where('user_id', $this->id)->whereIn('quiz_id', $quizIdsArray)
            ->get()->unique('quiz_id')->count();
    }

    public function getFollowExamNbBy($examIdsArray)
    {
        return UserExamResponse::where('user_id', $this->id)->whereIn('exam_id', $examIdsArray)
            ->get()->unique('quiz_id')->count();
    }

    public function userExamResponseCollections()
    {
        return $this->hasMany('App\UserExamResponse', 'user_id');
    }

    public function orders()
    {
        return $this->hasMany('App\BookOrder', 'user_id');
    }

    public function quizAnswers()
    {
        return $this->hasMany(UserQuizResponse::class, 'user_id');
    }

    public function computeQuizGoodResponse($quiz_id)
    {
        $responses = $this->quizAnswers()->where('quiz_id', $quiz_id)->get();

        $count = count($responses->unique('quiz_question_id'));

        return $count;
    }

    public function sendEmailVerificationNotification()
    {
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatars')
            ->singleFile();
    }
}
