<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{

    protected $table = 'exams';
    public $timestamps = true;
    protected $fillable = array('label', 'description', 'estimate_duration', 'subject_id');

    public function questions()
    {
        return $this->hasMany('App\ExamQuestion', 'exam_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function examResponses()
    {
        return $this->hasMany('App\UserExamResponse', 'exam_id');
    }

    public function userHasPassExam($user)
    {
        return $this->examResponses()->where('user_id', $user->id)->exists();
    }
}
