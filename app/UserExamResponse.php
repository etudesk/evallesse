<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserExamResponse extends Model
{

    protected $table = 'user_exam_responses';
    public $timestamps = true;
    protected $fillable = array('user_id', 'exam_answer_id', 'exam_question_id', 'exam_id', 'good');

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function examAnswer()
    {
        return $this->belongsTo('App\ExamAnswer', 'exam_answer_id');
    }


    public function exam()
    {
        return $this->belongsTo('App\Exam', 'exam_id');
    }

}
