<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookCategory extends Model
{

    protected $table = 'book_categories';
    public $timestamps = true;
    protected $fillable = array('label', 'description');

    public function books()
    {
        return $this->hasMany('Book', 'book_category_id');
    }

}
