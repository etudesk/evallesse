<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendResetPasswordCode extends Notification
{
    use Queueable;

    public $codeForReset;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($codeForReset)
    {
        $this->codeForReset = $codeForReset;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Notification de réinitialisation du mot de passe')
            ->greeting('Bonjour !')
            ->line('Le code de réinitialisation de votre mot de passe est le suivant : ' . $this->codeForReset . '')
            ->line('Veuillez ignorer ce mail, si vous n\'avez effectué aucune demande.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
