<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class ExamAnswer extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $table = 'exam_answers';
    public $timestamps = true;
    protected $fillable = array('label', 'image', 'explanation', 'explanation_type', 'correct_answer', 'exam_question_id', 'file');

    public function question()
    {
        return $this->belongsTo('App\ExamQuestion');
    }

    public function userExamResponseCollections()
    {
        return $this->hasMany('UserExamResponse', 'exam_answer_id');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('default')
            ->singleFile();
    }

}
