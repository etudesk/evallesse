<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Progression extends Model 
{

    protected $table = 'progressions';
    public $timestamps = true;
    protected $fillable = array('user_id', 'chapter_id', 'lesson_id');

    public function users()
    {
        return $this->belongsTo('App\user');
    }

    public function chapter()
    {
        return $this->belongsTo('App\Chapter');
    }

    public function lesson()
    {
        return $this->belongsTo('App\Lesson');
    }

}