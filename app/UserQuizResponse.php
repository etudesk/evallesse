<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserQuizResponse extends Model
{

    protected $table = 'user_quiz_responses';
    public $timestamps = true;
    protected $fillable = array('user_id', 'quiz_answer_id', 'quiz_id', 'quiz_question_id', 'good');

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function quiz_answer()
    {
        return $this->belongsTo('App\QuizAnswer', 'quiz_answer_id');
    }

    public function quiz()
    {
        return $this->belongsTo('App\Quiz', 'quiz_id');
    }

}
