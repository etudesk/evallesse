<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class ExamQuestion extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $table = 'exam_questions';
    public $timestamps = true;
    protected $fillable = array('type', 'label', 'image', 'scale', 'exam_id', 'file');

    public function exam()
    {
        return $this->belongsTo('App\Exam', 'exam_id');
    }

    public function answers()
    {
        return $this->hasMany('App\ExamAnswer');
    }

    public function userResponses()
    {
        return $this->hasMany('App\UserExamResponse', 'exam_question_id');
    }

    public function getUserResponses($user)
    {
        $responses = $this->userResponses()->where('user_id', $user->id)->get();

        return $responses;
    }

    public function verifyUserResponse($response)
    {
        if ($this->type === "unique") {

            $good_responses = $this->answers()->where('correct_answer', 1)
                ->pluck('id')->first();
        } else {

            $good_responses = $this->answers()->where('correct_answer', 1)
                ->pluck('id')->toArray();
        }

        if ($response == $good_responses) {
            return ['good' => 1];
        }

        return ['good' => 0];
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('default')
            ->singleFile();
    }
}
