<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookOrder extends Model
{

    protected $table = 'book_orders';
    public $timestamps = true;
    protected $fillable = array('user_id', 'book_id');

    public function book()
    {
        return $this->belongsTo('Book', 'book_id');
    }

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

}
