<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{

    protected $table = 'subjects';
    public $timestamps = true;
    protected $fillable = array('label', 'description', 'classroom_id');

    public function classroom()
    {
        return $this->belongsTo('App\Classroom');
    }

    public function lessons()
    {
        return $this->hasMany(Lesson::class, 'subject_id');
    }

    public function correctedExamFiles()
    {
        return $this->hasMany('CorrectedExamFile', 'subject_id');
    }

    public function quizs()
    {
        return $this->hasMany(Quiz::class, 'subject_id');
    }

    public function exams()
    {
        return $this->hasMany(Exam::class, 'subject_id');
    }

    public function books()
    {
        return $this->hasMany('Book', 'subject_id');
    }

}
