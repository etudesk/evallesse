<?php

namespace App\Http\Middleware;

use Closure;

class VerifyIfUserHasPreferences
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        if (!$user->hasPreferences()) {
            flashy()->error('Veuillez choisir une matière SVP !');
            return redirect()->to(route('front-new.home.profile.preference'));
        }

        return $next($request);
    }
}
