<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class VerifyIfUserHasPreferencesMobile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::guard('api')->user();

        if (!$user->hasPreferences()) {
            return Response::json([
                'status_code' => 204,
                'status_message' => 'Veuillez choisir une matière SVP !'
            ]);
        }

        return $next($request);
    }
}
