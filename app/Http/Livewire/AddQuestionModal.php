<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\QuizQuestion;
use App\Quiz;
use App\QuizAnswer;
use App\ExamQuestion;
use App\ExamAnswer;
use App\Subject;
use App\Chapter;
use App\Lesson;
use Illuminate\Support\Facades\Hash;
use App\Exam;
use App\User;
use App\CorrectedExamFile;
use App\Book;
use Livewire\WithFileUploads;

use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\MessageBag;

class AddQuestionModal extends Component
{
    use WithFileUploads;
    public $label, $quizId, $level, $subject_id, $type, $matiereId, $erreur,
        $correct, $reponse, $explication, $file, $questionImage, $answerImage, $question_id, $image, $description, $answer_id,
        $quizQuestionId, $estimate_time, $lessonId, $editImage, $content, $title, $chapterImage, $filePdf, $chapterId, $editPdf, $classeId, $nom, $prenom, $email, $password, $country, $adresse, $studentId, $studentName, $examId, $estimate_duration, $examQuestionImage, $examQuestionId,
        $editExamImage, $imageExam, $examAnswerId, $scale, $subjectId, $image_option, $correctionId, $bookFile, $page_number, $price, $bookId, $optionQuizFile, $quizQuestionImage, $quizExamQuestionImage, $examOptionImage;
    public $inputs = [];

    public $i = 1;


    protected $listeners = [
        'fileUpload'     => 'handleFileUpload',
        'sendQuizId', 'sendQuestionData', 'updateLabel',
        'fileUploadEditQuestion', 'sendAnswerData',
        'sendQuizData', 'sendQuizQuestionData',
        'sendLessonData', 'sendChapterData', 'sendClasseData', 'sendExamData', 'examFileUpload', 'sendExamQuestionData',
        'fileUploadEditExamQuestion', 'sendExamAnswerData', 'sendSubjectData', 'sendCorrectionData', 'sendBookData'
    ];

    public function updateLabel($label)
    {
        $this->label = $label;
    }

    public function handleFileUpload($imageData)
    {
        $this->questionImage = $imageData;
    }


    public function sendSubjectData($id, $label, $description)
    {
        $this->label = $label;
        $this->description = $description;
        $this->subjectId = $id;
    }

    public function sendBookData($id, $title, $description, $price, $page_number, $subject_id, $file)
    {
        $this->title = $title;
        $this->description = $description;
        $this->bookId = $id;
        $this->price = $price;
        $this->page_number = $page_number;
        $this->subject_id = $subject_id;
        $this->file = $file;
    }




    public function sendCorrectionData($id, $label, $description, $file, $subjectId)
    {
        $this->label = $label;
        $this->description = $description;
        $this->subject_id = $subjectId;
        $this->file = $file;
        $this->correctionId = $id;
    }

    public function fileUploadEditExamQuestion($imageData)
    {
        $this->imageExam = 2;
        $this->file = $imageData;
        $this->editExamImage = 1;
    }


    public function examFileUpload($imageData)
    {
        $this->examQuestionImage = $imageData;
    }

    public function sendClasseData($id, $student_id, $name)
    {
        $this->classeId = $id;
        $this->studentId = $student_id;
        $this->studentName = $name;
    }



    public function sendAnswerData($id, $label, $explication, $file, $image)
    {
        $this->answer_id = $id;
        $this->label = $label;
        $this->explication = $explication;
        $this->file = $file;
        $this->image = $image;
    }


    public function sendExamAnswerData($id, $label, $explication, $file, $image)
    {
        $this->examAnswerId = $id;
        $this->label = $label;
        $this->explication = $explication;
        $this->file = $file;
        $this->image = $image;
    }


    public function sendLessonData($id, $label, $description, $subject_id, $estimate_time)
    {
        $this->lessonId = $id;
        $this->label = $label;
        $this->subject_id = $subject_id;
        $this->estimate_time = $estimate_time;
        $this->description = $description;
    }
    public function sendQuizData($id, $label, $description, $subject_id, $level)
    {
        $this->quizId = $id;
        $this->label = $label;
        $this->description = $description;
        $this->level = $level;
        $this->subject_id = $subject_id;
    }


    public function sendExamData($id, $label, $description, $subject_id, $duree)
    {
        $this->examId = $id;
        $this->label = $label;
        $this->description = $description;
        $this->estimate_duration = $duree;
        $this->subject_id = $subject_id;
    }

    public function fileUploadEditQuestion($imageData)
    {

        $this->image = 2;
        $this->file = $imageData;
        $this->editImage = 1;
    }

    public function sendQuizId($value)
    {
        $this->quizId = $value;
    }


    public function sendChapterData($id, $title, $content)
    {
        $this->chapterId = $id;
        $this->title = $title;
        $this->content = $content;
        // dd($this->content);
        $this->editPdf = 1;
    }

    public function sendQuizQuestionData($value)
    {
        $this->quizQuestionId = $value;
    }

    public function sendExamQuestionData($id, $label, $type, $image, $file, $scale)
    {
        $this->examQuestionId = $id;
        $this->label = $label;
        $this->type = $type;
        $this->imageExam = $image;
        $this->file = $file;
        $this->scale = $scale;
    }

    public function sendQuestionData($id, $label, $type, $image, $file)
    {
        $this->question_id = $id;
        $this->label = $label;
        $this->type = $type;
        $this->image = $image;
        $this->file = $file;
    }

    public function delete($id)
    {

        if ($id) {

            QuizQuestion::where('id', $id)->delete();
            QuizAnswer::where('quiz_question_id', $id)->delete();
            session()->flash('message', 'Question supprimée avec succès.');

            $this->emit('quizStore');
            $this->emit('refreshdeletequestion');
        }
    }

    public function deleteExamQuestion($id)
    {

        if ($id) {

            ExamQuestion::where('id', $id)->delete();
            ExamAnswer::where('exam_question_id', $id)->delete();
            session()->flash('message', 'Question supprimée avec succès.');

            $this->emit('quizStore');
            $this->emit('refreshdeleteexamquestion');
        }
    }


    public function updateQuiz($id)
    {

        $validatedDate = $this->validate(
            [
                'label' => 'nullable',
                'description' => 'required',
                'level' => 'required',
                'subject_id' => 'required'



            ]
        );


        $quiz = Quiz::find($id);
        $quiz->update(
            [
                'label' => $this->label,
                'description' => $this->description,
                'level' => $this->level,
                'subject_id' => $this->subject_id


            ]
        );
        $this->resetInputFields();
        $this->emit('refresheditquiz');
        $this->emit('quizStore');
    }

    public function updateExam($id)
    {

        $validatedDate = $this->validate(
            [
                'label' => 'nullable',
                'description' => 'required',
                'estimate_duration' => 'required',
                'subject_id' => 'required'



            ]
        );


        $quiz = Exam::find($id);
        $quiz->update(
            [
                'label' => $this->label,
                'description' => $this->description,
                'estimate_duration' => $this->estimate_duration,
                'subject_id' => $this->subject_id


            ]
        );
        $this->resetInputFields();
        $this->emit('refresheditexam');
        $this->emit('quizStore');
    }


    public function addOptionReponse()
    {
        $validatedDate = $this->validate(
            [
                'label' => 'nullable',
                'explication' => 'nullable'


            ]
        );


        $correct_answer = 0;
        if (isset($this->correct) && $this->correct != false) {
            $correct_answer = 1;
        }

        $image = 0;

        if (!empty($this->optionQuizFile)) {
            $image = 1;
        }


        $answer = QuizAnswer::create(
            [
                'label' => $this->label,
                'explanation' =>  $this->explication,
                'correct_answer' => $correct_answer,
                'quiz_question_id' => $this->quizQuestionId,
                'image' => $image


            ]
        );
        if (!empty($this->optionQuizFile)) {
            $answer->addMedia($this->optionQuizFile->getRealPath())
                ->usingName($this->optionQuizFile->getClientOriginalName())
                ->toMediaCollection('default', get_storage_driver());
        }


        $this->emit('quizStore');
        $this->resetInputFields();
        $this->emit('refreshaddoptionreponse');
    }


    public function addExamOptionReponse()
    {

        $validatedDate = $this->validate(
            [
                'label' => 'nullable',
                'explication' => 'nullable'


            ]
        );
        $image = 0;
        $correct_answer = 0;
        if (!empty($this->examOptionImage)) {
            $image = 1;
        }
        if (isset($this->correct) && $this->correct != false) {
            $correct_answer = 1;
        }

        $examAnswer = ExamAnswer::create(
            [
                'label' => $this->label,
                'explanation' =>  $this->explication,
                'correct_answer' => $correct_answer,
                'exam_question_id' => $this->examQuestionId,
                'image' => $image


            ]
        );

        if (!empty($this->examOptionImage)) {
            $examAnswer->addMedia($this->examOptionImage->getRealPath())
                ->usingName($this->examOptionImage->getClientOriginalName())
                ->toMediaCollection('default', get_storage_driver());
        }

        $this->emit('quizStore');
        $this->resetInputFields();
        $this->emit('refreshaddoptionreponse');
    }


    public function deletelesson($id)
    {
        if ($id) {

            Lesson::where('id', $id)->delete();
            $this->emit('quizStore');
            $this->resetInputFields();
            $this->emit('refreshdeletelesson');
        }
    }

    public function cancel()
    {
        $this->resetErrorBag();
        $this->resetInputFields();
    }
    public function deleteOptionReponse($id)
    {

        if ($id) {


            QuizAnswer::where('id', $id)->delete();
            session()->flash('message', 'Reponse supprimée avec succès.');

            $this->emit('quizStore');
            $this->resetInputFields();
            $this->emit('refreshdeletereponse');
        }
    }

    public function deleteExamOptionReponse($id)
    {

        if ($id) {


            ExamAnswer::where('id', $id)->delete();


            $this->emit('quizStore');
            $this->resetInputFields();
            $this->emit('refreshdeletereponse');
        }
    }


    public function storelesson()
    {
        $validatedDate = $this->validate(
            [
                'label' => 'required',

                'description' => 'required',

                'subject_id' => 'required',
                'estimate_time' => 'required'
            ]
        );


        Lesson::create($validatedDate);

        $this->emit('quizStore');
        $this->resetInputFields();
        $this->emit('refreshaddlesson');
    }

    public function updatelesson()
    {
        $validatedDate = $this->validate(
            [
                'label' => 'required',

                'description' => 'required',

                'subject_id' => 'required',
                'estimate_time' => 'required'
            ]
        );


        $lesson = Lesson::find($this->lessonId);
        $lesson->update($validatedDate);

        $this->emit('quizStore');
        $this->resetInputFields();
        $this->emit('refreshupdatelesson');
    }


    public function deletequiz($id)
    {

        if ($id) {


            Quiz::where('id', $id)->delete();
            $question = QuizQuestion::where('quiz_id', $id)->get()->first();
            if (!empty($question->id)) {
                $question_id = $question->id;
                QuizAnswer::where('quiz_question_id', $question_id)->delete();
            }

            QuizQuestion::where('quiz_id', $id)->delete();





            $this->emit('quizStore');
            $this->resetInputFields();
            $this->emit('refreshdeletereponse');
        }
    }


    public function deleteExam($id)
    {

        if ($id) {


            Exam::where('id', $id)->delete();


            $this->emit('quizStore');
            $this->resetInputFields();
            $this->emit('refreshdeleteexam');
        }
    }


    public function updateOptionReponse()
    {

        $validatedDate = $this->validate(
            [
                'label' => 'nullable',
                'explication' => 'nullable'


            ]
        );


        $correct = 0;
        $file = $this->file;

        if ($this->image == 1) {
            $image = 1;
        } else {
            $image = 0;
        }

        if (!empty($this->image_option)) {

            $name  = Str::random() . '.pdf';
            // $file = Storage::disk('public')->put($name, $this->image_option);
            $image = 1;
        }



        if ($this->answer_id) {
            if (isset($this->correct) && $this->correct != false) {
                $correct = 1;
            }

            $answer = QuizAnswer::find($this->answer_id);
            $answer->update(
                [
                    'label' => $this->label,
                    'explanation' => $this->explication,
                    'correct_answer' => $correct,
                    'image' => $image,
                    // 'file' => $file


                ]
            );

            if (!empty($this->image_option)) {
                $answer->media()->delete($answer->id);
                $answer->addMedia($this->image_option->getRealPath())
                    ->usingName($this->image_option->getClientOriginalName())
                    ->toMediaCollection('default', get_storage_driver());
            }
        }



        $this->resetInputFields();

        $this->emit('quizStore');
        $this->emit('refreshupdateoptionreponse');
    }



    public function updateExamOptionReponse()
    {

        $validatedDate = $this->validate(
            [
                'label' => 'nullable',
                'explication' => 'nullable'


            ]
        );

        //$image          = $this->storeImage();
        $file = 0;
        $correct = 0;


        if (!empty($image)) {
            $file = 1;
        }

        if ($this->image == 1) {
            $image = 1;
        } else {
            $image = 0;
        }


        if (!empty($this->image_option)) {
            $image = 1;
        }
        if ($this->examAnswerId) {
            if (isset($this->correct) && $this->correct != false) {
                $correct = 1;
            }


            $answer = ExamAnswer::find($this->examAnswerId);
            $answer->update(
                [
                    'label' => $this->label,
                    'explanation' => $this->explication,
                    'correct_answer' => $correct,
                    'image' => $image


                ]
            );

            if (!empty($this->image_option)) {
                $answer->media()->delete($answer->id);
                $answer->addMedia($this->image_option->getRealPath())
                    ->usingName($this->image_option->getClientOriginalName())
                    ->toMediaCollection('default', get_storage_driver());
            }
        }



        $this->resetInputFields();

        $this->emit('quizStore');
        $this->emit('refreshupdateoptionreponse');
    }

    public function update()
    {


        $validatedDate = $this->validate(
            [
                'label' => 'nullable',
                'type' => 'required'


            ]
        );
        $image = $this->file;

        if (!empty($this->editImage)) {

            $image          = $this->storeImage();
        }


        $file = 0;

        if (!empty($image)) {
            $file = 1;
        }

        if ($this->question_id) {
            $question = QuizQuestion::find($this->question_id);
            $question->update(
                [
                    'label' => $this->label,
                    'type' => $this->type,
                    'image' => $file,
                    // 'file' => $image


                ]
            );

            if (!empty($this->quizQuestionImage)) {
                $question->media()->delete($question->id);
                $question->addMedia($this->quizQuestionImage->getRealPath())
                    ->usingName($this->quizQuestionImage->getClientOriginalName())
                    ->toMediaCollection('default', get_storage_driver());
            }




            session()->flash('message', 'Question modifié avec succès.');
            $this->resetInputFields();
            $this->emit('quizStore');
            $this->emit('refreshupdatequestion');
        }
    }



    public function updatingTitle()
    {

        $this->emit('resetPag');
    }

    public function updatingLabel()
    {

        $this->emit('resetPag');
    }


    private function resetInputFields()
    {

        $this->quizExamQuestionImage = "";
        $this->examOptionImage = "";
        $this->quizQuestionImage = "";
        $this->optionQuizFile = "";
        $this->bookFile = "";
        $this->bookId = "";
        $this->price = "";
        $this->correctionId = "";
        $this->examQuestionImage = "";
        $this->image_option = '';
        $this->label = '';
        $this->subjectId = '';
        $this->scale = '';
        $this->content = '';
        $this->nom = '';
        $this->correct = '';
        $this->prenom = '';
        $this->email = '';
        $this->password = '';
        $this->adresse = '';
        $this->country = '';

        $this->editPdf = '';
        $this->question_id = '';
        $this->editImage = '';

        $this->explication = '';
        $this->description = '';
        $this->estimate_time = '';
        $this->subject_id = '';

        $this->image = "";
        $this->chapterId = "";
        $this->editExamImage = "";

        $this->file = '';
        $this->questionImage = '';
        $this->quizId = '';
        $this->title = '';
        $this->filePdf = '';
        $this->studentId = "";
        $this->studentName = '';
        $this->inputs = [];
    }


    public function add($i)
    {
        $i = $i + 1;
        $this->i = $i;

        array_push($this->inputs, $i);
    }

    public function remove($i)
    {
        unset($this->inputs[$i]);
    }


    public function store()
    {



        /* 'reponse.2' => 'required', */
        $validatedDate = $this->validate(
            [
                'label' => 'nullable',
                'label.*' => 'required',
                'type' => 'required',
                'reponse.*' => 'required',




            ],
            [
                'label' => 'label field is required',
                'label.2.required' => 'label field is required',
                'reponse.2.required' => 'le champs reponse 1 est requis',
                'type.2.required' => 'type field is required',
                'correct.2.required' => 'Il faut au moins une bonne reponse',
                'correct.*.required' => 'Il faut au moins une bonne reponse',
                'label.*.required' => 'name field is required',
                'reponse.3.required' => 'le champs reponse 2 est requis',
                'reponse.4.required' => 'le champs reponse 3 est requis',
                'reponse.5.required' => 'le champs reponse 4 est requis',
                'reponse.*.required' => 'le champs reponse  est requis',
                'type.*.equired' => 'type field is required',

            ]
        );



        $image          = $this->storeImage();
        $file = 0;

        if (!empty($image)) {
            $file = 1;
        }


        $data1 = [
            'label' => $this->label,
            'type' =>   $this->type,
            'quiz_id' =>   $this->quizId,
            // 'file' => $image,
            'image' => $file,


        ];





        /* $this->questionImage = ''; */



        //taile du tableau

        $quizQuestion = QuizQuestion::create($data1);

        if (!empty($this->quizQuestionImage)) {
            $quizQuestion->addMedia($this->quizQuestionImage->getRealPath())
                ->usingName($this->quizQuestionImage->getClientOriginalName())
                ->toMediaCollection('default', get_storage_driver());
        }


        if (!empty($this->reponse)) {
            foreach ($this->reponse as $key => $value) {
                $correct_answer = 0;
                $explication = "";


                if (isset($this->correct[$key])) {

                    if ($this->correct[$key] != false) {
                        $correct_answer = 1;
                    }
                } else {
                    $correct_answer = 0;
                }

                if (isset($this->explication[$key])) {

                    $explication = $this->explication[$key];
                } else {
                    $explication = "";
                }

                $link = '';
                $image = 0;

                if (isset($this->image_option[$key])) {

                    $file = $this->image_option[$key];


                    $image = 1;

                    $name  = Str::random() . '.jpg';
                    // $link = Storage::disk('public')->put($name, $file);
                }


                $quiz_question = QuizQuestion::latest()->first();
                $quizQuestionAnswer = QuizAnswer::create(['label' => $this->reponse[$key], 'explanation' =>  $explication, 'correct_answer' => $correct_answer, 'quiz_question_id' => $quiz_question->id, 'image' => $image]);


                if (!empty($this->image_option[$key])) {
                    $quizQuestionAnswer->addMedia($this->image_option[$key]->getRealPath())
                        ->usingName($this->image_option[$key]->getClientOriginalName())
                        ->toMediaCollection('default', get_storage_driver());
                }
            }
        }


        flashy()->success('Questions créée avec succès.');


        $this->resetInputFields();
        $this->emit('refreshadd');
        $this->emit('quizStore');
    }



    public function storeExamQuestion()
    {



        /* 'reponse.2' => 'required', */
        $validatedDate = $this->validate(
            [
                'label' => 'required',
                'scale' => 'required',
                'label.*' => 'required',
                'type' => 'required',
                'reponse.*' => 'required',




            ],
            [
                'label' => 'label field is required',
                'label.2.required' => 'label field is required',
                'reponse.2.required' => 'le champs reponse 1 est requis',
                'type.2.required' => 'type field is required',
                'correct.2.required' => 'Il faut au moins une bonne reponse',
                'correct.*.required' => 'Il faut au moins une bonne reponse',
                'label.*.required' => 'name field is required',
                'reponse.3.required' => 'le champs reponse 2 est requis',
                'reponse.4.required' => 'le champs reponse 3 est requis',
                'reponse.5.required' => 'le champs reponse 4 est requis',
                'reponse.*.required' => 'le champs reponse  est requis',
                'type.*.equired' => 'type field is required',

            ]
        );



        $image          = $this->storeExamImage();
        $file = 0;

        if (!empty($image)) {
            $file = 1;
        }


        $data1 = [
            'label' => $this->label,
            'type' =>   $this->type,
            'exam_id' =>   $this->examId,
            // 'file' => $image,
            'image' => $file,
            'scale' => $this->scale


        ];





        $this->questionImage = '';



        //taile du tableau

        $examQuestion = ExamQuestion::create($data1);

        if (!empty($this->quizExamQuestionImage)) {
            $examQuestion->addMedia($this->quizExamQuestionImage->getRealPath())
                ->usingName($this->quizExamQuestionImage->getClientOriginalName())
                ->toMediaCollection('default', get_storage_driver());
        }

        if (!empty($this->reponse)) {
            foreach ($this->reponse as $key => $value) {
                $correct_answer = 0;
                $explication = "";


                if (isset($this->correct[$key])) {

                    if ($this->correct[$key] != false) {
                        $correct_answer = 1;
                    }
                } else {
                    $correct_answer = 0;
                }

                if (isset($this->explication[$key])) {

                    $explication = $this->explication[$key];
                } else {
                    $explication = "";
                }

                $link = '';
                $image = 0;
                if (isset($this->image_option[$key])) {

                    $file = $this->image_option[$key];
                    $name = $file->getClientOriginalName();
                    $image = 1;


                    // $link = Storage::disk('public')->put($name, $file);
                }

                $examen_question = ExamQuestion::latest()->first();
                $examQuestionAnswer = ExamAnswer::create(['label' => $this->reponse[$key], 'explanation' =>  $explication, 'correct_answer' => $correct_answer, 'exam_question_id' => $examen_question->id, 'image' => $image]);

                if (!empty($this->image_option[$key])) {
                    $examQuestionAnswer->addMedia($this->image_option[$key]->getRealPath())
                        ->usingName($this->image_option[$key]->getClientOriginalName())
                        ->toMediaCollection('default', get_storage_driver());
                }
            }
        }

        flashy()->success('Questions créée avec succès.', 'lien du quiz');


        $this->resetInputFields();
        $this->emit('refreshaddexamquestion');
        $this->emit('quizStore');
    }


    public function storeImage()
    {

        if (!$this->questionImage && !$this->file) {
            return null;
        }

        if (!empty($this->file)) {
            $file = $this->file;
        } else {
            $file = $this->questionImage;
        }
        // $img   = ImageManagerStatic::make($file)->encode('jpg');
        $name  = Str::random() . '.jpg';
        // Storage::disk('public')->put($name, $img);


        return $name;
    }


    public function storeExamImage()
    {


        if (!$this->examQuestionImage && !$this->file) {
            return null;
        }

        if (!empty($this->file)) {
            $file = $this->file;
        } else {
            $file = $this->examQuestionImage;
        }
        // $img   = ImageManagerStatic::make($file)->encode('jpg');
        $name  = Str::random() . '.jpg';
        // Storage::disk('public')->put($name, $img);

        return $name;
    }

    public function addChapter()
    {
        $validatedDate = $this->validate(
            [
                'title' => 'required',
                'filePdf' => 'required'

            ]
        );
        // $name  = Str::random() . '.pdf';
        // $link = Storage::disk(get_storage_driver())->put($name, $this->filePdf);

        $chapter = Chapter::create(
            [
                'title' => $this->title,
                'lesson_id' => $this->lessonId,
                // 'pdf' => $link,
                'content' => $this->content
            ]
        );

        if (!empty($this->filePdf)) {
            $chapter->addMedia($this->filePdf->getRealPath())
                ->usingName($this->filePdf->getClientOriginalName())
                ->toMediaCollection('default', get_storage_driver());
        }

        $this->emit('quizStore');
        $this->emit('refreshaddchapter');
        $this->resetInputFields();
    }


    public function deleteChapter($id)
    {
        if ($id) {

            $chapter = Chapter::find($id);
            // Chapter::where('id', $id)->delete();
            $chapter->getFirstMedia()->delete();
            $chapter->delete();

            $this->emit('quizStore');
            $this->resetInputFields();
            $this->emit('refreshdeletechapter');
        }
    }

    public function updateChapter()
    {

        $validatedDate = $this->validate(
            [
                'title' => 'required',
            ]
        );

        // $pd = $this->file;

        // if (!empty($this->filePdf)) {

        //     $name  = Str::random() . '.pdf';
        //     $pd = Storage::disk('public')->put($name, $this->filePdf);
        // }

        $chapter = Chapter::find($this->chapterId);
        $chapter->update(
            [
                'title' => $this->title,
                'lessonId' => $this->lessonId,
                'content' => $this->content
            ]
        );

        if (!empty($this->filePdf)) {
            $chapter->getFirstMedia()->delete();
            $chapter->addMedia($this->filePdf->getRealPath())
                ->usingName($this->filePdf->getClientOriginalName())
                ->toMediaCollection('default', get_storage_driver());
        }

        $this->resetInputFields();
        $this->emit('quizStore');
        $this->emit('refreshupdatechapter');
    }

    public function addUser()
    {
        $validatedDate = $this->validate(
            [
                'nom' => 'required',
                'prenom' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'adresse' => 'required',
                'country' => 'required'

            ]
        );
        User::create(
            [
                'name' => $this->nom,
                'surname' => $this->prenom,
                'email' => $this->email,
                'password' => Hash::make($this->password),
                'adresse' => $this->adresse,
                'country' => $this->country,
                'classroom_id' => $this->classeId
            ]
        );
        $this->resetInputFields();
        $this->emit('quizStore');
        $this->emit('refreshadduser');
    }

    public function deleteUser($id)
    {
        if ($id) {
            User::where('id', $id)->delete();
            $this->resetInputFields();
            $this->emit('quizStore');
            $this->emit('refreshdeleteuser');
        }
    }

    public function addSubject()
    {
        $validatedDate = $this->validate(
            [
                'label' => 'required',
                'description' => 'required',


            ]
        );
        Subject::create(
            [
                'label' => $this->label,
                'description' => $this->description,
                'classroom_id' => $this->classeId
            ]
        );
        $this->resetInputFields();
        $this->emit('quizStore');
        $this->emit('refreshaddsubject');
    }


    public function updateExamQuestion()
    {


        $validatedDate = $this->validate(
            [
                'label' => 'nullable',
                'type' => 'required'


            ]
        );
        $image = $this->file;

        if (!empty($this->editExamImage)) {

            $image  = $this->storeExamImage();
        }


        $file = 0;

        if (!empty($image)) {
            $file = 1;
        }

        if ($this->examQuestionId) {
            $question = ExamQuestion::find($this->examQuestionId);
            $question->update(
                [
                    'label' => $this->label,
                    'type' => $this->type,
                    'image' => $file,
                    // 'file' => $image,
                    'scale' => $this->scale
                ]
            );

            if (!empty($this->examQuestionImage)) {
                $question->media()->delete($question->id);
                $question->addMedia($this->examQuestionImage->getRealPath())
                    ->usingName($this->examQuestionImage->getClientOriginalName())
                    ->toMediaCollection('default', get_storage_driver());
            }

            $this->resetInputFields();
            $this->emit('quizStore');
            $this->emit('refreshupdatequestion');
        }
    }

    public function updateSubject($id)
    {

        $validatedDate = $this->validate(
            [
                'label' => 'required',
                'description' => 'required',




            ]
        );


        $subject = Subject::find($id);
        $subject->update(
            [
                'label' => $this->label,
                'description' => $this->description,



            ]
        );
        $this->resetInputFields();
        $this->emit('refresheditsubject');
        $this->emit('quizStore');
    }


    public function deleteSubject($id)
    {
        if ($id) {
            Subject::where('id', $id)->delete();
            $quizs = Quiz::where('subject_id', $id)->get()->all();
            $examens = Exam::where('subject_id', $id)->get()->all();
            $lessons = Lesson::where('subject_id', $id)->get()->all();
            foreach ($quizs as $quiz) {
                $quiz->delete();
            }

            foreach ($examens as $examen) {
                $examen->delete();
            }

            foreach ($lessons as $lesson) {
                $lesson->delete();
            }
            $this->resetInputFields();
            $this->emit('quizStore');
            $this->emit('refreshdeletesubject');
        }
    }


    public function storeCorrection()
    {
        $validatedDate = $this->validate(
            [
                'label' => 'required',
                'filePdf' => 'required',
                'description' => 'required',
                'subject_id' => 'required'

            ]
        );
        $name  = Str::random() . '.pdf';
        // $link = Storage::disk('public')->put($name, $this->filePdf);

        $correction = CorrectedExamFile::create(
            [
                'label' => $this->label,
                'subject_id' => $this->subject_id,
                // 'file' => $link,
                'description' => $this->description
            ]
        );
        if (!empty($this->filePdf)) {
            //$correction->media()->delete($correction->id);
            $correction->addMedia($this->filePdf->getRealPath())
                ->usingName($this->filePdf->getClientOriginalName())
                ->toMediaCollection('default', get_storage_driver());
        }

        $this->emit('quizStore');
        $this->emit('refreshaddcorrection');
        $this->resetInputFields();
    }


    public function deleteCorrection($id)
    {
        if ($id) {


            CorrectedExamFile::where('id', $id)->delete();


            $this->emit('quizStore');
            $this->resetInputFields();
            $this->emit('refreshdeletecorrection');
        }
    }


    public function updateCorrection()
    {




        $validatedDate = $this->validate(
            [
                'label' => 'required',
                'description' => 'required',

                'subject_id' => 'required'


            ]
        );



        $pd = $this->file;

        if (!empty($this->filePdf)) {

            $name  = Str::random() . '.pdf';
            // $pd = Storage::disk('public')->put($name, $this->filePdf);
        }



        $correction = CorrectedExamFile::find($this->correctionId);
        $correction->update(
            [
                'label' => $this->label,
                // 'file' => $pd,
                'subject_id' => $this->subject_id,
                'description' => $this->description


            ]
        );

        if (!empty($this->filePdf)) {
            $correction->media()->delete($correction->id);
            $correction->addMedia($this->filePdf->getRealPath())
                ->usingName($this->filePdf->getClientOriginalName())
                ->toMediaCollection('default', get_storage_driver());
        }



        $this->resetInputFields();
        $this->emit('quizStore');
        $this->emit('refreshupdatecorrection');
    }



    public function storeBook()
    {

        $validatedDate = $this->validate(
            [
                'title' => 'required',
                'bookFile' => 'required',
                'description' => 'required',
                'price' => 'required|numeric',
                'page_number' => 'required|numeric',
                'subject_id' => 'required|numeric'


            ]
        );
        $name = $this->bookFile->getClientOriginalName();
        // $link = Storage::disk('public')->put($name, $this->bookFile);

        // dd($this->bookFile->getRealPath());

        $book = Book::create(
            [
                'title' => $this->title,
                'description' => $this->description,
                // 'file' => $link,
                'price' => $this->price,
                'page_number' => $this->page_number,
                'subject_id' => $this->subject_id,
                'book_categoriy_id' => 1
            ]
        );



        $book->addMedia($this->bookFile->getRealPath())
            ->usingName($this->bookFile->getClientOriginalName())
            ->toMediaCollection('default', get_storage_driver());



        $this->emit('quizStore');
        $this->emit('refreshaddbook');
        $this->resetInputFields();
    }

    public function deleteBook($id)
    {
        if ($id) {


            Book::where('id', $id)->delete();


            $this->emit('quizStore');
            $this->resetInputFields();
            $this->emit('refreshdeletebook');
        }
    }

    public function updateBook()
    {




        $validatedDate = $this->validate(
            [
                'title' => 'required',
                'description' => 'required',
                'price' => 'required|numeric',
                'subject_id' => 'required|numeric',
                'page_number' => 'required|numeric'
            ]
        );



        // $pd = $this->file;

        if (!empty($this->bookFile)) {

            $name = $this->bookFile->getClientOriginalName();
            // $pd = Storage::disk('public')->put($name, $this->bookFile);
            $book = Book::find($this->bookId);
            $book->update(
                [
                    'title' => $this->title,
                    // 'file' => $pd,
                    'price' => $this->price,
                    'description' => $this->description,
                    'page_number' => $this->page_number,
                    'subject_id' => $this->subject_id


                ]
            );

            $book->media()->delete($book->id);
            $book->addMedia($this->bookFile->getRealPath())
                ->usingName($this->bookFile->getClientOriginalName())
                ->toMediaCollection('default', get_storage_driver());
        }


        $book = Book::find($this->bookId);
        $book->update(
            [
                'title' => $this->title,
                // 'file' => $pd,
                'price' => $this->price,
                'description' => $this->description,
                'subject_id' => $this->subject_id,
                'page_number' => $this->page_number
            ]
        );


        $this->resetInputFields();
        $this->emit('quizStore');
        $this->emit('refreshupdatebook');
    }

    public function render()
    {
        $subjects = Subject::all();
        return view('livewire.add-question-modal', ['subjects' => $subjects]);
    }
}
