<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Classroom;
use Livewire\WithPagination;
use App\Subject;

class Classe extends Component
{
    use WithPagination;
    public $query, $perPage = 6, $label, $description, $classe_id, $classeId;
    public $updateMode = false;


    protected $listeners = ['refreshadd', 'refreshupdate', 'refreshdeleteclasse'];

    public function refreshadd()
    {

        session()->flash('message', 'Classe créée avec succès.');
    }

    public function refreshupdate()
    {

        session()->flash('message', 'Classe modifiée avec succès.');
    }

    public function refreshdeleteclasse()
    {

        session()->flash('message', 'Classe supprimée avec succès.');
    }


    public function mount()
    {
    }

    public function updatingQuery()
    {

        $this->resetPage();
    }
    private function resetInputFields()
    {
        $this->label = '';
        $this->description = '';
        $this->classe_id = '';
    }


    public function delete($id)
    {
        if ($id) {

            Classroom::where('id', $id)->delete();
            Subject::where('classroom_id', $id)->delete();
            session()->flash('message', 'Classe supprimé avec succès.');
            $this->emit('quizStore');
        }
    }

    public function edit($id)
    {

        $classe = Classroom::where('id', $id)->first();
        $this->classe_id = $id;
        $this->label = $classe->label;
        $this->description = $classe->description;

        $this->emit('classeUpdate', $this->classe_id, $this->label,  $this->description);
    }

    public function deleting($id)
    {
        $classe = Classroom::where('id', $id)->first();
        $this->emit('sendClassroomData', $id, $classe->label);
    }


    public function render()
    {
        return view('livewire.classe', ['classes' => Classroom::where('label', 'like', '%' . $this->query . '%')->latest()->paginate($this->perPage), 'classe' => Classroom::get()->all()]);
    }
}
