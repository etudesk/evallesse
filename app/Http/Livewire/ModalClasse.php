<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Classroom;
use App\Subject;
use App\Quiz;
use App\Exam;

class ModalClasse extends Component
{
    public $classe_id, $label, $description, $classeId;
    protected $listeners = ['classeUpdate', 'sendClassroomData'];

    public function classeUpdate($classeId, $label, $description)
    {
        $this->label = $label;
        $this->description = $description;
        $this->classe_id = $classeId;
    }


    public function sendClassroomData($classeId, $label)
    {
        $this->label = $label;
        $this->classeId = $classeId;
    }
    private function resetInputFields()
    {
        $this->label = '';
        $this->description = '';
        $this->classeId = '';
    }
    public function store()
    {
        $validatedDate = $this->validate([
            'label' => 'required',

            'description' => 'required',


        ]);


        Classroom::create($validatedDate);
        $this->resetInputFields();

        session()->flash('message', 'Classe créée avec succès.');
        $this->emit('refreshadd');
        $this->emit('quizStore');
    }
    public function update()
    {
        $validatedDate = $this->validate([
            'label' => 'required',
            'description' => 'required',

        ]);

        if ($this->classe_id) {
            $classe = Classroom::find($this->classe_id);
            $classe->update([
                'label' => $this->label,
                'description' => $this->description,

            ]);

            session()->flash('message', 'Classe modifié avec succes.');
            $this->resetInputFields();
            $this->emit('refreshupdate');
            $this->emit('quizStore');
        }
    }



    public function cancel()
    {
        $this->resetErrorBag();
        $this->resetInputFields();
    }


    public function deleteClasse($id)
    {
        if ($id) {
            Classroom::where('id', $id)->delete();
            $subjects = Subject::where('classroom_id', $id)->get()->all();
            Subject::where('id', $id)->delete();

            foreach ($subjects as $subject) {
                $quizs = Quiz::where('subject_id', $subject->id)->get()->all();
                $examens = Exam::where('subject_id', $subject->id)->get()->all();
                $lessons = Lesson::where('subject_id', $subject->id)->get()->all();

                foreach ($quizs as $quiz) {
                    $quiz->delete();
                }

                foreach ($examens as $examen) {
                    $examen->delete();
                }
                foreach ($lessons as $lesson) {
                    $lesson->delete();
                }
            }





            $this->resetInputFields();
            $this->emit('quizStore');
            $this->emit('refreshdeleteclasse');
        }
    }


    public function render()
    {
        return view('livewire.modal-classe');
    }
}
