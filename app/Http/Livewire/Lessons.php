<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Lesson;
use App\Subject;
use App\Classroom;
use Livewire\WithPagination;
use App\Progression;

class Lessons extends Component
{
    //subject se reffere a l'id de matiere lors du tri
    public $query, $perPage = 6, $label, $subject, $subject_id, $description, $estimate_time, $classe, $classes, $lessonId;

    protected $listeners = ['refreshaddlesson', 'refreshdeletelesson', 'refreshupdatelesson'];

    use WithPagination;


    public function mount()
    {
    }

    public function updatingQuery()
    {

        $this->resetPage();
    }

    public function updatingSubject()
    {

        $this->resetPage();
    }
    private function resetInputFields()
    {
        $this->label = '';
        $this->description = '';
    }

    public function refreshaddlesson()
    {

        session()->flash('message', 'Leçon ajoutée avec succès.');
    }

    public function refreshupdatelesson()
    {
        session()->flash('message', 'Leçon modifiée avec succès.');
    }
    public function refreshdeletelesson()
    {

        session()->flash('message', 'Leçon supprimée avec succès.');
    }


    public function   deletinglesson($id)
    {



        $lesson = Lesson::where('id', $id)->get()->first();
        $this->lessonId = $lesson->id;
        $this->emit('sendLessonData', $this->lessonId, $lesson->label);
    }


    public function editinglesson($id)
    {

        $lesson = Lesson::where('id', $id)->get()->first();
        $this->lessonId = $lesson->id;
        $this->emit('sendLessonData', $this->lessonId, $lesson->label, $lesson->description, $lesson->subject_id, $lesson->estimate_time);
    }
    public function delete($id)
    {
        if ($id) {

            Lesson::where('id', $id)->delete();
            session()->flash('message', 'Leçon supprimée avec succès.');
        }
    }



    public function render()
    {
        $le = Progression::all();
        $total = $le->sum('chapter_id');

        return view('livewire.lessons', ['lessons' => Lesson::where('label', 'like', '%' . $this->query . '%')->where('subject_id', 'like', '%' . $this->subject . '%')->latest()->paginate($this->perPage), 'lesson' => Lesson::get()->all(), 'subjects' => Subject::get()->all()]);
    }
}
