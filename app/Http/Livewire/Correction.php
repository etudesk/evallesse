<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\CorrectedExamFile;
use Livewire\WithPagination;

class Correction extends Component
{
    use WithPagination;
    public $query;
    protected $listeners = ['refreshaddcorrection', 'resetPag', 'refreshdeletecorrection', 'refreshupdatecorrection'];

    public function refreshaddcorrection()
    {
        session()->flash('message', 'Corrigé de sujet ajouté avec success.');
    }

    public function refreshupdatecorrection()
    {
        session()->flash('message', 'Corrigé de sujet modifié avec success.');
    }

    public function refreshdeletecorrection()
    {
        session()->flash('message', 'Corrigé de sujet supprimé avec success.');
    }

    public function resetPag()
    {
        $this->resetPage();
    }


    public function updatingQuery()
    {
        $this->resetPage();
    }

    public function deleting($id)
    {



        $correction = CorrectedExamFile::where('id', $id)->first();

        $this->emit('sendCorrectionData', $id, $correction->label, $correction->description, $correction->file, $correction->subject_id);
    }



    public function editing($id)
    {



        $correction = CorrectedExamFile::where('id', $id)->first();

        $this->emit('sendCorrectionData', $id, $correction->label, $correction->description, $correction->file, $correction->subject_id);
    }
    public function render()
    {
        $sujets = CorrectedExamFile::where('label', 'like', '%' . $this->query . '%')->latest()->paginate(5);
        return view('livewire.correction', ['sujets' => $sujets]);
    }
}
