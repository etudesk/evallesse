<?php

namespace App\Http\Livewire;

use App\Book as Livre;
use Livewire\Component;
use Livewire\WithPagination;

class Book extends Component
{

    use WithPagination;
    public $query;

    protected $listeners = ['refreshaddbook', 'resetPag', 'refreshdeletebook', 'refreshupdatebook'];

    public function refreshaddbook()
    {
        session()->flash('message', 'Livre ajouté avec succes.');
    }

    public function refreshupdatebook()
    {
        session()->flash('message', 'Livre mise à jour avec succes.');
    }

    public function refreshdeletebook()
    {
        session()->flash('message', 'Livre supprimé avec succes.');
    }

    public function updatingQuery()
    {
        $this->resetPage();
    }

    public function resetPag()
    {
        $this->resetPage();
    }

    public function deletingBook($id)
    {

        $book = Livre::where('id', $id)->first();


        $this->emit('sendBookData', $book->id, $book->title,  $book->description, $book->price, $book->page_number, $book->subject_id, $book->file);
    }
    public function editingBook($id)
    {

        $book = Livre::where('id', $id)->first();


        $this->emit('sendBookData', $book->id, $book->title,  $book->description, $book->price, $book->page_number, $book->subject_id, $book->file);
    }
    public function render()
    {

        $books = Livre::where('title', 'like', '%' . $this->query . '%')->latest()->paginate(6);
        return view('livewire.book', ['books' => $books]);
    }
}
