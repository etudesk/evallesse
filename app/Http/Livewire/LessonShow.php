<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Lesson;
use App\Subject;
use App\Classroom;
use App\Chapter;
use Livewire\WithPagination;

class LessonShow extends Component
{
    use WithPagination;
    public $query, $label, $subject, $subject_id, $description, $estimate_time, $classes, $lessonId;

    protected $listeners = ['refreshaddchapter', 'refreshupdatelesson', 'refreshdeletechapter', 'resetPag', 'refreshupdatechapter'];


    public function refreshaddchapter()
    {

        session()->flash('message', 'Contenu ajouté avec succes.');
    }

    public function refreshupdatechapter()
    {

        session()->flash('message', 'Contenu modifié avec succes.');
    }
    public function refreshdeletechapter()
    {
        $this->ChapterId = "";
        session()->flash('message', 'Contenu supprimé avec succes.');
    }

    public function refreshupdatelesson()
    {
        session()->flash('message', 'Leçon modifiée avec succès.');
    }
    public function mount($id)
    {
        $this->lessonId = $id;
    }
    public function addingChapter($id)
    {
        $this->lessonId = $id;
        $this->emit('sendLessonData', $this->lessonId);
    }

    public function editinglesson($id)
    {

        $lesson = Lesson::where('id', $id)->get()->first();
        $this->lessonId = $lesson->id;
        $this->emit('sendLessonData', $this->lessonId, $lesson->label, $lesson->description, $lesson->subject_id, $lesson->estimate_time);
    }


    public function editing($id)
    {

        $chapter = Chapter::where('id', $id)->first();
        $this->chapterId = $id;
        $this->emit('sendChapterData', $this->chapterId, $chapter->title, $chapter->content);
    }

    public function deleting($id)
    {



        $chapter = Chapter::where('id', $id)->first();
        $this->chapterId = $id;
        $this->emit('sendChapterData', $this->chapterId, $chapter->title, $chapter->content);
    }

    public function resetPag()
    {

        $this->resetPage();
    }

    public function render()
    {
        $lesson = Lesson::where('id', $this->lessonId)->get()->first();

        $classe = Classroom::where('id', $lesson->subject->classroom_id)->get()->first();


        return view('livewire.lesson-show', ['lesson' => $lesson, 'classe' => $classe]);
    }
}
