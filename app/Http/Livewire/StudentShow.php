<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\User;
use App\Classroom;
use Livewire\WithPagination;

class StudentShow extends Component
{
    use WithPagination;
    public $classeId, $query, $perPage = 8;

    protected $listeners = ['refreshadduser', 'refreshdeleteuser'];

    public function updatingQuery()
    {

        $this->resetPage();
    }
    public function refreshadduser()
    {
        session()->flash('message', 'Eleve ajouté avec succes.');
    }


    public function refreshdeleteuser()
    {
        session()->flash('message', 'Eleve supprimé avec succes.');
    }
    public function mount($id)
    {
        $this->classeId = $id;
    }

    public function addingUser($id)
    {
        $this->classeId = $id;
        $this->emit('sendClasseData', $this->classeId);
    }

    public function deletingUser($id)
    {
        $student = User::where('id', $id)->first();

        $this->emit('sendClasseData', $id, $student->id, $student->name);
    }
    public function render()
    {
        $student = User::where('classroom_id', $this->classeId)->where('name', 'like', '%' . $this->query . '%')->latest()->paginate($this->perPage);



        $classe = Classroom::where('id', $this->classeId)->get()->first();
        return view('livewire.student-show', ['students' => $student, 'classe' => $classe]);
    }
}
