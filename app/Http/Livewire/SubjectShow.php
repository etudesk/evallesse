<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Classroom;
use App\Subject;

class SubjectShow extends Component
{
    public $classeId, $query, $perPage = 8;
    protected $listeners = ['refreshaddsubject', 'refreshdeleteuser', 'refresheditsubject', 'refreshdeletesubject'];

    public function mount($id)
    {
        $this->classeId = $id;
    }

    public function refreshaddsubject()
    {
        session()->flash('message', 'Matière ajouté avec succes.');
    }

    public function refreshdeletesubject()
    {
        session()->flash('message', 'Matière supprimé avec succes.');
    }

    public function refresheditsubject()
    {
        session()->flash('message', 'Matière modifiée avec succes.');
    }
    public function addingSubject($id)
    {
        $this->classeId = $id;
        $this->emit('sendClasseData', $this->classeId);
    }

    public function deletingSubject($id)
    {
        $subject = Subject::where('id', $id)->first();

        $this->emit('sendSubjectData', $id);
    }

    public function editing($id)
    {
        $subject = Subject::where('id', $id)->first();

        $this->emit('sendSubjectData', $id, $subject->label, $subject->description);
    }

    public function render()
    {
        $subject = Subject::where('classroom_id', $this->classeId)->where('label', 'like', '%' . $this->query . '%')->latest()->paginate($this->perPage);

        $classe = Classroom::where('id', $this->classeId)->get()->first();
        return view('livewire.subject-show', ['classe' => $classe, 'subjects' => $subject]);
    }
}
