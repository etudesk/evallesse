<?php

namespace App\Http\Livewire\Front\Exam;

use Livewire\Component;
use Livewire\WithPagination;

class Correction extends Component
{
    use WithPagination;

    public $exam;
    public $user;
    public $goodResp;
    public $userResp;

    public $options = [];
    public $choice;

    public function mount($exam, $user)
    {
        $this->exam = $exam;
        $this->user = $user;
    }

    public function render()
    {
        $questions = $this->exam->questions()->with(['answers', 'exam'])->paginate(1);
        $this->goodResp = $questions->first()->answers()->where('correct_answer', 1)
            ->pluck('id')->toArray();

        $this->userResp = $this->user->userExamResponseCollections()->where('exam_id', $this->exam->id)->pluck('exam_answer_id')
            ->toArray();

        return view(
            'livewire.front.exam.correction',
            [
                'questions' => $questions
            ]
        );
    }
}
