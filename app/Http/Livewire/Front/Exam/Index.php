<?php

namespace App\Http\Livewire\Front\Exam;

use App\ExamQuestion;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $exam;
    public $user;


    public $options = [];
    public $choice;
    public $userExamResponsesArray = [];

    public function mount($exam, $user)
    {
        $this->exam = $exam;
        $this->user = $user;
    }

    public function addExamResponse($question_id, $question_type, $hasMorePage, ExamQuestion $q)
    {
        if ($question_type == "unique") {
            if (is_null($this->choice)) {
                return redirect()->back();
            }
        }

        if ($question_type == "multiple") {
            if (empty($this->options)) {
                return redirect()->back();
            }
        }

        $question = $q->with('answers')->where('id', $question_id)->first();

        if ($question->type === "unique") {
            $v = $question->verifyUserResponse($this->choice);
        } else {
            $v = $question->verifyUserResponse($this->options);
        }

        $good = $v['good'];

        $allreadyAddR = $this->user->userExamResponseCollections()
            ->where('exam_question_id', $question_id)
            ->where('exam_id', $this->exam->id);

        if ($allreadyAddR->get()->isNotEmpty()) {

            if ($question_type == "unique") {

                $allreadyAddR->delete();

                $userResponse = $this->user->userExamResponseCollections()->create(
                    [
                    'exam_answer_id' => $this->choice,
                    'exam_question_id' => $question_id,
                    'exam_id' => $this->exam->id,
                    'good' => $good
                    ]
                );
            } else {

                $allreadyAddR->delete();

                foreach ($this->options as $key => $option) {
                    $userResponses[] = $this->user->userExamResponseCollections()->create(
                        [
                        'exam_answer_id' => $option,
                        'exam_question_id' => $question_id,
                        'exam_id' => $this->exam->id,
                        'good' => $good
                        ]
                    );
                }
            }

        } else {

            if ($question_type == "unique") {
                $userResponse = $this->user->userExamResponseCollections()->create(
                    [
                    'exam_answer_id' => $this->choice,
                    'exam_question_id' => $question_id,
                    'exam_id' => $this->exam->id,
                    'good' => $good
                    ]
                );
            } else {
                foreach ($this->options as $key => $option) {
                    $userResponses[] = $this->user->userExamResponseCollections()->create(
                        [
                        'exam_answer_id' => $option,
                        'exam_question_id' => $question_id,
                        'exam_id' => $this->exam->id,
                        'good' => $good
                        ]
                    );
                }
            }

        }

        if ($hasMorePage) {
            $this->nextPage();
        } else {
            $url = route('front-new.home.examens.result', ['slug' => \Str::slug($this->exam->label), 'examen_id' => $this->exam->id]);
            return redirect()->to($url);
        }
    }

    public function changeInputState($q_type, $value)
    {
        if ($q_type == "unique") {
            $this->choice = $value;
        } else {
            $this->options[] = $value;
        }
    }

    public function render()
    {
        $questions = $this->exam->questions()->with(['answers', 'exam'])->paginate(1);

        return view(
            'livewire.front.exam.index', [
            'questions' => $questions
            ]
        );
    }
}
