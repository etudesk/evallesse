<?php

namespace App\Http\Livewire\Front\Quiz;

use App\QuizQuestion;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $user;
    public $quiz;

    public $options = [];
    public $choice;

    protected $listeners = ['nextPage'];

    public function mount($quiz, $user)
    {
        $this->quiz = $quiz;
        $this->user = $user;
    }

    public function addUserQuizResponse($question_id, $hasMorePage, QuizQuestion $q)
    {
        $question = $q->with('answers')->where('id', $question_id)->first();

        if ($question->type === "unique") {
            $v = $question->verifyUserResponse($this->choice);
        } else {
            $v = $question->verifyUserResponse($this->options);
        }

        $good = $v['good'];

        // dd($good);

        if ($good == true) {
            if ($question->type === "unique") {

                $userResponse = $this->user->quizAnswers()->create(
                    [
                    'quiz_answer_id' => $this->choice,
                    'quiz_question_id' => $question_id,
                    'quiz_id' => $this->quiz->id,
                    ]
                );
            } else {
                foreach ($this->options as $key => $option) {
                    $userResponses[] = $this->user->quizAnswers()->create(
                        [
                        'quiz_answer_id' => $option,
                        'quiz_question_id' => $question_id,
                        'quiz_id' => $this->quiz->id,
                        ]
                    );
                }
            }

            $this->emit('good_answers', $hasMorePage);
        } else {

            $this->emit('bad_answers', $v['explanations'], $hasMorePage);
        }
    }

    public function render()
    {
        $userCorrectAnswerNumber = $this->user->computeQuizGoodResponse($this->quiz->id);
        $questions = $this->quiz->questions()->with(['answers', 'quiz'])->paginate(1);

        return view(
            'livewire.front.quiz.index', [
            'userCorrectAnswerNumber' => $userCorrectAnswerNumber,
            'questions' => $questions
            ]
        );
    }
}
