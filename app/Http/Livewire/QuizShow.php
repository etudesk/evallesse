<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Quiz;
use App\Classroom;
use App\QuizQuestion;
use App\QuizAnswer;
use Livewire\WithPagination;


class QuizShow extends Component
{
    use WithPagination;
    public $quiz_id, $quizId, $question_id, $answer_id, $quizQuestionId;

    protected $listeners = [
        'resetPag', 'refreshadd', 'refreshupdatequestion', 'refreshdeletereponse', 'refreshdeletequestion',
        'refreshupdateoptionreponse', 'refresheditquiz', 'refreshaddoptionreponse'
    ];



    public function refreshadd()
    {

        session()->flash('message', 'Question crée avec succes.');
        flashy()->success('Question crée avec succes.', 'lien du quiz');
    }

    public function refreshaddoptionreponse()
    {
        session()->flash('message', 'Réponse crée avec succes.');
        flashy()->success('Réponse crée avec succes.', 'lien du quiz');
    }

    public function refresheditquiz()
    {

        session()->flash('message', 'Quiz modifie avec succes.');
        flashy()->success('Quiz modifie avec succes.', 'lien du quiz');
    }

    public function refreshupdateoptionreponse()
    {
        session()->flash('message', 'Option de reponse modifiée avec succes.');
        flashy()->success('Option de reponse modifiée avec succes.', 'lien du quiz');
    }
    public function refreshupdatequestion()
    {

        session()->flash('message', 'Question modifiée avec succes.');
        flashy()->success('Question modifiée avec succes.', 'lien du quiz');
    }
    public function refreshdeletereponse()
    {
        $this->answer_id = "";
        session()->flash('message', 'Reponse supprimée avec succès.');
    }

    public function refreshdeletequestion()
    {
        $this->question_id = "";
        session()->flash('message', 'Question supprimée avec succès.');
    }
    public function mount($id)
    {
        $this->quiz_id = $id;
    }



    public function resetPag()
    {

        $this->resetPage();
    }

    public function edit($id)
    {

        $classe = Quiz::where('id', $id)->first();
        $this->quizId = $id;


        $this->emit('sendQuizId', $this->quizId);
    }

    public function deleting($id)
    {



        $question = QuizQuestion::where('id', $id)->first();
        $this->question_id = $id;
        $this->emit('sendQuestionData', $this->question_id);
    }


    public function deletingOptionReponse($id)
    {



        $quizAnswer = QuizAnswer::where('id', $id)->get()->first();
        $this->answer_id = $quizAnswer->id;
        $this->emit('sendAnswerData', $this->answer_id);
    }

    public function updatingOptionReponse($id)
    {

        $quizAnswer = QuizAnswer::where('id', $id)->get()->first();
        $this->answer_id = $quizAnswer->id;

        $this->emit('sendAnswerData', $this->answer_id, $quizAnswer->label, $quizAnswer->explanation, $quizAnswer->file, $quizAnswer->image);
    }

    public function updatingQuiz($id)
    {

        $quiz = Quiz::where('id', $id)->get()->first();
        $this->quiz_id = $quiz->id;

        $this->emit('sendQuizData', $this->quiz_id, $quiz->label, $quiz->description, $quiz->subject_id, $quiz->level);
    }


    public function editing($id)
    {



        $question = QuizQuestion::where('id', $id)->first();
        $this->question_id = $id;
        $this->emit('sendQuestionData', $this->question_id, $question->label, $question->type, $question->image, $question->file);
    }


    public function addingOptionReponse($id)
    {
        $question = QuizQuestion::where('id', $id)->first();
        $this->quizQuestionId = $id;
        $this->emit('sendQuizQuestionData', $this->quizQuestionId);
    }


    public function render()
    {
        $quiz = Quiz::where('id', $this->quiz_id)->get()->first();

        $classe = Classroom::where('id', $quiz->subject->classroom_id)->get()->first();
        return view('livewire.quiz-show', ['quiz' => $quiz, 'classe' => $classe]);
    }
}
