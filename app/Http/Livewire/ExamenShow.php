<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Exam;
use App\ExamQuestion;
use App\Classroom;
use App\ExamAnswer;

class ExamenShow extends Component
{
    public $examId;

    protected $listeners =  [
        'refreshdeleteexamquestion', 'refreshaddexamquestion', 'refresheditexam',
        'refreshupdatequestion', 'refreshaddoptionreponse', 'refreshdeletereponse', 'refreshupdateoptionreponse'
    ];
    public function mount($id)
    {
        $this->examId = $id;
    }


    public function refreshdeleteexamquestion()
    {
        session()->flash('message', 'Question supprimé avec succes.');
    }

    public function refreshupdateoptionreponse()
    {
        session()->flash('message', 'Option de réponse modifiée avec succes.');
    }
    public function refreshdeletereponse()
    {
        session()->flash('message', 'Option de reponse supprimé avec succes.');
    }


    public function refreshaddoptionreponse()
    {
        session()->flash('message', 'Option de ajouté avec succes.');
    }

    public function refreshupdatequestion()
    {
        session()->flash('message', 'Question modifiée avec succes.');
    }

    public function refresheditexam()
    {
        session()->flash('message', 'Examen modifié avec succes.');
    }
    public function refreshaddexamquestion()
    {
        session()->flash('message', 'Question ajouté avec succes.');
    }



    public function updatingOptionReponse($id)
    {

        $examAnswer = ExamAnswer::where('id', $id)->get()->first();


        $this->emit('sendExamAnswerData', $id, $examAnswer->label, $examAnswer->explanation, $examAnswer->file, $examAnswer->image);
    }

    public function addingOptionReponse($id)
    {



        $question = ExamQuestion::where('id', $id)->first();

        $this->emit('sendExamQuestionData', $id);
    }

    public function deletingOptionReponse($id)
    {



        $examAnswer = ExamAnswer::where('id', $id)->get()->first();

        $this->emit('sendExamAnswerData', $id);
    }
    public function deleting($id)
    {



        $question = ExamQuestion::where('id', $id)->first();
        $this->question_id = $id;
        $this->emit('sendExamQuestionData', $this->question_id);
    }

    public function updatingExam($id)
    {

        $exam = Exam::where('id', $id)->get()->first();


        $this->emit('sendExamData', $id, $exam->label, $exam->description, $exam->subject_id, $exam->estimate_duration);
    }


    public function editExam($id)
    {

        $classe = Exam::where('id', $id)->first();



        $this->emit('sendExamData', $id);
    }

    public function editingExamQuestion($id)
    {



        $exam = ExamQuestion::where('id', $id)->first();

        $this->emit('sendExamQuestionData', $id, $exam->label, $exam->type, $exam->image, $exam->file, $exam->scale);
    }


    public function render()
    {


        $exam = Exam::where('id', $this->examId)->get()->first();

        $classe = Classroom::where('id', $exam->subject->classroom_id)->get()->first();
        return view('livewire.examen-show', ['classe' => $classe, 'examens' => $exam]);
    }
}
