<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Quiz as Quizz;
use App\Subject;
use App\Exam;

class ModalQuiz extends Component
{
    public $label, $level, $description, $subject_id,  $perPage = 6, $query, $estimate_duration;

    public function mount()
    {
        $this->level = "easy";
        $this->subject_id = "1";
    }

    private function resetInputFields()
    {
        $this->label = '';
        $this->description = '';
        $this->subject_id = '';
        $this->level = '';
        $this->estimate_duration = '';
    }


    public function cancel()
    {
        $this->resetErrorBag();
        $this->resetInputFields();
    }
    public function store()
    {
        $validatedDate = $this->validate([
            'label' => 'required',
            'level' => 'required',
            'description' => 'required',
            'subject_id' => 'required',

        ]);


        Quizz::create($validatedDate);
        $this->resetInputFields();
        flashy()->success('Quiz créée avec succès.', 'lien du quiz');
        notify()->success('Laravel Notify is awesome!');




        $this->emit('refresh');
        $this->emit('quizStore'); //permet de faire disparaitre le modaal apres soumissions
    }

    public function storeExam()
    {
        $validatedDate = $this->validate([
            'label' => 'required',
            'estimate_duration' => 'required',
            'description' => 'required',
            'subject_id' => 'required',

        ]);

        Exam::create($validatedDate);
        $this->resetInputFields();

        $this->emit('refreshexam');
        $this->emit('quizStore'); //permet de faire disparaitre le modaal apres soumissions
    }
    public function render()
    {
        $subjects = Subject::all();
        return view('livewire.admin.modal-quiz', ['subjects' => $subjects]);
    }
}
