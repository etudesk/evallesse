<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Http\Request;
use App\Quiz as Quizz;
use App\Subject;
use Livewire\WithPagination;
use App\Http\Livewire\Field;

class Quiz extends Component
{
    public $label, $level, $description, $subject_id,  $perPage = 6, $subject, $query, $updateMode = false;

    use WithPagination;

    public function mount()
    {
        $this->level = "easy";
        $this->subject_id = "1";
    }
    protected $listeners = ['refresh', 'refresheditquizpublic', 'refreshdeletereponse'];

    public function refresh()
    {

        session()->flash('message', 'Quiz créée avec succès.');
    }



    public function updatingSubject()
    {

        $this->resetPage();
    }
    public function updatingQuiz($id)
    {

        $quiz = Quizz::where('id', $id)->get()->first();
        $this->quiz_id = $quiz->id;

        $this->emit('sendQuizData', $this->quiz_id, $quiz->label, $quiz->description, $quiz->subject_id, $quiz->level);
    }


    public function refreshdeletereponse()
    {
        $this->answer_id = "";
        session()->flash('message', 'Reponse supprimée avec succès.');
    }

    public function deletingQuiz($id)
    {
        $quiz = Quizz::where('id', $id)->get()->first();
        $this->quiz_id = $quiz->id;
        $this->emit('sendQuizData', $this->quiz_id, $quiz->label);
    }


    public function refresheditquizpublic()
    {


        session()->flash('message', 'Quiz modifié avec succes.');
    }
    public function updatingQuery()
    {

        $this->resetPage();
    }
    private function resetInputFields()
    {
        $this->label = '';
        $this->description = '';
    }


    public function delete($id)
    {
        if ($id) {

            Quizz::where('id', $id)->delete();
            session()->flash('message', 'Quiz supprimé avec succès.');
        }
    }


    public function store()
    {
        $validatedDate = $this->validate([
            'label' => 'required',
            'level' => 'required',
            'description' => 'required',
            'subject_id' => 'required',

        ]);


        Quizz::create($validatedDate);
        $this->resetInputFields();
        notify()->success('Laravel Notify is awesome!');
        session()->flash('message', 'Quiz créée avec succès.');
    }
    public function render()
    {
        return view('livewire.admin.quiz', ['quiz' => Quizz::where('label', 'like', '%' . $this->query . '%')->where('subject_id', 'like', '%' . $this->subject . '%')->latest()->paginate($this->perPage), 'quizz' => Quizz::get()->all(), 'subjects' => Subject::get()->all()]);;
    }
}
