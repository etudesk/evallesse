<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;
use App\Quiz as Quizz;
use App\Subject;
use App\Exam;
use Livewire\WithPagination;
use App\Http\Livewire\Field;

class Examen extends Component
{
    public $label, $level, $description, $subject_id,  $perPage = 6, $subject, $query, $updateMode = false, $estimate_duration;

    use WithPagination;

    public function mount()
    {
        $this->level = "easy";
        $this->subject_id = "1";
    }
    protected $listeners = ['refreshexam', 'refresheditquizpublic', 'refreshdeleteexam', 'refresheditexam'];


    public function refreshexam()
    {

        session()->flash('message', 'Examen créée avec succès.');
    }



    public function updatingSubject()
    {

        $this->resetPage();
    }
    public function updatingExam($id)
    {

        $exam = Exam::where('id', $id)->get()->first();
        $this->examId = $exam->id;

        $this->emit('sendExamData', $this->examId, $exam->label, $exam->description, $exam->subject_id, $exam->estimate_duration);
    }
    public function refreshdeleteexam()
    {

        session()->flash('message', 'Examen supprimée avec succès.');
    }

    public function deletingExam($id)
    {
        $exam = Exam::where('id', $id)->get()->first();
        $this->examId = $exam->id;
        $this->emit('sendExamData', $this->examId, $exam->label);
    }


    public function refresheditexam()
    {


        session()->flash('message', 'Examen modifié avec succes.');
    }
    public function updatingQuery()
    {

        $this->resetPage();
    }
    private function resetInputFields()
    {
        $this->label = '';
        $this->description = '';
    }


    public function delete($id)
    {
        if ($id) {

            Quizz::where('id', $id)->delete();
            session()->flash('message', 'Quiz supprimé avec succès.');
        }
    }


    public function store()
    {
        $validatedDate = $this->validate([
            'label' => 'required',
            'level' => 'required',
            'description' => 'required',
            'subject_id' => 'required',

        ]);


        Quizz::create($validatedDate);
        $this->resetInputFields();

        session()->flash('message', 'Quiz créée avec succès.');
    }
    public function render()
    {
        return view('livewire.examen', ['examen' => Exam::where('label', 'like', '%' . $this->query . '%')->where('subject_id', 'like', '%' . $this->subject . '%')->latest()->paginate($this->perPage), 'examens' => Exam::get()->all(), 'subjects' => Subject::get()->all()]);;
    }
}
