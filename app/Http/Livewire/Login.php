<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class Login extends Component
{
    public $form = [
        'email'   => '',
        'password' => '',
    ];

    public function submit()
    {
        $this->validate([
            'form.email'    => 'required|email',
            'form.password' => 'required',
        ], [
            'form.email.required' => 'Le champ email est obligatoire',
            'form.email.email' => 'Le champ email doit être un email valide',
            'form.password.required' => 'Le champ mot de passe est obligatoire',
        ]);

        $attempt = Auth::guard('admin')->attempt($this->form);

        if ($attempt == true) {
            return redirect(route('admin.dashboard'));
        } else {
            $error = $this->addError('form.email', trans('auth.failed'));
            redirect()->back()->withErrors($error);
        }
    }

    public function render()
    {
        return view('livewire.login');
    }
}
