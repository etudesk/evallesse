<?php

namespace App\Http\Controllers\FrontNew;

use App\Chapter;
use App\Http\Controllers\Controller;
use App\Lesson;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function show(Request $request, $slug, $lesson_id, $l_id)
    {
        $user = auth()->user();
        $chapter = Lesson::where('id', $lesson_id)->first();
        $lesson = Chapter::where('id', $l_id)->first();

        if (is_null($chapter)) {
            flashy()->error('Ce cours n\'existe pas !');
            return redirect()->back();
        }

        return view('front-new.courses.show', compact('chapter', 'user', 'lesson'));
    }

    public function computeProgress(Request $request)
    {
        $user = auth()->user();

        $validator = \Validator::make(
            $request->all(),
            [
                'c_id' => 'required',
                'l_id' => 'required',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back();
        }

        $progress = $user->progress()->where('lesson_id', $request->get('l_id'))
            ->where('chapter_id', $request->get('c_id'))->first();

        if (is_null($request->get('valid'))) {

            if (is_null($progress)) {
                flashy()->error('Coche la case marquer comme terminé pour avancer');
                return redirect()->back();
            }

            $progress->delete();

            flashy()->success('Ta progression a bien été supprimée !');
            return redirect()->back();
        }

        if (!is_null($progress)) {
            return redirect()->to(route('front-new.home.courses'));
        }

        $progress = $user->progress()->create(
            [
                'lesson_id' => $request->get('l_id'),
                'chapter_id' => $request->get('c_id')
            ]
        );

        $route = $progress->lesson->getProgressLink($user);

        if ($user->progress()->count() == $progress->lesson->chapters()->count()) {
            flashy()->success('Félicitations ! Tu as fini cette Leçon !');
        } else {
            flashy()->success('Félicitations ! Tu as fini cette Leçon !');
        }

        // return redirect()->to($route);
        return redirect()->to(route('front-new.home.courses'));
    }
}
