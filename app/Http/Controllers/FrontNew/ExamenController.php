<?php

namespace App\Http\Controllers\FrontNew;

use App\Exam;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExamenController extends Controller
{
    public function show(Request $request, $slug, $examen_id)
    {
        $user = auth()->user();
        $exam = Exam::where('id', $examen_id)->first();

        if (is_null($exam)) {
            flashy()->error('Cet examen n\'existe pas !');
            return redirect()->back();
        }

        if ($request->get('retake') == 1) {
            $user->userExamResponseCollections()
                ->where('exam_id', $exam->id)
                ->delete();
        }

        return view('front-new.examens.show', compact('exam', 'user'));
    }

    public function correction(Request $request, $slug, $examen_id)
    {
        $user = auth()->user();
        $exam = Exam::where('id', $examen_id)->first();

        if (is_null($exam)) {
            flashy()->error('Cet examen n\'existe pas !');
            return redirect()->back();
        }

        return view('front-new.examens.correction', compact('exam', 'user'));
    }

    public function result(Request $request, $slug, $examen_id)
    {
        $user = auth()->user();
        $exam = Exam::with('questions')->where('id', $examen_id)->first();

        if (is_null($exam)) {
            flashy()->error('Cet examen n\'existe pas !');
            return redirect()->back();
        }

        $userGoodResponse = $user->userExamResponseCollections()->where('good', 1)->pluck('exam_question_id')->toArray();
        $userNote = $exam->questions()->whereIn('id', $userGoodResponse)->sum('scale');
        $noteTotalScale = $exam->questions()->sum('scale');

        $percent = $userNote/$noteTotalScale*100;

        return view('front-new.examens.result', compact('exam', 'user', 'userNote', 'noteTotalScale', 'percent'));
    }
}
