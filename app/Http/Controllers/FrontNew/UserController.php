<?php

namespace App\Http\Controllers\FrontNew;

use App\Exam;
use App\Quiz;
use App\Lesson;
use App\Subject;
use App\Progression;
use App\CorrectedExamFile;
use Illuminate\Http\Request;
use App\Http\Livewire\Examen;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Spatie\MediaLibrary\Models\Media;

class UserController extends Controller
{
    private $user;

    public function home()
    {
        $user = auth()->user();
        /* $courses = $user->progress()->get()->unique('lesson_id');
        $quizzes = $user->quizAnswers()->get()->unique('quiz_id'); */

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();
        $subjects = Subject::whereIn('id', $userPreferences)->orderBy('label', 'asc')->get();

        return view('front-new.home', compact('subjects', 'user'));
    }

    public function courses(Request $request)
    {
        $user = auth()->user();

        $search = is_null($request->get('search')) ? "" : $request->get('search');
        $subject = is_null($request->get('subject')) ? $user->preferences()->first()->subject->id : $request->get('subject');

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();
        $subjects = Subject::whereIn('id', $userPreferences)->orderBy('label', 'asc')->get();

        if (!in_array($subject, $userPreferences)) {
            flashy()->error('Vous n\'avez pas accès à cette matière');
            return redirect()->back();
        }

        $c = Lesson::where('subject_id', $subject)
            ->when($search ?? null ,function($q) use ($search, $subject) {
                $q->where('subject_id', $subject)
                    ->where(function($query) use ($search) {
                        $query->where('label', 'LIKE', '%'.$search.'%')
                            ->orWhere('description', 'LIKE', '%'.$search.'%');
                    });
            })->get()->sortBy('label');

        $chapters = $this->paginateCollection($c, 9);

        return view('front-new.courses.index', compact('chapters', 'user', 'search', 'subject', 'subjects'));
    }

    public function quizs(Request $request)
    {
        $user = auth()->user();

        $search = is_null($request->get('search')) ? "" : $request->get('search');
        $subject = is_null($request->get('subject')) ? $user->preferences()->first()->subject->id : $request->get('subject');

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();
        $subjects = Subject::whereIn('id', $userPreferences)->orderBy('label', 'asc')->get();

        if (!in_array($subject, $userPreferences)) {
            flashy()->error('Vous n\'avez pas accès à cette matière');
            return redirect()->back();
        }

        $q = Quiz::where('subject_id', $subject)
            ->when($search ?? null ,function($q) use ($search, $subject) {
                $q->where('subject_id', $subject)
                    ->where(function($query) use ($search) {
                        $query->where('label', 'LIKE', '%'.$search.'%')
                            ->orWhere('description', 'LIKE', '%'.$search.'%');
                    });
            })->orderBy('description')->get();

        $quizzes = $this->paginateCollection($q, 9);

        return view('front-new.quizs.index', compact('quizzes', 'user', 'search', 'subject', 'subjects'));
    }

    public function examens(Request $request)
    {
        $user = auth()->user();

        $search = is_null($request->get('search')) ? "" : $request->get('search');
        $subject = is_null($request->get('subject')) ? $user->preferences()->first()->subject->id : $request->get('subject');

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();
        $subjects = Subject::whereIn('id', $userPreferences)->orderBy('label', 'asc')->get();

        if (!in_array($subject, $userPreferences)) {
            flashy()->error('Vous n\'avez pas accès à cette matière');
            return redirect()->back();
        }

        $e = Exam::where('subject_id', $subject)
            ->when($search ?? null ,function($q) use ($search, $subject) {
                $q->where('subject_id', $subject)
                    ->where(function($query) use ($search) {
                        $query->where('label', 'LIKE', '%'.$search.'%')
                            ->orWhere('description', 'LIKE', '%'.$search.'%');
                    });
            })->get()->sortBy('label');

        $exams = $this->paginateCollection($e, 9);

        return view('front-new.examens.index', compact('exams', 'user', 'search', 'subject', 'subjects'));
    }

    public function files(Request $request)
    {
        $user = auth()->user();

        $search = is_null($request->get('search')) ? "" : $request->get('search');
        $subject = is_null($request->get('subject')) ? $user->preferences()->first()->subject->id : $request->get('subject');

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();
        $subjects = Subject::whereIn('id', $userPreferences)->orderBy('label', 'asc')->get();

        if (!in_array($subject, $userPreferences)) {
            flashy()->error('Vous n\'avez pas accès à cette matière');
            return redirect()->back();
        }

        $examSubjects = CorrectedExamFile::where('subject_id', $subject)
            ->when($search ?? null ,function($q) use ($search, $subject) {
                $q->where('subject_id', $subject)
                    ->where(function($query) use ($search) {
                        $query->where('label', 'LIKE', '%'.$search.'%')
                            ->orWhere('description', 'LIKE', '%'.$search.'%');
                    });
            })
            ->orderBy('label', 'desc')->paginate(9);

        return view('front-new.files.index', compact('examSubjects', 'user', 'search', 'subject', 'subjects'));
    }

    public function filesDownload($es_id, Media $mediaItem)
    {
        $user = auth()->user();

        $examSubjects = CorrectedExamFile::where('id', $es_id)->first();

        $mediaT = $examSubjects->getFirstMedia();
        $media = $mediaItem->where('id', $mediaT->id)->first();

        flashy()->success('Sujet télécharger avec succès !');
        return $media;
    }

    public function profile()
    {
        return view('front-new.profile.index');
    }

    public function profileMake(Request $request)
    {
        $user = auth()->user();

        $validator = \Validator::make(
            $request->all(), [
                'name' => 'required',
                'tel' => 'unique:users,tel,'.$user->id
            ]
        );

        if ($validator->fails()) {
            flashy()->error($validator->errors()->first());
            return redirect()->back();
        }

        $user->name = $request->get('name');
        $user->country = $request->get('country');
        $user->email = $request->get('email');
        $user->city = $request->get('city');
        $user->gender = $request->get('gender');

        $user->save();

        flashy()->success("Les modifications ont bien été effectuées !");
        return redirect()->back();
    }

    public function preference()
    {
        $user = auth()->user();
        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();
        $subjects = Subject::whereIn('id', $userPreferences)->orderBy('label', 'asc')->get();

        $subjects = Subject::where('classroom_id', $user->classroom_id)->get();

        return view('front-new.profile.preference', compact('subjects', 'userPreferences'));
    }

    public function modifyPreference(Request $request)
    {
        $user = auth()->user();

        $validator = \Validator::make(
            $request->all(), [
                'subject_id' => 'required|array'
            ]
        );

        if ($validator->fails()) {
            flashy()->error($validator->errors()->first());
            return redirect()->back();
        }

        $user->preferences()->delete();

        foreach ($request->get('subject_id') as $key => $s_id) {
            $user->preferences()->create(
                [
                    'subject_id' => $s_id
                ]
            );
        }

        flashy()->success("Les préférences ont bien été modifiées !");
        return redirect()->back();
    }

    public function password()
    {
        return view('front-new.profile.password');
    }

    public function modifyPassword(Request $request)
    {
        $user = auth()->user();

        $validator = \Validator::make(
            $request->all(), [
                'old_password' => 'required',
                'password' => 'required|confirmed|min:6',
            ]
        );

        if ($validator->fails()) {
            flashy()->error($validator->errors()->first());
            return redirect()->back();
        }

        if (! Hash::check($request->old_password, $user->password)) {
            flashy()->error("Le mot de passe ne corresponds pas ! Réprenez SVP !");
            return redirect()->back()->withInput();
        }

        $user->password = bcrypt($request->get('password'));
        $user->save();

        flashy()->success('Le mot de passe a bien été modifié.');
        return redirect()->back();
    }
}
