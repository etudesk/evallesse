<?php

namespace App\Http\Controllers\FrontNew;

use App\Http\Controllers\Controller;
use App\Quiz;
use Illuminate\Http\Request;

class QuizController extends Controller
{
    public function show(Request $request, $slug, $quiz_id)
    {
        $user = auth()->user();
        $quiz = Quiz::where('id', $quiz_id)->first();

        if (is_null($quiz)) {
            flashy()->error('Ce quiz n\'existe pas !');
            return redirect()->back();
        }

        return view('front-new.quizs.show', compact('quiz', 'user'));
    }
}
