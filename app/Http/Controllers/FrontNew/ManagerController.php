<?php

namespace App\Http\Controllers\FrontNew;

use Auth;
use App\Book;
use App\Lesson;
use App\Subject;
use App\Mail\OrderBook;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class ManagerController extends Controller
{

    private $user;

    public function index()
    {
        return view('front-new.index');
    }

    public function showSignupPreferences()
    {
        return view('front-new.auth.preferences');
    }

    public function signupPreferences(Request $request)
    {
        $user = auth()->user();

        $validator = \Validator::make(
            $request->all(), [
            'subject_id' => 'required|array'
            ]
        );

        if ($validator->fails()) {
            flashy()->error('Selectionne au moins une matière');
            return redirect()->back();
        }

        $subjectIds = $request->get('subject_id');
        $userClassroomSubjectIds = Subject::where('classroom_id', $user->classroom_id)->pluck('id')->toArray();

        foreach ($subjectIds as $key => $subjectId) {
            if (in_array($subjectId, $userClassroomSubjectIds)) {
                $user->preferences()->create(
                    [
                    'subject_id' => $subjectId
                    ]
                );
            }
        }

        flashy()->success('Parfait ! Commence ton apprentissage !');

        return redirect()->route('front-new.home');
    }


    public function about()
    {
        return view('front-new.about');
    }

    public function activityBook()
    {
        $books = Book::orderBy('updated_at', 'desc')->paginate(8);

        return view('front-new.activity-book')->with(
            [
                "books" => $books
            ]
        );
    }


    public function activityBooking(Request $request)
    {
        $validator = \Validator::make(
            $request->all(), [
            "idBook" => 'required',
            "name" => 'required',
            "email" => 'required|email',
            "phone" => 'required|min:6',
            "quantity" => 'required|numeric'
            ]
        );

        if ($validator->fails()) {
            flashy()->error('Remplissez correctement le formulaire');
            return redirect()->back();
        }

        $book = Book::find($request->get('idBook'));

        $data = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'quantity' => $request->get('quantity'),
            'observation' => $request->get('observation'),
            'book' => $book
        ];

        // Send mail here
        Mail::send(new OrderBook($data));

        flashy()->success('Commande en cours de traitement...');
        return redirect()->back();
    }

    public function home()
    {
        return view('front-new.home');
    }

    public function courses()
    {

    }

    public function quizs()
    {
        return view('front-new.quizs.index');
    }

    public function examens()
    {
        return view('front-new.examens.index');
    }

    public function files()
    {
        return view('front-new.files.index');
    }
}
