<?php

namespace App\Http\Controllers\Admin;

use App\QuizAnswer;
use App\QuizQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuizQuestionController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('quiz_questions');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {



    $data1 = [
      'label' => $request->label,
      'type' =>  $request->type,
      'quiz_id' =>  $request->quiz_id,


    ];


    QuizQuestion::create($data1);

    //taile du tableau

    for ($i = 0; $i < count($request->reponse); $i++) {
      $correct_answer = 0;
      $response = $request->reponse[$i];
      $explanation = $request->explication[$i];

      if (isset($request->correct_answer[$i + 1])) {
        $correct_answer = 1;
      }

      $quiz_question = QuizQuestion::latest()->first();
      $data = [
        'label' => $response,
        'explanation' =>  $explanation,
        'correct_answer' =>  $correct_answer,

        'quiz_question_id' =>  $quiz_question->id,
      ];
      QuizAnswer::create($data);
    }

    flashy()->success('Questions créée avec succès.', 'lien du quiz');
    return redirect('/quiz')->with('success', 'Questions créée avec succès');
  }


  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
  }
}
