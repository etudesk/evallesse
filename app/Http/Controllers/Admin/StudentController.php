<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StudentController extends Controller
{
    //return view student
    public function index()
    {
        return view('administrator.student.index');
    }

    //return show student
    public function show()
    {
        return view('administrator.student.show');
    }
}
