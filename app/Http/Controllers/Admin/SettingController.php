<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    //return dashboard page
    public function index()
    {
        return view('administrator.setting.index');
    }
}
