<?php

namespace App\Http\Controllers\Admin;

use App\Quiz;
use App\User;
use Illuminate\Http\Request;
use App\Exam;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        $data = DB::table('users')
            ->selectRaw('count(*) as nombre, extract(month from users.created_at) as month')
            ->groupBy('month')
            ->pluck('nombre', 'month');

        $data =  array_replace(array_fill_keys(range(1, 12), 0), $data->all());

        $eleves = User::all();
        $quizNb = Quiz::count();
        $examNb = Exam::count();

        return view('administrator.dashboard.index', compact('data', 'eleves', 'quizNb', 'examNb'));
    }
}
