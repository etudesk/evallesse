<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResetPasswordController extends Controller
{
    // return password forgotten page
    public function index()
    {
        return view('administrator.password_forgotten.index');
    }
}
