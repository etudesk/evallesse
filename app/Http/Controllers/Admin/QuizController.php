<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Quiz as Quizz;
use App\Classroom;
use App\Http\Controllers\Controller;

class QuizController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  //return view quiz
  public function index()
  {
    $quiz = Quizz::all();
    return view('administrator.quiz.index', compact('quiz'));
  }



  //return view quiz test
  public function test($id)
  {

    $quiz = Quizz::all();
    return view('administrator.quiz.test', compact('quiz', 'id'));
  }


  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
  }


  /**
   * Store a newly created resource in storage.
   *
   * @return Response
   */
  public function store(Request $request)
  {
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {

    return view('administrator.quiz.show');
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)
  {
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
  }
}
