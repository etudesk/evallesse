<?php

namespace App\Http\Controllers\Api\V1;

use App\Quiz;
use App\Subject;
use App\QuizQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class QuizController extends Controller
{
    public function list(Request $request, $subject_id)
    {
        $user = Auth::guard('api')->user();

        $subjectObject = Subject::where('id', $subject_id)->first();

        if (is_null($subjectObject)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Cette matière n\'existe pas'
            ]);
        }

        $search = is_null($request->get('search')) ? "" : $request->get('search');
        $subject = $subject_id;

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();

        if (!in_array($subject, $userPreferences)) {
            return Response::json([
                'status_code' => 403,
                'status_message' => 'Vous n\'avez pas accès à cette matière'
            ]);
        }

        $q = Quiz::where('subject_id', $subject)
            ->when($search ?? null, function ($q) use ($search, $subject) {
                $q->where('subject_id', $subject)
                    ->where(function ($query) use ($search) {
                        $query->where('label', 'LIKE', '%' . $search . '%')
                            ->orWhere('description', 'LIKE', '%' . $search . '%');
                    });
            })->orderBy('description')->get();

        $quizzes = $this->paginateCollection($q, 9);

        $quizzesCollection = collect([]);

        foreach ($quizzes->items() as $key => $quiz) {
            $goodQuizResponse = $user->computeQuizGoodResponse($quiz->id);

            $quiz->showUrl = route('api.quiz.questions', ['quiz_id' => $quiz->id]);
            $quiz->questionNb = $quiz->questions()->count();
            $quiz->isStarted = $goodQuizResponse > 0 ? true : false;
            $quiz->isFinished = $goodQuizResponse == $quiz->questionNb ? true : false;
            $quiz->questionLeftNb = $quiz->questionNb - $goodQuizResponse;
            $quizzesCollection->push($quiz);
        }

        $data = [
            'current_page' => $quizzes->currentPage(),
            'items' => $quizzesCollection->toArray(),
            'per_page' => $quizzes->perPage(),
            'total' => $quizzes->total()
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Liste des quizzes',
            'data' => $data
        ]);
    }

    public function getQuestions(Request $request, $quiz_id)
    {
        $user = Auth::guard('api')->user();
        $quiz = Quiz::where('id', $quiz_id)->first();

        if (is_null($quiz)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Ce quiz n\'existe pas !'
            ]);
        }

        $quiz->userCorrectAnswerNb = $user->computeQuizGoodResponse($quiz->id);

        $questions = $quiz->questions()->with(['answers', 'quiz'])->get();

        $quizQuestions = collect([]);

        foreach ($questions as $key => $question) {

            $uGoodResp = $question->getUserResponses($user)->pluck('quiz_answer_id')->toArray();

            $question->userHasChecked = !empty($uGoodResp) ? true : false;
            $question->validateUrl = $question->userHasChecked == false ? route('api.quiz.validate-response', ['quiz_id' => $quiz->id, 'question_id' => $question->id]) : null;
            $question->userGoodGiveAnswerIdArray = $uGoodResp;

            $optElCollection = collect([]);

            foreach ($question->answers as $key => $a) {

                $a->illustration_type = !is_null($a->getFirstMedia()) ? 'image' : null;
                $a->illustration = !is_null($a->getFirstMedia()) ? $a->getFirstMedia()->getFullUrl() : null;
                $a->unsetRelation('media');

                $optElCollection->push($a);
            }

            $question->allOptions = $optElCollection->toArray();
            $question->illustration_type = !is_null($question->getFirstMedia()) ? 'image' : null;
            $question->illustration = !is_null($question->getFirstMedia()) ? $question->getFirstMedia()->getFullUrl() : null;

            $question->unsetRelation('media');
            $question->unsetRelation('answers');
            $question->unsetRelation('quiz');

            $quizQuestions->push($question);
        }

        $quiz->questionsObject = $quizQuestions->toArray();

        $data = [
            'quiz' => $quiz,
            'headerText' => $quiz->title,
            'subHeaderText' => $quiz->description
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'ok',
            'data' => $data
        ]);
    }

    public function addUserQuizResponse(Request $request, $quiz_id, $question_id, QuizQuestion $q)
    {
        $user = Auth::guard('api')->user();
        $quiz = Quiz::where('id', $quiz_id)->first();

        if (is_null($quiz)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Ce quiz n\'existe pas !'
            ]);
        }

        $question = $q->with('answers')->where('id', $question_id)->first();

        if ($question->type === "unique") {
            $validator = \Validator::make($request->all(), [
                'choice' => 'required',
            ]);

            if ($validator->fails()) {
                return Response::json([
                    'status_code' => 400,
                    'status_message' => $validator->errors()->first()
                ]);
            }

            $v = $question->verifyUserResponse($request->choice);
        } else {

            $validator = \Validator::make($request->all(), [
                'options' => 'required|array|min:1',
            ]);

            if ($validator->fails()) {
                return Response::json([
                    'status_code' => 400,
                    'status_message' => $validator->errors()->first()
                ]);
            }

            $v = $question->verifyUserResponse($request->options);
        }

        $good = $v['good'];


        if ($good == true) {

            if ($question->type === "unique") {

                $userResponse = $user->quizAnswers()->create(
                    [
                        'quiz_answer_id' => $request->choice,
                        'quiz_question_id' => $question_id,
                        'quiz_id' => $quiz->id,
                    ]
                );
            } else {
                foreach ($request->options as $key => $option) {
                    $userResponses[] = $user->quizAnswers()->create(
                        [
                            'quiz_answer_id' => $option,
                            'quiz_question_id' => $question_id,
                            'quiz_id' => $quiz->id,
                        ]
                    );
                }
            }
        }

        $quiz->userCorrectAnswerNb = $user->computeQuizGoodResponse($quiz->id);

        $questions = $quiz->questions()->with(['answers', 'quiz'])->get();

        $quizQuestions = collect([]);

        foreach ($questions as $key => $question) {

            $uGoodResp = $question->getUserResponses($user)->pluck('quiz_answer_id')->toArray();

            $question->userHasChecked = !empty($uGoodResp) ? true : false;
            $question->validateUrl = $question->userHasChecked == false ? route('api.quiz.validate-response', ['quiz_id' => $quiz->id, 'question_id' => $question->id]) : null;
            $question->userGoodGiveAnswerIdArray = $uGoodResp;

            $optElCollection = collect([]);

            foreach ($question->answers as $key => $a) {

                $a->illustration_type = !is_null($a->getFirstMedia()) ? 'image' : null;
                $a->illustration = !is_null($a->getFirstMedia()) ? $a->getFirstMedia()->getFullUrl() : null;
                $a->unsetRelation('media');

                $optElCollection->push($a);
            }

            $question->allOptions = $optElCollection->toArray();
            $question->illustration_type = !is_null($question->getFirstMedia()) ? 'image' : null;
            $question->illustration = !is_null($question->getFirstMedia()) ? $question->getFirstMedia()->getFullUrl() : null;

            $question->unsetRelation('media');
            $question->unsetRelation('answers');
            $question->unsetRelation('quiz');

            $quizQuestions->push($question);
        }

        $quiz->questionsObject = $quizQuestions->toArray();

        $data = [
            'quiz' => $quiz,
            'headerText' => $quiz->title,
            'subHeaderText' => $quiz->description,
            'explanations' => $good == true ? null : $v['explanations'][0]
        ];

        if ($good == true) {
            return Response::json([
                'status_code' => 200,
                'status_message' => 'good_answers',
                'data' => $data
            ]);
        }

        return Response::json([
            'status_code' => 200,
            'status_message' => 'bad_answers',
            'data' => $data
        ]);
    }
}
