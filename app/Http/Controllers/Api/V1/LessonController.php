<?php

namespace App\Http\Controllers\Api\V1;

use App\Lesson;
use App\Chapter;
use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class LessonController extends Controller
{
    public function list(Request $request, $subject_id)
    {
        $user = Auth::guard('api')->user();

        $subjectObject = Subject::where('id', $subject_id)->first();

        if (is_null($subjectObject)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Cette matière n\'existe pas'
            ]);
        }

        $search = is_null($request->get('search')) ? "" : $request->get('search');
        $subject = $subject_id;

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();

        if (!in_array($subject, $userPreferences)) {
            return Response::json([
                'status_code' => 403,
                'status_message' => 'Vous n\'avez pas accès à cette matière'
            ]);
        }

        $c = Lesson::where('subject_id', $subject)
            ->when($search ?? null, function ($q) use ($search, $subject) {
                $q->where('subject_id', $subject)
                    ->where(function ($query) use ($search) {
                        $query->where('label', 'LIKE', '%' . $search . '%')
                            ->orWhere('description', 'LIKE', '%' . $search . '%');
                    });
            })->get()->sortBy('label');

        $chapters = $this->paginateCollection($c, 9);

        $chaptersCollection = collect([]);

        foreach ($chapters->items() as $key => $chapter) {
            $isCompleted = $chapter->getProgressPercent($user) == 100;
            $chapter->urlShow = $chapter->getProgressLinkApi($user);

            $chapter->validateDate = $isCompleted == true ? $chapter->getLastProgressDate($user) : null;
            $chapter->isCompleted = $isCompleted;
            $chaptersCollection->push($chapter);
        }

        $data = [
            'current_page' => $chapters->currentPage(),
            'items' => $chaptersCollection->toArray(),
            'per_page' => $chapters->perPage(),
            'total' => $chapters->total()
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Liste des leçons',
            'data' => $data
        ]);
    }

    public function index(Request $request, $lesson_id, $c_id)
    {
        $user = Auth::guard('api')->user();
        $chapter = Lesson::where('id', $lesson_id)->first();
        $lesson = Chapter::where('id', $c_id)->first();

        if (is_null($chapter)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Ce cours n\'existe pas !'
            ]);
        }

        $lesson->toggleMarkUrl = route('api.compute-lesson-progress', ['lesson_id' => $chapter->id, 'c_id' => $lesson->id]);
        $lesson->quizListUrl = route('api.quiz.list', ['subject_id' => $chapter->subject_id, 'search' => $chapter->description]);
        $lesson->isMark = $lesson->isValidateL($user, $chapter->id);
        $lesson->pdfContent = !is_null($lesson->getFirstMedia()) ? $lesson->getFirstMedia()->getFullUrl() : null;

        $lesson->unsetRelation('media');

        $data = [
            'lesson' => $lesson,
            'headerText' => $lesson->description,
            'subHeaderText' => $lesson->title,
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Détails Leçon',
            'data' => $data
        ]);
    }

    public function computeProgress(Request $request, $lesson_id, $c_id)
    {
        $user = Auth::guard('api')->user();

        $chapter = Lesson::where('id', $lesson_id)->first();

        if (is_null($chapter)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Ce cours n\'existe pas !'
            ]);
        }

        $progress = $user->progress()->where('lesson_id', $lesson_id)
            ->where('chapter_id', $c_id)->first();

        if (!is_null($progress)) {
            $progress->delete();

            $message = 'Ta progression a bien été supprimée !';
        } else {

            $progress = $user->progress()->create(
                [
                    'lesson_id' => $lesson_id,
                    'chapter_id' => $c_id
                ]
            );

            $message = 'Félicitations ! Tu as fini cette Leçon !';
        }

        $data = [
            'quizListUrl' => route('api.quiz.list', ['subject_id' => $chapter->subject_id, 'search' => $chapter->description]),
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => $message,
            'data' => $data
        ]);
    }
}
