<?php

namespace App\Http\Controllers\Api\V1;

use App\Subject;
use App\CorrectedExamFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Support\Facades\Response;

class FileController extends Controller
{
    public function list(Request $request, $subject_id)
    {
        $user = Auth::guard('api')->user();

        $subjectObject = Subject::where('id', $subject_id)->first();

        if (is_null($subjectObject)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Cette matière n\'existe pas'
            ]);
        }

        $search = is_null($request->get('search')) ? "" : $request->get('search');
        $subject = $subject_id;

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();

        if (!in_array($subject, $userPreferences)) {
            return Response::json([
                'status_code' => 403,
                'status_message' => 'Vous n\'avez pas accès à cette matière'
            ]);
        }

        $examSubjects = CorrectedExamFile::where('subject_id', $subject)
            ->when($search ?? null, function ($q) use ($search, $subject) {
                $q->where('subject_id', $subject)
                    ->where(function ($query) use ($search) {
                        $query->where('label', 'LIKE', '%' . $search . '%')
                            ->orWhere('description', 'LIKE', '%' . $search . '%');
                    });
            })
            ->orderBy('label', 'desc')->paginate(9);

        $examSubjectsCollection = collect([]);

        foreach ($examSubjects->items() as $key => $examSubject) {
            $examSubject->downloadLink = route('api.file.download', ['file_id' => $examSubject->id]);
            $examSubjectsCollection->push($examSubject);
        }

        $data = [
            'current_page' => $examSubjects->currentPage(),
            'items' => $examSubjectsCollection->toArray(),
            'per_page' => $examSubjects->perPage(),
            'total' => $examSubjects->total()
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Liste des sujets d\'examen',
            'data' => $data
        ]);
    }

    public function download(Request $request, $file_id, Media $mediaItem)
    {
        $user = Auth::guard('api')->user();

        $examSubjects = CorrectedExamFile::where('id', $file_id)->first();

        if (is_null($examSubjects)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Cette sujet d\'examen n\'existe pas'
            ]);
        }

        $mediaT = $examSubjects->getFirstMedia();
        $media = $mediaItem->where('id', $mediaT->id)->first();

        if (is_null($media)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Ce fichier n\'existe pas'
            ]);
        }

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Sujet télécharger avec succès !',
            'data' => $media->getFullUrl()
        ]);
    }
}
