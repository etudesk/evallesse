<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class PreferenceController extends Controller
{
    public function subjectsList()
    {
        $user = Auth::guard('api')->user();
        $subjectsArray = get_subject_list($user->classroom_id)->toArray();

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Liste des classes',
            'data' => $subjectsArray
        ]);
    }

    public function add(Request $request)
    {
        $user = Auth::guard('api')->user();

        $validator = \Validator::make(
            $request->all(),
            [
                'subject_id' => 'required|array|min:1',
                'subject_id.*' => 'required',
            ]
        );

        if ($validator->fails()) {
            return Response::json([
                'status_code' => 400,
                'status_message' => 'Selectionne au moins une matière'
            ]);
        }

        $subjectIds = $request->get('subject_id');
        $userClassroomSubjectIds = Subject::where('classroom_id', $user->classroom_id)->pluck('id')->toArray();

        foreach ($subjectIds as $key => $subjectId) {
            if (in_array($subjectId, $userClassroomSubjectIds)) {
                $user->preferences()->create(
                    [
                        'subject_id' => $subjectId
                    ]
                );
            }
        }

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Parfait ! Commence ton apprentissage !',
            'data' => []
        ]);
    }
}
