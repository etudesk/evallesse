<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\User;
use App\Models\Instructor;
use Illuminate\Http\Request;
use App\Models\TrainingInvitation;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Auth\Events\PasswordReset;
use App\Notifications\VerifyAccountOnMobileApp;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'tel' => ['required', 'string', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'classroom_id' => ['required']
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status_code' => 400,
                'status_message' => $validator->errors()->first()
            ]);
        }

        $user = User::create(
            [
                'name' => $request->name,
                'tel' => $request->tel,
                'classroom_id' => $request->classroom_id,
                'password' => Hash::make($request->password)
            ]
        );

        $code = unique_code_digit();

        $user->validation_code = $code;
        $user->save();

        // Send SMS
        $sms = app('orange-sms');
        $msg = 'Evallesse - Le code activation de votre compte est le suivant : ' . $code;

        $telFormat = formatTel($request->tel);

        $res = $sms->message($msg)
            ->from(env('SENDER_NUMBER'), "Evallesse")
            ->to($telFormat)
            ->send();

        \Log::info($res);

        // Send Email
        if (!is_null($user->email)) {
            $user->notify(new VerifyAccountOnMobileApp($code));
        }

        if ($user->account_state == 0) {
            $token = null;
        } else {
            $token = $user->createToken("token")->accessToken;
        }

        $token = $user->createToken("token")->accessToken;

        $user->avatar = is_null($user->getFirstMedia('avatars')) ? "https://ui-avatars.com/api/?background=2196f3&size=100&color=fff&name=" . $user->name : $user->getFirstMedia('avatars')->getFullUrl();
        $user->unsetRelation('media');

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Inscription ok',
            'data' => [
                'token' => $token,
                'user' => $user
            ]
        ]);
    }

    public function sendActivatedAccountSmsTel(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'tel' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status_code' => 400,
                'status_message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('tel', $request->tel)->first();

        if (is_null($user)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => "Utilisateur introuvable"
            ]);
        }

        $code = unique_code_digit();

        $user->validation_code = $code;
        $user->save();

        // Send SMS
        $sms = app('orange-sms');
        $msg = 'Evallesse - Le code activation de votre compte est le suivant : ' . $code;

        $telFormat = formatTel($request->tel);

        $res = $sms->message($msg)
            ->from(env('SENDER_NUMBER'), "Evallesse")
            ->to($telFormat)
            ->send();

        \Log::info($res);

        // Send email
        $user->notify(new VerifyAccountOnMobileApp($code));

        return Response::json([
            'status_code' => 0,
            'status_message' => 'Votre code d\'activation a bien été envoyé.',
            'data' => []
        ]);
    }

    public function sendActivatedAccountSms(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'user_id' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status_code' => 400,
                'status_message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('id', $request->user_id)->first();

        if (is_null($user)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => "Utilisateur introuvable"
            ]);
        }

        $code = unique_code_digit();

        $user->validation_code = $code;
        $user->save();

        // Send SMS
        $sms = app('orange-sms');
        $msg = 'Evallesse - Le code activation de votre compte est le suivant : ' . $code;

        $telFormat = formatTel($user->tel);

        $res = $sms->message($msg)
            ->from(env('SENDER_NUMBER'), "Evallesse")
            ->to($telFormat)
            ->send();

        \Log::info($res);

        // Send email
        $user->notify(new VerifyAccountOnMobileApp($code));

        return Response::json([
            'status_code' => 0,
            'status_message' => 'Votre code d\'activation a bien été envoyé.',
            'data' => []
        ]);
    }

    public function activedAccount(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'tel' => 'required',
            'code' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status_code' => 400,
                'status_message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('tel', $request->tel)->where('validation_code', $request->code)->first();

        if (is_null($user)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => "Ce code d'activation de compte est incorrect."
            ]);
        }

        $user->validation_code = null;
        $user->account_state = 1;
        $user->save();

        $token = $user->createToken("token")->accessToken;

        $user->avatar = is_null($user->getFirstMedia('avatars')) ? "https://ui-avatars.com/api/?background=2196f3&size=100&color=fff&name=" . $user->name : $user->getFirstMedia('avatars')->getFullUrl();
        $user->unsetRelation('media');

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Votre compte a bien été activé.',
            'data' => [
                'token' => $token,
                'user' => $user
            ]
        ]);
    }

    public function verifyResetPasswordCode(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'tel' => 'required',
            'code' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status_code' => 400,
                'status_message' => $validator->errors()->first()
            ]);
        }

        $user = User::where('tel', $request->tel)->where('reset_password_code', $request->code)->first();

        if (is_null($user)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => "Ce code de réinitialisation du mot de passe de ce compte est incorrect."
            ]);
        }

        $user->reset_password_code = null;
        $user->save();

        $token = $user->createToken("token")->accessToken;

        $user->avatar = is_null($user->getFirstMedia('avatars')) ? "https://ui-avatars.com/api/?background=2196f3&size=100&color=fff&name=" . $user->name : $user->getFirstMedia('avatars')->getFullUrl();
        $user->unsetRelation('media');

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Code de réinitialisation correcte',
            'data' => [
                'token' => $token,
                'user' => $user
            ]
        ]);
    }

    public function resetPassword(Request $request)
    {
        $user = Auth::guard('api')->user();

        $validator = \Validator::make($request->all(), [
            'password' => 'required|same:password_confirmation',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status_code' => 400,
                'status_message' => $validator->errors()->first()
            ]);
        }

        $password = $request->password;

        $user->password = Hash::make($password);
        $user->setRememberToken(\Str::random(60));
        $user->save();

        event(new PasswordReset($user));

        $token = $user->createToken("token")->accessToken;

        $user->avatar = is_null($user->getFirstMedia('avatars')) ? "https://ui-avatars.com/api/?background=2196f3&size=100&color=fff&name=" . $user->name : $user->getFirstMedia('avatars')->getFullUrl();
        $user->unsetRelation('media');

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Mot de passe réinitialisé avec succès.',
            'data' => [
                'token' => $token,
                'user' => $user
            ]
        ]);
    }
}
