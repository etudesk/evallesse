<?php

namespace App\Http\Controllers\Api\V1\Auth;

use App\User;
use Throwable;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Response;
use App\Notifications\SendResetPasswordCode;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'tel' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status_code' => 400,
                'status_message' => $validator->errors()->first()
            ]);
        }

        $credentials = $request->validate([
            'tel' => 'required',
            'password' => 'required'
        ]);

        if (!Auth::attempt($credentials)) {
            return Response::json([
                'status_code' => 401,
                'status_message' => trans('auth.failed')
            ]);
        }

        $user = Auth::user();

        if ($user->account_state == 0) {

            // Auth::logout($user);

            return Response::json([
                'status_code' => 204,
                'status_message' => 'Ce compte n\'est pas encore activé.'
            ]);
        }

        // Deconnecter l'utilisateur de toute session en amont
        // $user->tokens()->delete();

        $token = $user->createToken("token")->accessToken;

        $user->avatar = is_null($user->getFirstMedia('avatars')) ? "https://ui-avatars.com/api/?background=2196f3&size=100&color=fff&name=" . $user->name : $user->getFirstMedia('avatars')->getFullUrl();
        $user->unsetRelation('media');

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Authentification ok',
            'data' => [
                'token' => $token,
                'user' => $user
            ]
        ]);
    }

    public function sendResetPasswordCode(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'tel' => 'required'
        ]);

        if ($validator->fails()) {
            return Response::json([
                'status_code' => 400,
                'status_message' => $validator->errors()->first()
            ]);
        }

        $response = $this->sendResetSMS(['tel' => $request->tel], $request);

        if ($response == 'passwords.sms-sent') {
            return Response::json([
                'status_code' => 200,
                'status_message' => trans($response),
                'data' => []
            ]);
        }

        return Response::json([
            'status_code' => 400,
            'status_message' => trans($response)
        ]);
    }

    public function logout()
    {
        Auth::guard('api')->user()->token()->revoke();

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Déconnexion',
            'data' => []
        ]);
    }

    protected function sendResetSMS($credentials, $request)
    {
        $tel = $credentials['tel'];
        $timeToWaitInSecond = 60;
        $numberOfAttemptToPermit = 3;

        $sessionKey = 'user-attempt-' . Str::slug($tel);

        if (session()->has($sessionKey) && session()->get($sessionKey)['tel'] == $tel) {
            $oldSessionValue = (array) session()->get($sessionKey);

            if ($oldSessionValue['attempNumber'] > $numberOfAttemptToPermit && $oldSessionValue['time']->isPast()) {
                session()->forget($sessionKey);
            }
        }

        if (session()->has($sessionKey) && session()->get($sessionKey)['tel'] == $tel) {
            $oldSessionValue = (array) session()->get($sessionKey);

            if ($oldSessionValue['attempNumber'] > $numberOfAttemptToPermit && !$oldSessionValue['time']->isPast()) {
                return 'passwords.throttled';
            }

            $newSessionValue = [
                'tel' => $tel,
                'attempNumber' => (int) $oldSessionValue['attempNumber'] + 1,
                'time' => Carbon::now()->addSeconds($timeToWaitInSecond)
            ];

            session()->put($sessionKey, $newSessionValue);
        }

        $user = User::where('tel', $tel)->first();

        if (is_null($user)) {
            return 'passwords.tel-user';
        }

        $codeForReset = unique_code_digit();
        $user->reset_password_code = $codeForReset;
        $user->save();

        // Send SMS
        $sms = app('orange-sms');
        $msg = 'Evallesse - Le code de reinitialisation de votre mot de passe est le suivant : ' . $codeForReset;

        $telFormat = formatTel($tel);

        $res = $sms->message($msg)
            ->from(env('SENDER_NUMBER'), "Evallesse")
            ->to($telFormat)
            ->send();

        \Log::info($res);

        // Send email
        if (!is_null($user->email)) {
            $user->notify(new SendResetPasswordCode($codeForReset));
        }

        if (!session()->has($sessionKey)) {
            $sessionValue = [
                'tel' => $tel,
                'attempNumber' => 1,
                'time' => Carbon::now()->addSeconds($timeToWaitInSecond)
            ];

            session()->put($sessionKey, $sessionValue);
        }

        return 'passwords.sms-sent';
    }
}
