<?php

namespace App\Http\Controllers\Api\V1;

use App\Exam;
use App\Subject;
use App\ExamQuestion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ExamenController extends Controller
{
    public function list(Request $request, $subject_id)
    {
        $user = Auth::guard('api')->user();

        $subjectObject = Subject::where('id', $subject_id)->first();

        if (is_null($subjectObject)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Cette matière n\'existe pas'
            ]);
        }

        $search = is_null($request->get('search')) ? "" : $request->get('search');
        $subject = $subject_id;

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();

        if (!in_array($subject, $userPreferences)) {
            return Response::json([
                'status_code' => 403,
                'status_message' => 'Vous n\'avez pas accès à cette matière'
            ]);
        }

        $e = Exam::where('subject_id', $subject)
            ->when($search ?? null, function ($q) use ($search, $subject) {
                $q->where('subject_id', $subject)
                    ->where(function ($query) use ($search) {
                        $query->where('label', 'LIKE', '%' . $search . '%')
                            ->orWhere('description', 'LIKE', '%' . $search . '%');
                    });
            })->get()->sortBy('label');

        $exams = $this->paginateCollection($e, 9);

        $examsCollection = collect([]);

        $userGoodResponse = $user->userExamResponseCollections()->where('good', 1)->pluck('exam_question_id')->toArray();

        foreach ($exams as $key => $exam) {

            $exam->hasPassExam = $exam->userHasPassExam($user);

            if ($exam->hasPassExam) {
                $userNote = $exam->questions()->whereIn('id', $userGoodResponse)->sum('scale');
                $noteTotalScale = $exam->questions()->sum('scale');
            }
            $exam->showUrl = $exam->hasPassExam == true ? route('api.examen.result', ['examen_id' => $exam->id]) : route('api.examen.questions', ['examen_id' => $exam->id]);
            $exam->questionNb = $exam->questions()->count();
            $exam->userNote = $exam->hasPassExam == true ? ($userNote . '/' . $noteTotalScale) : null;
            $examsCollection->push($exam);
        }

        $data = [
            'current_page' => $exams->currentPage(),
            'items' => $examsCollection->toArray(),
            'per_page' => $exams->perPage(),
            'total' => $exams->total()
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Liste des exams',
            'data' => $data
        ]);
    }

    public function getQuestions(Request $request, $examen_id)
    {
        $user = Auth::guard('api')->user();
        $exam = Exam::where('id', $examen_id)->first();

        if (is_null($exam)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Cet examen n\'existe pas !'
            ]);
        }

        if ($request->get('retake') == 1) {
            $user->userExamResponseCollections()
                ->where('exam_id', $exam->id)
                ->delete();
        }

        $questions = $exam->questions()->with(['answers', 'exam'])->get();

        $examQuestions = collect([]);

        foreach ($questions as $key => $question) {
            $uResp = $question->getUserResponses($user)->pluck('exam_answer_id')->toArray();

            $question->userHasChecked = !empty($uResp) ? true : false;
            $question->userGivenAnswerIdArray = $uResp;

            $optElCollection = collect([]);

            foreach ($question->answers as $key => $a) {

                $a->illustration_type = !is_null($a->getFirstMedia()) ? 'image' : null;
                $a->illustration = !is_null($a->getFirstMedia()) ? $a->getFirstMedia()->getFullUrl() : null;
                $a->unsetRelation('media');

                $optElCollection->push($a);
            }

            $question->allOptions = $optElCollection->toArray();
            $question->illustration_type = !is_null($question->getFirstMedia()) ? 'image' : null;
            $question->illustration = !is_null($question->getFirstMedia()) ? $question->getFirstMedia()->getFullUrl() : null;

            $question->unsetRelation('media');
            $question->unsetRelation('answers');
            $question->unsetRelation('exam');

            $examQuestions->push($question);
        }

        $exam->addResponseUrl = route('api.examen.add-responses', ['examen_id' => $exam->id]);
        $exam->questionsObject = $examQuestions->toArray();

        $data = [
            'exam' => $exam,
            'headerText' => $exam->label,
            'subHeaderText' => null
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'ok',
            'data' => $data
        ]);
    }

    public function addResponses(Request $request, $examen_id)
    {
        $user = Auth::guard('api')->user();
        $exam = Exam::where('id', $examen_id)->first();

        if (is_null($exam)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Cet examen n\'existe pas !'
            ]);
        }

        // Unique choice
        $choice = (array) $request->get('choice');

        // Multiple options
        $options = (array) $request->get('options');

        $user->userExamResponseCollections()->where('exam_id', $exam->id)->delete();

        foreach ($choice as $key => $c) {
            $question = ExamQuestion::where('id', $key)->with('answers')->first();
            $v = $question->verifyUserResponse($c);
            $good = $v['good'];
            if ($c) {
                $userResponse = $user->userExamResponseCollections()->create(
                    [
                        'exam_answer_id' => $c,
                        'exam_question_id' => $key,
                        'exam_id' => $exam->id,
                        'good' => $good
                    ]
                );
            }
        }

        foreach ($options as $key => $option) {
            $question = ExamQuestion::where('id', $key)->with('answers')->first();
            $v = $question->verifyUserResponse($option);
            $good = $v['good'];

            foreach ($option as $k => $opt) {
                if ($opt) {
                    $userResponses[] = $user->userExamResponseCollections()->create(
                        [
                            'exam_answer_id' => $opt,
                            'exam_question_id' => $key,
                            'exam_id' => $exam->id,
                            'good' => $good
                        ]
                    );
                }
            }
        }

        $userGoodResponse = $user->userExamResponseCollections()->where('good', 1)->pluck('exam_question_id')->toArray();
        $userNote = $exam->questions()->whereIn('id', $userGoodResponse)->sum('scale');
        $noteTotalScale = $exam->questions()->sum('scale');

        $percent = ceil($userNote / $noteTotalScale * 100);

        $note = $userNote . "/" . $noteTotalScale;
        $observations = $percent > 50 ? "Félicitations ! Vous réussir cette examen. Excellent Travail !" : "Désolé ! Vous n'avez réussir cette examen. Préparez-vous davantage.";

        $exam->userNote = $note;
        $exam->workObservations = $observations;
        $exam->showCorrectionUrl = route('api.examen.correction', ['examen_id' => $exam->id]);
        $exam->showRetakeUrl = route('api.examen.questions', ['examen_id' => $exam->id, 'retake' => 1]);

        $data = [
            'exam' => $exam,
            'headerText' => $exam->label,
            'subHeaderText' => null
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'ok',
            'data' => $data
        ]);
    }

    public function result(Request $request, $examen_id)
    {
        $user = Auth::guard('api')->user();
        $exam = Exam::where('id', $examen_id)->first();

        if (is_null($exam)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Cet examen n\'existe pas !'
            ]);
        }

        $userGoodResponse = $user->userExamResponseCollections()->where('good', 1)->pluck('exam_question_id')->toArray();
        $userNote = $exam->questions()->whereIn('id', $userGoodResponse)->sum('scale');
        $noteTotalScale = $exam->questions()->sum('scale');

        $percent = ceil($userNote / $noteTotalScale * 100);

        $note = $userNote . "/" . $noteTotalScale;
        $observations = $percent > 50 ? "Félicitations ! Vous réussir cette examen. Excellent Travail !" : "Désolé ! Vous n'avez réussir cette examen. Préparez-vous davantage.";

        $exam->userNote = $note;
        $exam->workObservations = $observations;
        $exam->showCorrectionUrl = route('api.examen.correction', ['examen_id' => $exam->id]);
        $exam->showRetakeUrl = route('api.examen.questions', ['examen_id' => $exam->id, 'retake' => 1]);

        $data = [
            'exam' => $exam,
            'headerText' => $exam->label,
            'subHeaderText' => null
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'ok',
            'data' => $data
        ]);
    }

    public function correction(Request $request, $examen_id)
    {
        $user = Auth::guard('api')->user();
        $exam = Exam::where('id', $examen_id)->first();

        if (is_null($exam)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Cet examen n\'existe pas !'
            ]);
        }

        $questions = $exam->questions()->with(['answers', 'exam'])->get();
        $userResp = $user->userExamResponseCollections()->where('exam_id', $exam->id)->pluck('exam_answer_id')
            ->toArray();

        $examQuestions = collect([]);

        foreach ($questions as $key => $question) {

            $goodResp = $question->answers()->where('correct_answer', 1)
                ->pluck('id')->toArray();

            $optElCollection = collect([]);

            foreach ($question->answers as $key => $a) {

                $a->chooseByUser = in_array($a->id, $userResp);
                $a->userFoundValue = in_array($a->id, $goodResp);

                $a->illustration_type = !is_null($a->getFirstMedia()) ? 'image' : null;
                $a->illustration = !is_null($a->getFirstMedia()) ? $a->getFirstMedia()->getFullUrl() : null;
                $a->unsetRelation('media');

                $optElCollection->push($a);
            }

            $question->allOptions = $optElCollection->toArray();
            $question->illustration_type = !is_null($question->getFirstMedia()) ? 'image' : null;
            $question->illustration = !is_null($question->getFirstMedia()) ? $question->getFirstMedia()->getFullUrl() : null;

            $question->unsetRelation('media');
            $question->unsetRelation('answers');
            $question->unsetRelation('exam');

            $examQuestions->push($question);
        }

        $exam->questionsObject = $examQuestions->toArray();

        $data = [
            'exam' => $exam,
            'headerText' => $exam->label,
            'subHeaderText' => null
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'ok',
            'data' => $data
        ]);
    }
}
