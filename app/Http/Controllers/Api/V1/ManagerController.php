<?php

namespace App\Http\Controllers\Api\V1;

use App\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ManagerController extends Controller
{
    public function userPreferSubjects()
    {
        $user = Auth::guard('api')->user();

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();
        $subjects = Subject::whereIn('id', $userPreferences)->orderBy('label', 'asc')->get();

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Liste des préférences de l\'élève',
            'data' => $subjects->toArray()
        ]);
    }

    public function classroomList()
    {
        $classrooms = get_classroom_list();

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Liste des classes',
            'data' => $classrooms->toArray()
        ]);
    }

    public function userProgressbySubject($subject_id)
    {
        $user = Auth::guard('api')->user();
        $subject = Subject::where('id', $subject_id)->first();

        if (is_null($subject)) {
            return Response::json([
                'status_code' => 404,
                'status_message' => 'Cette matière n\'existe pas'
            ]);
        }

        $lessonIdsArray = $subject->lessons()->pluck('id')->toArray();
        $nbFLesson = $user->getFollowLessonNbBy($lessonIdsArray);
        $nbLesson = $subject->lessons()->count();
        $lessonProgess = [
            'label' => $nbFLesson . "/" . $nbLesson . " Terminée" . ($nbFLesson > 1 ? "s" : ""),
            'percent' => ceil($nbFLesson / ($nbLesson == 0 ? 1 : $nbLesson) * 100)
        ];

        $quizIdsArray = $subject->quizs()->pluck('id')->toArray();
        $nbFQuiz = $user->getFollowQuizNbBy($quizIdsArray);
        $nbQuiz = $subject->quizs()->count();
        $quizProgess = [
            'label' => $nbFQuiz . "/" . $nbQuiz . " Terminé" . ($nbFQuiz > 1 ? "s" : ""),
            'percent' => ceil($nbFQuiz / ($nbQuiz == 0 ? 1 : $nbQuiz) * 100)
        ];

        $examIdsArray = $subject->exams()->pluck('id')->toArray();
        $nbFExam = $user->getFollowExamNbBy($examIdsArray);
        $nbExam = $subject->exams()->count();
        $examenProgress = [
            'label' => $nbFExam . "/" . $nbExam . " Terminé" . ($nbFExam > 1 ? "s" : ""),
            'percent' => ceil($nbFExam / ($nbExam == 0 ? 1 : $nbExam) * 100)
        ];

        $data = [
            'subject' => $subject,
            'progressions' => [
                'lesson' => $lessonProgess,
                'quiz' => $quizProgess,
                'examen' => $examenProgress,
            ]
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Progression ok',
            'data' => $data
        ]);
    }

    public function profile()
    {
        $user = Auth::guard('api')->user();

        $user->avatar = is_null($user->getFirstMedia('avatars')) ? "https://ui-avatars.com/api/?background=2196f3&size=100&color=fff&name=" . $user->name : $user->getFirstMedia('avatars')->getFullUrl();
        $user->unsetRelation('media');

        $data = [
            'user' => $user,
            'classroom' => $user->classroom
        ];

        return Response::json([
            'status_code' => 200,
            'status_message' => 'Profile ok',
            'data' => $user
        ]);
    }

    public function preferSubjectContentListByType($type)
    {
        $user = Auth::guard('api')->user();

        $userPreferences = $user->preferences()->pluck('subject_id')->toArray();
        $subjects = Subject::whereIn('id', $userPreferences)->orderBy('label', 'asc')->get();

        $data = $this->getListUrlBySubject($subjects, $type);

        return Response::json([
            'status_code' => 200,
            'status_message' => 'ok',
            'data' => $data
        ]);
    }

    public function getListUrlBySubject($subjects, $type)
    {
        if (!in_array($type, ['lesson', 'quiz', 'file', 'examen'])) {
            return [];
        }

        $links = collect([]);

        foreach ($subjects as $key => $subject) {

            $routeName = 'api.' . $type . '.list';

            $el = [
                'label' => $subject->label,
                'link' => route($routeName, ['subject_id' => $subject->id]),
            ];

            $links->push($el);
        }

        return $links->toArray();
    }
}
