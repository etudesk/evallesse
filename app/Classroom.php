<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model 
{

    protected $table = 'classrooms';
    public $timestamps = true;
    protected $fillable = array('label', 'description');

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function subjects()
    {
        return $this->hasMany('App\Subject');
    }

}