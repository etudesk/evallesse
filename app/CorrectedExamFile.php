<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class CorrectedExamFile extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $table = 'corected_exam_files';
    public $timestamps = true;
    protected $fillable = array('label', 'description', 'subject_id', 'file');

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('default')
            ->singleFile();
    }
}
