<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{

    protected $table = 'lessons';
    public $timestamps = true;
    protected $fillable = array('description', 'label', 'subject_id', 'estimate_time');

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function chapters()
    {
        return $this->hasMany('App\Chapter');
    }

    public function getGlobalProgress()
    {
        $courseSectionIds = $this->chapters()->count();
        $lessonCompleteItems = Progression::select(\DB::raw('user_id, count(*) as user_progess'))->where('lesson_id', $this->id)->groupBy('user_id')->get();
        $classCompleteItems = $lessonCompleteItems->sum('user_progess');
        $completeItemUserNb = UserSubject::where('subject_id', $this->subject_id)->count();

        $progress = (($classCompleteItems * 100) / ($courseSectionIds == 0 ? 1 : $courseSectionIds)) / ($completeItemUserNb == 0 ? 1 : $completeItemUserNb);

        return ceil($progress);
    }

    public function getProgressPercent($user)
    {
        $courseSectionIds = $this->chapters()->count();

        $userLastProgress = $user->progress()->where('lesson_id', $this->id)->count();

        return ceil($userLastProgress / ($courseSectionIds == 0 ? 1 : $courseSectionIds) * 100);
    }

    public function getLastProgressDate($user)
    {
        $userLastProgress = $user->progress()->where('lesson_id', $this->id)->orderBy('created_at', 'desc')->first();

        return $userLastProgress->created_at->format('d/m/Y');
    }

    public function getProgressLink($user)
    {
        $courseSectionIds = $this->chapters()->pluck('id')->toArray();
        $sectionNbKey = count($courseSectionIds) - 1;

        $userLastProgress = $user->progress()->where('lesson_id', $this->id)->orderBy('created_at', 'desc')->first();

        if (is_null($userLastProgress)) {
            if (count($courseSectionIds) > 0) {
                return route('front-new.home.courses.show', ['slug' => \Str::slug($this->label), 'lesson_id' => $this->id, 'l_id' => $courseSectionIds[0]]);
            } else {
                return "#";
            }
        }

        $key = array_search($userLastProgress->chapter_id, $courseSectionIds);

        if ($key == $sectionNbKey) {
            return route('front-new.home.courses.show', ['slug' => \Str::slug($this->label), 'lesson_id' => $this->id, 'l_id' => $courseSectionIds[$key]]);
        }

        return route('front-new.home.courses.show', ['slug' => \Str::slug($this->label), 'lesson_id' => $this->id, 'l_id' => $courseSectionIds[$key + 1]]);
    }

    public function getProgressLinkApi($user)
    {
        $courseSectionIds = $this->chapters()->pluck('id')->toArray();
        $sectionNbKey = count($courseSectionIds) - 1;

        $userLastProgress = $user->progress()->where('lesson_id', $this->id)->orderBy('created_at', 'desc')->first();

        if (is_null($userLastProgress)) {
            if (count($courseSectionIds) > 0) {
                return route('api.specific-lesson', ['lesson_id' => $this->id, 'c_id' => $courseSectionIds[0]]);
            } else {
                return "#";
            }
        }

        $key = array_search($userLastProgress->chapter_id, $courseSectionIds);

        if ($key == $sectionNbKey) {
            return route('api.specific-lesson', ['lesson_id' => $this->id, 'c_id' => $courseSectionIds[$key]]);
        }

        return route('api.specific-lesson', ['lesson_id' => $this->id, 'c_id' => $courseSectionIds[$key + 1]]);
    }

    public function isInProgress($user)
    {
        $userLastProgress = $user->progress()->where('lesson_id', $this->id)->get();

        if (count($userLastProgress) == 0) {
            return false;
        }

        return true;
    }
}
