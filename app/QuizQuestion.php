<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class QuizQuestion extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $table = 'quiz_questions';
    public $timestamps = true;
    protected $fillable = array('label', 'type', 'image', 'quiz_id', 'file');

    public function quiz()
    {
        return $this->belongsTo(Quiz::class, 'quiz_id');
    }

    public function answers()
    {
        return $this->hasMany('App\QuizAnswer', 'quiz_question_id');
    }

    public function userResponses()
    {
        return $this->hasMany('App\UserQuizResponse', 'quiz_question_id');
    }

    public function getUserResponses($user)
    {
        $responses = $this->userResponses()->where('user_id', $user->id)->get();

        return $responses;
    }

    public function verifyUserResponse($response)
    {
        if ($this->type === "unique") {

            $good_responses = $this->answers()->where('correct_answer', 1)
                ->pluck('id')->first();

            $explanations = $this->answers()->where('id', $response)->pluck('explanation')->toArray();
        } else {

            $good_responses = $this->answers()->where('correct_answer', 1)
                ->pluck('id')->toArray();

            $explanations = $this->answers()->whereIn('id', $response)->pluck('explanation')->toArray();
        }

        if ($response == $good_responses) {
            return ['good' => true];
        }

        return ['good' => false, 'explanations' => $explanations];
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('default')
            ->singleFile();
    }
}
