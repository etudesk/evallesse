<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\QuizQuestion;
use App\Subject;

class Quiz extends Model
{

    protected $table = 'quizs';
    public $timestamps = true;
    protected $fillable = array('label', 'level', 'subject_id', 'description');

    public function questions()
    {
        return $this->hasMany('App\QuizQuestion');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function getProgressPercent($user)
    {
        $quizQuestionNb = $this->questions()->count();

        $userProgressCount = $user->computeQuizGoodResponse($this->id);

        return ceil($userProgressCount / ($quizQuestionNb == 0 ? 1 : $quizQuestionNb) * 100);
    }
}
