-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 14 sep. 2020 à 10:32
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `evalesse1`
--

-- --------------------------------------------------------

--
-- Structure de la table `admins`
--

DROP TABLE IF EXISTS `admins`;
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `books`
--

DROP TABLE IF EXISTS `books`;
CREATE TABLE IF NOT EXISTS `books` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL,
  `book_categoriy_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `books_book_categoriy_id_foreign` (`book_categoriy_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `books`
--

INSERT INTO `books` (`id`, `title`, `file`, `description`, `price`, `book_categoriy_id`, `created_at`, `updated_at`) VALUES
(18, 'Soeurs de sang', 'livre2.PNG/4gpdRiaWuFHH4gpB5LGtFyl0IzayUF1wdTYbcKF3.png', 'test', 2000, 1, '2020-09-11 02:56:01', '2020-09-11 03:14:59');

-- --------------------------------------------------------

--
-- Structure de la table `book_categories`
--

DROP TABLE IF EXISTS `book_categories`;
CREATE TABLE IF NOT EXISTS `book_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `book_orders`
--

DROP TABLE IF EXISTS `book_orders`;
CREATE TABLE IF NOT EXISTS `book_orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `book_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `book_orders_user_id_foreign` (`user_id`),
  KEY `book_orders_book_id_foreign` (`book_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `chapters`
--

DROP TABLE IF EXISTS `chapters`;
CREATE TABLE IF NOT EXISTS `chapters` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pdf` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lesson_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `chapters_lesson_id_foreign` (`lesson_id`)
) ENGINE=MyISAM AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `chapters`
--

INSERT INTO `chapters` (`id`, `created_at`, `updated_at`, `title`, `content`, `pdf`, `lesson_id`) VALUES
(43, '2020-09-14 10:15:42', '2020-09-14 10:21:06', 'chap 3', '<div>tes</div>', 'HaSbp5V3ZJzFanFt.pdf/wLVCtPxIDdhyXTe2anskqDiolOrVGmav1iDk9Stw.pdf', 17);

-- --------------------------------------------------------

--
-- Structure de la table `classrooms`
--

DROP TABLE IF EXISTS `classrooms`;
CREATE TABLE IF NOT EXISTS `classrooms` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `classrooms`
--

INSERT INTO `classrooms` (`id`, `label`, `description`, `created_at`, `updated_at`) VALUES
(31, 'Terminale', 'termo', '2020-09-07 12:40:44', '2020-09-07 12:40:44');

-- --------------------------------------------------------

--
-- Structure de la table `corected_exam_files`
--

DROP TABLE IF EXISTS `corected_exam_files`;
CREATE TABLE IF NOT EXISTS `corected_exam_files` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `corected_exam_files_subject_id_foreign` (`subject_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `corected_exam_files`
--

INSERT INTO `corected_exam_files` (`id`, `label`, `description`, `file`, `subject_id`, `created_at`, `updated_at`) VALUES
(8, 'sujet 5', 'test', '3XkReaeqCIjFWDYm.pdf/SChW1PkrDfeTPI6kqX44KUpLiMqmVALR0nubjfpU.pdf', 19, '2020-09-14 10:24:38', '2020-09-14 10:27:43');

-- --------------------------------------------------------

--
-- Structure de la table `exams`
--

DROP TABLE IF EXISTS `exams`;
CREATE TABLE IF NOT EXISTS `exams` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estimate_duration` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exams_subject_id_foreign` (`subject_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `exams`
--

INSERT INTO `exams` (`id`, `label`, `description`, `estimate_duration`, `subject_id`, `created_at`, `updated_at`) VALUES
(9, 'exam', 'desc', '3h', 19, '2020-09-07 13:15:34', '2020-09-07 13:15:34');

-- --------------------------------------------------------

--
-- Structure de la table `exam_answers`
--

DROP TABLE IF EXISTS `exam_answers`;
CREATE TABLE IF NOT EXISTS `exam_answers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` tinyint(1) NOT NULL DEFAULT 0,
  `file` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `explanation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `explanation_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correct_answer` tinyint(1) NOT NULL DEFAULT 0,
  `exam_question_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_answers_exam_question_id_foreign` (`exam_question_id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `exam_answers`
--

INSERT INTO `exam_answers` (`id`, `label`, `image`, `file`, `explanation`, `explanation_type`, `correct_answer`, `exam_question_id`, `created_at`, `updated_at`) VALUES
(34, 'reponse 2', 0, '', '', NULL, 0, 28, '2020-09-11 16:50:16', '2020-09-11 16:50:16'),
(35, 'hh', 0, '', 'er', NULL, 0, 29, '2020-09-11 16:59:38', '2020-09-11 16:59:38'),
(33, 'reponse 1', 1, 'livre2.PNG/PHmHGbW2G0GYaUANXKwncWNN8xtiOyKFv4xlLmDO.png', 'j', NULL, 0, 28, '2020-09-11 16:50:16', '2020-09-11 16:52:27');

-- --------------------------------------------------------

--
-- Structure de la table `exam_questions`
--

DROP TABLE IF EXISTS `exam_questions`;
CREATE TABLE IF NOT EXISTS `exam_questions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` enum('unique','multiple') COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` tinyint(1) NOT NULL DEFAULT 0,
  `file` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scale` int(11) NOT NULL,
  `exam_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `exam_questions_exam_id_foreign` (`exam_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `exam_questions`
--

INSERT INTO `exam_questions` (`id`, `type`, `label`, `image`, `file`, `scale`, `exam_id`, `created_at`, `updated_at`) VALUES
(28, 'multiple', 'test', 1, 'PlkqnQTvcsXaoNH4.jpg', 3, 9, '2020-09-11 16:50:16', '2020-09-11 16:58:37'),
(29, 'unique', 'test', 1, '0PH0nVR8VLl0n2EI.jpg', 2, 9, '2020-09-11 16:59:38', '2020-09-11 16:59:49');

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `lessons`
--

DROP TABLE IF EXISTS `lessons`;
CREATE TABLE IF NOT EXISTS `lessons` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `estimate_time` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lessons_subject_id_foreign` (`subject_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `lessons`
--

INSERT INTO `lessons` (`id`, `description`, `label`, `subject_id`, `estimate_time`, `created_at`, `updated_at`) VALUES
(17, 'des', 'Limite et continuité', 19, '2h', '2020-09-10 19:06:09', '2020-09-10 19:06:09');

-- --------------------------------------------------------

--
-- Structure de la table `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `manipulations` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`manipulations`)),
  `custom_properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`custom_properties`)),
  `responsive_images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`responsive_images`)),
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=MyISAM AUTO_INCREMENT=62 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(1, 'App\\Book', 16, 'default', 'happy.png', 'egEqFi3KK4xYatsCoz4PlAPCVr27x8-metaaGFwcHkucG5n-.png', 'image/png', 'public', 158356, '[]', '[]', '[]', 1, '2020-09-11 02:31:59', '2020-09-11 02:31:59'),
(2, 'App\\Book', 17, 'default', '3.png', 'gubtk34rvH7c1sRO4jAnuLvyt8RyUE-metaMy5wbmc=-.png', 'image/png', 'public', 554591, '[]', '[]', '[]', 2, '2020-09-11 02:36:33', '2020-09-11 02:36:33'),
(5, 'App\\Book', 18, 'default', 'livre2.PNG', 'ZOr2GLrqGKmkR2eSrRhUqE0WGvkM6A-metabGl2cmUyLlBORw==-.png', 'image/png', 'public', 177776, '[]', '[]', '[]', 3, '2020-09-11 03:14:59', '2020-09-11 03:14:59'),
(6, 'App\\QuizAnswer', 161, 'default', 'livre2.PNG', 'vgsJk4omDAxXiXj3kmhRQfqjrxLL6E-metabGl2cmUyLlBORw==-.png', 'image/png', 'public', 177776, '[]', '[]', '[]', 4, '2020-09-11 14:08:59', '2020-09-11 14:08:59'),
(7, 'App\\QuizQuestion', 141, 'default', 'livre1.PNG', 'TcmOO63PJlvByUBwSeqEjJyKwchP1V-metabGl2cmUxLlBORw==-.png', 'image/png', 'public', 188686, '[]', '[]', '[]', 5, '2020-09-11 14:26:26', '2020-09-11 14:26:26'),
(8, 'App\\QuizQuestion', 142, 'default', 'livre3.PNG', 'rEQLgoyOY73oE6EZ4Rz8QfeOuse0h2-metabGl2cmUzLlBORw==-.png', 'image/png', 'public', 194329, '[]', '[]', '[]', 6, '2020-09-11 14:27:18', '2020-09-11 14:27:18'),
(9, 'App\\QuizQuestion', 143, 'default', 'livre1.PNG', 'MtxLuqA7KQhH1iDRgrVw6twsyWzs4n-metabGl2cmUxLlBORw==-.png', 'image/png', 'public', 188686, '[]', '[]', '[]', 7, '2020-09-11 14:31:49', '2020-09-11 14:31:49'),
(10, 'App\\QuizQuestion', 144, 'default', 'livre2.PNG', 'Er7QbsnumDHrxejzrni8c4EkPPByz9-metabGl2cmUyLlBORw==-.png', 'image/png', 'public', 177776, '[]', '[]', '[]', 8, '2020-09-11 14:33:24', '2020-09-11 14:33:24'),
(11, 'App\\QuizAnswer', 169, 'default', 'livre4.PNG', 'ISi0raxmrimaPHW7aRe5idemKtv3dm-metabGl2cmU0LlBORw==-.png', 'image/png', 'public', 163051, '[]', '[]', '[]', 9, '2020-09-11 14:33:24', '2020-09-11 14:33:24'),
(12, 'App\\QuizAnswer', 170, 'default', 'livre2.PNG', 'HZhuZOdyz7vfSatYVgvBKzyO1W0FQ2-metabGl2cmUyLlBORw==-.png', 'image/png', 'public', 177776, '[]', '[]', '[]', 10, '2020-09-11 14:33:24', '2020-09-11 14:33:24'),
(13, 'App\\QuizAnswer', 171, 'default', 'livre2.PNG', 'B83OGIPDx3T4CljZ3DGTF75AsY9Z9G-metabGl2cmUyLlBORw==-.png', 'image/png', 'public', 177776, '[]', '[]', '[]', 11, '2020-09-11 14:33:55', '2020-09-11 14:33:55'),
(15, 'App\\QuizQuestion', 145, 'default', 'livre1.PNG', 'lW7inoF7RmijM80xcWbKhOtCnWGbvR-metabGl2cmUxLlBORw==-.png', 'image/png', 'public', 188686, '[]', '[]', '[]', 12, '2020-09-11 14:46:19', '2020-09-11 14:46:19'),
(16, 'App\\QuizAnswer', 172, 'default', 'livre3.PNG', '6u1pBNK71YuolkwuyNtAASm8eWscry-metabGl2cmUzLlBORw==-.png', 'image/png', 'public', 194329, '[]', '[]', '[]', 13, '2020-09-11 14:49:14', '2020-09-11 14:49:14'),
(18, 'App\\QuizAnswer', 173, 'default', 'livre3.PNG', 'JDUB8VsfQSHdU5hKgJTgFM4EYXWAwj-metabGl2cmUzLlBORw==-.png', 'image/png', 'public', 194329, '[]', '[]', '[]', 14, '2020-09-11 14:52:17', '2020-09-11 14:52:17'),
(19, 'App\\QuizAnswer', 175, 'default', 'livre4.PNG', 'McXpPF1eKunTdwITqfFsLeb8gQ9agW-metabGl2cmU0LlBORw==-.png', 'image/png', 'public', 163051, '[]', '[]', '[]', 15, '2020-09-11 14:53:26', '2020-09-11 14:53:26'),
(20, 'App\\QuizAnswer', 176, 'default', 'livre2.PNG', 'WOKirUqkO3Mphl5eT1Try39MaeQwjv-metabGl2cmUyLlBORw==-.png', 'image/png', 'public', 177776, '[]', '[]', '[]', 16, '2020-09-11 14:57:17', '2020-09-11 14:57:17'),
(42, 'App\\QuizQuestion', 147, 'default', 'livre1.PNG', 'xiErsC0yNkbrHFTx70wQ10MjxJsSUI-metabGl2cmUxLlBORw==-.png', 'image/png', 'public', 188686, '[]', '[]', '[]', 31, '2020-09-11 16:29:00', '2020-09-11 16:29:00'),
(38, 'App\\QuizAnswer', 184, 'default', 'livre1.PNG', 'eeDNFgKkCdu165abTCtzwcVL8Ef4SA-metabGl2cmUxLlBORw==-.png', 'image/png', 'public', 188686, '[]', '[]', '[]', 28, '2020-09-11 16:20:36', '2020-09-11 16:20:36'),
(36, 'App\\QuizAnswer', 183, 'default', 'livre3.PNG', 'u9gmAv2g6Lff7nzSnMsLkSg3WJieJj-metabGl2cmUzLlBORw==-.png', 'image/png', 'public', 194329, '[]', '[]', '[]', 27, '2020-09-11 16:18:06', '2020-09-11 16:18:06'),
(39, 'App\\ExamAnswer', 24, 'default', 'livre3.PNG', 'X9dYVTuvwp6IPAxgtSgKBUtKdqtoMD-metabGl2cmUzLlBORw==-.png', 'image/png', 'public', 194329, '[]', '[]', '[]', 29, '2020-09-11 16:27:46', '2020-09-11 16:27:46'),
(41, 'App\\ExamAnswer', 25, 'default', 'livre2.PNG', 'brMfd9Io97O1ZAAtBtfK8dC8QeTva0-metabGl2cmUyLlBORw==-.png', 'image/png', 'public', 177776, '[]', '[]', '[]', 30, '2020-09-11 16:28:19', '2020-09-11 16:28:19'),
(43, 'App\\ExamAnswer', 27, 'default', 'livre3.PNG', 'pmTcRxTQItMdhQ79yfXR3NRyD7iQfb-metabGl2cmUzLlBORw==-.png', 'image/png', 'public', 194329, '[]', '[]', '[]', 32, '2020-09-11 16:32:12', '2020-09-11 16:32:12'),
(44, 'App\\ExamQuestion', 27, 'default', 'livre1.PNG', 'CBcGxiC1W2ZlOyXTLmyJ0mlZB8H4cu-metabGl2cmUxLlBORw==-.png', 'image/png', 'public', 188686, '[]', '[]', '[]', 33, '2020-09-11 16:41:46', '2020-09-11 16:41:46'),
(45, 'App\\ExamAnswer', 28, 'default', 'livre2.PNG', 'qPZ1qsaNNTvdpl3I1UBjLkyLaow9p8-metabGl2cmUyLlBORw==-.png', 'image/png', 'public', 177776, '[]', '[]', '[]', 34, '2020-09-11 16:41:46', '2020-09-11 16:41:46'),
(46, 'App\\ExamAnswer', 29, 'default', 'livre1.PNG', 'yaYHLs5kDAUQU5Mo3sKMN9QCHiRzRb-metabGl2cmUxLlBORw==-.png', 'image/png', 'public', 188686, '[]', '[]', '[]', 35, '2020-09-11 16:43:31', '2020-09-11 16:43:31'),
(47, 'App\\ExamAnswer', 30, 'default', 'livre2.PNG', 'q7Gi7W584UWAWS04JJoCrfKCZXWJQU-metabGl2cmUyLlBORw==-.png', 'image/png', 'public', 177776, '[]', '[]', '[]', 36, '2020-09-11 16:47:31', '2020-09-11 16:47:31'),
(48, 'App\\ExamAnswer', 31, 'default', 'livre1.PNG', 'LhDciknJITZRecI05B7k3sgE6lkYdr-metabGl2cmUxLlBORw==-.png', 'image/png', 'public', 188686, '[]', '[]', '[]', 37, '2020-09-11 16:48:01', '2020-09-11 16:48:01'),
(49, 'App\\ExamAnswer', 32, 'default', 'livre4.PNG', 'Zdq3WBHjqWEpW3LZhYLaGpT6ZXHCLH-metabGl2cmU0LlBORw==-.png', 'image/png', 'public', 163051, '[]', '[]', '[]', 38, '2020-09-11 16:48:37', '2020-09-11 16:48:37'),
(55, 'App\\ExamQuestion', 28, 'default', 'livre4.PNG', 'djBaFuujzficxcDOwRHqOgIFpjtKs1-metabGl2cmU0LlBORw==-.png', 'image/png', 'public', 163051, '[]', '[]', '[]', 41, '2020-09-11 16:58:37', '2020-09-11 16:58:37'),
(54, 'App\\ExamAnswer', 33, 'default', 'livre1.PNG', 'zaawTYHEfbrzfImUxUfHd25tWmmQcU-metabGl2cmUxLlBORw==-.png', 'image/png', 'public', 188686, '[]', '[]', '[]', 40, '2020-09-11 16:53:49', '2020-09-11 16:53:49'),
(56, 'App\\ExamQuestion', 29, 'default', 'livre3.PNG', 'fIBNydx7OqQ5qI598sgyBmeQbLgCJX-metabGl2cmUzLlBORw==-.png', 'image/png', 'public', 194329, '[]', '[]', '[]', 42, '2020-09-11 16:59:49', '2020-09-11 16:59:49'),
(57, 'App\\QuizAnswer', 186, 'default', 'livre2.PNG', 'QpSqmCSU0bc9kTDahvEqChfgBVyPuT-metabGl2cmUyLlBORw==-.png', 'image/png', 'public', 177776, '[]', '[]', '[]', 43, '2020-09-11 17:01:06', '2020-09-11 17:01:06'),
(59, 'App\\Chapter', 43, 'default', 'multiobjectif.pdf', 'K5v35PnygEQD6CeD4FS2H7Znwoflip-metabXVsdGlvYmplY3RpZi5wZGY=-.pdf', 'application/pdf', 'public', 118053, '[]', '[]', '[]', 44, '2020-09-14 10:21:06', '2020-09-14 10:21:06'),
(61, 'App\\CorrectedExamFile', 8, 'default', 'Interrogation ecrite.pdf', 'Pwz5RtrLER9QwhTexnQ1dn7caK5cG8-metaSW50ZXJyb2dhdGlvbiBlY3JpdGUucGRm-.pdf', 'application/pdf', 'public', 63329, '[]', '[]', '[]', 45, '2020-09-14 10:27:43', '2020-09-14 10:27:43');

-- --------------------------------------------------------

--
-- Structure de la table `medias`
--

DROP TABLE IF EXISTS `medias`;
CREATE TABLE IF NOT EXISTS `medias` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `media_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_08_07_114813_create_admins_table', 1),
(4, '2020_08_07_114813_create_classrooms_table', 1),
(5, '2020_08_07_114813_create_users_table', 1),
(6, '2020_08_07_114814_create_book_categories_table', 1),
(7, '2020_08_07_114814_create_book_orders_table', 1),
(8, '2020_08_07_114814_create_books_table', 1),
(9, '2020_08_07_114814_create_chapters_table', 1),
(10, '2020_08_07_114814_create_corected_exam_files_table', 1),
(11, '2020_08_07_114814_create_exam_answers_table', 1),
(12, '2020_08_07_114814_create_exam_questions_table', 1),
(13, '2020_08_07_114814_create_exams_table', 1),
(14, '2020_08_07_114814_create_lessons_table', 1),
(15, '2020_08_07_114814_create_medias_table', 1),
(16, '2020_08_07_114814_create_notifications_table', 1),
(17, '2020_08_07_114814_create_progressions_table', 1),
(18, '2020_08_07_114814_create_quiz_answers_table', 1),
(19, '2020_08_07_114814_create_quiz_questions_table', 1),
(20, '2020_08_07_114814_create_quizs_table', 1),
(21, '2020_08_07_114814_create_subjects_table', 1),
(22, '2020_08_07_114814_create_user_exam_responses_table', 1),
(23, '2020_08_07_114814_create_user_quiz_responses_table', 1),
(24, '2020_08_07_114824_create_foreign_keys', 1),
(25, '2020_09_11_011157_create_media_table', 2);

-- --------------------------------------------------------

--
-- Structure de la table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `recipient_id` int(11) NOT NULL,
  `recipient_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_id` int(11) DEFAULT NULL,
  `reference_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `sender_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `readed` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(250))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `progressions`
--

DROP TABLE IF EXISTS `progressions`;
CREATE TABLE IF NOT EXISTS `progressions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `chapter_id` int(10) UNSIGNED NOT NULL,
  `lesson_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `progressions_user_id_foreign` (`user_id`),
  KEY `progressions_chapter_id_foreign` (`chapter_id`),
  KEY `progressions_lesson_id_foreign` (`lesson_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `progressions`
--

INSERT INTO `progressions` (`id`, `user_id`, `chapter_id`, `lesson_id`, `created_at`, `updated_at`) VALUES
(1, 18, 40, 17, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(2, 19, 40, 17, '2020-09-10 00:00:00', '2020-09-10 00:00:00'),
(3, 19, 41, 17, '2020-09-10 00:00:00', '2020-09-10 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `quizs`
--

DROP TABLE IF EXISTS `quizs`;
CREATE TABLE IF NOT EXISTS `quizs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` enum('easy','medium','hard') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `quizs_subject_id_foreign` (`subject_id`)
) ENGINE=MyISAM AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quizs`
--

INSERT INTO `quizs` (`id`, `label`, `description`, `level`, `created_at`, `updated_at`, `subject_id`) VALUES
(95, 'Quiz math', 'quiz', 'medium', '2020-09-07 12:41:29', '2020-09-07 12:41:29', 19);

-- --------------------------------------------------------

--
-- Structure de la table `quiz_answers`
--

DROP TABLE IF EXISTS `quiz_answers`;
CREATE TABLE IF NOT EXISTS `quiz_answers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `explanation` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `explanation_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `correct_answer` tinyint(1) NOT NULL DEFAULT 0,
  `image` tinyint(1) NOT NULL DEFAULT 0,
  `file` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quiz_question_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quiz_answers_quiz_question_id_foreign` (`quiz_question_id`)
) ENGINE=MyISAM AUTO_INCREMENT=187 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quiz_answers`
--

INSERT INTO `quiz_answers` (`id`, `label`, `explanation`, `explanation_type`, `correct_answer`, `image`, `file`, `quiz_question_id`, `created_at`, `updated_at`) VALUES
(186, 'gh', 'j', NULL, 0, 1, NULL, 147, '2020-09-11 17:01:06', '2020-09-11 17:01:06'),
(183, 'reponse 1', 'mauvaise reponse', NULL, 0, 1, 'qbJ6Vq7t7zZbx6TQ.pdf/Ea4YvTjCREYTok2qVXYmT77cBufnWMqEHyV06uY6.png', 147, '2020-09-11 15:30:45', '2020-09-11 16:18:06'),
(184, 'reponse 2', 'test', NULL, 0, 1, '6TndQZFq1O2Eu5QC.pdf/QPR4VMcCcsIYzYXjUc2oGPD1x7WizTZfBBuC0sB3.png', 147, '2020-09-11 15:30:45', '2020-09-11 16:20:36');

-- --------------------------------------------------------

--
-- Structure de la table `quiz_questions`
--

DROP TABLE IF EXISTS `quiz_questions`;
CREATE TABLE IF NOT EXISTS `quiz_questions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('unique','multiple') COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` tinyint(1) NOT NULL DEFAULT 0,
  `file` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quiz_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quiz_questions_quiz_id_foreign` (`quiz_id`)
) ENGINE=MyISAM AUTO_INCREMENT=148 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `quiz_questions`
--

INSERT INTO `quiz_questions` (`id`, `label`, `type`, `image`, `file`, `quiz_id`, `created_at`, `updated_at`) VALUES
(147, 'Question 1', 'multiple', 1, 'C6JJRU8XyO5Rr2oX.jpg', 95, '2020-09-11 15:30:45', '2020-09-11 16:29:00');

-- --------------------------------------------------------

--
-- Structure de la table `subjects`
--

DROP TABLE IF EXISTS `subjects`;
CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `label` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `classroom_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subjects_classroom_id_foreign` (`classroom_id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `subjects`
--

INSERT INTO `subjects` (`id`, `label`, `description`, `classroom_id`, `created_at`, `updated_at`) VALUES
(19, 'Mathematique', 'math', 31, '2020-09-07 12:41:02', '2020-09-07 12:41:02');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `classroom_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`) USING HASH,
  KEY `users_classroom_id_foreign` (`classroom_id`)
) ENGINE=MyISAM AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `email_verified_at`, `password`, `country`, `adresse`, `classroom_id`, `remember_token`, `created_at`, `updated_at`, `tel`, `role`) VALUES
(18, 'admin', 'web', 'admin@gmail.com', NULL, '$2y$10$usdoexUL9oK1Pz3eG9onOuvISkW9/ybu8d2LqCLDMfA8li5JgbCHu', 'CI', 'Abidjan', 31, NULL, '2020-09-07 17:35:34', '2020-09-07 17:35:34', NULL, 'admin'),
(19, 'Miss Katrine Green MD', 'surname', 'flangosh@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Marshall Islands', '474 Julio Glens Apt. 454', 2, '6pnUYBPHaM', '2020-08-12 18:29:25', '2020-09-09 14:19:37', '+13717929666', 'user'),
(20, 'Osborne Metz', 'surname', 'chelsie.mann@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Moldova', '196 Baby Harbor', 2, '5JoDkNTMwR', '2020-07-18 09:55:33', '2020-09-09 14:19:37', '+17955577824', 'user'),
(21, 'Teresa Kerluke', 'surname', 'elizabeth.schultz@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Bosnia and Herzegovina', '37820 Lang Locks Apt. 704', 2, 'TweuHxLGGm', '2020-08-07 22:02:10', '2020-09-09 14:19:37', '978.407.1863 x405', 'user'),
(22, 'Mary Ankunding', 'surname', 'fgerhold@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Togo', '6362 Kunze Summit Suite 448', 2, 'EuP8QjPwxH', '2020-08-24 07:54:41', '2020-09-09 14:19:37', '293-397-9842', 'user'),
(23, 'Ms. Elizabeth Ruecker', 'surname', 'fay.khalil@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Morocco', '68390 Hahn Corners', 2, '83JWSBTJvr', '2020-07-14 00:32:12', '2020-09-09 14:19:37', '(651) 912-3634 x01122', 'user'),
(24, 'Lavinia Funk DVM', 'surname', 'maxine68@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Angola', '1549 Bernier Brook Suite 895', 2, 'oeZvNxLqYE', '2020-08-26 13:19:25', '2020-09-09 14:19:37', '620-337-5225 x508', 'user'),
(25, 'Syble Ullrich', 'surname', 'freddy05@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Kiribati', '4112 Kunze Club', 2, 'zoqHmY1peQ', '2020-06-20 09:10:54', '2020-09-09 14:19:37', '+1-334-991-8682', 'user'),
(26, 'Carley Adams', 'surname', 'cschuppe@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Slovenia', '43319 Gottlieb Mountains', 2, 'XFmnIkT60v', '2020-07-12 14:39:26', '2020-09-09 14:19:37', '+1-390-860-9295', 'user'),
(27, 'Prof. Hudson Miller', 'surname', 'laurie66@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Iceland', '15518 Elian Oval Apt. 263', 2, 'DvMIPlnC4X', '2020-08-28 08:41:42', '2020-09-09 14:19:37', '748-446-6063 x780', 'user'),
(28, 'Mr. Mckenna Ritchie DVM', 'surname', 'koepp.ludwig@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Gibraltar', '370 Earline Mountain', 2, 'tZXbHjV8FQ', '2020-08-12 11:22:06', '2020-09-09 14:19:37', '1-542-898-6333 x069', 'user'),
(29, 'Jayme Tillman', 'surname', 'mraz.savanna@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Georgia', '48974 Donnell Fort Apt. 260', 2, 'xasIBsHK2r', '2020-07-26 04:20:30', '2020-09-09 14:19:37', '(323) 598-0726 x7937', 'user'),
(30, 'Miss Sibyl Metz MD', 'surname', 'sienna91@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Tajikistan', '5760 Heidenreich Neck Apt. 860', 2, 'VUM2lYnCjd', '2020-06-25 00:21:43', '2020-09-09 14:19:37', '836-971-5567', 'user'),
(31, 'Dr. Alyson Daniel I', 'surname', 'sromaguera@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'United Arab Emirates', '58616 Gladys Mountains', 2, 'C83LPwKyCV', '2020-08-07 07:00:55', '2020-09-09 14:19:37', '1-919-325-1838', 'user'),
(32, 'Arlie Rice DDS', 'surname', 'sarai.kemmer@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Vietnam', '824 Verna Islands Suite 105', 2, 'NyQZFS27P7', '2020-07-21 17:33:47', '2020-09-09 14:19:37', '214.808.5328 x74187', 'user'),
(33, 'Sedrick O\'Kon', 'surname', 'kulas.elsie@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Congo', '62772 Fay Manor', 2, 'xNjKrxIAQG', '2020-06-19 10:18:53', '2020-09-09 14:19:37', '(209) 245-9215 x65165', 'user'),
(34, 'Ms. Jazmyne O\'Hara V', 'surname', 'heidi56@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Hong Kong', '792 O\'Conner Centers', 2, 'NjwWcqAt6Z', '2020-07-16 16:49:50', '2020-09-09 14:19:37', '+19364331903', 'user'),
(35, 'Edythe Dare', 'surname', 'gwolff@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'United Arab Emirates', '169 McCullough Pass Apt. 503', 2, 'zuet73m6je', '2020-07-30 15:34:44', '2020-09-09 14:19:37', '525-486-9367', 'user'),
(36, 'Zaria Bauch', 'surname', 'deckow.nelle@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Guernsey', '55065 Howe Village', 2, 'YT1wxjtsu8', '2020-09-08 12:29:25', '2020-09-09 14:19:37', '+1.531.533.4404', 'user'),
(37, 'Vince Fadel II', 'surname', 'phalvorson@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Syrian Arab Republic', '3946 Hill Stravenue', 2, 'bJTd3zL33T', '2020-06-23 22:56:55', '2020-09-09 14:19:37', '+1.594.630.9570', 'user'),
(38, 'Jose Cole', 'surname', 'madyson.upton@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Marshall Islands', '341 Witting Harbor', 2, 'WjLoBVwefM', '2020-08-14 12:55:37', '2020-09-09 14:19:37', '(343) 948-0835 x25147', 'user'),
(39, 'Jan Greenfelder', 'surname', 'obashirian@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Yemen', '94295 Isaiah Mount', 2, 'BuQyHPSyZM', '2020-08-20 11:48:37', '2020-09-09 14:19:37', '897.616.8597 x254', 'user'),
(40, 'Cordia Braun', 'surname', 'mcdermott.emilio@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Cameroon', '122 Bradtke Court Suite 484', 2, 'SQA8YvzM0P', '2020-07-04 06:58:02', '2020-09-09 14:19:37', '+1-738-327-0698', 'user'),
(41, 'Mrs. Ressie Grant II', 'surname', 'wunsch.jackie@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Bosnia and Herzegovina', '593 Kameron Roads Apt. 473', 2, 'S0TuQUzQkI', '2020-06-27 03:10:15', '2020-09-09 14:19:37', '894-272-6375', 'user'),
(42, 'Dr. Garrison Ortiz', 'surname', 'ubergstrom@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Malta', '7259 Osinski River', 2, 'FsgV8BPhzQ', '2020-08-27 11:45:45', '2020-09-09 14:19:37', '1-547-293-9154', 'user'),
(43, 'Dr. Virginie Ernser Sr.', 'surname', 'demond.schuster@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Ireland', '372 Vandervort Forge Suite 731', 2, 'PXshkZJilB', '2020-07-25 16:36:06', '2020-09-09 14:19:37', '1-338-888-2105', 'user'),
(44, 'Johnnie Monahan', 'surname', 'hintz.lonie@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'French Southern Territories', '10663 Darrick Shores', 2, 'pklU1dAuWS', '2020-07-22 16:32:33', '2020-09-09 14:19:37', '1-470-623-9216 x4308', 'user'),
(45, 'Kacey Cummings', 'surname', 'rlowe@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'New Zealand', '7959 Dominic Village Apt. 624', 2, 'z9yZo1Qh2S', '2020-06-15 21:48:52', '2020-09-09 14:19:37', '+1-956-437-0320', 'user'),
(46, 'Dr. Ahmed Zemlak DDS', 'surname', 'kris.shirley@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Kyrgyz Republic', '528 Melyna Prairie Apt. 154', 2, 'QyyYV95Sdg', '2020-06-21 03:06:29', '2020-09-09 14:19:37', '283.337.0169', 'user'),
(47, 'Mr. Kennith Conroy III', 'surname', 'kiera.pollich@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Norfolk Island', '209 Immanuel Pines Apt. 314', 2, 'FEWJcCDBgI', '2020-09-04 08:48:25', '2020-09-09 14:19:37', '658-415-7906', 'user'),
(48, 'Bethany Legros', 'surname', 'hermann.madaline@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Canada', '5542 Emma Corner', 2, 'xHjB2TrIAH', '2020-08-19 18:25:06', '2020-09-09 14:19:37', '+1-271-356-4136', 'user'),
(49, 'Samson Kshlerin', 'surname', 'luisa.braun@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Ghana', '24138 Amber Square', 2, '5X070x71sj', '2020-07-16 20:04:19', '2020-09-09 14:19:37', '708.401.6075', 'user'),
(50, 'Aurelio Reynolds', 'surname', 'icarter@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Isle of Man', '153 Shayna Via', 2, 'oCS2FCAV6y', '2020-08-20 16:20:55', '2020-09-09 14:19:37', '(479) 683-7982', 'user'),
(51, 'Edgardo Funk', 'surname', 'garth.ullrich@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Libyan Arab Jamahiriya', '2699 Mitchel Run Suite 719', 2, '0VRA7Zeiyh', '2020-07-09 17:16:44', '2020-09-09 14:19:37', '(703) 243-0551 x46776', 'user'),
(52, 'Mabelle Leuschke V', 'surname', 'zmills@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Iceland', '99773 Breitenberg Run Apt. 263', 2, 'pwD6SnJIGK', '2020-07-05 00:35:52', '2020-09-09 14:19:37', '+1 (552) 624-9939', 'user'),
(53, 'Letitia Balistreri', 'surname', 'fveum@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Guadeloupe', '74657 Mozelle Wells Suite 180', 2, 'N0OKok9pvE', '2020-08-26 13:41:34', '2020-09-09 14:19:37', '+1 (957) 308-9941', 'user'),
(54, 'Prof. Eliza Swift MD', 'surname', 'ewilderman@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Montenegro', '25447 Lilyan Forest Suite 765', 2, 'grrCezAjpK', '2020-06-18 06:04:08', '2020-09-09 14:19:37', '+1-440-301-7210', 'user'),
(55, 'Brycen Wisoky', 'surname', 'beryl.davis@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Nigeria', '321 Blanda Trace Apt. 166', 2, 'wv2k4ISdob', '2020-08-26 17:08:50', '2020-09-09 14:19:37', '892.677.3958', 'user'),
(56, 'Estell Haley I', 'surname', 'scarter@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Sweden', '200 White Station', 2, 'k6oS8FcpM7', '2020-09-06 16:12:35', '2020-09-09 14:19:37', '826.816.5175', 'user'),
(57, 'Raven Reichel', 'surname', 'rhoda29@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Angola', '8355 Kub Valleys', 2, 'n1U1JYIJJc', '2020-07-21 10:01:04', '2020-09-09 14:19:37', '614-746-6704 x616', 'user'),
(58, 'Prof. Adriel Lemke PhD', 'surname', 'antonina65@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Malta', '8672 Dayana Radial Apt. 153', 2, 'cWgBJ1jQ7W', '2020-06-22 08:13:50', '2020-09-09 14:19:37', '1-445-302-3940', 'user'),
(59, 'Prof. Jermey Flatley', 'surname', 'trisha.beahan@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Guam', '3352 Ashleigh Greens Apt. 242', 2, '5Yp5xJ0y46', '2020-07-04 09:26:26', '2020-09-09 14:19:37', '+1 (635) 374-0685', 'user'),
(60, 'Dr. Sean Nienow Jr.', 'surname', 'octavia.kreiger@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Holy See (Vatican City State)', '537 Jessie Fields', 2, 'q0jidfonTU', '2020-08-14 14:41:01', '2020-09-09 14:19:37', '819-879-5679 x299', 'user'),
(61, 'Tyler Jacobs', 'surname', 'raina.runolfsdottir@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Netherlands', '18461 Huel Islands', 2, 'VDjG0NCLML', '2020-07-26 12:27:30', '2020-09-09 14:19:37', '(447) 476-4690', 'user'),
(62, 'Mr. Ward Schuppe', 'surname', 'ymante@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Indonesia', '8868 Dietrich Forest', 2, 'qIa9finrMX', '2020-07-03 06:08:57', '2020-09-09 14:19:37', '+1 (746) 761-9525', 'user'),
(63, 'Mr. Conrad Jenkins Sr.', 'surname', 'spinka.ally@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Haiti', '20693 Queen Light', 2, 'mE4htvGnVn', '2020-06-14 13:03:05', '2020-09-09 14:19:37', '382.769.6520 x5065', 'user'),
(64, 'Mr. Lloyd Legros IV', 'surname', 'crawford82@example.net', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'France', '6832 Agustina Pike', 2, 'LZMZeYR2XK', '2020-06-23 02:02:20', '2020-09-09 14:19:37', '+1 (278) 705-1092', 'user'),
(65, 'Cathy Larson', 'surname', 'ktoy@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Haiti', '1071 Bartell Green Suite 364', 2, 'Rbyj1PsTbL', '2020-06-30 13:25:32', '2020-09-09 14:19:37', '1-995-442-6321', 'user'),
(66, 'Ottilie Turcotte MD', 'surname', 'marian23@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Tonga', '2252 Heller Glens', 2, 'EscZXx1nKY', '2020-07-26 19:14:49', '2020-09-09 14:19:37', '526-985-3670 x87600', 'user'),
(67, 'Bettye Jacobi', 'surname', 'lsipes@example.com', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Nepal', '96944 Corkery Mountain Suite 946', 2, 'sm6JaYRDUg', '2020-06-12 20:23:02', '2020-09-09 14:19:37', '302-695-7382 x91899', 'user'),
(68, 'Joelle Fahey', 'surname', 'ernser.elwin@example.org', '2020-09-09 14:19:37', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'French Polynesia', '4427 Domenic Estates', 2, 'C1I0XSeXva', '2020-08-12 11:28:58', '2020-09-09 14:19:37', '454.553.3214 x7463', 'user');

-- --------------------------------------------------------

--
-- Structure de la table `user_exam_responses`
--

DROP TABLE IF EXISTS `user_exam_responses`;
CREATE TABLE IF NOT EXISTS `user_exam_responses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `exam_answer_id` int(10) UNSIGNED NOT NULL,
  `exam_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_exam_responses_user_id_foreign` (`user_id`),
  KEY `user_exam_responses_exam_answer_id_foreign` (`exam_answer_id`),
  KEY `user_exam_responses_exam_id_foreign` (`exam_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_quiz_responses`
--

DROP TABLE IF EXISTS `user_quiz_responses`;
CREATE TABLE IF NOT EXISTS `user_quiz_responses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `quiz_answer_id` int(10) UNSIGNED NOT NULL,
  `quiz_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_quiz_responses_user_id_foreign` (`user_id`),
  KEY `user_quiz_responses_quiz_answer_id_foreign` (`quiz_answer_id`),
  KEY `user_quiz_responses_quiz_id_foreign` (`quiz_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
