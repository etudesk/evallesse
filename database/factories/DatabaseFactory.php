<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Admin;
use App\Classroom;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'country' => $faker->country,
        'adresse' => $faker->streetAddress,
        'created_at' => $faker->dateTimeBetween($startDate = '-3 month', $endDate = 'now', $timezone = null),
        'classroom_id' => "4",
        'tel' => $faker->phoneNumber,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Classroom::class, function (Faker $faker) {
    return [
        'label' => $faker->name,
        'description' => $faker->text(),
    ];
});

$factory->define(Admin::class, function (Faker $faker) {
    return [
        'name' => 'Ben Ismael D.',
        'email' => 'ismael.diomande225@gmail.com',
        'remember_token' => Str::random(10),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'tel' => $faker->phoneNumber,
    ];
});
