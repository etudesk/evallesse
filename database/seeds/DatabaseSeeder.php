<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        factory(\App\Admin::class, 1)->create();
        factory(\App\Classroom::class, 4)->create();
        factory(\App\User::class, 4)->create();
    }
}
