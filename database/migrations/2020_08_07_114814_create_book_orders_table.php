<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookOrdersTable extends Migration {

	public function up()
	{
		Schema::create('book_orders', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('book_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('book_orders');
	}
}