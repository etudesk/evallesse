<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuizsTable extends Migration {

	public function up()
	{
		Schema::create('quizs', function(Blueprint $table) {
			$table->increments('id');
			$table->string('label');
			$table->string('description')->nullable();
			$table->enum('level', array('easy', 'medium', 'hard'));
			$table->timestamps();
			$table->integer('subject_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('quizs');
	}
}