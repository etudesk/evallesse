<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuizQuestionsTable extends Migration
{

    public function up()
    {
        Schema::create(
            'quiz_questions', function (Blueprint $table) {
                $table->increments('id');
                $table->text('label')->nullable();
                $table->enum('type', array('unique', 'multiple'));
                $table->boolean('image')->default(0);
                $table->integer('quiz_id')->unsigned();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::drop('quiz_questions');
    }
}
