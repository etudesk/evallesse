<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCorectedExamFilesTable extends Migration {

	public function up()
	{
		Schema::create('corected_exam_files', function(Blueprint $table) {
			$table->increments('id');
			$table->string('label');
			$table->text('description')->nullable();
			$table->integer('subject_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('corected_exam_files');
	}
}