<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQuizQuestionIdToUserQuizResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'user_quiz_responses', function (Blueprint $table) {
                $table->unsignedInteger('quiz_question_id')->nullable();
                $table->boolean('good')->default(1);


                $table->foreign('quiz_question_id')->references('id')->on('quiz_questions')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'user_quiz_responses', function (Blueprint $table) {
                //
            }
        );
    }
}
