<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLessonsTable extends Migration
{

	public function up()
	{
		Schema::create('lessons', function (Blueprint $table) {
			$table->increments('id');
			$table->text('description')->nullable();
			$table->string('label')->nullable();
			$table->integer('subject_id')->unsigned();
			$table->string('estimate_time')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('lessons');
	}
}
