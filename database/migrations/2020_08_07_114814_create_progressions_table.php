<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProgressionsTable extends Migration {

	public function up()
	{
		Schema::create('progressions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('chapter_id')->unsigned();
			$table->integer('lesson_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('progressions');
	}
}