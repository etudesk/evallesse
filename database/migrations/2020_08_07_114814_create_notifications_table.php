<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNotificationsTable extends Migration {

	public function up()
	{
		Schema::create('notifications', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('recipient_id');
			$table->string('recipient_type')->nullable();
			$table->text('data')->nullable();
			$table->string('type');
			$table->integer('reference_id')->nullable();
			$table->string('reference_type')->nullable();
			$table->integer('sender_id')->nullable();
			$table->string('sender_type')->nullable();
			$table->boolean('readed')->default(0);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('notifications');
	}
}