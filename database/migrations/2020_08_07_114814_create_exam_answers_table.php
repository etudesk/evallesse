<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExamAnswersTable extends Migration {

	public function up()
	{
		Schema::create('exam_answers', function(Blueprint $table) {
			$table->increments('id');
			$table->text('label');
			$table->boolean('image')->default(0);
			$table->text('explanation')->nullable();
			$table->string('explanation_type')->nullable();
			$table->boolean('correct_answer')->default(0);
			$table->integer('exam_question_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('exam_answers');
	}
}