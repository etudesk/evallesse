<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration
{

	public function up()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->foreign('classroom_id')->references('id')->on('classrooms')
				->onDelete('cascade');
		});
		Schema::table('subjects', function (Blueprint $table) {
			$table->foreign('classroom_id')->references('id')->on('classrooms')
				->onDelete('cascade');
		});
		Schema::table('lessons', function (Blueprint $table) {
			$table->foreign('subject_id')->references('id')->on('subjects')
				->onDelete('cascade');
		});
		Schema::table('chapters', function (Blueprint $table) {
			$table->foreign('lesson_id')->references('id')->on('lessons')
				->onDelete('cascade');
		});
		Schema::table('quizs', function (Blueprint $table) {
			$table->foreign('subject_id')->references('id')->on('subjects')
				->onDelete('cascade');
		});
		Schema::table('quiz_questions', function (Blueprint $table) {
			$table->foreign('quiz_id')->references('id')->on('quizs')
				->onDelete('cascade');
		});
		Schema::table('quiz_answers', function (Blueprint $table) {
			$table->foreign('quiz_question_id')->references('id')->on('quiz_questions')
				->onDelete('cascade');
		});
		Schema::table('exams', function (Blueprint $table) {
			$table->foreign('subject_id')->references('id')->on('subjects')
				->onDelete('cascade');
		});
		Schema::table('exam_questions', function (Blueprint $table) {
			$table->foreign('exam_id')->references('id')->on('exams')
				->onDelete('cascade');
		});
		Schema::table('exam_answers', function (Blueprint $table) {
			$table->foreign('exam_question_id')->references('id')->on('exam_questions')
				->onDelete('cascade');
		});
		/* Schema::table('books', function (Blueprint $table) {
			$table->foreign('book_categoriy_id')->references('id')->on('books')
				->onDelete('cascade');
		}); */
		Schema::table('book_orders', function (Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('cascade');
		});
		Schema::table('book_orders', function (Blueprint $table) {
			$table->foreign('book_id')->references('id')->on('books')
				->onDelete('cascade');
		});
		Schema::table('corected_exam_files', function (Blueprint $table) {
			$table->foreign('subject_id')->references('id')->on('subjects')
				->onDelete('cascade');
		});
		Schema::table('user_quiz_responses', function (Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('cascade');
		});
		Schema::table('user_quiz_responses', function (Blueprint $table) {
			$table->foreign('quiz_answer_id')->references('id')->on('quiz_answers')
				->onDelete('cascade');
		});
		Schema::table('user_quiz_responses', function (Blueprint $table) {
			$table->foreign('quiz_id')->references('id')->on('quizs')
				->onDelete('cascade');
		});
		Schema::table('progressions', function (Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('cascade');
		});
		Schema::table('progressions', function (Blueprint $table) {
			$table->foreign('chapter_id')->references('id')->on('chapters')
				->onDelete('cascade');
		});
		Schema::table('progressions', function (Blueprint $table) {
			$table->foreign('lesson_id')->references('id')->on('lessons')
				->onDelete('cascade');
		});
		Schema::table('user_exam_responses', function (Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('cascade');
		});
		Schema::table('user_exam_responses', function (Blueprint $table) {
			$table->foreign('exam_answer_id')->references('id')->on('exam_answers')
				->onDelete('cascade');
		});
		Schema::table('user_exam_responses', function (Blueprint $table) {
			$table->foreign('exam_id')->references('id')->on('exams')
				->onDelete('cascade');
		});
	}

	public function down()
	{
		Schema::table('users', function (Blueprint $table) {
			$table->dropForeign('users_classroom_id_foreign');
		});
		Schema::table('subjects', function (Blueprint $table) {
			$table->dropForeign('subjects_classroom_id_foreign');
		});
		Schema::table('lessons', function (Blueprint $table) {
			$table->dropForeign('lessons_subject_id_foreign');
		});
		Schema::table('chapters', function (Blueprint $table) {
			$table->dropForeign('chapters_lesson_id_foreign');
		});
		Schema::table('quizs', function (Blueprint $table) {
			$table->dropForeign('quizs_subject_id_foreign');
		});
		Schema::table('quiz_questions', function (Blueprint $table) {
			$table->dropForeign('quiz_questions_quiz_id_foreign');
		});
		Schema::table('quiz_answers', function (Blueprint $table) {
			$table->dropForeign('quiz_answers_quiz_question_id_foreign');
		});
		Schema::table('exams', function (Blueprint $table) {
			$table->dropForeign('exams_subject_id_foreign');
		});
		Schema::table('exam_questions', function (Blueprint $table) {
			$table->dropForeign('exam_questions_exam_id_foreign');
		});
		Schema::table('exam_answers', function (Blueprint $table) {
			$table->dropForeign('exam_answers_exam_question_id_foreign');
		});
		/* Schema::table('books', function (Blueprint $table) {
			$table->dropForeign('books_book_categoriy_id_foreign');
		}); */
		Schema::table('book_orders', function (Blueprint $table) {
			$table->dropForeign('book_orders_user_id_foreign');
		});
		Schema::table('book_orders', function (Blueprint $table) {
			$table->dropForeign('book_orders_book_id_foreign');
		});
		Schema::table('corected_exam_files', function (Blueprint $table) {
			$table->dropForeign('corected_exam_files_subject_id_foreign');
		});
		Schema::table('user_quiz_responses', function (Blueprint $table) {
			$table->dropForeign('user_quiz_responses_user_id_foreign');
		});
		Schema::table('user_quiz_responses', function (Blueprint $table) {
			$table->dropForeign('user_quiz_responses_quiz_answer_id_foreign');
		});
		Schema::table('user_quiz_responses', function (Blueprint $table) {
			$table->dropForeign('user_quiz_responses_quiz_id_foreign');
		});
		Schema::table('progressions', function (Blueprint $table) {
			$table->dropForeign('progressions_user_id_foreign');
		});
		Schema::table('progressions', function (Blueprint $table) {
			$table->dropForeign('progressions_chapter_id_foreign');
		});
		Schema::table('progressions', function (Blueprint $table) {
			$table->dropForeign('progressions_lesson_id_foreign');
		});
		Schema::table('user_exam_responses', function (Blueprint $table) {
			$table->dropForeign('user_exam_responses_user_id_foreign');
		});
		Schema::table('user_exam_responses', function (Blueprint $table) {
			$table->dropForeign('user_exam_responses_exam_answer_id_foreign');
		});
		Schema::table('user_exam_responses', function (Blueprint $table) {
			$table->dropForeign('user_exam_responses_exam_id_foreign');
		});
	}
}
