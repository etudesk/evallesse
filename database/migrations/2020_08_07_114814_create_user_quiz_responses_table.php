<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserQuizResponsesTable extends Migration
{

    public function up()
    {
        Schema::create(
            'user_quiz_responses', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('user_id')->unsigned();
                $table->integer('quiz_answer_id')->unsigned();
                $table->integer('quiz_id')->unsigned();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::drop('user_quiz_responses');
    }
}
