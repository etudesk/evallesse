<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChaptersTable extends Migration {

	public function up()
	{
		Schema::create('chapters', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('title');
			$table->text('content')->nullable();
			$table->integer('lesson_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('chapters');
	}
}