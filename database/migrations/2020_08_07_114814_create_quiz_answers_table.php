<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuizAnswersTable extends Migration
{

	public function up()
	{
		Schema::create('quiz_answers', function (Blueprint $table) {
			$table->increments('id');
			$table->text('label');
			$table->text('explanation')->nullable();
			$table->string('explanation_type')->nullable();
			$table->boolean('correct_answer')->default(0);
			$table->boolean('image')->default(0);
			$table->integer('quiz_question_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('quiz_answers');
	}
}
