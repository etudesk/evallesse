<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserExamResponsesTable extends Migration {

	public function up()
	{
		Schema::create('user_exam_responses', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('exam_answer_id')->unsigned();
			$table->integer('exam_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('user_exam_responses');
	}
}