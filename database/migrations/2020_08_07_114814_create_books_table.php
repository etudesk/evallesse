<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBooksTable extends Migration
{

	public function up()
	{
		Schema::create('books', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->integer('price');
			$table->text('description')->nullable();
			$table->integer('book_categoriy_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('books');
	}
}
