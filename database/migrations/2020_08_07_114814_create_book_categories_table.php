<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookCategoriesTable extends Migration {

	public function up()
	{
		Schema::create('book_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('label');
			$table->text('description')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('book_categories');
	}
}