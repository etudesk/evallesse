<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUserExamResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'user_exam_responses', function (Blueprint $table) {
                $table->unsignedInteger('exam_question_id')->nullable();
                $table->boolean('good')->default(0);


                $table->foreign('exam_question_id')->references('id')->on('exam_questions')
                    ->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'user_exam_responses', function (Blueprint $table) {
                //
            }
        );
    }
}
