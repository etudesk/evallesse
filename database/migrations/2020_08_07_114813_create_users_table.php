<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create(
            'users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->string('tel')->unique();
                $table->string('email')->nullable();
                $table->timestamp('email_verified_at')->nullable();
                $table->string('password');
                $table->string('country')->nullable();
                $table->text('adresse')->nullable();
                $table->integer('classroom_id')->unsigned();
                $table->rememberToken();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::drop('users');
    }
}
