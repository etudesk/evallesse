<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubjectsTable extends Migration
{

    public function up()
    {
        Schema::create(
            'subjects', function (Blueprint $table) {
                $table->increments('id');
                $table->string('label');
                $table->string('description')->nullable();
                $table->integer('classroom_id')->unsigned();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::drop('subjects');
    }
}
