<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExamQuestionsTable extends Migration {

	public function up()
	{
		Schema::create('exam_questions', function(Blueprint $table) {
			$table->increments('id');
			$table->enum('type', array('unique', 'multiple'));
			$table->text('label');
			$table->boolean('image')->default(0);
			$table->integer('scale');
			$table->integer('exam_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('exam_questions');
	}
}