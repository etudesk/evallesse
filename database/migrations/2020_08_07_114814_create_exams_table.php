<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExamsTable extends Migration
{

	public function up()
	{
		Schema::create('exams', function (Blueprint $table) {
			$table->increments('id');
			$table->string('label');
			$table->text('description')->nullable();
			$table->string('estimate_duration')->nullable();
			$table->integer('subject_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('exams');
	}
}
